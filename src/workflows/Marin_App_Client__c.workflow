<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>IsReviewedTrue</fullName>
        <field>Is_Reviewed__c</field>
        <literalValue>1</literalValue>
        <name>IsReviewedTrue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>IsReviewed</fullName>
        <actions>
            <name>IsReviewedTrue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Account__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
