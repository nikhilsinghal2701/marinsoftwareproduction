<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>APAC_Support_Case_Created_General</fullName>
        <description>APAC Support - Case Created - General</description>
        <protected>false</protected>
        <recipients>
            <recipient>will.liu@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/EU_Support_New_Case_Notification_v1</template>
    </alerts>
    <alerts>
        <fullName>APAC_Support_Case_Taken_CS_Owner</fullName>
        <description>APAC Support - Case Taken -&gt; Case Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/Case_Taken_Support</template>
    </alerts>
    <alerts>
        <fullName>APAC_new_case_time_triggered_email_alert</fullName>
        <description>APAC new case time triggered email alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>APACSupportServices</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/SUPPORTSERVICESCaseCreation</template>
    </alerts>
    <alerts>
        <fullName>AlertAppOpsCaseCreated</fullName>
        <description>AlertAppOpsCaseCreated</description>
        <protected>false</protected>
        <recipients>
            <recipient>Customer Engagement Manager - AMER</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>abauer@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>daniel.feger@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>will.liu@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Primary_AM__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Primary_OMM_del__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/AppOpsNotification</template>
    </alerts>
    <alerts>
        <fullName>AlertAppOpsCaseCreatedPager</fullName>
        <ccEmails>page-app-ops@marinsw.pagerduty.com</ccEmails>
        <description>AlertAppOpsCaseCreatedPager</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Sales_Ops/AppOpsPagerNotification</template>
    </alerts>
    <alerts>
        <fullName>Alert_Pro_Case_was_created</fullName>
        <description>Alert Pro Case was created</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>ahughes@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nsandford@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/Professional_Case_Created_Alert</template>
    </alerts>
    <alerts>
        <fullName>Analytics_email_trigger</fullName>
        <description>Analytics email trigger</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ANALYTICS_CSAT_Survey</template>
    </alerts>
    <alerts>
        <fullName>AppOps_P1_holiday_watchlist</fullName>
        <ccEmails>app-ops@marinsoftware.com</ccEmails>
        <description>AppOps P1 holiday watchlist</description>
        <protected>false</protected>
        <recipients>
            <recipient>ming@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>oclary@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>psimmonds@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>skashani@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/holiday_p1</template>
    </alerts>
    <alerts>
        <fullName>Application_Support_Page_on_call_SS_for_P1_kickoff_bidding_requests</fullName>
        <ccEmails>page-ss@marinsw.pagerduty.com</ccEmails>
        <ccEmails>oclary@marinsoftware.com</ccEmails>
        <description>Application Support - Page on-call SS for P1/kickoff bidding requests</description>
        <protected>false</protected>
        <recipients>
            <recipient>oclary@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/APPSUPP_NEW_P1_Case_Created_Internal</template>
    </alerts>
    <alerts>
        <fullName>CEM_Case_Closure</fullName>
        <ccEmails>gs_support@marinsoftware.com</ccEmails>
        <description>EMAIL: CEM - Case Closure</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/CEM_Case_Closure_v2</template>
    </alerts>
    <alerts>
        <fullName>CEM_Case_Creation_External</fullName>
        <ccEmails>gs_support@marinsoftware.com</ccEmails>
        <description>EMAIL: CEM - Case Creation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/CEM_Case_Creation_v2</template>
    </alerts>
    <alerts>
        <fullName>CLIENT_SERVICES_Case_Handoff_APAC_EU_Support</fullName>
        <description>Handoff APAC/EU Support</description>
        <protected>false</protected>
        <recipients>
            <recipient>EUSupportServices</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/CLIENT_SERVICES_Case_Handoff_APAC_EU_Support</template>
    </alerts>
    <alerts>
        <fullName>CSAT_Survey</fullName>
        <description>CSAT survey</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Support_Services_CSAT_Survey</template>
    </alerts>
    <alerts>
        <fullName>CSAT_Survey_CH</fullName>
        <description>CSAT Survey CH</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Support_Services_CSAT_Survey_CH</template>
    </alerts>
    <alerts>
        <fullName>CSC_Holiday_watch_list</fullName>
        <ccEmails>csc-global@marinsoftware.com</ccEmails>
        <ccEmails>gs-ss@marinsoftware.com</ccEmails>
        <ccEmails>appsupport-holiday@marinsw.pagerduty.com</ccEmails>
        <description>CSC Holiday watch list</description>
        <protected>false</protected>
        <recipients>
            <recipient>ming@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>oclary@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>psimmonds@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>skashani@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/holiday_p1</template>
    </alerts>
    <alerts>
        <fullName>CSR_Alert</fullName>
        <description>CSR Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/CLIENTSERVICESCaseAssignment</template>
    </alerts>
    <alerts>
        <fullName>CSR_Alert_OCDF</fullName>
        <description>CSR Alert - OC/DF</description>
        <protected>false</protected>
        <recipients>
            <recipient>dfishman@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>oclary@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CS_Templates/CLIENTSERVICESCaseAssignment</template>
    </alerts>
    <alerts>
        <fullName>CS_Bid_Setup_Closure_External_Email_Alert</fullName>
        <description>CS Bid Setup Closure - External Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_5__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/CS_Bid_Setup_Closure_External</template>
    </alerts>
    <alerts>
        <fullName>CS_Consulting_Closure_External_Email_Alert</fullName>
        <description>CS Consulting Closure - External Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/CS_Consulting_Closure_External</template>
    </alerts>
    <alerts>
        <fullName>CS_Tracking_Closure_External_Email_Alert</fullName>
        <description>CS Tracking Closure - External Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/CS_Tracking_Closure_External</template>
    </alerts>
    <alerts>
        <fullName>CS_Training_Closure_External_Email_Alert</fullName>
        <description>CS Training Closure - External Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_5__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/CS_Training_Closure_External</template>
    </alerts>
    <alerts>
        <fullName>CS_Training_Closure_External_Email_Alert_CN</fullName>
        <description>CS Training Closure - External Email Alert CN</description>
        <protected>false</protected>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/CS_Training_Closure_External_CN</template>
    </alerts>
    <alerts>
        <fullName>CS_Training_Closure_External_Email_Alert_JP</fullName>
        <description>CS Training Closure - External Email Alert JP</description>
        <protected>false</protected>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/CS_Training_Closure_External_JP</template>
    </alerts>
    <alerts>
        <fullName>CS_Training_Survey_Negative_Feedback_Alert</fullName>
        <description>CS Training Survey Negative Feedback Alert!</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>samlee@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/Survey_Negative_Feedback_Alert</template>
    </alerts>
    <alerts>
        <fullName>Case_Closed</fullName>
        <ccEmails>gs_support@marinsoftware.com</ccEmails>
        <description>Case Closed</description>
        <protected>false</protected>
        <recipients>
            <recipient>Customer Engagement Manager - AMER</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Primary_OMM_del__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/CLIENT_SERVICES_Case_Closure</template>
    </alerts>
    <alerts>
        <fullName>Case_Created_Feature_Request_Internal</fullName>
        <description>New Case Created (Feature Request) - Internal</description>
        <protected>false</protected>
        <recipients>
            <recipient>Account Director - EMEA</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Account Manager - AMER</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Account Manager - APAC</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Customer Engagement Manager - AMER</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Customer Engagement Manager - APAC</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Customer Engagement Manager - EMEA</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/NEW_Case_Created_Internal</template>
    </alerts>
    <alerts>
        <fullName>Case_Taken</fullName>
        <description>Case Taken Marin Help</description>
        <protected>false</protected>
        <recipients>
            <recipient>PodMarinHelp</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CS_Templates/CLIENT_SERVICES_Case_Taken</template>
    </alerts>
    <alerts>
        <fullName>Case_Taken_2</fullName>
        <description>Case Taken CS Help (High)</description>
        <protected>false</protected>
        <recipients>
            <recipient>PodCSHelp</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CS_Templates/CLIENT_SERVICES_Case_Taken</template>
    </alerts>
    <alerts>
        <fullName>Case_Taken_3</fullName>
        <description>Case Taken Support Help (High)</description>
        <protected>false</protected>
        <recipients>
            <recipient>PodSupportHelp</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CS_Templates/CLIENT_SERVICES_Case_Taken</template>
    </alerts>
    <alerts>
        <fullName>Case_Taken_9</fullName>
        <description>Case Taken OMM Help (High)</description>
        <protected>false</protected>
        <recipients>
            <recipient>PodOMMHelp</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CS_Templates/CLIENT_SERVICES_Case_Taken</template>
    </alerts>
    <alerts>
        <fullName>Case_Taken_All_High</fullName>
        <description>Case Taken All (High)</description>
        <protected>false</protected>
        <recipients>
            <recipient>PodCSHelp</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>PodMarinHelp</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>PodOMMHelp</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>PodSupportHelp</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>UKCSAD</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CS_Templates/CLIENT_SERVICES_Case_Taken</template>
    </alerts>
    <alerts>
        <fullName>Case_Taken_All_Low</fullName>
        <description>Case Taken All (Low)</description>
        <protected>false</protected>
        <recipients>
            <recipient>PodCSHelp</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>PodMarinHelp</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>PodOMMHelp</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>PodSupportHelp</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>UKCSAD</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CS_Templates/CLIENT_SERVICES_Case_Taken</template>
    </alerts>
    <alerts>
        <fullName>Case_Taken_All_Medium</fullName>
        <description>Case Taken All (Medium)</description>
        <protected>false</protected>
        <recipients>
            <recipient>PodCSHelp</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>PodMarinHelp</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>PodOMMHelp</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>PodSupportHelp</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>UKCSAD</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CS_Templates/CLIENT_SERVICES_Case_Taken</template>
    </alerts>
    <alerts>
        <fullName>Case_Taken_CS_Help_Low</fullName>
        <description>Case Taken CS Help (Low)</description>
        <protected>false</protected>
        <recipients>
            <recipient>PodCSHelp</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CS_Templates/CLIENT_SERVICES_Case_Taken</template>
    </alerts>
    <alerts>
        <fullName>Case_Taken_CS_Help_Low_2</fullName>
        <description>Case Taken OMM Help (Low)</description>
        <protected>false</protected>
        <recipients>
            <recipient>PodOMMHelp</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CS_Templates/CLIENT_SERVICES_Case_Taken</template>
    </alerts>
    <alerts>
        <fullName>Case_Taken_CS_Help_Medium</fullName>
        <description>Case Taken CS Help (Medium)</description>
        <protected>false</protected>
        <recipients>
            <recipient>PodCSHelp</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/CLIENT_SERVICES_Case_Taken</template>
    </alerts>
    <alerts>
        <fullName>Case_Taken_CS_Help_Medium_2</fullName>
        <description>Case Taken OMM Help (Medium)</description>
        <protected>false</protected>
        <recipients>
            <recipient>PodOMMHelp</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CS_Templates/CLIENT_SERVICES_Case_Taken</template>
    </alerts>
    <alerts>
        <fullName>Case_Taken_Customer</fullName>
        <description>Case Taken - Customer</description>
        <protected>false</protected>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/CLIENT_SERVICES_Case_Taken_Customer</template>
    </alerts>
    <alerts>
        <fullName>Case_Taken_EU_Support_To_Owner</fullName>
        <description>Case Taken (EU Support To Owner)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/Case_Taken</template>
    </alerts>
    <alerts>
        <fullName>Case_Taken_Marin_Help_Low</fullName>
        <description>Case Taken Marin Help (Low)</description>
        <protected>false</protected>
        <recipients>
            <recipient>PodMarinHelp</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/CLIENT_SERVICES_Case_Taken</template>
    </alerts>
    <alerts>
        <fullName>Case_Taken_Marin_Help_Medium</fullName>
        <description>Case Taken Marin Help (Medium)</description>
        <protected>false</protected>
        <recipients>
            <recipient>PodMarinHelp</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CS_Templates/CLIENT_SERVICES_Case_Taken</template>
    </alerts>
    <alerts>
        <fullName>Case_Taken_Support_Help_Low</fullName>
        <description>Case Taken Support Help (Low)</description>
        <protected>false</protected>
        <recipients>
            <recipient>PodSupportHelp</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CS_Templates/CLIENT_SERVICES_Case_Taken</template>
    </alerts>
    <alerts>
        <fullName>Case_Taken_Support_Help_Medium</fullName>
        <description>Case Taken Support Help (Medium)</description>
        <protected>false</protected>
        <recipients>
            <recipient>PodSupportHelp</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CS_Templates/CLIENT_SERVICES_Case_Taken</template>
    </alerts>
    <alerts>
        <fullName>Case_Taken_US_SS_to_Owner</fullName>
        <description>Case Taken (US SS to Owner)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/Case_Taken</template>
    </alerts>
    <alerts>
        <fullName>Client_Alert</fullName>
        <description>Client Alert</description>
        <protected>false</protected>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/New_Case_Created_External</template>
    </alerts>
    <alerts>
        <fullName>Customer_Support_Survey_DE</fullName>
        <description>Customer Support Survey DE</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Support_Services_CSAT_Survey_DE</template>
    </alerts>
    <alerts>
        <fullName>Customer_Support_Survey_FR</fullName>
        <description>Customer Support Survey FR</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Support_Services_CSAT_Survey_FR</template>
    </alerts>
    <alerts>
        <fullName>Customer_Support_Survey_JP</fullName>
        <description>Customer Support Survey JP</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Support_Services_CSAT_Survey_JP</template>
    </alerts>
    <alerts>
        <fullName>Data_delay_notification</fullName>
        <description>Data delay notification</description>
        <protected>false</protected>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Primary_OMM_del__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Daily_Check_Templates/Morning_Checks_Data_delay_notification</template>
    </alerts>
    <alerts>
        <fullName>EMAIL_Case_Closed_Customer_Owner_External_CS_Bid_Setup</fullName>
        <description>EMAIL: Case Closed - Customer &amp; Owner (External) CS Bid Setup</description>
        <protected>false</protected>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/CS_Bid_Setup_Closure_External</template>
    </alerts>
    <alerts>
        <fullName>EMEA_Analytics_Case_taken_to_case_owner</fullName>
        <description>EMEA Analytics Case taken to case owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/Case_Taken_APAC_EMEA_Support</template>
    </alerts>
    <alerts>
        <fullName>EMEA_Handoff_to_EU_Analytics</fullName>
        <description>EMEA Handoff to EU Analytics</description>
        <protected>false</protected>
        <recipients>
            <recipient>EUAnalytics</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/CLIENT_SERVICES_Case_Handoff_EU_Analytics</template>
    </alerts>
    <alerts>
        <fullName>EMEA_PS_ticket_assignment</fullName>
        <description>EMEA PS ticket assignment</description>
        <protected>false</protected>
        <recipients>
            <field>PS_Owner_EMEA_ONLY__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/SUPPORT_SERVICES_PS_Owner_assigned</template>
    </alerts>
    <alerts>
        <fullName>EMEA_PS_ticket_closed_to_CS</fullName>
        <description>EMEA PS ticket closed to CS</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/SUPPORT_SERVICES_PS_closure</template>
    </alerts>
    <alerts>
        <fullName>EMEA_PS_ticket_closed_to_SS</fullName>
        <description>EMEA PS ticket closed to SS</description>
        <protected>false</protected>
        <recipients>
            <field>Co_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/SUPPORT_SERVICES_PS_closure</template>
    </alerts>
    <alerts>
        <fullName>EMEA_PS_ticket_reopened</fullName>
        <description>EMEA PS ticket reopened</description>
        <protected>false</protected>
        <recipients>
            <field>PS_Owner_EMEA_ONLY__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/SUPPORT_SERVICES_PS_Case_reopened</template>
    </alerts>
    <alerts>
        <fullName>EMEA_PS_ticket_taken_email_notification</fullName>
        <description>EMEA PS ticket taken email notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>kstewart@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>robert.wickert@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Co_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/SUPPORT_SERVICES_PS_ticket_taken</template>
    </alerts>
    <alerts>
        <fullName>EMEA_handoff_CS_to_PS</fullName>
        <description>EMEA handoff CS to PS</description>
        <protected>false</protected>
        <recipients>
            <recipient>kstewart@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>oclary@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>robert.wickert@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/SUPPORT_SERVICES_Handoff_CS_to_PS</template>
    </alerts>
    <alerts>
        <fullName>EMEA_handoff_SS_to_PS</fullName>
        <description>EMEA handoff SS to PS</description>
        <protected>false</protected>
        <recipients>
            <recipient>kstewart@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>oclary@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/SUPPORT_SERVICES_Handoff_SS_to_PS</template>
    </alerts>
    <alerts>
        <fullName>EMEA_new_case_time_triggered_email_alert</fullName>
        <description>EMEA new case time triggered email alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>EUSupportServices</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>amarjit.grewal@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/EU_Support_New_Case_Notification_v2</template>
    </alerts>
    <alerts>
        <fullName>EU_Support_CS_Taken_Task</fullName>
        <description>EU Support-&gt;CS Completed Task</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/CLIENTSERVICESCaseHandoffEUUKCS</template>
    </alerts>
    <alerts>
        <fullName>EU_Support_Case_Close</fullName>
        <description>EU Support Case Close</description>
        <protected>false</protected>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/CLIENT_SERVICES_EU_Support_Case_Closure</template>
    </alerts>
    <alerts>
        <fullName>EU_Support_Case_taken_customer</fullName>
        <description>EU Support Case taken - customer</description>
        <protected>false</protected>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/CLIENT_SERVICES_EU_Support_Case_Taken_Customer</template>
    </alerts>
    <alerts>
        <fullName>EU_Support_Notification</fullName>
        <description>EU Support Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>EUSupportServices</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/EU_Support_New_Case_Notification_v2</template>
    </alerts>
    <alerts>
        <fullName>EU_Support_Rupesh_Sharma</fullName>
        <description>EU Support-&gt;Rupesh Sharma</description>
        <protected>false</protected>
        <recipients>
            <recipient>rupesh.sharma@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/CLIENTSERVICESCaseHandoffEUUKCS</template>
    </alerts>
    <alerts>
        <fullName>EU_Support_Taken_Task</fullName>
        <description>EU Support-&gt;CS Taken Task</description>
        <protected>false</protected>
        <recipients>
            <recipient>APACSupportServices</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>EUSupportServices</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/Case_Taken_APAC_EMEA_Support</template>
    </alerts>
    <alerts>
        <fullName>EU_Support_response_template</fullName>
        <description>EU Support - response template</description>
        <protected>false</protected>
        <recipients>
            <field>Co_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/Support_Services_HTML_without_letterhead</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_for_Tier_3_cases_to_Account_Owners</fullName>
        <ccEmails>nsandford@marinsoftware.com</ccEmails>
        <ccEmails>bbooth@marinsoftware.com</ccEmails>
        <description>Email Alert for Tier 3 cases to Account Owners</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Tier_3_Notification_to_Account_Owner</template>
    </alerts>
    <alerts>
        <fullName>Email_Case_Closed_External_CSC</fullName>
        <ccEmails>gs_support@marinsoftware.com</ccEmails>
        <description>Email Case Closed - External - CSC</description>
        <protected>false</protected>
        <recipients>
            <recipient>Customer Engagement Manager - AMER</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Primary_OMM_del__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/NEW_Case_Closure_External_CSC</template>
    </alerts>
    <alerts>
        <fullName>Email_Case_Picked_Up_External</fullName>
        <ccEmails>gs_support@marinsoftware.com</ccEmails>
        <description>Email Case Picked Up - External</description>
        <protected>false</protected>
        <recipients>
            <recipient>Customer Engagement Manager - AMER</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Primary_OMM_del__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/New_Case_Picked_Up_External</template>
    </alerts>
    <alerts>
        <fullName>Email_Case_Picked_Up_External_CSC</fullName>
        <ccEmails>gs_support@marinsoftware.com</ccEmails>
        <description>Email Case Picked Up - External - CSC</description>
        <protected>false</protected>
        <recipients>
            <recipient>Customer Engagement Manager - AMER</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Primary_OMM_del__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/New_Case_Picked_Up_External_CSC</template>
    </alerts>
    <alerts>
        <fullName>Email_Case_Picked_Up_Internal</fullName>
        <ccEmails>gs_support@marinsoftware.com</ccEmails>
        <description>Email Case Picked Up - Internal</description>
        <protected>false</protected>
        <recipients>
            <recipient>Customer Engagement Manager - AMER</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Primary_OMM_del__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Project_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Solution_Architect__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/NEW_Case_Picked_Up_Internal</template>
    </alerts>
    <alerts>
        <fullName>Email_Feature_Request_Update</fullName>
        <description>Email: Feature Request Update</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>Customer Engagement Manager - AMER</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Customer Engagement Manager - APAC</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Customer Engagement Manager - EMEA</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/Feature_Request_JIRA_Update</template>
    </alerts>
    <alerts>
        <fullName>Email_New_Case_Created_Template</fullName>
        <ccEmails>gs_support@marinsoftware.com</ccEmails>
        <description>Email New Case Created Template</description>
        <protected>false</protected>
        <recipients>
            <recipient>Customer Engagement Manager - AMER</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Primary_OMM_del__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Project_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Solution_Architect__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/NEW_Case_Created_Internal</template>
    </alerts>
    <alerts>
        <fullName>Emails_co_owner_with_details_of_case</fullName>
        <description>Emails co-owner with details of case</description>
        <protected>false</protected>
        <recipients>
            <field>Co_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/SUPPORT_SERVICES_First_Response</template>
    </alerts>
    <alerts>
        <fullName>Handoff_APAC_EU_Support</fullName>
        <description>Handoff EU Support/APAC</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/CLIENTSERVICESCaseHandoffEUUKCS</template>
    </alerts>
    <alerts>
        <fullName>Handoff_EU_Support_UK_PS</fullName>
        <description>Handoff:  EU Support-&gt;UK PS</description>
        <protected>false</protected>
        <recipients>
            <recipient>EUSupportServices</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/CLIENTSERVICESCaseHandoffEUUKCS</template>
    </alerts>
    <alerts>
        <fullName>Handoff_UK_EU</fullName>
        <description>Handoff UK/EU Support</description>
        <protected>false</protected>
        <recipients>
            <recipient>EUSupportServices</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/CLIENT_SERVICES_Case_Handoff_UK_EU_Support</template>
    </alerts>
    <alerts>
        <fullName>Handoff_UK_US</fullName>
        <description>Handoff UK/US</description>
        <protected>false</protected>
        <recipients>
            <recipient>PodCSHelp</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>PodMarinHelp</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>PodSupportHelp</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CS_Templates/CLIENTSERVICESCaseHandoffUK</template>
    </alerts>
    <alerts>
        <fullName>Handoff_US_UK</fullName>
        <description>Handoff US/UK</description>
        <protected>false</protected>
        <recipients>
            <recipient>EUSupportServices</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/CLIENTSERVICESCaseHandoff</template>
    </alerts>
    <alerts>
        <fullName>Handoff_to_EU_Analytics</fullName>
        <description>Handoff to EU Analytics</description>
        <protected>false</protected>
        <recipients>
            <recipient>EUAnalytics</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/CLIENT_SERVICES_Case_Handoff_EU_Analytics</template>
    </alerts>
    <alerts>
        <fullName>Hotels_com_Ticket_Closed_Notification</fullName>
        <description>Hotels.com Ticket Closed Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>jeremyevans@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Hotels_com_ticket_closed_notification</template>
    </alerts>
    <alerts>
        <fullName>Management_alert_of_ongoing_issue</fullName>
        <description>Management alert of ongoing issue</description>
        <protected>false</protected>
        <recipients>
            <recipient>oclary@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/SS_mgt_escalation</template>
    </alerts>
    <alerts>
        <fullName>NEW_Case_Closure_External_CN</fullName>
        <ccEmails>gs_support@marinsoftware.com</ccEmails>
        <description>NEW - Case Closure - External CN</description>
        <protected>false</protected>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Primary_OMM_del__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/NEW_Case_Closure_External_CN</template>
    </alerts>
    <alerts>
        <fullName>NEW_Case_Closure_External_DE</fullName>
        <ccEmails>gs_support@marinsoftware.com</ccEmails>
        <description>NEW - Case Closure - External DE</description>
        <protected>false</protected>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Primary_OMM_del__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/NEW_Case_Closure_External_DE</template>
    </alerts>
    <alerts>
        <fullName>NEW_Case_Closure_External_FR</fullName>
        <ccEmails>gs_support@marinsoftware.com</ccEmails>
        <description>NEW - Case Closure - External  FR</description>
        <protected>false</protected>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Primary_OMM_del__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/NEW_Case_Closure_External_FR</template>
    </alerts>
    <alerts>
        <fullName>New_Case_Closed_Internal_Users</fullName>
        <description>New - Case Closed - Internal Users</description>
        <protected>false</protected>
        <recipients>
            <recipient>Customer Engagement Manager - AMER</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Primary_OMM_del__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Project_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Solution_Architect__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/NEW_Case_Closure_Internal</template>
    </alerts>
    <alerts>
        <fullName>New_Case_Closure_External_JP</fullName>
        <ccEmails>gs_support@marinsoftware.com</ccEmails>
        <description>New - Case Closure - External JP</description>
        <protected>false</protected>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Primary_OMM_del__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/NEW_Case_Closure_External_JP</template>
    </alerts>
    <alerts>
        <fullName>New_Case_Created_External</fullName>
        <description>New Case Created - External</description>
        <protected>false</protected>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/New_Case_Created_External</template>
    </alerts>
    <alerts>
        <fullName>New_Case_Created_External_Email_Alert</fullName>
        <description>New Case Created External Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/New_Case_Created_External</template>
    </alerts>
    <alerts>
        <fullName>New_Case_Picked_Up_External_CN</fullName>
        <ccEmails>gs_support@marinsoftware.com</ccEmails>
        <description>New - Case Picked Up- External (CN):</description>
        <protected>false</protected>
        <recipients>
            <recipient>Customer Engagement Manager - AMER</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Primary_OMM_del__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/New_Case_Picked_Up_External_CN</template>
    </alerts>
    <alerts>
        <fullName>New_Case_Picked_Up_External_FR</fullName>
        <ccEmails>gs_support@marinsoftware.com</ccEmails>
        <description>New - Case Picked Up- External (FR)</description>
        <protected>false</protected>
        <recipients>
            <recipient>Customer Engagement Manager - AMER</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Primary_OMM_del__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/New_Case_Picked_Up_External_FR</template>
    </alerts>
    <alerts>
        <fullName>New_Case_Picked_Up_External_JP</fullName>
        <ccEmails>gs_support@marinsoftware.com</ccEmails>
        <description>New - Case Picked Up- External (JP)</description>
        <protected>false</protected>
        <recipients>
            <recipient>Customer Engagement Manager - AMER</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Primary_OMM_del__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/New_Case_Picked_Up_External_JP</template>
    </alerts>
    <alerts>
        <fullName>SS_Completion_PM_SA</fullName>
        <description>SS Completion - PM/SA</description>
        <protected>false</protected>
        <recipients>
            <field>Project_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Solution_Architect__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/SSCaseCompleted</template>
    </alerts>
    <alerts>
        <fullName>Survey_Negative_Feedback_Alert</fullName>
        <description>Survey Negative Feedback Alert!</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>abauer@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>daniel.feger@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jennifer.cedarleaf@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mspence@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>oclary@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vrussell@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/Survey_Negative_Feedback_Alert</template>
    </alerts>
    <alerts>
        <fullName>UKPod1_High</fullName>
        <description>UKPod1_High</description>
        <protected>false</protected>
        <recipients>
            <recipient>UKPod1</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/Case_Taken</template>
    </alerts>
    <alerts>
        <fullName>UKPod1_Low</fullName>
        <description>UKPod1_Low</description>
        <protected>false</protected>
        <recipients>
            <recipient>UKPod1</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/Case_Taken</template>
    </alerts>
    <alerts>
        <fullName>UKPod1_Medium</fullName>
        <description>UKPod1_Medium</description>
        <protected>false</protected>
        <recipients>
            <recipient>UKPod1</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/Case_Taken</template>
    </alerts>
    <alerts>
        <fullName>UKPod2_High</fullName>
        <description>UKPod2_High</description>
        <protected>false</protected>
        <recipients>
            <recipient>UKPod2</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/Case_Taken</template>
    </alerts>
    <alerts>
        <fullName>UKPod2_Low</fullName>
        <description>UKPod2_Low</description>
        <protected>false</protected>
        <recipients>
            <recipient>UKPod2</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/Case_Taken</template>
    </alerts>
    <alerts>
        <fullName>UKPod2_Medium</fullName>
        <description>UKPod2_Medium</description>
        <protected>false</protected>
        <recipients>
            <recipient>UKPod2</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/Case_Taken</template>
    </alerts>
    <alerts>
        <fullName>UK_PS_EU_Support_Handoff</fullName>
        <description>Handoff UK PS/EU Support</description>
        <protected>false</protected>
        <recipients>
            <recipient>EUSupportServices</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/CLIENT_SERVICES_Case_Handoff_UK_EU_Support</template>
    </alerts>
    <alerts>
        <fullName>US_SS_Case_Close</fullName>
        <description>US SS Case Close</description>
        <protected>false</protected>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/CLIENT_SERVICES_EU_Support_Case_Closure</template>
    </alerts>
    <alerts>
        <fullName>US_SS_Case_taken_customer</fullName>
        <description>US SS Case taken - customer</description>
        <protected>false</protected>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/CLIENT_SERVICES_EU_Support_Case_Taken_Customer</template>
    </alerts>
    <alerts>
        <fullName>US_SS_New_OB_Notification</fullName>
        <description>US SS New OB Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>USSupportServices</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/SUPPORTSERVICESCaseCreation</template>
    </alerts>
    <alerts>
        <fullName>US_SS_New_OB_Notification_PMSA</fullName>
        <description>US SS Taken OB Notification - PM/SA</description>
        <protected>false</protected>
        <recipients>
            <field>Project_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Solution_Architect__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/SSCaseTaken_PS</template>
    </alerts>
    <alerts>
        <fullName>US_SS_New_OB_Notification_PM_SA</fullName>
        <description>US SS New OB Notification - PM/SA</description>
        <protected>false</protected>
        <recipients>
            <field>Project_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Solution_Architect__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/SSCaseNew_PS</template>
    </alerts>
    <alerts>
        <fullName>US_SS_New_OB_Notification_SS</fullName>
        <description>US SS New OB Notification - PM/SA</description>
        <protected>false</protected>
        <recipients>
            <recipient>USSupportServices</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/SSCaseTaken_SS</template>
    </alerts>
    <alerts>
        <fullName>US_SS_Taken_Task</fullName>
        <description>US SS-&gt;CS Taken Task</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/Case_Taken_Support</template>
    </alerts>
    <alerts>
        <fullName>US_Support_new_case_notification</fullName>
        <description>US Support - new case notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>USSupportServices</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/EU_Support_New_Case_Notification_v1</template>
    </alerts>
    <alerts>
        <fullName>US_new_case_time_triggered_email_alert</fullName>
        <description>US new case time triggered email alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>USSupportServices</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/SUPPORTSERVICESCaseCreation</template>
    </alerts>
    <alerts>
        <fullName>Weekend_Support_Email</fullName>
        <ccEmails>gs_support@marinsoftware.com</ccEmails>
        <description>Weekend Support Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>Customer Engagement Manager - AMER</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Primary_OMM_del__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/New_Case_Weekend_Support_External</template>
    </alerts>
    <alerts>
        <fullName>david_test</fullName>
        <description>david_test</description>
        <protected>false</protected>
        <recipients>
            <recipient>dfishman@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/SUPPORT_SERVICES_First_Response</template>
    </alerts>
    <alerts>
        <fullName>e_Picked_Up_Customer_Owner_External_DE</fullName>
        <ccEmails>gs_support@marinsoftware.com</ccEmails>
        <description>Case Picked Up- Customer &amp; Owner (External) DE</description>
        <protected>false</protected>
        <recipients>
            <recipient>Customer Engagement Manager - AMER</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <field>CC_1__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_4__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CC_5__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Primary_OMM_del__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/New_Case_Picked_Up_External_DE</template>
    </alerts>
    <alerts>
        <fullName>email_pager_duty</fullName>
        <ccEmails>p1-emails@marinsoftware1.pagerduty.com</ccEmails>
        <description>email pager duty</description>
        <protected>false</protected>
        <recipients>
            <recipient>oclary@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/NEW_Case_Closure_Internal</template>
    </alerts>
    <alerts>
        <fullName>football_fanatics</fullName>
        <ccEmails>gs-ss@marinsoftware.com, david.rogers@marinsoftware.com, bcalvin@marinsoftware.com</ccEmails>
        <description>football_fanatics</description>
        <protected>false</protected>
        <recipients>
            <recipient>dfishman@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CS_Templates/fanatics</template>
    </alerts>
    <fieldUpdates>
        <fullName>Analytics_AMER_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Analytics</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Analytics - AMER Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Analytics_EMEA_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Analytics_EMEA</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Analytics - EMEA Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Owner_APAC_PS</fullName>
        <field>OwnerId</field>
        <lookupValue>Professional_Services_APAC</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case Owner APAC PS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Client_Services_Bidding</fullName>
        <description>Assigned Worked for bidding issues, set-ups, and questions related to bidding.</description>
        <field>OwnerId</field>
        <lookupValue>Client_Services_Bidding</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Client Services - Bidding</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Client_Services_Tracking_Assignment</fullName>
        <description>Assigned Worked for tracking issues, set-ups, and questions related to tracking.</description>
        <field>OwnerId</field>
        <lookupValue>Client_Services</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Client Services - Tracking Assignment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_Time_tracking_cases</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Close Time tracking cases</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMEA_PS_ticket_closed_time_to_CS</fullName>
        <field>PS_Closure_time__c</field>
        <formula>NOW()</formula>
        <name>EMEA PS ticket closed time (to CS)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMEA_PS_ticket_reop_delete_close_time</fullName>
        <field>PS_Closure_time__c</field>
        <name>EMEA PS ticket reop delete close time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMEA_handoff_CS_to_PS_status_update</fullName>
        <field>PS_Status__c</field>
        <literalValue>New</literalValue>
        <name>EMEA handoff CS to PS status update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_Analytics_APAC_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Analytics_APAC</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Field Update: Analytics APAC Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_Client_Health_Status</fullName>
        <description>Updates the Client Health field with the status that is listed on the Account at the time the Account Name is added or changed.</description>
        <field>Client_Health_Status__c</field>
        <formula>IF(

ISNULL( 
BLANKVALUE(AccountId, null)),
null , 

TEXT(Account.Client_Health_Status__c)
)</formula>
        <name>Field Update: Client Health Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Holiday_watchlist_update</fullName>
        <field>Holiday_Watchlist__c</field>
        <formula>&quot;Holiday watchlist&quot;</formula>
        <name>Holiday watchlist update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Holiday_watchlist_update_appops</fullName>
        <field>Holiday_Watchlist__c</field>
        <formula>&quot;Holiday watchlist&quot;</formula>
        <name>Holiday watchlist update (appops)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Insert_Date_Pick_Up</fullName>
        <field>Date_Pick_Up__c</field>
        <formula>DATEVALUE( Case_Pick_up_time__c )</formula>
        <name>Insert Date Pick-Up</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_Department</fullName>
        <description>Workflow that identifies the department that the person who is taking the case is in.</description>
        <field>Owner_Department__c</field>
        <formula>Owner_copy__r.Department</formula>
        <name>Owner Department</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_Region</fullName>
        <description>Displays the case owners region</description>
        <field>Owner_Region__c</field>
        <formula>IF(  
TEXT(Owner_copy__r.User_Region__c) = &quot;AMER&quot;, &quot;AMER&quot;,

IF(
TEXT(Owner_copy__r.User_Region__c) = &quot;EMEA&quot;, &quot;EMEA&quot;,

IF(
TEXT(Owner_copy__r.User_Region__c) = &quot;APAC&quot;, &quot;APAC&quot;, NULL
)))</formula>
        <name>Owner Region</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PSEng_Queue</fullName>
        <description>PS Eng Queue Case Assignment</description>
        <field>OwnerId</field>
        <lookupValue>PS_Engineering</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>PSEng Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PS_handof_SS_to_PS_set_PS_status_to_new</fullName>
        <field>PS_Status__c</field>
        <literalValue>New</literalValue>
        <name>PS handof SS to PS set PS status to new</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PS_ticked_taken_assigned_time</fullName>
        <field>PS_pick_up_time_assigned__c</field>
        <formula>NOW()</formula>
        <name>PS ticked taken (assigned) time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PS_ticket_closed_time</fullName>
        <field>PS_Closure_time__c</field>
        <formula>NOW()</formula>
        <name>PS ticket closed time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PS_ticket_opened_time_update</fullName>
        <field>PS_ticket_opened_v2__c</field>
        <formula>NOW()</formula>
        <name>PS ticket opened time update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PS_ticket_opened_time_update_from_SS</fullName>
        <field>PS_ticket_opened_v2__c</field>
        <formula>NOW()</formula>
        <name>PS ticket opened time update (from SS)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Parent_Case_Description</fullName>
        <description>Places the parent Description onto the child case in the Description field</description>
        <field>Description</field>
        <formula>Parent.Description</formula>
        <name>Parent Case Description</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ProServeEMEA_Queue</fullName>
        <description>ProServAMER Queue Case Assignment</description>
        <field>OwnerId</field>
        <lookupValue>ProServEMEA</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>ProServeEMEA Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ProServe_Queue</fullName>
        <description>ProServ Queue Case Assignment</description>
        <field>OwnerId</field>
        <lookupValue>ProServAMER</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>ProServe Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Queue_AppOps</fullName>
        <description>AppOps - AMER assigned as case owner.</description>
        <field>OwnerId</field>
        <lookupValue>AppOps</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Queue: AppOps</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Queue_Assigned</fullName>
        <description>Support Services Queue assigned</description>
        <field>OwnerId</field>
        <lookupValue>Support_Services</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Queue Assigned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Queue_EC_SWAT</fullName>
        <field>OwnerId</field>
        <lookupValue>EC_SWAT_Team</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Queue: EC SWAT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Queue_Marin_Pro_AM</fullName>
        <field>OwnerId</field>
        <lookupValue>Marin_Pro_AM</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Queue: Marin Pro AM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected_Handover</fullName>
        <field>Status</field>
        <literalValue>Handoff UK CS To EU Support</literalValue>
        <name>Rejected Handover</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Requestor_Department</fullName>
        <field>Requestor_Department__c</field>
        <formula>Requestor__r.Department</formula>
        <name>Requestor Department</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Requestor_Region</fullName>
        <description>Displays the region of the requestor</description>
        <field>Requestor_Region__c</field>
        <formula>IF( Requestor__r.Country  = &quot;USA&quot;, &quot;AMER&quot;, 
IF(Requestor__r.Country = &quot;United States&quot;, &quot;AMER&quot;, 
IF(Requestor__r.Country = &quot;AU&quot;, &quot;APAC&quot;, 
IF(Requestor__r.Country = &quot;Australia&quot;, &quot;APAC&quot;, 
IF(Requestor__r.Country = &quot;France&quot;, &quot;EMEA&quot;, 
IF(Requestor__r.Country = &quot;Germany&quot;, &quot;EMEA&quot;, 
IF(Requestor__r.Country = &quot;Japan&quot;, &quot;APAC&quot;, 
IF(Requestor__r.Country = &quot;Singapore&quot;, &quot;APAC&quot;, 
IF(Requestor__r.Country = &quot;UK&quot;, &quot;EMEA&quot;, 
IF(Requestor__r.Country = &quot;United Kingdom&quot;,&quot;EMEA&quot;, null))))))))))</formula>
        <name>Requestor Region</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SS_Pick_up_time_definition</fullName>
        <field>Case_Pick_up_time__c</field>
        <formula>IF(ISBLANK(Case_Pick_up_time__c),NOW(),Case_Pick_up_time__c)</formula>
        <name>SS Pick-up time definition</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SS_Queue</fullName>
        <description>SS Queue Case Assignment</description>
        <field>OwnerId</field>
        <lookupValue>Customer_Success_Center</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>SS Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SUPPORT_SERVICES_checkhandoff_to_US_box</fullName>
        <field>Handed_off_to_US__c</field>
        <literalValue>1</literalValue>
        <name>SUPPORT SERVICES: checkhandoff to US box</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SUPPORT_SERVICE_e2c_clientid_extract</fullName>
        <field>Client_ID__c</field>
        <formula>value(right(Subject, len(Subject)-4))</formula>
        <name>SUPPORT SERVICE: e2c clientid extract</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WorkIt2__WorkIt_Case_Overtime_Level_Green</fullName>
        <field>WorkIt2__Color__c</field>
        <literalValue>Green</literalValue>
        <name>WorkIt! Case Overtime Level: Green</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WorkIt2__WorkIt_Case_Overtime_Level_Red</fullName>
        <field>WorkIt2__Color__c</field>
        <literalValue>Red</literalValue>
        <name>WorkIt! Case Overtime Level: Red</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WorkIt2__WorkIt_Case_Overtime_Level_Yellow</fullName>
        <field>WorkIt2__Color__c</field>
        <literalValue>Yellow</literalValue>
        <name>WorkIt! Case Overtime Level: Yellow</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WorkIt2__WorkIt_Set_Case_Allow_Update_to_On</fullName>
        <field>WorkIt2__Allow_Update__c</field>
        <literalValue>1</literalValue>
        <name>WorkIt! Set Case &quot;Allow Update&quot; to On</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WorkIt2__WorkIt_Set_Closed_with_Package_to_On</fullName>
        <field>WorkIt2__Closed_with_Package__c</field>
        <literalValue>1</literalValue>
        <name>WorkIt! Set &quot;Closed with Package&quot; to On</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WorkIt2__WorkIt_Set_Don_t_Time_to_Off</fullName>
        <field>WorkIt2__Dont_Time__c</field>
        <literalValue>0</literalValue>
        <name>WorkIt! Set &quot;Don&apos;t Time&quot; to Off</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WorkIt2__WorkIt_Set_Don_t_Time_to_On</fullName>
        <field>WorkIt2__Dont_Time__c</field>
        <literalValue>1</literalValue>
        <name>WorkIt! Set &quot;Don&apos;t Time&quot; to On</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WorkIt_Set_Case_Allow_Update_to_on</fullName>
        <field>WorkIt2__Allow_Update__c</field>
        <literalValue>1</literalValue>
        <name>WorkIt! Set Case &quot;Allow Update&quot; to on</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>client_facing_case_status_update</fullName>
        <field>Case_status__c</field>
        <formula>IF(ISPICKVAL(Status,&quot;New&quot;),&quot;New&quot;,IF(ISPICKVAL(Status,&quot;In Progress&quot;),&quot;In Progress&quot;,IF(ISPICKVAL(Status,&quot;Closed&quot;),&quot;Closed&quot;,IF(ISPICKVAL(Status,&quot;Closed - Pending Customer&quot;),&quot;Pending&quot;,IF(ISPICKVAL(Status,&quot;Closed - Pending Mantis&quot;),&quot;Pending&quot;,IF(ISPICKVAL(Status,&quot;Closed - Pend Product Release&quot;),&quot;Pending&quot;,IF(ISPICKVAL(Status,&quot;Closed - Pending RT&quot;),&quot;Pending&quot;,IF(ISPICKVAL(Status,&quot;Closed - Pending PS&quot;),&quot;Pending&quot;,IF(ISPICKVAL(Status,&quot;Pending&quot;),&quot;In Progress&quot;,IF(ISPICKVAL(Status,&quot;Handoff UK CS To EU Support&quot;),&quot;In Progress&quot;,IF(ISPICKVAL(Status,&quot;Handoff EU Support To UK CS&quot;),&quot;In Progress&quot;,IF(ISPICKVAL(Status,&quot;Handoff US to UK&quot;),&quot;In Progress&quot;,IF(ISPICKVAL(Status,&quot;Handoff UK to US&quot;),&quot;In Progress&quot;,IF(ISPICKVAL(Status,&quot;Withdrawn&quot;),&quot;Withdrawn&quot;,IF(ISPICKVAL(Status,&quot;Handoff UK PS/AM/Sales to EU Support&quot;),&quot;In Progress&quot;,IF(ISPICKVAL(Status,&quot;Handoff APAC to EU Support&quot;),&quot;In Progress&quot;,IF(ISPICKVAL(Status,&quot;Handoff EU Support to APAC&quot;),&quot;In Progress&quot;,IF(ISPICKVAL(Status,&quot;Handoff to EU Analytics&quot;),&quot;In Progress&quot;,IF(ISPICKVAL(Status,&quot;Handoff to US SS&quot;),&quot;In Progress&quot;,IF(ISPICKVAL(Status,&quot;Handoff to PS (EMEA ONLY)&quot;),&quot;In Progress&quot;,&quot;ERROR&quot;))))))))))))))))))))</formula>
        <name>client facing case status update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>client_facing_case_status_update_v2</fullName>
        <field>Case_status__c</field>
        <formula>IF(ISPICKVAL(Status,&quot;In Progress&quot;),&quot;In Progress&quot;,IF(ISPICKVAL(Status,&quot;Closed&quot;),&quot;Closed&quot;,IF(ISPICKVAL(Status,&quot;Closed - Pending Customer&quot;),&quot;Pending&quot;,IF(ISPICKVAL(Status,&quot;Closed - Pending Mantis&quot;),&quot;Pending&quot;,IF(ISPICKVAL(Status,&quot;Closed - Pend Product Release&quot;),&quot;Pending&quot;,IF(ISPICKVAL(Status,&quot;Closed - Pending RT&quot;),&quot;Pending&quot;,IF(ISPICKVAL(Status,&quot;Closed - Pending PS&quot;),&quot;Pending&quot;,IF(ISPICKVAL(Status,&quot;Pending&quot;),&quot;In Progress&quot;,IF(ISPICKVAL(Status,&quot;Handoff UK CS To EU Support&quot;),&quot;In Progress&quot;,IF(ISPICKVAL(Status,&quot;Handoff EU Support To UK CS&quot;),&quot;In Progress&quot;,IF(ISPICKVAL(Status,&quot;Handoff US to UK&quot;),&quot;In Progress&quot;,IF(ISPICKVAL(Status,&quot;Handoff UK to US&quot;),&quot;In Progress&quot;,IF(ISPICKVAL(Status,&quot;Withdrawn&quot;),&quot;Withdrawn&quot;,IF(ISPICKVAL(Status,&quot;Handoff UK PS/AM/Sales to EU Support&quot;),&quot;In Progress&quot;,IF(ISPICKVAL(Status,&quot;Handoff APAC to EU Support&quot;),&quot;In Progress&quot;,IF(ISPICKVAL(Status,&quot;Handoff EU Support to APAC&quot;),&quot;In Progress&quot;,IF(ISPICKVAL(Status,&quot;Handoff to EU Analytics&quot;),&quot;In Progress&quot;,IF(ISPICKVAL(Status,&quot;Handoff to US SS&quot;),&quot;In Progress&quot;,IF(ISPICKVAL(Status,&quot;Handoff to PS (EMEA ONLY)&quot;),&quot;In Progress&quot;,&quot;ERROR&quot;)))))))))))))))))))</formula>
        <name>client facing case status update v2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>eLearning_Queue</fullName>
        <description>eLearning Queue Case Assignment</description>
        <field>OwnerId</field>
        <lookupValue>eLearning</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>eLearning Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>product_edition</fullName>
        <description>Enterprise vs pro</description>
        <field>Product__c</field>
        <formula>&quot;Pro&quot;</formula>
        <name>product_edition</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>re_open_ticket_back_to_in_progress</fullName>
        <description>flips case back to in progress status</description>
        <field>Status</field>
        <literalValue>Customer - reopened</literalValue>
        <name>re-open ticket back to in progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_closed_case_back_to_new</fullName>
        <description>sets closed case back to new status</description>
        <field>Status</field>
        <literalValue>New</literalValue>
        <name>set closed case back to new</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_resolved_time</fullName>
        <description>sets time resolved</description>
        <field>Date_Time_Resolved__c</field>
        <formula>IF(ISBLANK( Date_Time_Resolved__c),NOW(), Date_Time_Resolved__c)</formula>
        <name>set resolved time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ANALYTICS CSAT Survey</fullName>
        <actions>
            <name>Analytics_email_trigger</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Analytics</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>APAC %26 EU Support %26 EU Analytics - Case Closed</fullName>
        <actions>
            <name>EU_Support_Case_Close</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Co_Owner__c</field>
            <operation>equals</operation>
            <value>daisy vincent,joe southin,phil hunter,will liu,Oliver Clary,Daniel Feger,Rahila Wajahat,Leon Reynolds,Julie Tournier,shaojie (jack) wang,amarjit grewal</value>
        </criteriaItems>
        <description>Notifies Client user (and CCs) &amp; Case Owner upon case closure by EMEA/APAC SS.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>APAC %26 EU Support %26 EU Analytics- Case Taken - Customer</fullName>
        <actions>
            <name>EU_Support_Case_taken_customer</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>EU_Support_response_template</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress,In Progress - Escalation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Co_Owner__c</field>
            <operation>equals</operation>
            <value>daisy vincent,joe southin,Philip hunter,will liu,julie tournier,daniel feger,leon reynolds,oliver clary,rahila wajahat,shaojie (jack) wang</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Pick_up_time__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>** Needs modification - merge with US SS and avoid client notification after handoff? ** Notifies client user (and CCs) upon EMEA/APAC SS or EMEA Analytics taking case.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>APAC %26 EU Support Case Taken%2FCS Case Owner</fullName>
        <actions>
            <name>EU_Support_Taken_Task</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress,In Progress - Escalation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Co_Owner__c</field>
            <operation>equals</operation>
            <value>daisy vincent,joe southin,phil hunter,will liu,rahila wajahat,oliver clary,daniel feger,Julie Tournier,Leon Reynolds,Shaojie (Jack) Wang</value>
        </criteriaItems>
        <description>** Needs Modification - only alert US/APAC OMMs? ** Notifies EMEA/APAC SS upon a EMEA/APAC SS or Analytics person taking ticket.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>APAC Support - Case Created - General</fullName>
        <actions>
            <name>APAC_Support_Case_Created_General</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>roy chiu,adrian man</value>
        </criteriaItems>
        <description>Deactivated on 30/01/2012 - replaced with time triggered alert.
notifies APAC support when case is created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>APAC Support - Case taken - CS case Owner</fullName>
        <actions>
            <name>APAC_Support_Case_Taken_CS_Owner</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>contains</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Co_Owner__c</field>
            <operation>equals</operation>
            <value>will liu,julie tournier,daniel feger,leon reynolds,oliver clary,rahila wajahat,shaojie wang</value>
        </criteriaItems>
        <description>-- not is use --</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>APAC time triggered case assignemnt</fullName>
        <actions>
            <name>APAC_new_case_time_triggered_email_alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND ((value(MID (TEXT (CreatedDate), 12, 2))&gt;=0) , (value(MID (TEXT (CreatedDate), 12, 2)) &lt; 10))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>AppOps P1 holiday watchlist alert v2</fullName>
        <actions>
            <name>AppOps_P1_holiday_watchlist</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Holiday_watchlist_update_appops</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>14 AND (1 OR 2 OR 3 OR 4  OR 5 OR 6 OR 7 OR 8 OR 9 OR 10 OR 11 OR 12 OR 13 or 15 or 16 or 17 or 18 or 19)</booleanFilter>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>20507,4711,40551,40550,15212,36617,36610,36612,36615,36619,36621,39165,39168,39169,39172,39173,2415,2416,32551,32520,32544,20152,11162,8887,36609,36608,36614,36613,36611,36616,36620,30488,30622,36618,30680,34254,35341</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>33761,19212,19209,19213,19193,21907,19191,16575,19216,29772,21885,19215,19217,19211,36689,36690,16545,16577,19200,21906,19194,19198,19203,19207,19201,19195,19196,19190,19210,10943,19197,19202,19192,19208,19205,19204</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>19214,19206,19199,16579,36688,28628,21908,36691,36692,25821,18000,39897,40373,40371,40368,40365,40341,40346,40345,40344,40340,39958,39961,39960,39959,39957,40387,40392,40391,40390,40386,39950,39953,39951,39949,39952</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>39968,39944,39947,39946,39945,39943,39937,39940,39939,39938,39936,40394,40398,40395,40416,40393,39963,39966,39965,39964,39962,40400,40403,40402,40401,40399,40382,40385,40384,40383,40381,40377,40380,40379,40378,40375</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>,39929,39932,39931,39930,39928,18978,18980,37332,37315,20155,20154,20153,20151,24696,17168,25300,25301,23040,25302,17169,25299,25298,25303,25297,42137,21506,40509,40703,40734,2849,21573,2854,2190,6488,2851,2840,2837</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>5127,15439,2857,2850,12355,2838,2862,9638,2848,2847,2844,18471,2859,4610,6487,2839,26167,26166,11719,2853,26168,30117,12343,2841,26102,30122,4615,28996,26103,10769,10770,26141,27875,18046,31057,29045,10732,18390,10739</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>,18389,18391,10729,10716,18392,10718,10714,10721,10727,28890,10740,28751,28756,28743,28749,28747,9573,28757,28761,28755,28745,2385,2389,9718,31062,30201,2384,21778,29442,28754,28762,28750,28760,28759,4641,37830,2382</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>9639,22831,32752,2386,23483,23805,11799,30486,35434,28742,28765,11485,30682,11484,28758,2380,13341,28752,28733,17125,28763,28739,28741,2381,2387,17339,17176,30200,17508,21779,30517,19163,30038,16773,26311,20516,8981</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>25187,25189,25186,25185,25196,25188,25191,25183,25182,25192,25180,25169,25195,8667,23312,13516,13523,13478,13485,13495,13496,13503,13504,13508,13509,13526,13527,13484,13490,13494,13514,13517,13528,13535,13536,13537,8553</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>13491,13510,13534,13479,13486,13505,7626,8006,7644,14135,6774,13518,13529,7612,7758,7619,34980,8554,7630,7629,7741,7628,6775,6776,6873,25208,13497,13506,13511,13539,10777,24640,7862,7863,7859,7642,7643,34244,41822</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>7928,7932,7931,7936,7938,7937,6672,8056,8664,8665,12281,8663,41778,38482,38483,38484,27254,40987,25184,34171,29051,29053,28678,18026,10737,10738,32530,32525,36136,36138,36139,42127,8831,41845,34245,34246,34247,34252</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>17480,11009</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>8881,14591,2287,20638,2378,28736,25190,27738,30620,310,313,17477,10724,10730,10735,18388,17567,8662,16357,2836,2842,2852,12607,2856,2858,2861,4261,3425,19379,33531,15217,17575</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>AppOps</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>210,544,545,546,790,1291,1292,2191,3611,3612,4201,4202,4203,4204,4205,4260,4263,4264,4265,4266,4632,5751,5752,5753,7828,8913,13683,13803,14968,14969,30477,2909,3198,4068,8642,8876,10425,10426,11509,11606,19870</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>19897,20825,21867,21875,21898,22086,22105,22317,22322,22460,22672,22673,22856,22988,23052,23053,23152,25523,27150,27525,27687,27700,28738,33425,44047,4296,4297,4546,5017,5164,5648,11197,11198,11199,11200,11201,11202</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>11203,11204,13843,13844,13845,15621,16780,16781,17579,17580,18152,18156,20411,20414,20985,22616,23352,23357,23366,23367,23368,23369,23370,23371,23372,23373,23374,23375,23376,23377,23378,23379,23380,23381,23382,23383</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>23384,23385,23439,23472,23732,23733,24633,24910,25390,25391,25593,26777,27499,27665,27678,27885,27896,27897,29602,29699,29732,30182,31941,34395,34410,35689,35785,36030,36316,36317,37218,37470</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>38900,43685,43961,43987,44020,396,4634,4718,4723,4799,4801,5050,5149,5289,5748,5749,5750,6077,6142,6144,6392,6395,6396,6400,6514,6552,6676,6683,6699,6702,6731,6732,6734,6735,6736,6901,6931,7681,7685,7996,8265,8306</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AppSupport - P1 or kick off bidding notification EMEA</fullName>
        <actions>
            <name>Application_Support_Page_on_call_SS_for_P1_kickoff_bidding_requests</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Workflow rule to alert on-call that a P1 Support case or a kick off bidding request has been raised. ,</description>
        <formula>AND(
RecordType.Name =&quot;Support Services&quot;, 
OR(Day_of_Week__c = &quot;6 Saturday&quot;,Day_of_Week__c = &quot;7 Sunday&quot;), 
OR(Priority__r.Name__c =&quot;Critical&quot;,Priority__r.Name__c =&quot;Urgent&quot;, AND(ISPICKVAL(Case_Reason__c, &quot;Bidding Support&quot;),                         ISPICKVAL(Case_Reason_Detail__c, &quot;Kick off Bidding&quot;))))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CS - All %28High%29 Case Taken</fullName>
        <actions>
            <name>Case_Taken_All_High</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 OR 5) AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Tim Granshaw,Casey Fuentes,Melody Park,David Fishman,Jeremy Evans,Michael Poynter,Bryan Calvin,Benjamin Guéret,Tu (Mickey) Nguyen,Kamilla Khaydarov,Samuel Lee,Scott Liu</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>High</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress - Escalation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>greaterThan</operation>
            <value>90.01</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>rupesh sharma,Marie Boivent,Adam Hawkins,anuj sharma,josh boxer,eu wen teh,Marc Stefani</value>
        </criteriaItems>
        <description>** Needs modification ** Notifies all Pods (US + UK) upon Escalated case being taken by OMM or SS. - (sends email from Case Owner rather than Co-Owner)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CS - All %28Low%29 Case Taken</fullName>
        <actions>
            <name>Case_Taken_All_Low</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 OR 5) AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Tim Granshaw,Casey Fuentes,Melody Park,David Fishman,Jeremy Evans,Michael Poynter,Bryan Calvin,Benjamin Guéret,Tu (Mickey) Nguyen,Kamilla Khaydarov,Samuel Lee,Scott Liu</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>Low</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress - Escalation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>greaterThan</operation>
            <value>360.01</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>rupesh sharma,Marie Boivent,Adam Hawkins,anuj sharma,josh boxer,eu wen teh,Marc Stefani</value>
        </criteriaItems>
        <description>** Needs modification ** Notifies all Pods (US + UK) upon Escalated case being taken by OMM or SS. - (sends email from Case Owner rather than Co-Owner)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CS - All %28Medium%29 Case Taken</fullName>
        <actions>
            <name>Case_Taken_All_Medium</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 OR 5) AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Tim Granshaw,Casey Fuentes,Melody Park,David Fishman,Jeremy Evans,Michael Poynter,Bryan Calvin,Benjamin Guéret,Tu (Mickey) Nguyen,Kamilla Khaydarov,Samuel Lee,Scott Liu</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>Medium</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress - Escalation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>greaterThan</operation>
            <value>180.01</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Marc Stefani,rupesh sharma,Marie Boivent,Adam Hawkins,anuj sharma,josh boxer,eu wen teh</value>
        </criteriaItems>
        <description>** Needs modification ** Notifies all Pods (US + UK) upon Escalated case being taken by OMM or SS. - (sends email from Case Owner rather than Co-Owner)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CS - Case Closed - Customer %26 Owner</fullName>
        <actions>
            <name>Case_Closed</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 and 3</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Co_Owner__c</field>
            <operation>notEqual</operation>
            <value>joe southin,philip hunter,will liu,Shaojie Wang,julie tournier,daniel feger,oliver clary,rahila wajahat,kamilla khaydarov,joel seaton,carren enriquez,david fishman,karl leong,chitwan kaur,perry nourani,jennifer cedarleaf</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Co_Owner__c</field>
            <operation>notEqual</operation>
            <value>kelsey lam,Ivan Trachter,Zoey Glassberg,Paul Yen,Kazuki Horiguchi,Andy Bauer,Jacques Caspi</value>
        </criteriaItems>
        <description>Notifies client user (and CCs) + Case Owner upon case being closed by non-SS team member.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CS - Case Taken - Customer</fullName>
        <actions>
            <name>Case_Taken_Customer</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 and 3 and 4</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress,In Progress - Escalation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Co_Owner__c</field>
            <operation>notEqual</operation>
            <value>joe southin,philip hunter,will liu,Shaojie Wang,julie tournier,daniel feger,oliver clary,rahila wajahat,kamilla khaydarov,joel seaton,carren enriquez,david fishman,karl leong,chitwan kaur,perry nourani,jennifer cedarleaf</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Co_Owner__c</field>
            <operation>notEqual</operation>
            <value>daisy vincent,amarjit grewal,kelsey lam,Ivan Trachter,Zoey Glassberg,Paul Yen,Kazuki Horiguchi,Jacques Caspi,Andy Bauer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Pick_up_time__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Notifies client user upon an OMM taking a case by changing status to &quot;in Progress&quot; or &quot;in Progress - Escalation&quot;.
This rule does not apply when a member of SS takes a ticket by adding themselves as co-owner.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CS - New Case Created - General</fullName>
        <actions>
            <name>CSR_Alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>notEqual</operation>
            <value>Marin Support Services</value>
        </criteriaItems>
        <description>Notifies Case owner of new case being created</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CS - New Case Created - No CSR</fullName>
        <actions>
            <name>CSR_Alert_OCDF</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>NULL</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Notifies David Fishman and Oli Clary if new case created and no CSR found</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CS Help %28High%29 Case Taken</fullName>
        <actions>
            <name>Case_Taken_2</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Jeremy Evans,Michael Poynter,Bryan Calvin,Benjamin Guéret</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>High</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress - Escalation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>greaterThan</operation>
            <value>60</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>lessThan</operation>
            <value>90.01</value>
        </criteriaItems>
        <description>** Needs modification ** Notifies Pod upon Escalated case being taken by OMM or CSR. - (sends email from Case Owner rather than Co-Owner)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CS Help %28Low%29 Case Taken</fullName>
        <actions>
            <name>Case_Taken_CS_Help_Low</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Jeremy Evans,Michael Poynter,Bryan Calvin,Benjamin Guéret</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>Low</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress - Escalation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>greaterThan</operation>
            <value>240</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>lessThan</operation>
            <value>360</value>
        </criteriaItems>
        <description>** Needs modification ** Notifies Pod upon Escalated case being taken by OMM or CSR. - (sends email from Case Owner rather than Co-Owner)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CS Help %28Medium%29 Case Taken</fullName>
        <actions>
            <name>Case_Taken_CS_Help_Medium</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Jeremy Evans,Michael Poynter,Bryan Calvin,Benjamin Guéret</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>Medium</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress - Escalation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>greaterThan</operation>
            <value>120</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>lessThan</operation>
            <value>180.01</value>
        </criteriaItems>
        <description>** Needs modification ** Notifies Pod upon Escalated case being taken by OMM or CSR. - (sends email from Case Owner rather than Co-Owner)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CS Survey Feedback Alert</fullName>
        <actions>
            <name>CS_Training_Survey_Negative_Feedback_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>((1 OR 2 OR 3) AND 4)</booleanFilter>
        <criteriaItems>
            <field>Case.Quality_Score__c</field>
            <operation>equals</operation>
            <value>1,3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Response_time_Score__c</field>
            <operation>equals</operation>
            <value>1,3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Resolution_time_score__c</field>
            <operation>equals</operation>
            <value>1,3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Client Services</value>
        </criteriaItems>
        <description>Alerting OMMs &amp; Case Owner on negative training feedback</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CSAT Survey</fullName>
        <actions>
            <name>CSAT_Survey</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notContain</operation>
            <value>marinsoftware</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Language__c</field>
            <operation>contains</operation>
            <value>English</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Support Services</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CSAT Survey CH</fullName>
        <actions>
            <name>CSAT_Survey_CH</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notContain</operation>
            <value>marinsoftware</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Language__c</field>
            <operation>equals</operation>
            <value>中文 - Mandarin</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Support Services</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CSAT Survey DE</fullName>
        <actions>
            <name>Customer_Support_Survey_DE</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notContain</operation>
            <value>marinsoftware</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Language__c</field>
            <operation>equals</operation>
            <value>Deutsch - German</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Support Services</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CSAT Survey FR</fullName>
        <actions>
            <name>Customer_Support_Survey_FR</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notContain</operation>
            <value>marinsoftware</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Language__c</field>
            <operation>equals</operation>
            <value>Français - French</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Support Services</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CSAT Survey JP</fullName>
        <actions>
            <name>Customer_Support_Survey_JP</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notContain</operation>
            <value>marinsoftware</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Language__c</field>
            <operation>equals</operation>
            <value>日本語 - Japanese</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Support Services</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CSC P1 holiday watchlist alert v2</fullName>
        <actions>
            <name>CSC_Holiday_watch_list</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Holiday_watchlist_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>14 AND (1 OR 2 OR 3 OR 4  OR 5 OR 6 OR 7 OR 8 OR 9 OR 10 OR 11 OR 12 OR 13)</booleanFilter>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>20507,4711,40551,40550,15212,36617,36610,36612,36615,36619,36621,39165,39168,39169,39172,39173,2415,2416,32551,32520,32544,20152,11162,8887,36609,36608,36614,36613,36611,36616,36620,30488,30622,36618,30680,34254,35341</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>33761,19212,19209,19213,19193,21907,19191,16575,19216,29772,21885,19215,19217,19211,36689,36690,16545,16577,19200,21906,19194,19198,19203,19207,19201,19195,19196,19190,19210,10943,19197,19202,19192,19208,19205,19204</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>19214,19206,19199,16579,36688,28628,21908,36691,36692,25821,18000,39897,40373,40371,40368,40365,40341,40346,40345,40344,40340,39958,39961,39960,39959,39957,40387,40392,40391,40390,40386,39950,39953,39951,39949,39952</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>39968,39944,39947,39946,39945,39943,39937,39940,39939,39938,39936,40394,40398,40395,40416,40393,39963,39966,39965,39964,39962,40400,40403,40402,40401,40399,40382,40385,40384,40383,40381,40377,40380,40379,40378,40375</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>,39929,39932,39931,39930,39928,18978,18980,37332,37315,20155,20154,20153,20151,24696,17168,25300,25301,23040,25302,17169,25299,25298,25303,25297,42137,21506,40509,40703,40734,2849,21573,2854,2190,6488,2851,2840,2837</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>5127,15439,2857,2850,12355,2838,2862,9638,2848,2847,2844,18471,2859,4610,6487,2839,26167,26166,11719,2853,26168,30117,12343,2841,26102,30122,4615,28996,26103,10769,10770,26141,27875,18046,31057,29045,10732,18390,10739</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>,18389,18391,10729,10716,18392,10718,10714,10721,10727,28890,10740,28751,28756,28743,28749,28747,9573,28757,28761,28755,28745,2385,2389,9718,31062,30201,2384,21778,29442,28754,28762,28750,28760,28759,4641,37830,2382</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>9639,22831,32752,2386,23483,23805,11799,30486,35434,28742,28765,11485,30682,11484,28758,2380,13341,28752,28733,17125,28763,28739,28741,2381,2387,17339,17176,30200,17508,21779,30517,19163,30038,16773,26311,20516,8981</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>25187,25189,25186,25185,25196,25188,25191,25183,25182,25192,25180,25169,25195,8667,23312,13516,13523,13478,13485,13495,13496,13503,13504,13508,13509,13526,13527,13484,13490,13494,13514,13517,13528,13535,13536,13537,8553</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>13491,13510,13534,13479,13486,13505,7626,8006,7644,14135,6774,13518,13529,7612,7758,7619,34980,8554,7630,7629,7741,7628,6775,6776,6873,25208,13497,13506,13511,13539,10777,24640,7862,7863,7859,7642,7643,34244,41822</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>7928,7932,7931,7936,7938,7937,6672,8056,8664,8665,12281,8663,41778,38482,38483,38484,27254,40987,25184,34171,29051,29053,28678,18026,10737,10738,32530,32525,36136,36138,36139,42127,8831,41845,34245,34246,34247,34252</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>17480,11009</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
            <value>8881,14591,2287,20638,2378,28736,25190,27738,30620,310,313,17477,10724,10730,10735,18388,17567,8662,16357,2836,2842,2852,12607,2856,2858,2861,4261,3425,19379,33531,15217,17575</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Support Services</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Close Time tracking cases</fullName>
        <actions>
            <name>Close_Time_tracking_cases</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Time Tracking</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Age__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL %28CN%29%3A Case Closed - Customer %26 Owner %28External%29</fullName>
        <actions>
            <name>NEW_Case_Closure_External_CN</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Support Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Language__c</field>
            <operation>equals</operation>
            <value>中文 - Mandarin</value>
        </criteriaItems>
        <description>Notifies CN client user (and CCs) upon case being closed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL %28DE%29%3A Case Closed - Customer %26 Owner %28External%29</fullName>
        <actions>
            <name>NEW_Case_Closure_External_DE</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Support Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Language__c</field>
            <operation>equals</operation>
            <value>Deutsch - German</value>
        </criteriaItems>
        <description>Notifies DE client user (and CCs) upon case being closed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL %28FR%29%3A Case Closed - Customer %26 Owner %28External%29</fullName>
        <actions>
            <name>NEW_Case_Closure_External_FR</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Support Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Language__c</field>
            <operation>equals</operation>
            <value>Français - French</value>
        </criteriaItems>
        <description>Notifies FR client user (and CCs) upon case being closed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL %28JP%29%3A Case Closed - Customer %26 Owner %28External%29</fullName>
        <actions>
            <name>New_Case_Closure_External_JP</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Support Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Language__c</field>
            <operation>equals</operation>
            <value>日本語 - Japanese</value>
        </criteriaItems>
        <description>Notifies JP client user (and CCs) upon case being closed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL%3A CEM - Case Closure</fullName>
        <actions>
            <name>CEM_Case_Closure</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Client Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>This email alert is intended to end the discussion with the client for Customer Engagement Managers and to track email communications in our CRM system.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL%3A CEM - Case Creation</fullName>
        <actions>
            <name>CEM_Case_Creation_External</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Client Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Pick_up_time__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <description>This email alert is intended to create the discussion with the client for Customer Engagement Managers and to track email communications in our CRM system.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL%3A CS Tracking Case Created - Internal</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Client Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Reason_Detail__c</field>
            <operation>equals</operation>
            <value>Tracking</value>
        </criteriaItems>
        <description>This email alert is triggered when a Case is created. It emails the Owner, OMM, Requester, and Webmail (if applicable)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL%3A Case Active Status Change- Internal Users</fullName>
        <actions>
            <name>Email_Case_Picked_Up_Internal</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Email notification to internal Marin users whenever a case is changed to In Progress or Pending for Professional Service (012500000005TPQ) cases, PS Engineering (012500000005UCT) Cases, and AppOps (012500000005TeC)</description>
        <formula>AND(
OR(
ISPICKVAL(Status, &apos;New&apos;),
ISPICKVAL(Status, &apos;In Progress&apos;),
ISPICKVAL(Status, &apos;Pending&apos;)
),
OR(
ISCHANGED(Status),
ISCHANGED(OwnerId)
),
OR(
RecordType.Name = &apos;AppOps&apos;,
RecordType.Name = &apos;Professional Services&apos;,
RecordType.Name = &apos;PS Engineering&apos;
)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL%3A Case Closed - Customer %26 Owner %28External%29</fullName>
        <actions>
            <name>Case_Closed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND ((2 AND 3 AND 4 AND ((5 and 6) OR 7 OR 8)) OR 9)</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Support Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Language__c</field>
            <operation>notEqual</operation>
            <value>Français - French,Deutsch - German,日本語 - Japanese,中文 - Chinese</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Product_Version__c</field>
            <operation>notEqual</operation>
            <value>Enterprise</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Region__c</field>
            <operation>equals</operation>
            <value>NA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Region__c</field>
            <operation>equals</operation>
            <value>EMEA,APAC,LATAM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Region__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Analytics</value>
        </criteriaItems>
        <description>Notifies client user (and CCs) upon case being closed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL%3A Case Closed - Customer %26 Owner %28External%29 - CSC</fullName>
        <actions>
            <name>Email_Case_Closed_External_CSC</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Support Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Product_Version__c</field>
            <operation>equals</operation>
            <value>Enterprise</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Region__c</field>
            <operation>equals</operation>
            <value>NA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Language__c</field>
            <operation>notEqual</operation>
            <value>Français - French,Deutsch - German,日本語 - Japanese,中文 - Chinese</value>
        </criteriaItems>
        <description>Notifies client (and CCs) upon case being closed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL%3A Case Closed - Customer %26 Owner %28External%29 CS Bid Setup1</fullName>
        <actions>
            <name>CS_Bid_Setup_Closure_External_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CC_1__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Client Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Reason_Detail__c</field>
            <operation>equals</operation>
            <value>Bid Setup</value>
        </criteriaItems>
        <description>Notifies client user (and CCs) upon case being closed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL%3A Case Closed - Customer %26 Owner %28External%29 CS Consulting Setup</fullName>
        <actions>
            <name>CS_Consulting_Closure_External_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CC_1__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Client Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Reason_Detail__c</field>
            <operation>equals</operation>
            <value>Consulting/Bidding</value>
        </criteriaItems>
        <description>Notifies client user (and CCs) upon case being closed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL%3A Case Closed - Customer %26 Owner %28External%29 CS Tracking</fullName>
        <actions>
            <name>CS_Tracking_Closure_External_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CC_1__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Client Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Reason_Detail__c</field>
            <operation>equals</operation>
            <value>Tracking</value>
        </criteriaItems>
        <description>Notifies client user (and CCs) upon case being closed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL%3A Case Closed - Customer %26 Owner %28External%29 CS Training</fullName>
        <actions>
            <name>CS_Training_Closure_External_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND (5 OR 6)</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CC_1__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Client Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Reason__c</field>
            <operation>equals</operation>
            <value>Training</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Language__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Language__c</field>
            <operation>equals</operation>
            <value>English</value>
        </criteriaItems>
        <description>Notifies client user (and CCs) upon case being closed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL%3A Case Closed - Customer %26 Owner %28External%29 CS Training CN</fullName>
        <actions>
            <name>CS_Training_Closure_External_Email_Alert_CN</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CC_1__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Client Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Reason__c</field>
            <operation>equals</operation>
            <value>Training</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Language__c</field>
            <operation>equals</operation>
            <value>中文 - Mandarin</value>
        </criteriaItems>
        <description>Notifies client user (and CCs) upon case being closed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL%3A Case Closed - Customer %26 Owner %28External%29 CS Training JP</fullName>
        <actions>
            <name>CS_Training_Closure_External_Email_Alert_JP</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CC_1__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Client Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Reason__c</field>
            <operation>equals</operation>
            <value>Training</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Language__c</field>
            <operation>equals</operation>
            <value>日本語 - Japanese</value>
        </criteriaItems>
        <description>Notifies client user (and CCs) upon case being closed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL%3A Case Closed - Internal Users</fullName>
        <actions>
            <name>New_Case_Closed_Internal_Users</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>,Time Tracking,Analytics,Enhanced Campaign Migration,Customer Success Center</value>
        </criteriaItems>
        <description>Email notification rule for all cases that are completed (minus Time Tracking).</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL%3A Case Created - Feature Request - Product Management</fullName>
        <actions>
            <name>Case_Created_Feature_Request_Internal</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Feature Request</value>
        </criteriaItems>
        <description>Created 2014-04-02 by Mark Klinski per Andy Hollingsworth for Product Management for new feature requests.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL%3A Case Created - Internal</fullName>
        <actions>
            <name>Email_New_Case_Created_Template</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2 AND 5) OR (1 AND 2 AND 3 AND 4)</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>,Time Tracking,Client Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>AppOps</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>Email</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>AppOps,Enhanced Campaign Migration</value>
        </criteriaItems>
        <description>This email alert is triggered when a Case is created. It emails the Owner, OMM, Requester, and Webmail (if applicable)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL%3A Case Created - Internal - AppOps</fullName>
        <actions>
            <name>AlertAppOpsCaseCreated</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>AppOps</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email</value>
        </criteriaItems>
        <description>This email alert is triggered when a Case is created. It emails the Owner, OMM, Requester, and Webmail (if applicable)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL%3A Case Created - Internal - AppOps Pager</fullName>
        <actions>
            <name>AlertAppOpsCaseCreatedPager</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(
ISPICKVAL(Status,&apos;New&apos;),
 RecordType.Name = &apos;AppOps&apos;,
OR(
 Priority__r.Name__c = &apos;1&apos;,
Priority__r.Name__c = &apos;2&apos;
)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL%3A Case Created- Customer %26 Owner %28External%29</fullName>
        <actions>
            <name>Client_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web to Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Support Services</value>
        </criteriaItems>
        <description>Notifies client user (and CCs) upon new case creation.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL%3A Case Created- Customer %26 Owner %28External%29 - Broken%3F</fullName>
        <actions>
            <name>New_Case_Created_External</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Support Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ParentId</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Notifies client user (and CCs) upon case being closed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL%3A Case Picked Up- Customer %26 Owner %28External%29</fullName>
        <actions>
            <name>Email_Case_Picked_Up_External</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND ((4 AND 5) OR 6)</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Pick_up_time__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Language__c</field>
            <operation>notEqual</operation>
            <value>Français - French,Deutsch - German,日本語 - Japanese,中文 - Chinese</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Support Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Analytics</value>
        </criteriaItems>
        <description>Notifies client user (and CCs) upon case being picked up</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL%3A Case Picked Up- Customer %26 Owner %28External%29 - CSC</fullName>
        <actions>
            <name>Email_Case_Picked_Up_External_CSC</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Support Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Pick_up_time__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Product_Version__c</field>
            <operation>equals</operation>
            <value>Enterprise</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Region__c</field>
            <operation>equals</operation>
            <value>NA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Language__c</field>
            <operation>notEqual</operation>
            <value>Français - French,Deutsch - German,日本語 - Japanese,中文 - Chinese</value>
        </criteriaItems>
        <description>This workflow is emailed to the client (and CCs) upon case being picked up with additional messaging for the Customer Success Center. NA only</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL%3A Case Picked Up- Customer %26 Owner %28External%29 CN</fullName>
        <actions>
            <name>New_Case_Picked_Up_External_CN</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Support Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Pick_up_time__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Language__c</field>
            <operation>equals</operation>
            <value>中文 - Chinese</value>
        </criteriaItems>
        <description>Notifies CN client user (and CCs) upon case being picked up.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL%3A Case Picked Up- Customer %26 Owner %28External%29 DE</fullName>
        <actions>
            <name>e_Picked_Up_Customer_Owner_External_DE</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Support Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Pick_up_time__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Language__c</field>
            <operation>equals</operation>
            <value>Deutsch - German</value>
        </criteriaItems>
        <description>Notifies DE client user (and CCs) upon case being picked up</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL%3A Case Picked Up- Customer %26 Owner %28External%29 FR</fullName>
        <actions>
            <name>New_Case_Picked_Up_External_FR</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Support Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Pick_up_time__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Language__c</field>
            <operation>equals</operation>
            <value>Français - French</value>
        </criteriaItems>
        <description>Notifies FR client user (and CCs) upon case being picked up</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL%3A Case Picked Up- Customer %26 Owner %28External%29 JP</fullName>
        <actions>
            <name>New_Case_Picked_Up_External_JP</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Support Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Pick_up_time__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Language__c</field>
            <operation>equals</operation>
            <value>日本語 - Japanese</value>
        </criteriaItems>
        <description>Notifies JP client user (and CCs) upon case being picked up.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL%3A Case Weekend Support - Customer %26 Owner %28External%29</fullName>
        <actions>
            <name>Weekend_Support_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND ((4 AND 5) OR 6)</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Hand-off to Weekend</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Pick_up_time__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Language__c</field>
            <operation>notEqual</operation>
            <value>Français - French,Deutsch - German,日本語 - Japanese,中文 - Chinese</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Support Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Analytics</value>
        </criteriaItems>
        <description>Notifies client user (and CCs) upon case being moved to Weekend Support.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL%3A EMEA time triggered case assignemnt</fullName>
        <actions>
            <name>EMEA_new_case_time_triggered_email_alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND ((value(MID (TEXT (CreatedDate), 12, 2))&gt;=7) , (value(MID (TEXT (CreatedDate), 12, 2)) &lt; 17), $RecordType.Name =&quot;Support Services&quot;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EMEA Analytics - Case taken - case owner</fullName>
        <actions>
            <name>EMEA_Analytics_Case_taken_to_case_owner</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>contains</operation>
            <value>progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Co_Owner__c</field>
            <operation>equals</operation>
            <value>joe southin,phil hunter</value>
        </criteriaItems>
        <description>Notifies case owner upon case being taken by EMEA Analytics</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMEA Handoff to EU Analytics</fullName>
        <actions>
            <name>EMEA_Handoff_to_EU_Analytics</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Handoff to EU Analytics</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMEA PS ticket assigned</fullName>
        <actions>
            <name>EMEA_PS_ticket_assignment</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.PS_Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.PS_Owner_EMEA_ONLY__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Handoff to PS (EMEA ONLY)</value>
        </criteriaItems>
        <description>Notify PS individual of ticket assignment.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMEA PS ticket closed to CS</fullName>
        <actions>
            <name>EMEA_PS_ticket_closed_to_CS</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>EMEA_PS_ticket_closed_time_to_CS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.PS_Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Co_Owner__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMEA PS ticket closed to SS</fullName>
        <actions>
            <name>EMEA_PS_ticket_closed_to_SS</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>PS_ticket_closed_time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.PS_Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Co_Owner__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMEA PS ticket reopened</fullName>
        <actions>
            <name>EMEA_PS_ticket_reopened</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>EMEA_PS_ticket_reop_delete_close_time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.PS_Status__c</field>
            <operation>equals</operation>
            <value>Re-opened</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.PS_Closure_time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMEA PS ticket taken to SS</fullName>
        <actions>
            <name>EMEA_PS_ticket_taken_email_notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>PS_ticked_taken_assigned_time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Handoff to PS (EMEA ONLY)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.PS_Status__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.PS_Owner_EMEA_ONLY__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>notifies case owner + co-owner when ticket is picked up.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMEA SS - New Case Created - General</fullName>
        <actions>
            <name>EU_Support_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Anuj Sharma,Tim Granshaw,Marie Boivent,Josh Boxer,Rupesh Sharma,Eu Wen Teh,Marc Stefani,Matthew Thomas,Jakob Schiffkorn,Pierre da costa</value>
        </criteriaItems>
        <description>Deactivated on 30/01/2012 - replaced with time triggered alert
Notifies EMEA SS to a new UK based ticket coming through (for selected OMMs)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EMEA handoff CS to PS notification</fullName>
        <actions>
            <name>EMEA_handoff_CS_to_PS</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>EMEA_handoff_CS_to_PS_status_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PS_ticket_opened_time_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Handoff to PS (EMEA ONLY)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Co_Owner__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Notifies EMEA PS of a case being handed off for investigation.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMEA handoff SS to PS notification</fullName>
        <actions>
            <name>EMEA_handoff_SS_to_PS</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>PS_handof_SS_to_PS_set_PS_status_to_new</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PS_ticket_opened_time_update_from_SS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Handoff to PS (EMEA ONLY)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Co_Owner__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Notifies EMEA PS of a case being handed off for investigation.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMEA time triggered case assignemnt</fullName>
        <actions>
            <name>EMEA_new_case_time_triggered_email_alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND ((value(MID (TEXT (CreatedDate), 12, 2))&gt;=7) , (value(MID (TEXT (CreatedDate), 12, 2)) &lt; 17), NOT( ISPICKVAL(Case_Reason__c , &quot;Time Tracking&quot;)))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email Alert%3A ProCase Creation Alert</fullName>
        <actions>
            <name>Alert_Pro_Case_was_created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Created by Gary Sopko
Requested that if a Pro case is created that an email alert would be sent to Philip Pillsbury.

5-2013: Removed Enhanced Campaign Migration cases from rule. Asa</description>
        <formula>AND(
ISPICKVAL(Account.Product_Version__c, &quot;PROFESSIONAL&quot;),
 RecordType.DeveloperName  &lt;&gt; &quot;Enhanced Campaign Migration&quot;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email Notification on Tier 3 Cases</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Account_Tier__c</field>
            <operation>equals</operation>
            <value>Tier 3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Tier_manual__c</field>
            <operation>equals</operation>
            <value>Advanced</value>
        </criteriaItems>
        <description>When a Tier 3 Case is created with Account Tier = Advanced, this workflow triggers an email notification to the Account owner and Nate Sandford</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A JIRA Ticket Update</fullName>
        <actions>
            <name>Email_Feature_Request_Update</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Provides the Marin internal team with updates for a case in relation to the JIRA ticket that is linked.</description>
        <formula>AND(
RecordType.Name = &apos;Feature Request&apos;,
OR(
ISCHANGED(JIRA_Resolution__c),
ISCHANGED(JIRA_Target_Version__c)
)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Survey Feedback Alert</fullName>
        <actions>
            <name>Survey_Negative_Feedback_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>((1 OR 2 OR 3) AND 4)</booleanFilter>
        <criteriaItems>
            <field>Case.Quality_Score__c</field>
            <operation>equals</operation>
            <value>1,3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Response_time_Score__c</field>
            <operation>equals</operation>
            <value>1,3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Resolution_time_score__c</field>
            <operation>equals</operation>
            <value>1,3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Analytics</value>
        </criteriaItems>
        <description>Alerting OMMs &amp; Case Owner on negative feedback</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Enterprise_v_Pro</fullName>
        <actions>
            <name>product_edition</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Niraj Shah</value>
        </criteriaItems>
        <description>Tells Marin if the case is from Enterprise or Pro</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Client Health Status</fullName>
        <actions>
            <name>Field_Update_Client_Health_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISBLANK(AccountId) || NOT(ISBLANK(AccountId))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Enterprise_v_Pro</fullName>
        <actions>
            <name>product_edition</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Niraj Shah</value>
        </criteriaItems>
        <description>Tells Marin if the case is from Enterprise or Pro</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Insert Date Pick-Up</fullName>
        <actions>
            <name>Insert_Date_Pick_Up</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Case_Pick_up_time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Uses the Data/Time Pick-Up and converts it to a standard Date.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Owner Information</fullName>
        <actions>
            <name>Owner_Department</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Owner_Region</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Pulls the case owner data based off of a hidden field called &quot;Owner Copy&quot; that is programmed to copy the owner of the case. This allows look the workflow to place the Owner data on the case record.</description>
        <formula>Owner_copy__c  &lt;&gt; null || ischanged( Owner_copy__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Pick-up time definition</fullName>
        <actions>
            <name>SS_Pick_up_time_definition</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>contains</operation>
            <value>Scheduled,In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Pick_up_time__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Requester Data</fullName>
        <actions>
            <name>Requestor_Department</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Requestor_Region</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Identifies the information of the case owner.</description>
        <formula>Requestor__c   &lt;&gt; null || ischanged( Requestor__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Handoff APAC%2FUK</fullName>
        <actions>
            <name>CLIENT_SERVICES_Case_Handoff_APAC_EU_Support</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Handoff APAC to EU Support</value>
        </criteriaItems>
        <description>Notifies EMEA SS upon handoff from APAC.(trigger: Status = Handoff APAC to EU Support)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Handoff EU Support%2FUK CS</fullName>
        <actions>
            <name>EU_Support_CS_Taken_Task</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Handoff EU Support To UK CS</value>
        </criteriaItems>
        <description>Notifies Case Owner (Primary OMM) upon case handoff.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Handoff EU Support%2FUK PS</fullName>
        <actions>
            <name>Handoff_EU_Support_UK_PS</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Handoff EU Support to UK PS</value>
        </criteriaItems>
        <description>-- not in use --</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Handoff UK CS%2FEU Support</fullName>
        <actions>
            <name>Handoff_UK_EU</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Handoff UK CS To EU Support</value>
        </criteriaItems>
        <description>Notifies EMEA SS upon case handoff (trigger: Status = Handoff UK CS to EU Support)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Handoff UK PS%2FEU Support</fullName>
        <actions>
            <name>UK_PS_EU_Support_Handoff</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Handoff UK PS/AM/Sales to EU Support</value>
        </criteriaItems>
        <description>Notifies EMEA SS upon handoff (trigger: Status = Handoff UK PS/AM/Sales to EU Support)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Handoff UK%2FAPAC</fullName>
        <actions>
            <name>Handoff_APAC_EU_Support</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Handoff EU Support to APAC</value>
        </criteriaItems>
        <description>** Needs modifying - change to alert APAC SS first? ** Notifies Case owner upon handoff (trigger: Handoff EU Support to APAC)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Handoff UK%2FUS</fullName>
        <actions>
            <name>Handoff_UK_US</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SUPPORT_SERVICES_checkhandoff_to_US_box</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Handoff UK to US</value>
        </criteriaItems>
        <description>** Needs modification - notify US SS? ** Notifies 3 US Pods upon handoff.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Handoff US%2FUK</fullName>
        <actions>
            <name>Handoff_US_UK</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Handoff US to UK</value>
        </criteriaItems>
        <description>Notifies EMEA SS upon case handoff.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Handoff to EU Analytics</fullName>
        <actions>
            <name>Handoff_to_EU_Analytics</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Handoff to EU Analytics</value>
        </criteriaItems>
        <description>Notifies EMEA Anaytics upon handoff</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Handoff to SS</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>contains</operation>
            <value>Handoff to SS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Hotels%2Ecom ticket closed notification</fullName>
        <actions>
            <name>Hotels_com_Ticket_Closed_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>equals</operation>
            <value>Hotels.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Management alert for ongoing issues</fullName>
        <actions>
            <name>Management_alert_of_ongoing_issue</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notContain</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>greaterThan</operation>
            <value>2880</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Marin Help %28High%29 Case Taken</fullName>
        <actions>
            <name>Case_Taken</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Curt Weaver,Tim Granshaw,Casey Fuentes,Melody Park,David Fishman</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>High</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress - Escalation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>greaterThan</operation>
            <value>60</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>lessThan</operation>
            <value>90.01</value>
        </criteriaItems>
        <description>** Needs modification ** Notifies Pod upon Escalated case being taken by OMM or CSR. - (sends email from Case Owner rather than Co-Owner)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Marin Help %28Low%29 Case Taken</fullName>
        <actions>
            <name>Case_Taken_Marin_Help_Low</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Curt Weaver,Tim Granshaw,Casey Fuentes,Melody Park,David Fishman</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>Low</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress - Escalation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>greaterThan</operation>
            <value>240</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>lessThan</operation>
            <value>360</value>
        </criteriaItems>
        <description>** Needs modification ** Notifies Pod upon Escalated case being taken by OMM or CSR. - (sends email from Case Owner rather than Co-Owner)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Marin Help %28Medium%29 Case Taken</fullName>
        <actions>
            <name>Case_Taken_Marin_Help_Medium</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Curt Weaver,Tim Granshaw,Casey Fuentes,Melody Park,David Fishman</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>Medium</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress - Escalation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>greaterThan</operation>
            <value>120</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>lessThan</operation>
            <value>180.01</value>
        </criteriaItems>
        <description>** Needs modification ** Notifies Pod upon Escalated case being taken by OMM or CSR. - (sends email from Case Owner rather than Co-Owner)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Morning Checks Data delay notification</fullName>
        <actions>
            <name>Data_delay_notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.Case_status__c</field>
            <operation>equals</operation>
            <value>NEW</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>equals</operation>
            <value>Morning Checks - data delay alert</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Rahila Wajahat</value>
        </criteriaItems>
        <description>Client notification email for publisher and/or revenue data delays.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New CSR Created - No CID</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Client_ID__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>** Needs modification - is it required? ** Notifies Tim G upon case creation for customer with no CID.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OMM Help %28High%29 Case Taken</fullName>
        <actions>
            <name>Case_Taken_9</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Amy Pentzien,Benjamin Guéret,Isaac Chinitz,Makiko Muraoka,Michael Sykora,Miriam Nachum,Roy Chiu,Rob Simonoff</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>High</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress - Escalation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>greaterThan</operation>
            <value>60</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>lessThan</operation>
            <value>90.01</value>
        </criteriaItems>
        <description>** Needs modification ** Notifies Pod upon Escalated case being taken by OMM or SS. - (sends email from Case Owner rather than Co-Owner)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OMM Help %28Low%29 Case Taken</fullName>
        <actions>
            <name>Case_Taken_CS_Help_Low_2</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Amy Pentzien,Benjamin Guéret,Isaac Chinitz,Makiko Muraoka,Michael Sykora,Miriam Nachum,Roy Chiu,Rob Simonoff</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>Low</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress - Escalation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>greaterThan</operation>
            <value>240</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>lessThan</operation>
            <value>360</value>
        </criteriaItems>
        <description>** Needs modification ** Notifies Pod upon Escalated case being taken by OMM or SS. - (sends email from Case Owner rather than Co-Owner)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OMM Help %28Medium%29 Case Taken</fullName>
        <actions>
            <name>Case_Taken_CS_Help_Medium_2</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Amy Pentzien,Benjamin Guéret,Isaac Chinitz,Makiko Muraoka,Michael Sykora,Miriam Nachum,Roy Chiu,Rob Simonoff</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>Medium</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress - Escalation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>greaterThan</operation>
            <value>120</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>lessThan</operation>
            <value>180.01</value>
        </criteriaItems>
        <description>** Needs modification ** Notifies Pod upon Escalated case being taken by OMM or SS. - (sends email from Case Owner rather than Co-Owner)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Pick-up time definition</fullName>
        <actions>
            <name>SS_Pick_up_time_definition</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>contains</operation>
            <value>progress</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Queue%3A AMER PS Case</fullName>
        <actions>
            <name>ProServe_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Group__c</field>
            <operation>equals</operation>
            <value>Professional Services - AMER</value>
        </criteriaItems>
        <description>Assigns the &quot;Case Owner&quot; to be the queue for Professional Services - AMER</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Queue%3A APAC PS Case</fullName>
        <actions>
            <name>Case_Owner_APAC_PS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Group__c</field>
            <operation>equals</operation>
            <value>Professional Services - APAC</value>
        </criteriaItems>
        <description>Assigns the &quot;Case Owner&quot; to be the queue for Professional Services - APAC</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Queue%3A Analytics - AMER</fullName>
        <actions>
            <name>Analytics_AMER_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Group__c</field>
            <operation>equals</operation>
            <value>Analytics - AMER</value>
        </criteriaItems>
        <description>Assigns the &quot;Case Owner&quot; to be the queue for Analytics - AMER</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Queue%3A Analytics - APAC</fullName>
        <actions>
            <name>Field_Update_Analytics_APAC_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Group__c</field>
            <operation>equals</operation>
            <value>Analytics - APAC</value>
        </criteriaItems>
        <description>Assigns the &quot;Case Owner&quot; to be the queue for Analytics - APAC</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Queue%3A Analytics - EMEA</fullName>
        <actions>
            <name>Analytics_EMEA_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Group__c</field>
            <operation>equals</operation>
            <value>Analytics - EMEA</value>
        </criteriaItems>
        <description>Assigns the &quot;Case Owner&quot; to be the queue for Analytics - EMEA</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Queue%3A AppOps</fullName>
        <actions>
            <name>Queue_AppOps</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2) OR 3</booleanFilter>
        <criteriaItems>
            <field>Case.Group__c</field>
            <operation>equals</operation>
            <value>AppOps</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Enhanced Campaign Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>AppOps</value>
        </criteriaItems>
        <description>Assigned the AppOps - AMER queue as the case owner during case creation.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Queue%3A AppOps Hand-off</fullName>
        <actions>
            <name>Queue_AppOps</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Hand-off to AMER,Hand-off to APAC,Hand-off to EMEA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>AppOps</value>
        </criteriaItems>
        <description>When handing off a case within SS this rule assigns the Support Services Queue when changing the status to any of the &quot;Hand-off to...&quot; options under Status.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Queue%3A Client Services - Bidding</fullName>
        <actions>
            <name>Client_Services_Bidding</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Group__c</field>
            <operation>equals</operation>
            <value>Client Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Reason_Detail__c</field>
            <operation>equals</operation>
            <value>Bidding</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Client Services</value>
        </criteriaItems>
        <description>Assigns the &quot;Case Owner&quot; to be the queue for Client Services Bidding Queue

Assigned Worked for tracking issues, set-ups, and questions related to tracking.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Queue%3A Client Services - Tracking</fullName>
        <actions>
            <name>Client_Services_Tracking_Assignment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Group__c</field>
            <operation>equals</operation>
            <value>Client Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Reason_Detail__c</field>
            <operation>equals</operation>
            <value>Tracking</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Client Services</value>
        </criteriaItems>
        <description>Assigns the &quot;Case Owner&quot; to be the queue for Client Services Tracker Queue.... Until the queue is built, Scott will be the owner.

Assigned Worked for tracking issues, set-ups, and questions related to tracking.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Queue%3A EC SWAT</fullName>
        <actions>
            <name>Queue_EC_SWAT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Enhanced Campaign Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Group__c</field>
            <operation>equals</operation>
            <value>EC SWAT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Group__c</field>
            <operation>equals</operation>
            <value>Support Services</value>
        </criteriaItems>
        <description>Assigned the EC SWAT queue as the case owner.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Queue%3A EMEA PS Case</fullName>
        <actions>
            <name>ProServeEMEA_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR (2 AND 3)</booleanFilter>
        <criteriaItems>
            <field>Case.Group__c</field>
            <operation>equals</operation>
            <value>Professional Services - EMEA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Enhanced Campaign Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Group__c</field>
            <operation>equals</operation>
            <value>Professional Services - EMEA</value>
        </criteriaItems>
        <description>Assigns the &quot;Case Owner&quot; to be the queue for Professional Services - EMEA</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Queue%3A Marin Pro AM</fullName>
        <actions>
            <name>Queue_Marin_Pro_AM</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Support Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Product_Version__c</field>
            <operation>equals</operation>
            <value>Professional</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject__c</field>
            <operation>contains</operation>
            <value>Cancel</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Description</field>
            <operation>contains</operation>
            <value>Cancel</value>
        </criteriaItems>
        <description>This queue identifies which Pro Cancellation cases come in through the web2case form and puts them into a PRO queue.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Queue%3A PS Engineering</fullName>
        <actions>
            <name>PSEng_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR (2 AND 3)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>PS Engineering</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Enhanced Campaign Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Group__c</field>
            <operation>equals</operation>
            <value>PS Engineering</value>
        </criteriaItems>
        <description>Assigns the &quot;Case Owner&quot; to be the queue for PS Engineering</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Queue%3A Support Services Case</fullName>
        <actions>
            <name>SS_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>((1 and 2) OR 3) AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.Group__c</field>
            <operation>equals</operation>
            <value>Support Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Marin Support Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Marin Support Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Enhanced Campaign Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Reason_Detail__c</field>
            <operation>notEqual</operation>
            <value>Missing or Incorrect Universal Channels Revenue,Missing or Incorrect Universal Channels Cost,Universal Channels</value>
        </criteriaItems>
        <description>Assigns the &quot;Case Owner&quot; to be the queue for Support Services Queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Queue%3A Support Services Hand-off</fullName>
        <actions>
            <name>Queue_Assigned</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Hand-off to AMER,Hand-off to APAC,Hand-off to EMEA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Support Services</value>
        </criteriaItems>
        <description>When handing off a case within SS this rule assigns the Support Services Queue when changing the status to any of the &quot;Hand-off to...&quot; options under Status.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Queue%3A Universal Channels Support</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Support Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Reason_Detail__c</field>
            <operation>equals</operation>
            <value>Universal Channels Only: Missing or Incorrect Revenue,Universal Channels Only: Missing or Incorrect Cost</value>
        </criteriaItems>
        <description>This is a temporary workflow rule that separates universal channel Support cases out from the other cases.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Queue%3A eLearning Case</fullName>
        <actions>
            <name>eLearning_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Group__c</field>
            <operation>equals</operation>
            <value>eLearning</value>
        </criteriaItems>
        <description>Assigns the &quot;Case Owner&quot; to be the queue for eLearning</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Resolution time definition</fullName>
        <actions>
            <name>set_resolved_time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>contains</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>time when case set to a closed status</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SUPPORT SERVICES%3A email to case template trial</fullName>
        <actions>
            <name>SUPPORT_SERVICE_e2c_clientid_extract</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>equals</operation>
            <value>oclary@marinsoftware.com</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Satisfaction Survey</fullName>
        <active>false</active>
        <description>This workflow is designed to send a timba survey to every 50th or 100th case contact email.</description>
        <formula>if(and(contains( text(value(CaseNumber)  / 50),&quot;.&quot;),  ISPICKVAL( Status , &quot;Closed&quot;) ),true, false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Support Help %28High%29 Case Taken</fullName>
        <actions>
            <name>Case_Taken_3</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Tu (Mickey) Nguyen,Kamilla Khaydarov,Samuel Lee,Scott Liu</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>High</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress - Escalation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>greaterThan</operation>
            <value>60</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>lessThan</operation>
            <value>90.01</value>
        </criteriaItems>
        <description>** Needs modification ** Notifies Pod upon Escalated case being taken by OMM or CSR. - (sends email from Case Owner rather than Co-Owner)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Support Help %28Low%29 Case Taken</fullName>
        <actions>
            <name>Case_Taken_Support_Help_Low</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Tu (Mickey) Nguyen,Kamilla Khaydarov,Samuel Lee,Scott Liu</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>Low</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress - Escalation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>greaterThan</operation>
            <value>240</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>lessThan</operation>
            <value>360</value>
        </criteriaItems>
        <description>** Needs modification ** Notifies Pod upon Escalated case being taken by OMM or CSR. - (sends email from Case Owner rather than Co-Owner)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Support Help %28Medium%29 Case Taken</fullName>
        <actions>
            <name>Case_Taken_Support_Help_Medium</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Tu (Mickey) Nguyen,Kamilla Khaydarov,Samuel Lee,Scott Liu</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>Medium</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress - Escalation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>greaterThan</operation>
            <value>120</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>lessThan</operation>
            <value>180.01</value>
        </criteriaItems>
        <description>** Needs modification ** Notifies Pod upon Escalated case being taken by OMM or CSR. - (sends email from Case Owner rather than Co-Owner)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Tier 3 Case notification to Account Owner</fullName>
        <actions>
            <name>Email_Alert_for_Tier_3_cases_to_Account_Owners</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Account_Tier__c</field>
            <operation>equals</operation>
            <value>Tier 3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Tier_manual__c</field>
            <operation>equals</operation>
            <value>Advanced</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>UKPod1 %28High%29 Case Taken</fullName>
        <actions>
            <name>UKPod1_High</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>marc stefani,Marie Boivent,Anuj Sharma,Adam Hawkins</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>High</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress - Escalation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>greaterThan</operation>
            <value>60</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>lessThan</operation>
            <value>90.01</value>
        </criteriaItems>
        <description>-- not in use --</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UKPod1 %28Low%29 Case Taken</fullName>
        <actions>
            <name>UKPod1_Low</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Marie Boivent,Anuj Sharma,Adam Hawkins</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>Low</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress - Escalation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>greaterThan</operation>
            <value>240</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>lessThan</operation>
            <value>360</value>
        </criteriaItems>
        <description>-- not in use --</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UKPod1 %28Medium%29 Case Taken</fullName>
        <actions>
            <name>UKPod1_Medium</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Marie Boivent,Anuj Sharma,Adam Hawkins</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>Medium</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress - Escalation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>greaterThan</operation>
            <value>120</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>lessThan</operation>
            <value>180.01</value>
        </criteriaItems>
        <description>-- not in use --</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UKPod2 %28High%29 Case Taken</fullName>
        <actions>
            <name>UKPod2_High</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Josh Boxer,Eu Wen Teh,Rupesh Sharma</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>High</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress - Escalation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>greaterThan</operation>
            <value>60</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>lessThan</operation>
            <value>90.01</value>
        </criteriaItems>
        <description>-- not in use --</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UKPod2 %28Low%29 Case Taken</fullName>
        <actions>
            <name>UKPod2_Low</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Josh Boxer,Eu Wen Teh,Rupesh Sharma</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>Low</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress - Escalation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>greaterThan</operation>
            <value>240</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>lessThan</operation>
            <value>360</value>
        </criteriaItems>
        <description>-- not in use --</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UKPod2 %28Medium%29 Case Taken</fullName>
        <actions>
            <name>UKPod2_Medium</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Josh Boxer,Eu Wen Teh,Rupesh Sharma</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>Medium</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress - Escalation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>greaterThan</operation>
            <value>120</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Lapsed_Time__c</field>
            <operation>lessThan</operation>
            <value>180.01</value>
        </criteriaItems>
        <description>-- not in use --</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>US SS - Case Closed</fullName>
        <actions>
            <name>US_SS_Case_Close</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Co_Owner__c</field>
            <operation>equals</operation>
            <value>joel seaton,carren enriquez,david fishman,karl leong,chitwan kaur,perry nourani,jennifer cedarleaf,kelsey lam,Ivan Trachter,Zoey Glassberg,paul yen,Kazuki Horiguchi,Andy Bauer,Jacques Caspi</value>
        </criteriaItems>
        <description>** Needs modification ** Notifies Case OWner and Client user upon case closure by US SS.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>US SS - Case Taken - Customer</fullName>
        <actions>
            <name>US_SS_Case_taken_customer</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress,In Progress - Escalation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Co_Owner__c</field>
            <operation>equals</operation>
            <value>joel seaton,carren enriquez,david fishman,karl leong,chitwan kaur,perry nourani,jennifer cedarleaf,kelsey lam,Ivan Trachter,Zoey Glassberg,paul yen,Kazuki Horiguchi,Andy Bauer,Jacques Caspi</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Pick_up_time__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>**Needs modification -  merge with APAC/EMEA rule? ** Notifies client user (and CCs) upon case being taken by US SS team member. Email send from Co-owner</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>US SS - new case</fullName>
        <actions>
            <name>US_Support_new_case_notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>contains</operation>
            <value>New,Handoff to US SS</value>
        </criteriaItems>
        <description>Notifies US SS upon new case creation or Handoff to US SS or Onboarding (meeting defined criteria, e.g.: specific OMM, Customer/Account, Tier etc...)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>US SS Case Taken - CS Case Owner</fullName>
        <actions>
            <name>US_SS_Taken_Task</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress,In Progress - Escalation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Co_Owner__c</field>
            <operation>equals</operation>
            <value>joel seaton,carren enriquez,david fishman,karl leong,chitwan kaur,perry nourani,jennifer cedarleaf,kelsey lam,Ivan Trachter,Zoey Glassberg,paul yen,Kazuki Horiguchi,Andy Bauer,Jacques Caspi</value>
        </criteriaItems>
        <description>Notifies Case Owner upon case being taken by US SS.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>US SS Case Taken - Co-Owner notification</fullName>
        <actions>
            <name>Emails_co_owner_with_details_of_case</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Co_Owner__c</field>
            <operation>equals</operation>
            <value>joel seaton,carren enriquez,david fishman,karl leong,chitwan kaur,perry nourani,jennifer cedarleaf,kelsey lam,Ivan Trachter,Zoey Glassberg,paul yen,Kazuki Horiguchi,Andy Bauer,Jacques Caspi</value>
        </criteriaItems>
        <description>Notifies Case Co-Owner upon case being taken by US SS.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>US time triggered case assignemnt</fullName>
        <actions>
            <name>US_new_case_time_triggered_email_alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND ((value(MID (TEXT (CreatedDate), 12, 2))&gt;=16) , (value(MID (TEXT (CreatedDate), 12, 2)) &lt; 2))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>WorkIt%21 Enable Time Totals</fullName>
        <actions>
            <name>WorkIt_Set_Case_Allow_Update_to_on</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.WorkIt2__Allow_Update__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Allows for time totals to be calculated on the case record</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WorkIt2__WorkIt%21 Case Close</fullName>
        <actions>
            <name>WorkIt2__WorkIt_Set_Closed_with_Package_to_On</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WorkIt2__WorkIt_Set_Don_t_Time_to_On</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Makes WorkIt! stop timing a case when it&apos;s closed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WorkIt2__WorkIt%21 Case Open</fullName>
        <actions>
            <name>WorkIt2__WorkIt_Set_Don_t_Time_to_Off</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Makes WorkIt! start timing a case when it&apos;s opened.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>football_fanatics</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>escalates to GS-SS on fanatics issues</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>football_fanatics_rule</fullName>
        <actions>
            <name>football_fanatics</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>contains</operation>
            <value>fanatics</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>escalates to to GS-SS</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>re-open ticket on client resposne</fullName>
        <actions>
            <name>re_open_ticket_back_to_in_progress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Case_status__c</field>
            <operation>equals</operation>
            <value>Closed - Pending</value>
        </criteriaItems>
        <description>rule re-opens a ticket when the status of a ticket is in closed - pending and a client replies</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>test4</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Case_status__c</field>
            <operation>equals</operation>
            <value>test</value>
        </criteriaItems>
        <description>dsfds</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
