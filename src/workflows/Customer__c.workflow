<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>UK_Customer_Handover_PS_to_CS</fullName>
        <description>UK Customer Handover (PS to CS)</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_CSR__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CS_Templates/Customer_handover_PS_to_CS_template</template>
    </alerts>
    <alerts>
        <fullName>US_Customer_Handover_PS_to_CS</fullName>
        <description>US Customer Handover (PS to CS)</description>
        <protected>false</protected>
        <recipients>
            <recipient>benjamin.gueret@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Account_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>PM_setup__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Primary_CSR__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>SA_setup__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CS_Templates/Customer_handover_PS_to_CS_template</template>
    </alerts>
    <alerts>
        <fullName>US_Customer_Ready_for_QA_PS_to_CS</fullName>
        <description>US Customer Ready for QA (PS to CS)</description>
        <protected>false</protected>
        <recipients>
            <recipient>msykora@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>paitken@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rbryant@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Account_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>PM_setup__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Primary_CSR__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/US_Customer_Ready_for_QA_PS_to_CS_template</template>
    </alerts>
    <rules>
        <fullName>Customer Created</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Customer__c.CreatedDate</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Customer Record Created</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Customer__c.CreatedDate</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>UK Customer handover %28PS to CS%29</fullName>
        <actions>
            <name>UK_Customer_Handover_PS_to_CS</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Customer__c.On_boarding_complete__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Customer__c.Name</field>
            <operation>contains</operation>
            <value>(UK)</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>US Customer Ready for QA %28PS to CS%29</fullName>
        <actions>
            <name>US_Customer_Ready_for_QA_PS_to_CS</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Customer__c.Ready_for_QA__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Customer__c.Name</field>
            <operation>notContain</operation>
            <value>(UK)</value>
        </criteriaItems>
        <description>Alerts Sam Lee and Primary OMM when new customer is onboarded</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>US Customer handover %28PS to CS%29</fullName>
        <actions>
            <name>US_Customer_Handover_PS_to_CS</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Customer__c.On_boarding_complete__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Customer__c.Name</field>
            <operation>notContain</operation>
            <value>(UK)</value>
        </criteriaItems>
        <description>Alerts Sam Lee, the Primary OMM, the PM, the SA, and the AM when new customer is on-boarded</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
