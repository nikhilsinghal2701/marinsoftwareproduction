<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Dispo_Subject_Fill</fullName>
        <field>Subject</field>
        <formula>CallDisposition</formula>
        <name>Dispo Subject Fill</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EmailResponseTaskTypeEdit</fullName>
        <description>Set task type for Yesware events (message opened, link clicked)
Set task type for Marketo Sales Insight (opened sales email, clicked link in Sales Email)</description>
        <field>Type</field>
        <literalValue>Email response</literalValue>
        <name>EmailResponseTaskTypeEdit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EmailTaskTypeEdit</fullName>
        <description>Set task type for Yesware and Marketo Sales Insight emails</description>
        <field>Type</field>
        <literalValue>Email</literalValue>
        <name>EmailTaskTypeEdit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Bad Contact Update</fullName>
        <active>false</active>
        <formula>NOT(ISNULL(WhoId))  &amp;&amp;  ISPICKVAL(Call_Disposition__c, &apos;Positive&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Dispo Subject</fullName>
        <actions>
            <name>Dispo_Subject_Fill</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Refractive Dialer Automated Call</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EmailResponseTaskRules</fullName>
        <actions>
            <name>EmailResponseTaskTypeEdit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3 OR 4</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Message Opened</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Link Clicked in Message</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Opened Sales Email</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Clicked Link in Sales Email</value>
        </criteriaItems>
        <description>Workflow for Yesware-created response tasks (message opened, link clicked)
Workflow for Sales Insight - created response tasks (Opened Sales Email, Clicked Link in Sales Email)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EmailTaskRules</fullName>
        <actions>
            <name>EmailTaskTypeEdit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Message Sent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Reply</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Was Sent Sales Email</value>
        </criteriaItems>
        <description>Workflow for Yesware-created email tasks (message sent, reply received)
Workflow for Sales Insight-created email tasks (Was Sent Sales Email)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Task Closed - Task Created</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
