<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Alert_Implementation_Change_Notification</fullName>
        <description>Alert: Implementation Change Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Templates/Alert_Implementation_Change_Notice</template>
    </alerts>
    <alerts>
        <fullName>Approval_Request_Email</fullName>
        <description>Approval Request Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>fkristyati@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>radams@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Project_Requester__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Professional_Services/Integration_Finance_Approval_v2</template>
    </alerts>
    <alerts>
        <fullName>Approval_Request_Submitted_Email</fullName>
        <description>Approval Request Submitted Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>brian.riordan@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jbarron@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kristina.williams@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>msykora@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Professional_Services/Approval_Request_Submitted</template>
    </alerts>
    <alerts>
        <fullName>Approval_Request_Technical_Approval_Email</fullName>
        <description>Approval Request Technical Approval Email</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_Solution_Architect__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Project_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Project_Requester__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Professional_Services/Approval_Request_Technical_Approval</template>
    </alerts>
    <alerts>
        <fullName>Approval_Response_Email</fullName>
        <description>Approval Response Email</description>
        <protected>false</protected>
        <recipients>
            <field>Advisory_Solution_Architect__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Backup_Project_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Primary_Solution_Architect__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Project_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Project_Requester__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Professional_Services/Integration_Finance_Approval_Response_v2</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_Exception_Request_Approval</fullName>
        <description>Email Alert: Exception Request Approval</description>
        <protected>false</protected>
        <recipients>
            <recipient>brian.riordan@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jbarron@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kelli.scott@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kristina.williams@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>msykora@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Professional_Services/Exception_Request_Alert</template>
    </alerts>
    <alerts>
        <fullName>Implementation_Record_Has_Been_Created</fullName>
        <description>Implementation Record Has Been Created</description>
        <protected>false</protected>
        <recipients>
            <recipient>clund@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>skornreich@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>wister@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Ops/Implementation_Record_Has_Been_Created</template>
    </alerts>
    <alerts>
        <fullName>Integration_Not_Approved_Finance</fullName>
        <description>Integration Not Approved Finance</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_Solution_Architect__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Project_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Project_Requester__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Professional_Services/Integration_Finance_Not_Approval_Response</template>
    </alerts>
    <alerts>
        <fullName>Low_Integration_Satisfaction_Score_AMER</fullName>
        <description>Low Integration Satisfaction Score - AMER</description>
        <protected>false</protected>
        <recipients>
            <recipient>brian.riordan@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>crmsupport@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jbarron@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kelli.scott@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>msykora@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>paitken@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Primary_Solution_Architect__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Project_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Professional_Services/Integration_Survey_Low_Score_Response</template>
    </alerts>
    <alerts>
        <fullName>Low_Integration_Satisfaction_Score_APAC</fullName>
        <description>Low Integration Satisfaction Score - APAC</description>
        <protected>false</protected>
        <recipients>
            <recipient>bmarin@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tsakamoto@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Professional_Services/Integration_Survey_Low_Score_Response</template>
    </alerts>
    <alerts>
        <fullName>Low_Integration_Satisfaction_Score_EMEA</fullName>
        <description>Low Integration Satisfaction Score - EMEA</description>
        <protected>false</protected>
        <recipients>
            <recipient>bmarin@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kelly.menir@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>richard.maylor@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Professional_Services/Integration_Survey_Low_Score_Response</template>
    </alerts>
    <alerts>
        <fullName>Re_Send_Integration_Satisfaction_Survey_English</fullName>
        <description>Re-Send Integration Satisfaction Survey - English</description>
        <protected>false</protected>
        <recipients>
            <field>Integration_POC__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Professional_Services/Integration_Survey_English_v2</template>
    </alerts>
    <alerts>
        <fullName>Send_Scoping_to_Customer</fullName>
        <description>Send Scoping to Customer</description>
        <protected>false</protected>
        <recipients>
            <field>Integration_POC__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Project_Requester__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Professional_Services/Scoping_Document_Customer_Emailv2</template>
    </alerts>
    <alerts>
        <fullName>Send_Scoping_to_Marin_Staff</fullName>
        <description>Send Scoping to Marin Staff</description>
        <protected>false</protected>
        <recipients>
            <field>Project_Requester__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Professional_Services/Scoping_Document_Marin_Staff_Email</template>
    </alerts>
    <alerts>
        <fullName>Trigger_Integration_Satisfaction_Survey_English</fullName>
        <description>Trigger Integration Satisfaction Survey - English</description>
        <protected>false</protected>
        <recipients>
            <field>Integration_POC__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Professional_Services/Integration_Survey_English_v2</template>
    </alerts>
    <alerts>
        <fullName>Trigger_Integration_Satisfaction_Survey_French</fullName>
        <description>Trigger Integration Satisfaction Survey - French</description>
        <protected>false</protected>
        <recipients>
            <field>Integration_POC__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Professional_Services/Integration_Survey_French</template>
    </alerts>
    <alerts>
        <fullName>Trigger_Integration_Satisfaction_Survey_German</fullName>
        <description>Trigger Integration Satisfaction Survey - German</description>
        <protected>false</protected>
        <recipients>
            <field>Integration_POC__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Professional_Services/Integration_Survey_German</template>
    </alerts>
    <alerts>
        <fullName>Trigger_Integration_Satisfaction_Survey_Japanese</fullName>
        <description>Trigger Integration Satisfaction Survey - Japanese</description>
        <protected>false</protected>
        <recipients>
            <field>Integration_POC__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Professional_Services/Integration_Survey_Japanese</template>
    </alerts>
    <fieldUpdates>
        <fullName>Field_Update_Exception_Request</fullName>
        <field>Integration_Category__c</field>
        <literalValue>Exception Request</literalValue>
        <name>Field Update: Exception Request</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Integration_Date_Created_Today</fullName>
        <description>Places &quot;Todays&quot; date as the value for the Integration Date Created</description>
        <field>Project_Start_Date__c</field>
        <formula>TODAY()</formula>
        <name>Integration Date Created Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PS_Project_Name</fullName>
        <description>Auto Fills the Project Name based on the Opportunity name or the Case Record subject.</description>
        <field>Project_Name__c</field>
        <formula>if(primary_Opportunity__c &lt;&gt; null, primary_Opportunity__r.Name , 
if(Primary_Case__c &lt;&gt; null,  Primary_Case__r.Subject , null))</formula>
        <name>PS Project Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Requester_Organization</fullName>
        <description>Workflow Displays the Organization that the Requestor is in.</description>
        <field>Requester_Organization__c</field>
        <formula>Project_Requester__r.Department</formula>
        <name>Requester Organization</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Requester_Region</fullName>
        <description>Workflow to identify the region of the requestor based off the requestor data.</description>
        <field>Requester_Region__c</field>
        <formula>IF(Project_Requester__r.Country = &quot;USA&quot;, &quot;AMER&quot;, 
IF(Project_Requester__r.Country = &quot;United States&quot;, &quot;AMER&quot;,
IF(Project_Requester__r.Country = &quot;AU&quot;, &quot;APAC&quot;,
IF(Project_Requester__r.Country = &quot;Australia&quot;, &quot;APAC&quot;,
IF(Project_Requester__r.Country = &quot;France&quot;, &quot;EMEA&quot;,
IF(Project_Requester__r.Country = &quot;Germany&quot;, &quot;EMEA&quot;,
IF(Project_Requester__r.Country = &quot;Japan&quot;, &quot;APAC&quot;,
IF(Project_Requester__r.Country = &quot;Singapore&quot;, &quot;APAC&quot;,
IF(Project_Requester__r.Country = &quot;UK&quot;, &quot;EMEA&quot;,
IF(Project_Requester__r.Country = &quot;United Kingdom&quot;,&quot;EMEA&quot;, null))))))))))</formula>
        <name>Requester Region</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Alert%3A Implementation change notification</fullName>
        <actions>
            <name>Alert_Implementation_Change_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>To notify Requestor (Creator) of change on Implementation record</description>
        <formula>LastModifiedById &lt;&gt; CreatedById</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email Alert%3A Exception Approval Needed</fullName>
        <actions>
            <name>Email_Alert_Exception_Request_Approval</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PS_Project_Record__c.Integration_Category__c</field>
            <operation>equals</operation>
            <value>Exception Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>PS_Project_Record__c.Project_Stage__c</field>
            <operation>equals</operation>
            <value>Request,Presales</value>
        </criteriaItems>
        <criteriaItems>
            <field>PS_Project_Record__c.onboarding_Location__c</field>
            <operation>equals</operation>
            <value>North America - West,North America - Central,North America - East</value>
        </criteriaItems>
        <criteriaItems>
            <field>PS_Project_Record__c.Exception_Approved__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Alert%3A Integration Complete</fullName>
        <active>false</active>
        <criteriaItems>
            <field>PS_Project_Record__c.Complete_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>PS_Project_Record__c.Project_Stage__c</field>
            <operation>equals</operation>
            <value>Completed,Cancelled</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Alert%3A Low Survey Score - AMER</fullName>
        <actions>
            <name>Low_Integration_Satisfaction_Score_AMER</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3) AND 4</booleanFilter>
        <criteriaItems>
            <field>PS_Project_Record__c.On_Boarding_Region__c</field>
            <operation>equals</operation>
            <value>AMER</value>
        </criteriaItems>
        <criteriaItems>
            <field>PS_Project_Record__c.Survey_Result_Satisfaction_Rating__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>PS_Project_Record__c.Survey_Result_Satisfaction_Rating__c</field>
            <operation>equals</operation>
            <value>2</value>
        </criteriaItems>
        <criteriaItems>
            <field>PS_Project_Record__c.Project_Stage__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <description>This alert notifies the managers of the AMER region whenever a low survey score is returned. A low score is 1 or 2.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Alert%3A Low Survey Score - APAC</fullName>
        <actions>
            <name>Low_Integration_Satisfaction_Score_APAC</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>PS_Project_Record__c.On_Boarding_Region__c</field>
            <operation>equals</operation>
            <value>APAC</value>
        </criteriaItems>
        <criteriaItems>
            <field>PS_Project_Record__c.Project_Stage__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>PS_Project_Record__c.Survey_Result_Satisfaction_Rating__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>PS_Project_Record__c.Survey_Result_Integration_Improvements__c</field>
            <operation>equals</operation>
            <value>2</value>
        </criteriaItems>
        <description>This alert notifies the managers of the EMEA region whenever a low survey score is returned. A low score is 1 or 2.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Alert%3A Low Survey Score - EMEA</fullName>
        <actions>
            <name>Low_Integration_Satisfaction_Score_EMEA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>PS_Project_Record__c.On_Boarding_Region__c</field>
            <operation>equals</operation>
            <value>EMEA</value>
        </criteriaItems>
        <criteriaItems>
            <field>PS_Project_Record__c.Project_Stage__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>PS_Project_Record__c.Survey_Result_Satisfaction_Rating__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>PS_Project_Record__c.Survey_Result_Integration_Improvements__c</field>
            <operation>equals</operation>
            <value>2</value>
        </criteriaItems>
        <description>This alert notifies the managers of the EMEA region whenever a low survey score is returned. A low score is 1 or 2.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Approval Request Submitted</fullName>
        <actions>
            <name>Approval_Request_Submitted_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PS_Project_Record__c.Finance_Approval__c</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Approval Request Technical Approval</fullName>
        <actions>
            <name>Approval_Request_Technical_Approval_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PS_Project_Record__c.Finance_Approval__c</field>
            <operation>equals</operation>
            <value>Technical Committee Approval</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Finance Approval Needed</fullName>
        <actions>
            <name>Approval_Request_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PS_Project_Record__c.Finance_Approval__c</field>
            <operation>equals</operation>
            <value>Finance Review</value>
        </criteriaItems>
        <description>This work flow triggers an email to the finance team so they can review the contract for the client and confirm that the work is being paid for by the client.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Finance Approval Response</fullName>
        <actions>
            <name>Approval_Response_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PS_Project_Record__c.Finance_Approval__c</field>
            <operation>equals</operation>
            <value>Finance Approved</value>
        </criteriaItems>
        <description>Workflow triggers the emails the finance response to the request for approval by the Finance team.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Finance Not Approved Response</fullName>
        <actions>
            <name>Integration_Not_Approved_Finance</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PS_Project_Record__c.Finance_Approval__c</field>
            <operation>equals</operation>
            <value>Finance Not Approved</value>
        </criteriaItems>
        <description>Workflow triggers the emails the finance response to the request for approval by the Finance team.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Integration Survey - English</fullName>
        <active>true</active>
        <description>This workflow rule sends out the Integration Survey to the customer in English at the time of the completed project</description>
        <formula>and(
ISPICKVAL(Project_Classification__c , &quot;New Customer - New Onboarding (NCNO)&quot;),
ISPICKVAL( Project_Stage__c , &quot;Completed&quot;),
Integration_POC__c &lt;&gt; NULL,

OR(
ISPICKVAL(Customer_Language__c , &quot;English&quot;),
ISPICKVAL(Customer_Language__c , &apos;&apos;)
)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Trigger_Integration_Satisfaction_Survey_English</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>PS_Project_Record__c.Complete_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Email%3A Integration Survey - French</fullName>
        <active>true</active>
        <description>This workflow rule sends out the Integration Survey to the customer in French at the time of the completed project</description>
        <formula>and(
ISPICKVAL(Project_Classification__c , &quot;New Customer - New Onboarding (NCNO)&quot;),
ISPICKVAL( Project_Stage__c , &quot;Completed&quot;),
Integration_POC__c &lt;&gt; NULL,

ISPICKVAL(Customer_Language__c , &quot;French&quot;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Trigger_Integration_Satisfaction_Survey_French</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>PS_Project_Record__c.Complete_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Email%3A Integration Survey - German</fullName>
        <active>true</active>
        <description>This workflow rule sends out the Integration Survey to the customer in German at the time of the completed project</description>
        <formula>and(
ISPICKVAL(Project_Classification__c , &quot;New Customer - New Onboarding (NCNO)&quot;),
ISPICKVAL( Project_Stage__c , &quot;Completed&quot;),
Integration_POC__c &lt;&gt; NULL,

ISPICKVAL(Customer_Language__c , &quot;German&quot;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Trigger_Integration_Satisfaction_Survey_German</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>PS_Project_Record__c.Complete_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Email%3A Integration Survey - Japanese</fullName>
        <active>true</active>
        <description>This workflow rule sends out the Integration Survey to the customer in Japanese at the time of the completed project</description>
        <formula>and(
ISPICKVAL(Project_Classification__c , &quot;New Customer - New Onboarding (NCNO)&quot;),
ISPICKVAL( Project_Stage__c , &quot;Completed&quot;),
Integration_POC__c &lt;&gt; NULL,

ISPICKVAL(Customer_Language__c , &quot;Japanese&quot;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Trigger_Integration_Satisfaction_Survey_Japanese</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>PS_Project_Record__c.Complete_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Email%3A Re-Trigger Integration Survey - English</fullName>
        <actions>
            <name>Re_Send_Integration_Satisfaction_Survey_English</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>and( 
ISPICKVAL(Project_Classification__c , &quot;New Customer - New Onboarding (NCNO)&quot;), 
ISPICKVAL( Project_Stage__c , &quot;Completed&quot;), 
Integration_POC__c &lt;&gt; NULL,
Trigger_Survey_Email__c = TRUE)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Send Scoping Document to Customer</fullName>
        <actions>
            <name>Send_Scoping_to_Customer</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PS_Project_Record__c.Scoping_Status__c</field>
            <operation>equals</operation>
            <value>Sent to Customer Contact</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Send Scoping Document to Marin Staff</fullName>
        <actions>
            <name>Send_Scoping_to_Marin_Staff</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PS_Project_Record__c.Scoping_Status__c</field>
            <operation>equals</operation>
            <value>Sent to Marin Staff</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Integration Date Created</fullName>
        <actions>
            <name>Integration_Date_Created_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>PS_Project_Record__c.Project_Start_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>PS_Project_Record__c.Project_Stage__c</field>
            <operation>equals</operation>
            <value>Request,Presales,Active,On-Hold,Completed,Cancelled,Withdrawn</value>
        </criteriaItems>
        <description>This workflow updates the Integration Date Created with Today&apos;s date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Marin Labs Exception</fullName>
        <actions>
            <name>Field_Update_Exception_Request</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PS_Project_Record__c.Project_Type__c</field>
            <operation>equals</operation>
            <value>Marin Labs</value>
        </criteriaItems>
        <description>This rule will default Integration Category to &apos;Exception Request&apos;.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Implementation Change%3A Brandon Lau</fullName>
        <active>false</active>
        <criteriaItems>
            <field>PS_Project_Record__c.OwnerId</field>
            <operation>equals</operation>
            <value>brandon lau</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New Implementation Created</fullName>
        <actions>
            <name>Implementation_Record_Has_Been_Created</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>PS_Project_Record__c.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PS Project - Requester Data</fullName>
        <actions>
            <name>Requester_Organization</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Requester_Region</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow that assigned the region and organization of the requestor based on the user profile within SFDC.</description>
        <formula>Project_Requester__c &lt;&gt; null || ischanged(Project_Requester__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
