<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Owner_Office</fullName>
        <field>Owner_Office__c</field>
        <formula>TEXT( Owner:User.User_Office__c )</formula>
        <name>Update Owner Office</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_Region</fullName>
        <field>Owner_Region__c</field>
        <formula>TEXT( Owner:User.User_Region__c )</formula>
        <name>Update Owner Region</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_Team</fullName>
        <description>Update Owner Team Field</description>
        <field>Owner_Team__c</field>
        <formula>TEXT(Owner:User.User_Team__c)</formula>
        <name>Update Owner Team</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Owner Team%2C Region%2C Office Stamp</fullName>
        <actions>
            <name>Update_Owner_Office</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Owner_Region</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Owner_Team</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Created 2014-05-19 per Jake, Christine to stamp attainments with values from the owner&apos;s user record.</description>
        <formula>OR(
ISNEW(), 
ISCHANGED( OwnerId), 
ISBLANK( Owner_Region__c),
ISBLANK( Owner_Office__c), 
ISBLANK( Owner_Team__c )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
