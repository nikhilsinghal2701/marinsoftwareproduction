<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Bad_Contact_Update</fullName>
        <field>DoNotCall</field>
        <literalValue>1</literalValue>
        <name>Bad Contact Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Fill_Data_com_Key</fullName>
        <field>Jigsaw</field>
        <formula>jigsaw_clean__Jigsaw_Id__c</formula>
        <name>Contact- Fill Data.com Key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EA_NLT_True_Update</fullName>
        <description>Marks the Checkbox for No Longer w/ co. as TRUE when DQed for this reason.</description>
        <field>No_Longer_w_co__c</field>
        <literalValue>1</literalValue>
        <name>EA_NLT_True_Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_MQL_Reached_Date_Field_Contact</fullName>
        <field>MQL_Reached_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set MQL Reached Date Field (Contact)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Disqualified_Date_Today</fullName>
        <field>Disqualified_Date__c</field>
        <formula>Today()</formula>
        <name>Update Disqualified Date - Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_MQL_Reached_Date_Last</fullName>
        <field>MQL_Reached_Date_Last__c</field>
        <formula>TODAY()</formula>
        <name>Update MQL Reached Date Last</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Contact %E2%80%93 Fill Data%2Ecom  Id</fullName>
        <actions>
            <name>Contact_Fill_Data_com_Key</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Jigsaw</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.jigsaw_clean__Jigsaw_Id__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EA_NLT_Checkbox</fullName>
        <actions>
            <name>EA_NLT_True_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>Contact.FirstName</field>
            <operation>contains</operation>
            <value>NLT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.LastName</field>
            <operation>contains</operation>
            <value>NLT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Unqualified_Reasons__c</field>
            <operation>includes</operation>
            <value>No longer w/ co.</value>
        </criteriaItems>
        <description>Update the NLT Checkbox on the Contact object when DQed for this reason.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Disqualified Date</fullName>
        <actions>
            <name>Update_Disqualified_Date_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Unqualified_Reasons__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Updates the disqualified date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set MQL Last Reached Date %28Contact%29</fullName>
        <actions>
            <name>Update_MQL_Reached_Date_Last</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Marketing_Qualified_Reason__c</field>
            <operation>equals</operation>
            <value>Target Profile,Engaged,Engaged Target Profile</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Email</field>
            <operation>notEqual</operation>
            <value>dkim@marinsoftware.com</value>
        </criteriaItems>
        <description>Sets the MQL Last Rached Date field based on the last time a contact/lead reached MQL</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set MQL Reached Date %28Contact%29</fullName>
        <actions>
            <name>Set_MQL_Reached_Date_Field_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Set MQL Reached Date field when a contact. first reaches an A or B grade.</description>
        <formula>AND(ISBLANK(MQL_Reached_Date__c), OR(UPPER(Lead_Grade__c) = &apos;A&apos;, UPPER(Lead_Grade__c) = &apos;B&apos;), UPPER(PRIORVALUE(Lead_Grade__c)) != &apos;A&apos;, UPPER(PRIORVALUE(Lead_Grade__c)) != &apos;B&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set MQL Reached Date %28Contact%29 v2</fullName>
        <actions>
            <name>Set_MQL_Reached_Date_Field_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set MQL Reached Date field when a contact is marketing qualified as engaged or target profile</description>
        <formula>AND(ISCHANGED(Marketing_Qualified__c),Marketing_Qualified__c, ISBLANK( MQL_Reached_Date__c ), $User.Email &lt;&gt; &apos;dkim@marinsoftware.com&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
