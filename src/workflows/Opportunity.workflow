<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AlertSalesOpsNegAmountForecastnotOmitted</fullName>
        <ccEmails>salesops@marinsoftware.com</ccEmails>
        <description>AlertSalesOpsNegAmountForecastnotOmitted</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Sales_Ops/AlertSalesOpsNegativeOpp</template>
    </alerts>
    <alerts>
        <fullName>AlertSalesOpsOppOpenedfromClosedWon</fullName>
        <ccEmails>salesops@marinsoftware.com</ccEmails>
        <description>AlertSalesOpsOppOpenedfromClosedWon</description>
        <protected>false</protected>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Sales_Ops/AlertSalesOpsOppOpenedfromCW</template>
    </alerts>
    <alerts>
        <fullName>Alert_to_Bobby_of_Opps_going_from_0_to_5_NA_EMEA</fullName>
        <description>Alert to Bobby of Opps going from 0% to 5% NA_EMEA</description>
        <protected>false</protected>
        <recipients>
            <recipient>bhollis@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>eafable@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Marketing_Operations/Notification_Opportunitity_CL_to_LP</template>
    </alerts>
    <alerts>
        <fullName>Alert_to_Mike_Grossman_of_Opps_going_from_0_to_5</fullName>
        <description>Alert to Mike Grossman of Opps going from 0% to 5%</description>
        <protected>false</protected>
        <recipients>
            <recipient>dbrady@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>eafable@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mei.lei@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>phuston@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Marketing_Operations/Notification_Opportunitity_CL_to_LP</template>
    </alerts>
    <alerts>
        <fullName>Close_Date_Change_notification</fullName>
        <description>Close Date Change notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Close_Date_Change_notification</template>
    </alerts>
    <alerts>
        <fullName>EA_Alert_Bobby_1mil</fullName>
        <description>EA_Alert_Bobby_1mil</description>
        <protected>false</protected>
        <recipients>
            <recipient>azakem@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>bhollis@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>eafable@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Marketing_Operations/EA_Bobby_1mil_Agency_Notification</template>
    </alerts>
    <alerts>
        <fullName>EA_PG_Research_Opp_Notification</fullName>
        <description>EA_PG_Research_Opp_Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>eafable@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>paul.greer@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Marketing_Operations/EA_PG_Notification_Opp_Create_5</template>
    </alerts>
    <alerts>
        <fullName>EA_Phil_Team_Alert</fullName>
        <ccEmails>marketing-operations@marinsoftware.com</ccEmails>
        <description>EA_Phil_Team_Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>dbrady@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ppillsbury@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Marketing_Operations/EA_Phil_Team_Alerts</template>
    </alerts>
    <alerts>
        <fullName>EMEA_Inside_Sales_Alert_Manager_of_New_Opp</fullName>
        <ccEmails>ddesmond@marinsoftware.com</ccEmails>
        <description>EMEA Inside Sales Alert Manager of New Opp</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Marketing_Operations/EA_Notification_Opp_Create_5</template>
    </alerts>
    <alerts>
        <fullName>EMEA_PC</fullName>
        <description>EMEA PC Opportunity Close Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Product_Consultant__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Closed_UK_Email</template>
    </alerts>
    <alerts>
        <fullName>Email_Finance_about_Credit_Check</fullName>
        <ccEmails>credit@marinsoftware.com</ccEmails>
        <description>Email Finance about Credit Check</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Finance_Opp_at_60_Credit_Check</template>
    </alerts>
    <alerts>
        <fullName>Email_to_Bobby_Hollis_NA_APAC</fullName>
        <description>Email_to_Bobby_Hollis_NA_APAC</description>
        <protected>false</protected>
        <recipients>
            <recipient>bhollis@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>eafable@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tthompson@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Marketing_Operations/EA_Notification_Opp_Create_5</template>
    </alerts>
    <alerts>
        <fullName>Email_to_Bobby_Hollis_Pearl_Agency</fullName>
        <description>Email_to_Bobby_Hollis_Pearl_Agency</description>
        <protected>false</protected>
        <recipients>
            <recipient>bhollis@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>eafable@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>estevenson@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Marketing_Operations/EA_Notification_Opp_Create_5</template>
    </alerts>
    <alerts>
        <fullName>Email_to_Mike_Grossman</fullName>
        <description>Email_to_Mike_Grossman</description>
        <protected>false</protected>
        <recipients>
            <recipient>dbrady@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>eafable@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mei.lei@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>phuston@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Marketing_Operations/EA_Notification_Opp_Create_5</template>
    </alerts>
    <alerts>
        <fullName>MPro_Opp_moved_from_Won_to_Lost</fullName>
        <description>Opp moved from Won to Lost</description>
        <protected>false</protected>
        <recipients>
            <recipient>claszlo@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kbashaw@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/Marin_Pro_Won_to_Lost_Notice</template>
    </alerts>
    <alerts>
        <fullName>MPro_Trial_Ended</fullName>
        <description>MPro Trial Ended</description>
        <protected>false</protected>
        <recipients>
            <recipient>claszlo@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/Marin_Pro_Trial_has_ended</template>
    </alerts>
    <alerts>
        <fullName>MPro_Trial_Ending_in_2_Days</fullName>
        <description>MPro Trial Ending in 2 Days</description>
        <protected>false</protected>
        <recipients>
            <recipient>claszlo@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/MPro_Trial_Ending</template>
    </alerts>
    <alerts>
        <fullName>MPro_Trial_Ending_in_2_Days2</fullName>
        <description>MPro Trial Ending in 2 Days</description>
        <protected>false</protected>
        <recipients>
            <recipient>claszlo@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/MPro_Trial_Ending</template>
    </alerts>
    <alerts>
        <fullName>Marin_Pro_Cancellation</fullName>
        <description>Marin Pro Cancellation</description>
        <protected>false</protected>
        <recipients>
            <recipient>claszlo@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/Marin_Pro_Cancellation_Notice</template>
    </alerts>
    <alerts>
        <fullName>New_Customer_Account_Opportunity_Email_to_Sales_Owner</fullName>
        <description>New Customer Account Opportunity Email to Sales Owner</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Customer_Engagement_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>LeadGen_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Sales_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_Opportunity_creation_Notification</template>
    </alerts>
    <alerts>
        <fullName>OpportunityApproved</fullName>
        <description>OpportunityApproved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/OpportunityApprovalAccepted</template>
    </alerts>
    <alerts>
        <fullName>OpportunityRejected</fullName>
        <description>OpportunityRejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/OpportunityApprovalRejected</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Autoclose_Warning_10_Days</fullName>
        <description>Opportunity Autoclose Warning 10 Days</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Autoclose_Warning</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Autoclose_Warning_3_Days</fullName>
        <description>Opportunity Autoclose Warning 3 Days</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Autoclose_Warning1</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Closed_Lost_UK</fullName>
        <description>Opportunity Closed Lost (UK)</description>
        <protected>false</protected>
        <recipients>
            <field>Product_Consultant__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Record_Templates/Opportunity_Closed_Lost_UK_Email</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Closed_UK</fullName>
        <description>Opportunity Closed (UK)</description>
        <protected>false</protected>
        <recipients>
            <recipient>Opportunity_CloseUK</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Closed_UK_Email</template>
    </alerts>
    <alerts>
        <fullName>PC_Notification_for_Closed_Won_Opp</fullName>
        <description>PC Notification for Closed Won Opp</description>
        <protected>false</protected>
        <recipients>
            <field>Product_Consultant__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/PC_Notification_of_Closed_Won_Opp</template>
    </alerts>
    <alerts>
        <fullName>PS_Resources</fullName>
        <description>PS Resources</description>
        <protected>false</protected>
        <recipients>
            <recipient>adam.wise@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>crmsupport@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jmcgonagle@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kelli.scott@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>paitken@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>skornreich@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>terrell.holmes@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/PS_Resource</template>
    </alerts>
    <alerts>
        <fullName>Product_Version_Change</fullName>
        <description>Product Version Change</description>
        <protected>false</protected>
        <recipients>
            <recipient>claszlo@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/Product_Version_Change</template>
    </alerts>
    <alerts>
        <fullName>Send_Scoping_to_Customer</fullName>
        <description>Send Scoping to Customer</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Product_Consultant__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Professional_Services/Scoping_Document_Customer_Emailv2</template>
    </alerts>
    <alerts>
        <fullName>Send_Scoping_to_Product_Consultant</fullName>
        <description>Send Scoping to Product Consultant</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Product_Consultant__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Professional_Services/Scoping_Document_Marin_Staff_Email</template>
    </alerts>
    <alerts>
        <fullName>Sends_an_Email_to_Sales_Ops_when_a_Churn_Opp_is_Created_During_Flex_Term</fullName>
        <ccEmails>salesops@marinsoftware.com</ccEmails>
        <description>Sends an Email to Sales Ops when a Churn Opp is Created During Flex Term</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ChurnDuringFlex</template>
    </alerts>
    <alerts>
        <fullName>Strategic_Account_Opportunity_Created_Edited</fullName>
        <description>Strategic Account Opportunity Created/Edited</description>
        <protected>false</protected>
        <recipients>
            <recipient>azakem@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jtoomey@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mcook@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mtucker@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rwirth@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Ops/Deal_just_went_sideways</template>
    </alerts>
    <alerts>
        <fullName>Trial_Ending_Notification</fullName>
        <description>Trial Ending Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>claszlo@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tom.garland@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/MPro_Trial_Ending</template>
    </alerts>
    <alerts>
        <fullName>Trial_Ending_Notification1</fullName>
        <description>Trial Ending Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>claszlo@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tom.garland@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/MPro_Trial_Ending</template>
    </alerts>
    <alerts>
        <fullName>X95_Opp_Alert</fullName>
        <description>95% Opp Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>ahughes@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>phuston@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_95_and_over</template>
    </alerts>
    <alerts>
        <fullName>email_alert</fullName>
        <description>email alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Close_Date_Moved</template>
    </alerts>
    <fieldUpdates>
        <fullName>AM_Closed_Won_Billing_Start_Date</fullName>
        <field>Billing_Start_Date__c</field>
        <formula>TODAY()</formula>
        <name>AM Closed Won Billing Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AM_Closed_Won_Customer_Status_Update</fullName>
        <field>Customer_Status__c</field>
        <literalValue>Customer</literalValue>
        <name>AM Closed Won Customer Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AM_Closed_Won_Procurement_Update</fullName>
        <field>Procurement_Process__c</field>
        <literalValue>Add On - No Procurement</literalValue>
        <name>AM Closed Won Procurement Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Auto_Close_TRUE_25</fullName>
        <field>Auto_Closed__c</field>
        <literalValue>1</literalValue>
        <name>Auto Close - TRUE 25%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Auto_Close_TRUE_60</fullName>
        <field>Auto_Closed__c</field>
        <literalValue>1</literalValue>
        <name>Auto Close - TRUE 60%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Auto_Close_TRUE_75</fullName>
        <field>Auto_Closed__c</field>
        <literalValue>1</literalValue>
        <name>Auto Close - TRUE 75%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Auto_Close_TRUE_90</fullName>
        <field>Auto_Closed__c</field>
        <literalValue>1</literalValue>
        <name>Auto Close - TRUE 90%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Record_Owner_to_Phil</fullName>
        <field>OwnerId</field>
        <lookupValue>ppillsbury@marinsoftware.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Change Record Owner to Phil Pillsbury</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Stage_To_Lead_Passed</fullName>
        <field>StageName</field>
        <literalValue>Lead Passed</literalValue>
        <name>Change Stage To Lead Passed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Closed_Date_Today</fullName>
        <field>CloseDate</field>
        <formula>today()</formula>
        <name>Closed Date Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Customer_Engagement_Manager_Email</fullName>
        <field>Customer_Engagement_Manager_Email__c</field>
        <formula>Account.Customer_Engagement_Manager__r.Email</formula>
        <name>Copy Customer Engagement Manager Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_LeadGen_Owner_Email_from_Accounts</fullName>
        <field>LeadGen_Owner_Email__c</field>
        <formula>Account.LeadGen_Owner__r.Email</formula>
        <name>Copy LeadGen Owner Email from Accounts</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Sales_Owner_Email_from_Accounts</fullName>
        <field>Sales_Owner_Email__c</field>
        <formula>Account.Sales_Owner__r.Email</formula>
        <name>Copy Sales Owner Email from Accounts</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CurrentContractEndDateUpdate</fullName>
        <field>Current_Contract_End_Date__c</field>
        <formula>DATE(YEAR(Contract_Record__r.EndDate), MONTH(Contract_Record__r.EndDate), DAY(Contract_Record__r.EndDate))</formula>
        <name>CurrentContractEndDateUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_Closed_Lost_25</fullName>
        <description>Turns the Opportunity Stage to closed lost</description>
        <field>StageName</field>
        <literalValue>Closed Lost</literalValue>
        <name>Field Update: Closed Lost 25%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_Closed_Lost_60</fullName>
        <description>Turns the Opportunity Stage to closed lost</description>
        <field>StageName</field>
        <literalValue>Closed Lost</literalValue>
        <name>Field Update: Closed Lost 60%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_Closed_Lost_75</fullName>
        <description>Turns the Opportunity Stage to closed lost</description>
        <field>StageName</field>
        <literalValue>Closed Lost</literalValue>
        <name>Field Update: Closed Lost 75%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_Closed_Lost_90</fullName>
        <description>Turns the Opportunity Stage to closed lost</description>
        <field>StageName</field>
        <literalValue>Closed Lost</literalValue>
        <name>Field Update: Closed Lost 90%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_Lost_Reason_Other</fullName>
        <field>Lost_Reason__c</field>
        <literalValue>Other</literalValue>
        <name>Field Update: Lost Reason Other</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_Stalled_Opp_Cat_25</fullName>
        <description>This field update is used on the stalled opportunity category field and sets the value at Solid Prospect</description>
        <field>Stalled_Opp_Category__c</field>
        <literalValue>Solid Prospect</literalValue>
        <name>Field Update: Stalled Opp Cat 25%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_Stalled_Opp_Cat_60</fullName>
        <description>This field update is used on the stalled opportunity category field and sets the value at Seeking Approval</description>
        <field>Stalled_Opp_Category__c</field>
        <literalValue>Seeking Approval</literalValue>
        <name>Field Update: Stalled Opp Cat 60%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_Stalled_Opp_Cat_75</fullName>
        <description>This field update is used on the stalled opportunity category field and sets the value at Selected</description>
        <field>Stalled_Opp_Category__c</field>
        <literalValue>Selected</literalValue>
        <name>Field Update: Stalled Opp Cat 75%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_Stalled_Opp_Cat_90</fullName>
        <field>Stalled_Opp_Category__c</field>
        <literalValue>Negotiation/Review</literalValue>
        <name>Field Update: Stalled Opp Cat 90%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Created</fullName>
        <field>Lead_Created__c</field>
        <formula>$Profile.CreatedDate</formula>
        <name>Lead Created</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lost_Reason_Detail_Auto_Closed</fullName>
        <field>Lost_Reason_Detail__c</field>
        <formula>&apos;Auto Closed&apos;</formula>
        <name>Lost Reason Detail Auto Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MPro_Opportunity_Assigned_to_Amjad_Sheha</fullName>
        <field>OwnerId</field>
        <lookupValue>ashehade@marinsoftware.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>MPro Opportunity Assigned to Amjad Sheha</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MPro_Opportunity_Assigned_to_Ben_Beal</fullName>
        <field>OwnerId</field>
        <lookupValue>ppillsbury@marinsoftware.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>MPro Opportunity Assigned to Ben Beal</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MPro_Opportunity_Assigned_to_Breck</fullName>
        <field>OwnerId</field>
        <lookupValue>bhandy@marinsoftware.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>MPro Opportunity Assigned to Breck</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MPro_Opportunity_Assigned_to_Cary_Dunst</fullName>
        <field>OwnerId</field>
        <lookupValue>tdevitto@marinsoftware.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>MPro Opportunity Assigned to Tony DeVitt</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MPro_Opportunity_Assigned_to_Greg_Garbo</fullName>
        <field>OwnerId</field>
        <lookupValue>ggarbo@marinsoftware.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>MPro Opportunity Assigned to Greg Garbo</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MPro_Opportunity_Assigned_to_Matt_Forcey</fullName>
        <field>OwnerId</field>
        <lookupValue>mforcey@marinsoftware.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>MPro Opportunity Assigned to Matt Forcey</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MPro_Opportunity_Assigned_to_Phil</fullName>
        <field>OwnerId</field>
        <lookupValue>ppillsbury@marinsoftware.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>MPro Opportunity Assigned to Phil</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MPro_Opportunity_Assigned_to_Tony_DeVitt</fullName>
        <field>OwnerId</field>
        <lookupValue>tdevitto@marinsoftware.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>MPro Opportunity Assigned to Tony DeVitt</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MPro_Owner_Assignment_Mark_Boyich</fullName>
        <field>OwnerId</field>
        <lookupValue>mboyich@marinsoftware.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>MPro Owner Assignment: Mark Boyich</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MPro_Owner_Assignment_Sam_Sheldon</fullName>
        <field>OwnerId</field>
        <lookupValue>jmeyer@marinsoftware.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>MPro Owner Assignment: Sam Sheldon</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Manager_email_update</fullName>
        <field>Manager_Email__c</field>
        <formula>Opportunity_Owner_Manager__c</formula>
        <name>Manager email update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Modify_Monthly_Spend_100_000_1_000_000</fullName>
        <field>Mthly_Srch_Mktg_Spend__c</field>
        <literalValue>$100,000-$1,000,000</literalValue>
        <name>Modify Monthly Spend-$100,000-$1,000,000</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Modify_Monthly_Spend_10_000_40_000</fullName>
        <field>Mthly_Srch_Mktg_Spend__c</field>
        <literalValue>$10,000-$40,000</literalValue>
        <name>Modify Monthly Spend-$10,000-$40,000-</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Modify_Monthly_Spend_1_000_000_and_abov</fullName>
        <field>Mthly_Srch_Mktg_Spend__c</field>
        <literalValue>$1,000,000 and above</literalValue>
        <name>Modify Monthly Spend-$1,000,000 and abov</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Modify_Monthly_Spend_40_000_100_000</fullName>
        <field>Mthly_Srch_Mktg_Spend__c</field>
        <literalValue>$40,000-$100,000</literalValue>
        <name>Modify Monthly Spend-$40,000-$100,000</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Move_Account_Status_to_Prospect</fullName>
        <field>Account_Status__c</field>
        <literalValue>Prospect</literalValue>
        <name>Move Account Status to Prospect</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OmitfromAttainment</fullName>
        <field>Omitted_from_Attainment__c</field>
        <literalValue>1</literalValue>
        <name>OmitfromAttainment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Region_Central</fullName>
        <field>Region__c</field>
        <literalValue>Central</literalValue>
        <name>Opp Region Central</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Region_East</fullName>
        <description>Updates Opp Region to East</description>
        <field>Region__c</field>
        <literalValue>East</literalValue>
        <name>Opp Region East</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Region_West</fullName>
        <description>Updates Region to West</description>
        <field>Region__c</field>
        <literalValue>West</literalValue>
        <name>Opp Region West</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Autoclose_Close_Lost_Reason</fullName>
        <field>Lost_Reason__c</field>
        <literalValue>Auto Close / Age</literalValue>
        <name>Opportunity Autoclose Close Lost Reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Project_Stage_PS_not_involved</fullName>
        <field>Project_Stage__c</field>
        <literalValue>PS not involved</literalValue>
        <name>Project Stage: PS not involved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_from_attainmeent</fullName>
        <field>Omitted_from_Attainment__c</field>
        <literalValue>1</literalValue>
        <name>Remove from attainmeent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Resources_Not_Needed_for_Pro</fullName>
        <description>This workflow flips PS Resources to Not Needed which will then make the Project Stage field say PS Not Involved.</description>
        <field>PS_Resources__c</field>
        <literalValue>Not Required</literalValue>
        <name>Resources Not Needed for Pro</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stalled_OppCategory_90</fullName>
        <field>Stalled_Opp_Category10__c</field>
        <formula>TEXT( Stalled_Opp_Category__c )</formula>
        <name>Stalled Opp Category 90%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stalled_Opp_Category10</fullName>
        <field>Stalled_Opp_Category10__c</field>
        <formula>TEXT( Stalled_Opp_Category__c )</formula>
        <name>Stalled Opp Category 10</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stalled_Opp_Category3</fullName>
        <field>Stalled_Opp_Category3__c</field>
        <formula>TEXT( Stalled_Opp_Category__c )</formula>
        <name>Stalled Opp Category 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stalled_Opp_Category75</fullName>
        <field>Stalled_Opp_Category3__c</field>
        <formula>TEXT( Stalled_Opp_Category__c )</formula>
        <name>Stalled Opp Category 75%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stalled_Opp_Category_0</fullName>
        <field>Stalled_Opp_Category10__c</field>
        <formula>TEXT( Stalled_Opp_Category__c )</formula>
        <name>Stalled Opp Category 75%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stalled_Opp_Category_10</fullName>
        <field>Stalled_Opp_Category10__c</field>
        <formula>TEXT(Stalled_Opp_Category__c)</formula>
        <name>Stalled Opp Category 10</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stalled_Opp_Category_3</fullName>
        <field>Stalled_Opp_Category3__c</field>
        <formula>TEXT( Stalled_Opp_Category__c )</formula>
        <name>Stalled Opp Category 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stalled_Opp_Category_75</fullName>
        <field>Stalled_Opp_Category3__c</field>
        <formula>TEXT( Stalled_Opp_Category__c )</formula>
        <name>Stalled Opp Category 75%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stalled_Opp_Category_9</fullName>
        <field>Stalled_Opp_Category3__c</field>
        <formula>TEXT( Stalled_Opp_Category__c )</formula>
        <name>Stalled Opp Category 90%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Trial_End_Date_2_weeks</fullName>
        <field>Trial_End_Date__c</field>
        <formula>Trial_Start_Date__c + 20</formula>
        <name>Trial End Date - 3 weeks</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateAccountStatusActiveCustomer</fullName>
        <field>Account_Status__c</field>
        <literalValue>Active Customer</literalValue>
        <name>UpdateAccountStatusActiveCustomer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateAccountStatusInActiveCustomer</fullName>
        <field>Account_Status__c</field>
        <literalValue>Inactive Customer</literalValue>
        <name>UpdateAccountStatusInActiveCustomer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateCreditStatusApprove</fullName>
        <field>Credit_Approval_Status__c</field>
        <literalValue>Finance Approved</literalValue>
        <name>UpdateCreditStatusApproved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateCreditStatusBlank</fullName>
        <field>Credit_Approval_Status__c</field>
        <name>UpdateCreditStatusBlank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateCreditStatusDeptApproved</fullName>
        <field>Credit_Approval_Status__c</field>
        <literalValue>Department Approved</literalValue>
        <name>UpdateCreditStatusDeptApproved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateCreditStatusDeptSubmitted</fullName>
        <field>Credit_Approval_Status__c</field>
        <literalValue>Department Submitted</literalValue>
        <name>UpdateCreditStatusDeptSubmitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateCreditStatusFinanceApproved</fullName>
        <field>Credit_Approval_Status__c</field>
        <literalValue>Finance Approved</literalValue>
        <name>UpdateCreditStatusFinanceApproved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateCreditStatusFinanceSubmitted</fullName>
        <field>Credit_Approval_Status__c</field>
        <literalValue>Finance Submitted</literalValue>
        <name>UpdateCreditStatusFinanceSubmitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateCreditStatusRejected</fullName>
        <field>Credit_Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>UpdateCreditStatusRejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateCreditStatusSubmittedd</fullName>
        <field>Credit_Approval_Status__c</field>
        <literalValue>Department Submitted</literalValue>
        <name>UpdateCreditStatusSubmittedd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateDND</fullName>
        <field>Do_Not_Contact_Marketing__c</field>
        <literalValue>1</literalValue>
        <name>UpdateDND</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateOwnerPhil</fullName>
        <field>OwnerId</field>
        <lookupValue>ppillsbury@marinsoftware.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>UpdateOwnerPhil</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateRevenueImpactNull</fullName>
        <field>Revenue_Impact__c</field>
        <name>UpdateRevenueImpactNull</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateStage50</fullName>
        <field>StageName</field>
        <literalValue>Loss Likely</literalValue>
        <name>UpdateStage50</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateStage95</fullName>
        <field>StageName</field>
        <literalValue>Loss Submitted</literalValue>
        <name>UpdateStage95</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Initial_Qualification_Value_field</fullName>
        <field>Initial_Qualification_Value__c</field>
        <formula>IF(
   OR(TEXT(Customer_Type_for_Qualification__c) = &quot;Direct Advertiser/Agency&quot;, TEXT(Customer_Type_for_Qualification__c) = &quot;Japan&quot;), 
    IF(OR(ISBLANK(Actual_Monthly_Spend__c), Actual_Monthly_Spend__c = null, Actual_Monthly_Spend__c = 0, Actual_Monthly_Spend__c &lt; 1), null,
    IF(Actual_Monthly_Spend__c &lt; 25000, 250,
    IF(Actual_Monthly_Spend__c &lt; 50000, 750,
    IF(Actual_Monthly_Spend__c &lt; 100000, 1500,
    IF(Actual_Monthly_Spend__c &lt; 150000, 4000,
    IF(Actual_Monthly_Spend__c &lt; 250000, 5200,
    IF(Actual_Monthly_Spend__c &lt; 350000, 6500,
    IF(Actual_Monthly_Spend__c &lt; 450000, 8000,
    IF(Actual_Monthly_Spend__c &lt; 700000, 9000,
    IF(Actual_Monthly_Spend__c &lt; 1000000, 10000,
    IF(Actual_Monthly_Spend__c &lt; 1500000, 13000,
    IF(Actual_Monthly_Spend__c &lt; 2000000, 16000, 20000)))))))))))),
    
    IF(OR(ISBLANK(Actual_Monthly_Spend__c), Actual_Monthly_Spend__c = null, Actual_Monthly_Spend__c = 0, Actual_Monthly_Spend__c &lt; 1), null,
    IF(Actual_Monthly_Spend__c &lt; 25000, 125,
    IF(Actual_Monthly_Spend__c &lt; 50000, 375,
    IF(Actual_Monthly_Spend__c &lt; 100000, 750,
    IF(Actual_Monthly_Spend__c &lt; 150000, 2000,
    IF(Actual_Monthly_Spend__c &lt; 250000, 2600,
    IF(Actual_Monthly_Spend__c &lt; 350000, 3250,
    IF(Actual_Monthly_Spend__c &lt; 450000, 4000,
    IF(Actual_Monthly_Spend__c &lt; 700000, 4500,
    IF(Actual_Monthly_Spend__c &lt; 1000000, 5000,
    IF(Actual_Monthly_Spend__c &lt; 1500000, 6500,
    IF(Actual_Monthly_Spend__c &lt; 2000000, 8000, 10000)))))))))))))</formula>
        <name>Update Initial Qualification Value field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Monthly_Spend_less_than_10_000</fullName>
        <field>Mthly_Srch_Mktg_Spend__c</field>
        <literalValue>less than $10,000</literalValue>
        <name>Update Monthly Spend-less than 10,000</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_Office</fullName>
        <field>Owner_Office__c</field>
        <formula>TEXT( Owner.User_Office__c )</formula>
        <name>Update Owner Office</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_Region</fullName>
        <field>Owner_Region__c</field>
        <formula>TEXT( Owner.User_Region__c )</formula>
        <name>Update Owner Region</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_Team</fullName>
        <description>Update Owner Team Field</description>
        <field>Owner_Team__c</field>
        <formula>TEXT(Owner.User_Team__c)</formula>
        <name>Update Owner Team</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>95%25 over notification</fullName>
        <actions>
            <name>X95_Opp_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Probability</field>
            <operation>greaterOrEqual</operation>
            <value>95</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Product_Version__c</field>
            <operation>equals</operation>
            <value>Professional</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AM Add On Closed Won</fullName>
        <actions>
            <name>AM_Closed_Won_Billing_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>AM_Closed_Won_Customer_Status_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>AM_Closed_Won_Procurement_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Account Manager</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Signed Order Form Returned</value>
        </criteriaItems>
        <description>Procurement Process: Add On - No Procurement
Customer Status: Customer
Billing Start Date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Alert%3A Product Version Field Edit</fullName>
        <actions>
            <name>Product_Version_Change</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(  ISCHANGED( Product_Version__c ),  OR(  ISPICKVAL(StageName , &quot;Qualification Confirmed&quot;), ISPICKVAL(StageName , &quot;Solid Prospect&quot;), ISPICKVAL(StageName , &quot;Seriously Considering Us&quot;), ISPICKVAL(StageName , &quot;Seeking Approval&quot;), ISPICKVAL(StageName , &quot;Selected&quot;), ISPICKVAL(StageName , &quot;Negotiation/Review&quot;), ISPICKVAL(StageName , &quot;Signed Order Form Returned&quot;), ISPICKVAL(StageName , &quot;Closed Won&quot;) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>AlertSalesOpsOppForecastnotOmittedNegAmount</fullName>
        <actions>
            <name>AlertSalesOpsNegAmountForecastnotOmitted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.ForecastCategoryName</field>
            <operation>notEqual</operation>
            <value>Omitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Amount</field>
            <operation>lessThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Probability</field>
            <operation>greaterOrEqual</operation>
            <value>95</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AlertSalesOpsOppOpenedfromCW</fullName>
        <actions>
            <name>AlertSalesOpsOppOpenedfromClosedWon</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(
PRIORVALUE(IsWon) = TRUE,
PRIORVALUE(Probability) = 100,
IsClosed = FALSE,
NOT($User.UserRoleId = &apos;00E50000000w331&apos;)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Bidding Eligible</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Marin_Pro_Milestones__c</field>
            <operation>equals</operation>
            <value>Bidding Eligible</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ChangeProTrailStage</fullName>
        <actions>
            <name>Change_Stage_To_Lead_Passed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Specific_Lead_Source__c</field>
            <operation>equals</operation>
            <value>Pro Website Free Trial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Seriously Considering Us</value>
        </criteriaItems>
        <description>The CS App is setting Stage to &quot;Seriously Considering Us&quot;, but should be set to &quot;Lead Passed&quot;. Created by Mark Klinski on 2014-02-25 per Phil Pillsbury.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ChurnDuringFlex</fullName>
        <actions>
            <name>Sends_an_Email_to_Sales_Ops_when_a_Churn_Opp_is_Created_During_Flex_Term</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(
ISPICKVAL( Type_Detail__c , &apos;AM: CHURN&apos;),
 CloseDate &lt;=  Contract_Record__r.Contract_Option_Due_Date__c 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Close Date Moved Notification</fullName>
        <actions>
            <name>email_alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.LastModifiedById</field>
            <operation>equals</operation>
            <value>aditya vyas</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Close Date Notification</fullName>
        <actions>
            <name>Close_Date_Change_notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Close-won Deal</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Probability</field>
            <operation>equals</operation>
            <value>95</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Product_Version__c</field>
            <operation>notEqual</operation>
            <value>Professional</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Close-won Deal %28Professional%29</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Probability</field>
            <operation>equals</operation>
            <value>95</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Product_Version__c</field>
            <operation>equals</operation>
            <value>Professional</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Closed Won Deal</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Probability</field>
            <operation>equals</operation>
            <value>95</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Closed Won Notification for PC</fullName>
        <actions>
            <name>PC_Notification_for_Closed_Won_Opp</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Product_Consultant__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy User Email address</fullName>
        <actions>
            <name>Copy_Customer_Engagement_Manager_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Copy_LeadGen_Owner_Email_from_Accounts</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Copy_Sales_Owner_Email_from_Accounts</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This Workflow will copy the email address for the Sales Owner, LeadGen Owner and the Customer Engagement Manager emails to Opportunity</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CreditCategoryChangedUpdateCreditApproval</fullName>
        <actions>
            <name>UpdateCreditStatusBlank</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(
ISPICKVAL(Type_Detail__c, &apos;AM: CREDIT&apos;),

OR(

ISCHANGED( Credit_Category__c ),

AND(
ISCHANGED( Amount),
NOT(PRIORVALUE( Amount) = NULL),
PRIORVALUE(Amount)&gt;Amount
)
)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CreditRevImpactChangedUpdateApproval</fullName>
        <actions>
            <name>UpdateCreditStatusDeptApproved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(
ISPICKVAL( Type_Detail__c, &apos;AM: CREDIT&apos;),
NOT(PRIORVALUE( Revenue_Impact__c) = NULL),
ISCHANGED(  Revenue_Impact__c )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CurrentContractEndDate</fullName>
        <actions>
            <name>CurrentContractEndDateUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EA_Bobby_1mil_Agency_Notice</fullName>
        <actions>
            <name>EA_Alert_Bobby_1mil</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Actual_Monthly_Spend__c</field>
            <operation>greaterOrEqual</operation>
            <value>1000000</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Agency</value>
        </criteriaItems>
        <description>Notifiy Bobby and Andrew of 1mil plus Opp values for agency</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EA_Bobby_Pearl_Agency_5_Create</fullName>
        <actions>
            <name>Email_to_Bobby_Hollis_Pearl_Agency</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Workflow to track Opportunities created by Pearl per Bobby&apos;s request.</description>
        <formula>AND
(
$User.Id = &quot;00550000001j0YM&quot;,
ISPICKVAL(StageName,&quot;Lead Passed&quot;)
)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EA_Opp_0_to_5</fullName>
        <actions>
            <name>Alert_to_Mike_Grossman_of_Opps_going_from_0_to_5</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This is to notify Inside Sales MGMT and Marketing for Opps that go from 0% to 5%</description>
        <formula>AND(
ISPICKVAL(PRIORVALUE(StageName),&quot;Closed Lost&quot;),
ISPICKVAL(StageName,&quot;Lead Passed&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EA_Opp_Bobby_NA_APAC</fullName>
        <actions>
            <name>Email_to_Bobby_Hollis_NA_APAC</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Workflow to send an email for Bobby when Opps are Created at 5%</description>
        <formula>AND(
ISPICKVAL(StageName,&quot;Lead Passed&quot;),
OR(
$User.UserRoleId = &quot;00E50000001GjGB&quot;
,$User.UserRoleId = &quot;00E50000000ocXM&quot;
,$User.UserRoleId = &quot;00E50000001GvTb&quot;
,$User.UserRoleId = &quot;00E50000001GYrQ&quot;
,$User.UserRoleId = &quot;00E50000000orKu&quot;
,$User.UserRoleId = &quot;00E50000000ot1F&quot;
,$User.UserRoleId = &quot;00E50000000w33G&quot;
,$User.UserRoleId = &quot;00E50000000wKEb&quot;
,$User.UserRoleId = &quot;00E50000000zFoY&quot;
,$User.UserRoleId = &quot;00E50000000zFQq&quot;
,$User.UserRoleId = &quot;00E50000000zHn8&quot;
,$User.UserRoleId = &quot;00E50000001GgHa&quot;
,$User.UserRoleId = &quot;00E50000001GgHf&quot;
,$User.UserRoleId = &quot;00E50000001GgHQ&quot;
,$User.UserRoleId = &quot;00E50000001GjGk&quot;
,$User.UserRoleId = &quot;00E50000001GjwH&quot;
,$User.UserRoleId = &quot;00E50000001GjwM&quot;
,$User.UserRoleId = &quot;00E50000001GjwR&quot;
,$User.UserRoleId = &quot;00E50000001GjwW&quot;
,$User.UserRoleId = &quot;00E50000001GpxG&quot;
,$User.UserRoleId = &quot;00E50000001Gq0F&quot;
,$User.UserRoleId = &quot;00E50000001Gtoh&quot;
,$User.UserRoleId = &quot;00E50000001GYke&quot;
,$User.UserRoleId = &quot;00E50000001GYkj&quot;
,$User.UserRoleId = &quot;00E50000001Gypz&quot;
,$User.UserRoleId = &quot;00E50000001H0gG&quot;
,$User.UserRoleId = &quot;00E50000001H0gL&quot;



)
)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EA_Opp_Bobby_NA_APAC_0_to_5</fullName>
        <actions>
            <name>Alert_to_Bobby_of_Opps_going_from_0_to_5_NA_EMEA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Email notification for any Closed Lost Opps moving to 5%</description>
        <formula>AND( 
ISPICKVAL(PRIORVALUE(StageName),&quot;Closed Lost&quot;), 
ISPICKVAL(StageName,&quot;Lead Passed&quot;),
OR(
$User.UserRoleId = &quot;00E50000001GjGB&quot;
,$User.UserRoleId = &quot;00E50000000ocXM&quot;
,$User.UserRoleId = &quot;00E50000001GvTb&quot;
,$User.UserRoleId = &quot;00E50000001GYrQ&quot;
,$User.UserRoleId = &quot;00E50000000orKu&quot;
,$User.UserRoleId = &quot;00E50000000ot1F&quot;
,$User.UserRoleId = &quot;00E50000000w33G&quot;
,$User.UserRoleId = &quot;00E50000000wKEb&quot;
,$User.UserRoleId = &quot;00E50000000zFoY&quot;
,$User.UserRoleId = &quot;00E50000000zFQq&quot;
,$User.UserRoleId = &quot;00E50000000zHn8&quot;
,$User.UserRoleId = &quot;00E50000001GgHa&quot;
,$User.UserRoleId = &quot;00E50000001GgHf&quot;
,$User.UserRoleId = &quot;00E50000001GgHQ&quot;
,$User.UserRoleId = &quot;00E50000001GjGk&quot;
,$User.UserRoleId = &quot;00E50000001GjwH&quot;
,$User.UserRoleId = &quot;00E50000001GjwM&quot;
,$User.UserRoleId = &quot;00E50000001GjwR&quot;
,$User.UserRoleId = &quot;00E50000001GjwW&quot;
,$User.UserRoleId = &quot;00E50000001GpxG&quot;
,$User.UserRoleId = &quot;00E50000001Gq0F&quot;
,$User.UserRoleId = &quot;00E50000001Gtoh&quot;
,$User.UserRoleId = &quot;00E50000001GYke&quot;
,$User.UserRoleId = &quot;00E50000001GYkj&quot;
,$User.UserRoleId = &quot;00E50000001Gypz&quot;
,$User.UserRoleId = &quot;00E50000001H0gG&quot;
,$User.UserRoleId = &quot;00E50000001H0gL&quot;
)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EA_Opp_Create_5</fullName>
        <actions>
            <name>Email_to_Mike_Grossman</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This is to notify Inside Sales MGMT and Marketing of any Opportunities created at 5%.</description>
        <formula>ISPICKVAL(StageName,&quot;Lead Passed&quot;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EA_PG_Notification_ResearchMistake</fullName>
        <actions>
            <name>EA_PG_Research_Opp_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Opportunity creation mistake.</description>
        <formula>OR(
$User.Id = &apos;005500000010Thb&apos;
,$User.Id = &apos;00550000001k0ju&apos;
,$User.Id = &apos;00550000001jqPG&apos;
,$User.Id = &apos;00550000001k3es&apos;
,$User.Id = &apos;00550000001kBXG&apos;
,$User.Id = &apos;00550000001lcPC&apos;
,$User.Id = &apos;00550000001lcOs&apos;
,$User.Id = &apos;00550000001lcOx&apos;
,$User.Id = &apos;00550000001lfsv&apos;
,$User.Id = &apos;00550000001lkyZ&apos;
,$User.Id = &apos;00550000001llAG&apos;
,$User.Id = &apos;00550000001j9MZ&apos;
,$User.Id = &apos;00550000001ksrt&apos;
,$User.Id = &apos;00550000001jcrE&apos;
,$User.Id = &apos;00550000001ln4U&apos;
,$User.Id = &apos;00550000001ihJI&apos;
)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EA_PRO_Opp_Notification</fullName>
        <active>false</active>
        <description>Requested by Phil.  Can&apos;t recycle the global notification because he wants PRO only -_-</description>
        <formula>AND(ISPICKVAL(StageName,&quot;Lead Passed&quot;),
ISPICKVAL(Product_Version__c,&quot;Professional&quot;),
OR(
$User.Id = &quot;00550000001kPT9&quot;
,$User.Id = &quot;00550000001DtTM&quot;
,$User.Id = &quot;00550000001kyaF&quot;
,$User.Id = &quot;00550000001k1oC&quot;
,$User.Id = &quot;00550000001jqK6&quot;
,$User.Id = &quot;00550000001lQoK&quot;
,$User.Id = &quot;00550000001DtXO&quot;)
)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EA_Phil_Notify_PRO_Closed</fullName>
        <actions>
            <name>EA_Phil_Team_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>PRO Closed and Created Global</description>
        <formula>AND(OR(ISPICKVAL(StageName,&quot;Closed Lost&quot;),ISPICKVAL(StageName,&quot;Lead Passed&quot;),
ISPICKVAL(StageName,&quot;Seriously Considering Us&quot;)),
ISPICKVAL(Product_Version__c,&quot;Professional&quot;),
OR(
$User.Id = &quot;00550000001kPT9&quot;
,$User.Id = &quot;00550000001DtTM&quot;
,$User.Id = &quot;00550000001kyaF&quot;
,$User.Id = &quot;00550000001k1oC&quot;
,$User.Id = &quot;00550000001jqK6&quot;
,$User.Id = &quot;00550000001lQoK&quot;
,$User.Id = &quot;00550000001DtXO&quot;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMEA_InsideSales_OppCreation_Notice</fullName>
        <actions>
            <name>EMEA_Inside_Sales_Alert_Manager_of_New_Opp</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Workflow to send an email to EMEA Inside Sales manager when an email is created.</description>
        <formula>$User.ManagerId = &quot;0055000000457cm&quot;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email Account Owners on New Opp Creation</fullName>
        <active>false</active>
        <description>Email Account Owners on New Opp Creation</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email Alert%3A Opportunity Passed Closed Date</fullName>
        <active>false</active>
        <formula>AND(

OR(
ISPICKVAL( StageName , &quot;Lead Passed&quot;),
ISPICKVAL( StageName , &quot;Qualification Confirmed&quot;),
ISPICKVAL( StageName , &quot;Solid Prospect&quot;),
ISPICKVAL( StageName , &quot;Developing Proposal&quot;),
ISPICKVAL( StageName , &quot;Seeking Approval&quot;),
ISPICKVAL( StageName , &quot;Selected&quot;),
ISPICKVAL( StageName , &quot;Negotiation/Review&quot;)),

(CloseDate + 16) &gt; Today()
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>15</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Email%3A Opportunity Close Lost %28UK%29</fullName>
        <actions>
            <name>Opportunity_Closed_Lost_UK</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Probability</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Region__c</field>
            <operation>equals</operation>
            <value>EMEA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Product_Version__c</field>
            <operation>notEqual</operation>
            <value>Professional</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type_Detail__c</field>
            <operation>equals</operation>
            <value>New Business: Existing Agency Contract,New Business: Full Term,New Business: Trial,AM: New Line of Business,PLA</value>
        </criteriaItems>
        <description>Closed Lost Opportunity for EMEA</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A PS Resources Needed</fullName>
        <actions>
            <name>PS_Resources</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.PS_Resources__c</field>
            <operation>equals</operation>
            <value>Needed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Probability</field>
            <operation>greaterOrEqual</operation>
            <value>60</value>
        </criteriaItems>
        <description>This workflow identifies when PS Resources are needed for an opportunity. Once it meets the criteria listed the workflow will send an email to PS letting them know.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Send Scoping Document to Customer</fullName>
        <actions>
            <name>Send_Scoping_to_Customer</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Scoping_Document_Status__c</field>
            <operation>equals</operation>
            <value>Sent to Customer Contact</value>
        </criteriaItems>
        <description>This workflow triggers the email delivery of the scoping document to the Product Consultant so that they can do the initial review for the client.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Send Scoping Document to Marin Staff</fullName>
        <actions>
            <name>Send_Scoping_to_Product_Consultant</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Scoping_Document_Status__c</field>
            <operation>equals</operation>
            <value>Sent to Marin Staff</value>
        </criteriaItems>
        <description>This workflow triggers the email delivery of the scoping document to the Product Consultant and Opportunity Owner so that they can do the initial review for the client.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Opp Category 25%25</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Probability</field>
            <operation>lessThan</operation>
            <value>25</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Closed Won,Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>New Business</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Auto_Close_Override__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type_Detail__c</field>
            <operation>notEqual</operation>
            <value>New Business: TrueUp</value>
        </criteriaItems>
        <description>This workflow has two purposes:
- Updates the opportunity field to 25% which triggers a chatter alert to the Opp Onwer after 60 days
- &apos;Closed Lost&apos; the opportunity after 90 days &amp; Checks the Auto Close box for reporting</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Stalled_Opp_Category10</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.CreatedDate</offsetFromField>
            <timeLength>80</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Stalled_Opp_Category3</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.CreatedDate</offsetFromField>
            <timeLength>87</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Field_Update_Stalled_Opp_Cat_25</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.CreatedDate</offsetFromField>
            <timeLength>60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Auto_Close_TRUE_25</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Closed_Date_Today</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Field_Update_Closed_Lost_25</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Field_Update_Lost_Reason_Other</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Lost_Reason_Detail_Auto_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.CreatedDate</offsetFromField>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Field Update%3A Opp Category 60%25</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Probability</field>
            <operation>lessThan</operation>
            <value>60</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Closed Won,Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>New Business</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Auto_Close_Override__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type_Detail__c</field>
            <operation>notEqual</operation>
            <value>New Business: TrueUp</value>
        </criteriaItems>
        <description>This workflow has two purposes:
- Updates the opportunity field to 60% which triggers a chatter alert to the Opp Onwer after 150 days
- &apos;Closed Lost&apos; the opportunity after 180 days &amp; Checks the Auto Close box for reporting</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Stalled_Opp_Category_10</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.CreatedDate</offsetFromField>
            <timeLength>170</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Stalled_Opp_Category_3</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.CreatedDate</offsetFromField>
            <timeLength>177</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Auto_Close_TRUE_60</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Closed_Date_Today</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Field_Update_Closed_Lost_60</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Field_Update_Lost_Reason_Other</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Lost_Reason_Detail_Auto_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.CreatedDate</offsetFromField>
            <timeLength>180</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Field_Update_Stalled_Opp_Cat_60</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.CreatedDate</offsetFromField>
            <timeLength>150</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Field Update%3A Opp Category 75%25</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Probability</field>
            <operation>lessThan</operation>
            <value>75</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Closed Won,Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>New Business</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Auto_Close_Override__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type_Detail__c</field>
            <operation>notEqual</operation>
            <value>New Business: TrueUp</value>
        </criteriaItems>
        <description>This workflow has two purposes:
- Updates the opportunity field to 75% which triggers a chatter alert to the Opp Onwer after 240 days
- &apos;Closed Lost&apos; the opportunity after 270 days &amp; Checks the Auto Close box for reporting</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Stalled_Opp_Category_0</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.CreatedDate</offsetFromField>
            <timeLength>260</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Stalled_Opp_Category75</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.CreatedDate</offsetFromField>
            <timeLength>267</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Auto_Close_TRUE_75</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Closed_Date_Today</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Field_Update_Closed_Lost_75</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Field_Update_Lost_Reason_Other</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Lost_Reason_Detail_Auto_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.CreatedDate</offsetFromField>
            <timeLength>270</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Field_Update_Stalled_Opp_Cat_75</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.CreatedDate</offsetFromField>
            <timeLength>240</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Field Update%3A Opp Category 90%25</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Probability</field>
            <operation>lessThan</operation>
            <value>90</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Closed Won,Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>New Business</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Auto_Close_Override__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type_Detail__c</field>
            <operation>notEqual</operation>
            <value>New Business: TrueUp</value>
        </criteriaItems>
        <description>This workflow has two purposes:
- Updates the opportunity field to 90% which triggers a chatter alert to the Opp Onwer after 330 days
- &apos;Closed Lost&apos; the opportunity after 365 days &amp; Checks the Auto Close box for reporting</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Stalled_OppCategory_90</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.CreatedDate</offsetFromField>
            <timeLength>355</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Stalled_Opp_Category_9</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.CreatedDate</offsetFromField>
            <timeLength>362</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Auto_Close_TRUE_90</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Closed_Date_Today</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Field_Update_Closed_Lost_90</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Field_Update_Lost_Reason_Other</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Lost_Reason_Detail_Auto_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.CreatedDate</offsetFromField>
            <timeLength>365</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Field_Update_Stalled_Opp_Cat_90</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.CreatedDate</offsetFromField>
            <timeLength>330</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Finance%3A Opp at 60%25 Credit Check</fullName>
        <actions>
            <name>Email_Finance_about_Credit_Check</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Created on 2014-05-12 by Mark Klinski per Ryan Adams.</description>
        <formula>AND(
RecordTypeId = &apos;012500000005Sac&apos;,
Probability &gt;= 0.6,
PRIORVALUE(Probability) &lt; 0.6,
NOT(ISPICKVAL(Finance_Credit_Approved__c, &quot;&quot;)),
NOT(ISPICKVAL(Product_Version__c , &quot;Professional&quot;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>InitQualValue</fullName>
        <actions>
            <name>Update_Initial_Qualification_Value_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 5) AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Initial_Qualification_Value__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Probability</field>
            <operation>greaterOrEqual</operation>
            <value>10</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type_Detail__c</field>
            <operation>equals</operation>
            <value>New Business: Existing Agency Contract,New Business: Full Term,New Business: Trial</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Customer_Type_for_Qualification__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Initial_Qualification_Value__c</field>
            <operation>lessThan</operation>
            <value>1</value>
        </criteriaItems>
        <description>Set Initial Qualification Value when AE qualifies an opportunity. Created 1/6/14 by Mark Klinski per Jake Meyer.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IsNetNew</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.Count_of_Qualified_Opportunities__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Qualified_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IsNetNewUpdate</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.Count_of_Qualified_Opportunities__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Qualified_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lost to Kenshoo</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Competitors__c</field>
            <operation>includes</operation>
            <value>Kenshoo</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MPro Owner Assignment%3A Amjad Shehade</fullName>
        <actions>
            <name>MPro_Opportunity_Assigned_to_Amjad_Sheha</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Product_Version__c</field>
            <operation>equals</operation>
            <value>Professional</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Mthly_Srch_Mktg_Spend__c</field>
            <operation>equals</operation>
            <value>&quot;$1,000,000 and above&quot;,&quot;$100,000 - $1,000,000&quot;</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingState</field>
            <operation>equals</operation>
            <value>New York,NY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>contains</operation>
            <value>Greer</value>
        </criteriaItems>
        <description>$100,000 and above – AE by territory</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MPro Owner Assignment%3A Ben Beal</fullName>
        <actions>
            <name>MPro_Opportunity_Assigned_to_Ben_Beal</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Product_Version__c</field>
            <operation>equals</operation>
            <value>Professional</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Mthly_Srch_Mktg_Spend__c</field>
            <operation>equals</operation>
            <value>&quot;$1,000,000 and above&quot;,&quot;$100,000 - $1,000,000&quot;</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingState</field>
            <operation>equals</operation>
            <value>Illinois,IL,Indiana,IN,Michigan,MI,Minnesota,MN,Wisconsin,WI,ND,North Dakota,SD,South Dakota,Montana,MT,WY,Wyoming,MB,Manitoba,SK,Saskatchewan,AB,Alberta</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>contains</operation>
            <value>Greer</value>
        </criteriaItems>
        <description>$100,000 and above – AE by territory</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MPro Owner Assignment%3A Brad Serlin</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Product_Version__c</field>
            <operation>equals</operation>
            <value>Professional</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Mthly_Srch_Mktg_Spend__c</field>
            <operation>equals</operation>
            <value>&quot;$1,000,000 and above&quot;,&quot;$100,000 - $1,000,000&quot;</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingState</field>
            <operation>equals</operation>
            <value>Connecticut,CT,Maine,ME,Massachusetts,MA,New Hampshire,NH,New Jersey,NJ,Pennsylvania,PA,Rhode Island,RI,Vermont,VT,Quebec,QC,Nova Scotia,NS,New Brunswick,NB,Newfoundland,NL</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>contains</operation>
            <value>Greer</value>
        </criteriaItems>
        <description>$100,000 and above – AE by territory</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MPro Owner Assignment%3A Breck Handy</fullName>
        <actions>
            <name>MPro_Opportunity_Assigned_to_Breck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND (3 OR (4 AND 5)) AND 6</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Product_Version__c</field>
            <operation>equals</operation>
            <value>Professional</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Mthly_Srch_Mktg_Spend__c</field>
            <operation>equals</operation>
            <value>&quot;$1,000,000 and above&quot;,&quot;$100,000 - $1,000,000&quot;</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingState</field>
            <operation>equals</operation>
            <value>WA,Washington,OR,Oregon</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingState</field>
            <operation>equals</operation>
            <value>ca,california</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingPostalCode</field>
            <operation>contains</operation>
            <value>93900</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>contains</operation>
            <value>Greer</value>
        </criteriaItems>
        <description>$100,000 and above – AE by territory</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MPro Owner Assignment%3A Cary Dunst</fullName>
        <actions>
            <name>MPro_Opportunity_Assigned_to_Cary_Dunst</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Product_Version__c</field>
            <operation>equals</operation>
            <value>Professional</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Mthly_Srch_Mktg_Spend__c</field>
            <operation>equals</operation>
            <value>&quot;$1,000,000 and above&quot;,&quot;$100,000 - $1,000,000&quot;</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingState</field>
            <operation>equals</operation>
            <value>Alabama,AL,Washington D.C.,District of Columbia,DC,Delaware,DE,Florida,FL,Georgia,GA,Maryland,MD,NC,North Carolina,SC,South Carolina,VA,Virginia,WV,West Virginia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>contains</operation>
            <value>Greer</value>
        </criteriaItems>
        <description>$100,000 and above – AE by territory</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MPro Owner Assignment%3A Dan Whymark - Europe</fullName>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3) AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Product_Version__c</field>
            <operation>equals</operation>
            <value>Professional</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>Albania,Andorra,Austria,Azerbaijan,Belarus,Belgium,Bosnia and Herzegovina,Bulgaria,Croatia,Czech Republic,Denmark,Estonia,Finland,France,Georgia,Germany,Greece,Hungary,Iceland,Italy,Kazakhstan,Latvia,Liechtenstein,Lithuania</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>Luxembourg,Macedonia,Malta,Moldova,Monaco,Montenegro,Netherlands,Norway,Poland,Portugal,Republic of Ireland,Romania,Russia,San Marino,Serbia,Slovakia,Slovenia,Spain,Sweden,Switzerland,Total,Turkey,Ukraine,United Kingdom,UK</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>contains</operation>
            <value>Greer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Mthly_Srch_Mktg_Spend__c</field>
            <operation>equals</operation>
            <value>&quot;$40,000 - $100,000&quot;,&quot;less than $10,000&quot;,&quot;$10,000 - $40,000&quot;</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MPro Owner Assignment%3A Greg Garbo</fullName>
        <actions>
            <name>MPro_Opportunity_Assigned_to_Greg_Garbo</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND (3 OR (4 AND 5)) AND 6</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Product_Version__c</field>
            <operation>equals</operation>
            <value>Professional</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Mthly_Srch_Mktg_Spend__c</field>
            <operation>equals</operation>
            <value>&quot;$1,000,000 and above&quot;,&quot;$100,000 - $1,000,000&quot;</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingState</field>
            <operation>notEqual</operation>
            <value>NV,Nevada,AZ,Arizona,UT,Utah,CO,Colorado,ID,Idaho</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingState</field>
            <operation>equals</operation>
            <value>ca,california</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingPostalCode</field>
            <operation>equals</operation>
            <value>93900</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>equals</operation>
            <value>Greer</value>
        </criteriaItems>
        <description>$100,000 and above – AE by territory</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MPro Owner Assignment%3A Marianne Coste - Europe</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Product_Version__c</field>
            <operation>equals</operation>
            <value>Professional</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>France,Spain,Portugal,Belgium,Luxembourg,Switzerland</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>equals</operation>
            <value>Greer</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MPro Owner Assignment%3A Mark Boyich</fullName>
        <actions>
            <name>MPro_Owner_Assignment_Mark_Boyich</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND (3 OR 4) AND 5</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Product_Version__c</field>
            <operation>equals</operation>
            <value>Professional</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Mthly_Srch_Mktg_Spend__c</field>
            <operation>equals</operation>
            <value>&quot;$40,000 - $100,000&quot;,&quot;less than $10,000&quot;,&quot;$10,000 - $40,000&quot;</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingState</field>
            <operation>equals</operation>
            <value>Ontario,ON,Quebec,QB,Wisconsin,WI,Michigan,MI,Illinois,IL,Indiana,IN,Ohio,OH,Maine,ME,New Hampshire,NH,Massachusetts,MA,Vermont,VT,New York,NY,Rhode Island,RI,Connecticut,CT,New Jersey,NJ,Pennsylvania,PA,Delaware,DE,Maryland,MD,Washington DC,DC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>equals</operation>
            <value>District of Columbia,Kentucky,KY,West Virginia,WV,Virginia,VA,Tennessee,TN,AR,Arkansas,Louisiana,LA,Mississippi,MS,Alabama,AL,North Carolina,NC,South Carolina,SC,Georgia,GA,Florida,FL</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>contains</operation>
            <value>Greer</value>
        </criteriaItems>
        <description>$0-$99,999 Monthly Search Marketing Spend
Territory - Eastern US</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MPro Owner Assignment%3A Nick Gill</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Product_Version__c</field>
            <operation>equals</operation>
            <value>Professional</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
            <value>American Samoa,Australia,Brunei,Burma,Cambodia,China,East Timor,Hong Kong,India,Indonesia,Japan,Laos,Macau,Malaysia,Mongolia,New Zealand,North Korea,Papua New Guinea,Philippines,Singapore,South Korea,Taiwan,Thailand,Vietnam</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Mthly_Srch_Mktg_Spend__c</field>
            <operation>equals</operation>
            <value>&quot;$40,000 - $100,000&quot;,&quot;$1,000,000 and above&quot;,&quot;$100,000 - $1,000,000&quot;</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>contains</operation>
            <value>Greer</value>
        </criteriaItems>
        <description>All MPro Opps in APAC</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MPro Owner Assignment%3A Sam Sheldon</fullName>
        <actions>
            <name>MPro_Owner_Assignment_Sam_Sheldon</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND (3 or 4) and 5</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Product_Version__c</field>
            <operation>equals</operation>
            <value>Professional</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Mthly_Srch_Mktg_Spend__c</field>
            <operation>equals</operation>
            <value>&quot;$40,000 - $100,000&quot;,&quot;less than $10,000&quot;,&quot;$10,000 - $40,000&quot;</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingState</field>
            <operation>equals</operation>
            <value>British Columbia,BC,Alberta,AB,Saskatchewan,SA,Manitoba,MB,Washington,WA,Oregon,OR,California,CA,Idaho,ID,Nevada,NV,Utah,UT,Arizona,AZ,New Mexico,NM,Colorado,CO,Wyoming,WY,Montana,MT,North Dakota,ND,South Dakota,SD,Nebraska,NE,Kansas,KS,Oklahoma,OK,Texas</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingState</field>
            <operation>equals</operation>
            <value>TX,Minnesota,MN,IA,Iowa,MO,Missouri,Hawaii,HI,Alaska,AK</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>contains</operation>
            <value>Greer</value>
        </criteriaItems>
        <description>$0-$99,999 Monthly Search Marketing Spend
Territory - Western US</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MPro Owner Assignment%3A Tony DeVitto</fullName>
        <actions>
            <name>MPro_Opportunity_Assigned_to_Tony_DeVitt</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Product_Version__c</field>
            <operation>equals</operation>
            <value>Professional</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Mthly_Srch_Mktg_Spend__c</field>
            <operation>equals</operation>
            <value>&quot;$1,000,000 and above&quot;,&quot;$100,000 - $1,000,000&quot;</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingState</field>
            <operation>equals</operation>
            <value>Alabama,AL,Washington D.C.,District of Columbia,DC,Delaware,DE,Florida,FL,Georgia,GA,Maryland,MD,NC,North Carolina,SC,South Carolina,VA,Virginia,WV,West Virginia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>contains</operation>
            <value>Greer</value>
        </criteriaItems>
        <description>$100,000 and above – AE by territory</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MPro Trial End</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Trial_End_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Trial End Date notification</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Opportunity.Trial_End_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Marin_Pro_trial_ended</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Opportunity.Trial_End_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>MPro Trial End 2</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Trial_End_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Trial End Date notification</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>MPro_trial_is_ending_soon</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Opportunity.Trial_End_Date__c</offsetFromField>
            <timeLength>-5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Manager email update</fullName>
        <actions>
            <name>Manager_email_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Manager_Email__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Modify Monthly Spend-%241%2C000%2C000 and above</fullName>
        <actions>
            <name>Modify_Monthly_Spend_1_000_000_and_abov</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Mthly_Srch_Mktg_Spend__c</field>
            <operation>equals</operation>
            <value>750.000 € and above,&quot;£600,000 and above&quot;</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Modify Monthly Spend-%2410%2C000-%2440%2C000-</fullName>
        <actions>
            <name>Modify_Monthly_Spend_10_000_40_000</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Mthly_Srch_Mktg_Spend__c</field>
            <operation>equals</operation>
            <value>&quot;£6,000 - £25,000&quot;,7.500 € - 30.000 €</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Modify Monthly Spend-%24100%2C000-%241%2C000%2C000</fullName>
        <actions>
            <name>Modify_Monthly_Spend_100_000_1_000_000</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Mthly_Srch_Mktg_Spend__c</field>
            <operation>equals</operation>
            <value>,&quot;$75,000-$1,000,000&quot;,&quot;£50,000 - £600,000&quot;,50.000 € - 750.000 €</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Modify Monthly Spend-%2440%2C000-%24100%2C000</fullName>
        <actions>
            <name>Modify_Monthly_Spend_40_000_100_000</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Mthly_Srch_Mktg_Spend__c</field>
            <operation>equals</operation>
            <value>30.000 € - 50.000 €,&quot;$40,000-$75,000&quot;,&quot;£25,000 - £50,000&quot;</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Modify Monthly Spend-Less Than 10%2C000</fullName>
        <actions>
            <name>Update_Monthly_Spend_less_than_10_000</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Mthly_Srch_Mktg_Spend__c</field>
            <operation>equals</operation>
            <value>&quot;Less than £6,000&quot;,Less than 7.500 €</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Opportunity on Customer Account</fullName>
        <active>true</active>
        <formula>AND(Account.RecordTypeId = &quot;012500000005TxJ&quot;,NOT(ISPICKVAL(Type_Detail__c,&quot;New Business: TrueUp&quot;)))</formula>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>New_Customer_Account_Opportunity_Email_to_Sales_Owner</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>OmittedfromAttainment</fullName>
        <actions>
            <name>OmitfromAttainment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Type_Detail__c</field>
            <operation>equals</operation>
            <value>AM: Credit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>Professional Services</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp moved from Won to Lost</fullName>
        <actions>
            <name>MPro_Opp_moved_from_Won_to_Lost</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Opp was moved from Closed Won to Closed Lost</description>
        <formula>AND(  ISCHANGED( StageName ), ISPICKVAL(PRIORVALUE( StageName), &quot;Closed Won&quot;),  ISPICKVAL( StageName, &quot;Closed Lost&quot;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Autoclose Close Lost Reason update</fullName>
        <actions>
            <name>Opportunity_Autoclose_Close_Lost_Reason</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Auto_Closed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Close %28UK%29</fullName>
        <actions>
            <name>EMEA_PC</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Opportunity_Closed_UK</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Probability</field>
            <operation>equals</operation>
            <value>95</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Region__c</field>
            <operation>equals</operation>
            <value>EMEA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Product_Version__c</field>
            <operation>notEqual</operation>
            <value>Professional</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type_Detail__c</field>
            <operation>equals</operation>
            <value>New Business: Existing Agency Contract,New Business: Full Term,New Business: Trial,AM: New Line of Business,PLA</value>
        </criteriaItems>
        <description>Closed Opportunity for EMEA</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity fallout</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Lead Passed,Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LastModifiedById</field>
            <operation>equals</operation>
            <value>chris lee,philip pillsbury,chris wine,andrew zakem,breck handy,arthur gross,weston hyter</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>equals</operation>
            <value>chris lee,philip pillsbury,chris wine,andrew zakem,breck handy,arthur gross,weston hyter</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Owner Team%2C Region%2C Office Stamp</fullName>
        <actions>
            <name>Update_Owner_Office</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Owner_Region</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Owner_Team</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Created 2014-05-19 per Jake, Christine to stamp opportunity with values from the owner&apos;s user record.</description>
        <formula>OR(ISNEW(), ISCHANGED( OwnerId))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PS Project Stage</fullName>
        <actions>
            <name>Project_Stage_PS_not_involved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.PS_Resources__c</field>
            <operation>equals</operation>
            <value>Not Required</value>
        </criteriaItems>
        <description>“Project Stage” to slip to “PS not involved” whenever the “PS Resources” is flipped to “Not Required”</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ProCasestoPhilTEMP</fullName>
        <actions>
            <name>UpdateOwnerPhil</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Product_Version__c</field>
            <operation>equals</operation>
            <value>Professional</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.CreatedById</field>
            <operation>equals</operation>
            <value>CS @ Marin Software</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Project Team Location APAC</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.ps_pm1__c</field>
            <operation>equals</operation>
            <value>Adam Wise,Terrell Holmes,Chin Lun,Patrick Hutchison</value>
        </criteriaItems>
        <description>This rule fills in the project location based on the location of the PM at the time of assignment.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Region - Central</fullName>
        <actions>
            <name>Opp_Region_Central</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>equals</operation>
            <value>David Fahey</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Region__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>All David Fahey Opps are set to Central</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Region - East</fullName>
        <actions>
            <name>Opp_Region_East</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>equals</operation>
            <value>Cameron Jackson</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Region__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>All Cameron Jackson Opps are set to East</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Region - West</fullName>
        <actions>
            <name>Opp_Region_West</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>equals</operation>
            <value>Jay Revels</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Region__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>All Jay Revels Opps are set to West</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Resources Not Needed for Pro</fullName>
        <actions>
            <name>Resources_Not_Needed_for_Pro</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Product_Version__c</field>
            <operation>equals</operation>
            <value>Professional</value>
        </criteriaItems>
        <description>This workflow designated the &quot;PS Resources&quot; field as &quot;Not Needed&quot; whenever the &quot;Product Version&quot; is set to &quot;Professional&quot;. This will improve PS ability to keep track of what opportunities have PS Projects associated to them.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Routing AU to Nick Gill</fullName>
        <actions>
            <name>Change_Record_Owner_to_Phil</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>contains</operation>
            <value>AU,Australia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Specific_Lead_Source__c</field>
            <operation>contains</operation>
            <value>Pro Free Trial</value>
        </criteriaItems>
        <description>Marin Pro Free Trial</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Target Account Updated%2FCreated</fullName>
        <actions>
            <name>Strategic_Account_Opportunity_Created_Edited</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Target_Account__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <description>Every time a Target Account is Edited or Created</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Trial End Date Autofill</fullName>
        <actions>
            <name>Trial_End_Date_2_weeks</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Trial_Start_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Autofills Trial End Date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UpdateAccountStatusActiveCustomer</fullName>
        <actions>
            <name>UpdateAccountStatusActiveCustomer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Probability</field>
            <operation>greaterOrEqual</operation>
            <value>100</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>New Business</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type_Detail__c</field>
            <operation>equals</operation>
            <value>New Business: Rollout,New Business: Trial,New Business: Full Term</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UpdateAccountStatusProspect</fullName>
        <actions>
            <name>Move_Account_Status_to_Prospect</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Status__c</field>
            <operation>equals</operation>
            <value>Hunter Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>New Business</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type_Detail__c</field>
            <operation>equals</operation>
            <value>New Business: Rollout,New Business: Trial,New Business: Full Term</value>
        </criteriaItems>
        <description>When an account in the Hunter Lead status has a New Business opportunity created the account status moved to prospect</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>MPro_trial_is_ending_soon</fullName>
        <assignedTo>ppillsbury@marinsoftware.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>-5</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Opportunity.Trial_End_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Marin Pro trial is ending soon</subject>
    </tasks>
    <tasks>
        <fullName>Marin_Pro_trial_ended</fullName>
        <assignedTo>ppillsbury@marinsoftware.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Please change Stage to either Signed Order Form Returned for a continuing customer or Closed Lost if canceled.</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Opportunity.Trial_End_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Marin Pro trial has ended</subject>
    </tasks>
</Workflow>
