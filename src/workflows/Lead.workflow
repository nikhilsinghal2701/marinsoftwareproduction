<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Alert_to_Bobby_of_DQ_Leads_Happy_with_Agency</fullName>
        <description>Alert to Bobby of DQ Leads Happy with Agency</description>
        <protected>false</protected>
        <recipients>
            <recipient>azakem@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>bhollis@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>eafable@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Marketing_Operations/EA_Bobby_DQ_Leads</template>
    </alerts>
    <alerts>
        <fullName>Internal_Lead_Notice_for_Pro_Demo</fullName>
        <ccEmails>marinprodemo@marinsoftware.com</ccEmails>
        <description>Internal Lead Notice for Pro Demo</description>
        <protected>false</protected>
        <senderType>DefaultWorkflowUser</senderType>
        <template>WebtoLead/Internal_Lead_Notice_for_Pro_Demo</template>
    </alerts>
    <fieldUpdates>
        <fullName>Activity_Score_Auto_Fill</fullName>
        <field>Activity_Score_Hidden__c</field>
        <formula>Activity_Score__c</formula>
        <name>Activity Score Auto Fill</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Auto_duplicate_Situation</fullName>
        <description>Autofill the Situation (hidden) for conversion of Leads to Contacts</description>
        <field>Situation_hidden__c</field>
        <formula>Situation__c</formula>
        <name>Auto duplicate Situation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Autofill_Lead_Grade_hidden</fullName>
        <field>Lead_Grade_hidden__c</field>
        <formula>Lead_Grade__c</formula>
        <name>Autofill Lead Grade (hidden)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Autofill_Lead_Score_hidden</fullName>
        <field>Lead_Score_hidden__c</field>
        <formula>mkto2__Lead_Score__c</formula>
        <name>Autofill Lead Score (hidden)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Data_com_Checkbox</fullName>
        <field>Data_com_Import__c</field>
        <literalValue>1</literalValue>
        <name>Data.com Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Created</fullName>
        <field>Lead_Created__c</field>
        <formula>CreatedDate</formula>
        <name>Lead Created</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Fill_Data_com_Key</fullName>
        <field>Jigsaw</field>
        <formula>jigsaw_clean__Jigsaw_Id__c</formula>
        <name>Lead- Fill Data.com Key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Product_Version_field_Professio</fullName>
        <field>Product_Version__c</field>
        <literalValue>Professional</literalValue>
        <name>Lead - Product Version field - Professio</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Reassignment_to_PPillsbury</fullName>
        <description>Reassigns Marin Pro Abandoners to Phil</description>
        <field>OwnerId</field>
        <lookupValue>ppillsbury@marinsoftware.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Lead Reassignment to PPillsbury</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Source_First_Autofill</fullName>
        <field>Lead_Source_First_hidden__c</field>
        <formula>text(Lead_Source_First__c)</formula>
        <name>Lead Source First Autofill</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Status_Unqualified</fullName>
        <description>If a DQ Date has been entered, an Unqualified Lead Status is set</description>
        <field>Status</field>
        <literalValue>Unqualified</literalValue>
        <name>Lead Status = Unqualified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pro_Lead_CTA_Option_Demo</fullName>
        <description>1.	If customer comes from “Specific Lead Source” noted below then “Pro Lead CTA” would be Demo
a.	Marin SEM Demo LP 1
b.	Marin SEM Demo LP 2
c.	Marin SEM Demo LP 3
d.	LinkedIn Ads</description>
        <field>Pro_Lead_CTA__c</field>
        <literalValue>Demo</literalValue>
        <name>Pro Lead CTA (Option: Demo)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Researcher_Lead_Source_Autofill</fullName>
        <field>Researcher_Lead_Source_hidden__c</field>
        <formula>Researcher_Lead_Source__c</formula>
        <name>Researcher Lead Source Autofill</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_MQL_Reached_Date_Field_Lead</fullName>
        <field>MQL_Reached_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set MQL Reached Date Field (Lead)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Specific_Lead_Source_Original_autofill</fullName>
        <field>Specific_Lead_Source_Original_hidden__c</field>
        <formula>Specific_Lead_Source_Original__c</formula>
        <name>Specific Lead Source Original autofill</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Specific_Lead_Source_Recent_Autofill</fullName>
        <field>Specific_Lead_Source_Recent_hidden__c</field>
        <formula>Specific_Lead_Source_Recent__c</formula>
        <name>Specific Lead Source Recent Autofill</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_MQL_Reached_Date_Last</fullName>
        <field>MQL_Reached_Date_Last__c</field>
        <formula>TODAY()</formula>
        <name>Update MQL Reached Date Last</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Web_to_Lead_as_Enterprise</fullName>
        <field>Product_Version__c</field>
        <literalValue>Enterprise</literalValue>
        <name>Web-to-Lead as Enterprise</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Web_to_Lead_as_Professional</fullName>
        <field>Product_Version__c</field>
        <literalValue>Professional</literalValue>
        <name>Web-to-Lead as Professional</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>auto_populate_specific_lead_source_hidde</fullName>
        <description>recreation of auto populating field update that was removed to update the specific lead source hidden field type</description>
        <field>Specific_Lead_Source_hidden__c</field>
        <formula>Specific_Lead_Source__c</formula>
        <name>auto populate specific lead source hidde</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Activity Score Auto Fill</fullName>
        <actions>
            <name>Activity_Score_Auto_Fill</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Activity Score Auto Filled for conversion into 2 objects</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Autofill Lead Grade %28hidden%29</fullName>
        <actions>
            <name>Autofill_Lead_Grade_hidden</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Autofills the Lead Grade (hidden) for when Converting Leads to Contacts</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Autofill Lead Score %28hidden%29</fullName>
        <actions>
            <name>Autofill_Lead_Score_hidden</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Autofills the Lead Score (hidden) for when Converting Leads to Contacts</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Autofill Lead Source First %28hidden%29</fullName>
        <actions>
            <name>Lead_Source_First_Autofill</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Autofills the Lead Source First (hidden) for when Converting Leads to Contacts</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Autofill Researcher Lead Source %28hidden%29</fullName>
        <actions>
            <name>Researcher_Lead_Source_Autofill</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Autofills the Researcher Lead Source (hidden) for when Converting Leads to Contacts</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Autofill Situation %28hidden%29</fullName>
        <actions>
            <name>Auto_duplicate_Situation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Autofills the Situation (hidden) for when Converting Leads to Contacts</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Autofill Specific Lead Source %28hidden%29</fullName>
        <actions>
            <name>auto_populate_specific_lead_source_hidde</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Autofills the Specific Lead Source (hidden) for when Converting Leads to Contacts</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Autofill Specific Lead Source Original %28hidden%29</fullName>
        <actions>
            <name>Specific_Lead_Source_Original_autofill</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Autofills the Specific Lead Source Original (hidden) for when Converting Leads to Contacts</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Autofill Specific Lead Source Recent %28hidden%29</fullName>
        <actions>
            <name>Specific_Lead_Source_Recent_Autofill</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Autofills the Specific Lead Source Recent (hidden) for when Converting Leads to Contacts</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>C%26S Task</fullName>
        <actions>
            <name>C_S_selected_lead_to_call</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.ConnectandSell__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Task is created for new C&amp;S checked leads</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Data%2Ecom Checkbox</fullName>
        <actions>
            <name>Data_com_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Jigsaw</value>
        </criteriaItems>
        <description>If Jigsaw is the Lead Source, the Data.com checkbox is marked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Disqualified Date sets Unqualified Status</fullName>
        <actions>
            <name>Lead_Status_Unqualified</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Disqualified_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EA_Bobby_DQ_Lead_Workflow</fullName>
        <actions>
            <name>Alert_to_Bobby_of_DQ_Leads_Happy_with_Agency</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Unqualified_Reasons__c</field>
            <operation>includes</operation>
            <value>Happy with agency</value>
        </criteriaItems>
        <description>This is used to send Bobby the email for Leads that DQ with the reason &apos;Happy with agency&apos;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead %E2%80%93 Fill Data%2Ecom Id</fullName>
        <actions>
            <name>Lead_Fill_Data_com_Key</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Jigsaw</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.jigsaw_clean__Jigsaw_Id__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead Created</fullName>
        <actions>
            <name>Lead_Created</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Lead_Created__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Marin Pro Lead Assignment</fullName>
        <actions>
            <name>Lead_Reassignment_to_PPillsbury</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Product_Version__c</field>
            <operation>equals</operation>
            <value>Professional</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>CS @ Marin Software</value>
        </criteriaItems>
        <description>Leads which have been created by the App after abandoning Step 2 are assigned to Phil.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Product Version %28Enterprise%29</fullName>
        <actions>
            <name>Web_to_Lead_as_Enterprise</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Search_Marketing_Spend__c</field>
            <operation>equals</operation>
            <value>&quot;$1,000,000-$2,000,000&quot;,&quot;$2,000,000 and above&quot;,&quot;$1,000,000 and above&quot;,&quot;$100,000-$500,000&quot;,&quot;$500,000-$1,000,000&quot;,&quot;$100,000-$1,000,000&quot;</value>
        </criteriaItems>
        <description>Product Version field gets updated to Enterprise if spend on web-to-lead form is $100,000 or greater</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Product Version %28Professional%29</fullName>
        <actions>
            <name>Web_to_Lead_as_Professional</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Search_Marketing_Spend__c</field>
            <operation>equals</operation>
            <value>&quot;Less than $20,000&quot;,&quot;$10,000-$40,000&quot;,&quot;Less than $10,000&quot;,&quot;$10,000-$100,000&quot;,&quot;$40,000-$100,000&quot;,&quot;$20,000-$100,000&quot;</value>
        </criteriaItems>
        <description>Product Version gets updated to Professional if spend in web to lead form is less than $100,000</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set MQL Last Reached Date %28Lead%29</fullName>
        <actions>
            <name>Update_MQL_Reached_Date_Last</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Marketing_Qualified_Reason__c</field>
            <operation>equals</operation>
            <value>Target Profile,Engaged,Engaged Target Profile</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Email</field>
            <operation>notEqual</operation>
            <value>dkim@marinsoftware.com</value>
        </criteriaItems>
        <description>Sets MQL Last Reached Date field to the date that the lead reached MQL status.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set MQL Reached Date %28Lead%29</fullName>
        <actions>
            <name>Set_MQL_Reached_Date_Field_Lead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Set MQL Reached Date field when a lead first reaches an A or B grade.</description>
        <formula>AND( ISBLANK(MQL_Reached_Date__c), OR(UPPER(Lead_Grade__c) = &apos;A&apos;, UPPER(Lead_Grade__c) = &apos;B&apos;), UPPER(PRIORVALUE(Lead_Grade__c)) != &apos;A&apos;, UPPER(PRIORVALUE(Lead_Grade__c)) != &apos;B&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set MQL Reached Date %28Lead%29 v2</fullName>
        <actions>
            <name>Set_MQL_Reached_Date_Field_Lead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set MQL Reached Date field when a contact is marketing qualified as engaged or target profile</description>
        <formula>AND(ISCHANGED(Marketing_Qualified__c),Marketing_Qualified__c, ISBLANK(MQL_Reached_Date__c), $User.Email &lt;&gt; &apos;dkim@marinsoftware.com&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update MQL Reached Date Last</fullName>
        <active>false</active>
        <formula>NOT(ISPICKVAL( Marketing_Qualified_Reason__c , &quot;Disqualified&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>C_S_selected_lead_to_call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>C&amp;S: selected lead to call</subject>
    </tasks>
</Workflow>
