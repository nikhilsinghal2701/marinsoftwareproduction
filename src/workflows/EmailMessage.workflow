<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Change_Closed_to_New</fullName>
        <field>Status</field>
        <literalValue>New</literalValue>
        <name>Change Status from Closed to New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_Reason_Client_ReOpened</fullName>
        <description>When a client replies to a closed ticket, then Pending Reason updates to &quot;Client Re-Opened&quot;</description>
        <field>Pending_reason__c</field>
        <literalValue>Client Re-Opened</literalValue>
        <name>Set Pending Reason to Client Re-Opened</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>back_to_in_progress</fullName>
        <field>Status</field>
        <literalValue>Customer - reopened</literalValue>
        <name>status change of case back to in progres</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Re-Open Closed Tickets - Support Services</fullName>
        <actions>
            <name>Change_Closed_to_New</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Pending_Reason_Client_ReOpened</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_status__c</field>
            <operation>contains</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>This rule is designed to re-open a closed ticket when a client follows up via email. The status will update to New and co-owner remains the same. The Support Specialist can then retrieve their case from the Support queue.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>re-open ticket back to in progress on client reply</fullName>
        <actions>
            <name>back_to_in_progress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed - Pending</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>re-opens ticket back to in progress when in pending status</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
