<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CM_Last_Response_Date</fullName>
        <description>Update the last response date of a campaign member</description>
        <field>Last_Response_Date__c</field>
        <formula>TODAY()</formula>
        <name>CM Last Response Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CM_Number_of_Touches</fullName>
        <field>Number_of_touches__c</field>
        <formula>Number_of_touches__c + 1</formula>
        <name>CM Number of Touches</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CM_Revert_Status_to_Responded</fullName>
        <description>Reverts the &quot;touched&quot; status back to &quot;Responded&quot; after recording last touch date and number of touches.</description>
        <field>Status</field>
        <literalValue>Responded</literalValue>
        <name>CM Revert Status to Responded</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Campaign_Member_MQL_True</fullName>
        <description>Field Update to update lead/contact MQL status on campaign member.</description>
        <field>Maketing_Qualified__c</field>
        <literalValue>1</literalValue>
        <name>Campaign Member MQL True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GradeCaptureAFTER_A</fullName>
        <description>Captures the lead/contact &quot;Grade = A&quot; after campaign association</description>
        <field>After_Grade_Snapshot__c</field>
        <literalValue>A</literalValue>
        <name>GradeCaptureAFTER_A</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GradeCaptureAFTER_B</fullName>
        <description>Captures the lead/contact &quot;Grade = B&quot; after campaign association</description>
        <field>After_Grade_Snapshot__c</field>
        <literalValue>B</literalValue>
        <name>GradeCaptureAFTER_B</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GradeCaptureAFTER_C</fullName>
        <description>Captures the lead/contact &quot;Grade = C&quot; after campaign association</description>
        <field>After_Grade_Snapshot__c</field>
        <literalValue>C</literalValue>
        <name>GradeCaptureAFTER_C</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GradeCaptureAFTER_D</fullName>
        <description>Captures the lead/contact &quot;Grade = D&quot; after campaign association</description>
        <field>After_Grade_Snapshot__c</field>
        <literalValue>D</literalValue>
        <name>GradeCaptureAFTER_D</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GradeCaptureBEFORE_A</fullName>
        <description>Captures the lead/contact &quot;Grade = A&quot; before campaign association</description>
        <field>Before_Grade_Snapshot__c</field>
        <literalValue>A</literalValue>
        <name>GradeCaptureBEFORE_A</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GradeCaptureBEFORE_B</fullName>
        <description>Captures the lead/contact &quot;Grade = B&quot; before campaign association</description>
        <field>Before_Grade_Snapshot__c</field>
        <literalValue>B</literalValue>
        <name>GradeCaptureBEFORE_B</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GradeCaptureBEFORE_C</fullName>
        <description>Captures the lead/contact &quot;Grade = C&quot; before campaign association</description>
        <field>Before_Grade_Snapshot__c</field>
        <literalValue>C</literalValue>
        <name>GradeCaptureBEFORE_C</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GradeCaptureBEFORE_D</fullName>
        <description>Captures the lead/contact &quot;Grade = D&quot; before campaign association</description>
        <field>Before_Grade_Snapshot__c</field>
        <literalValue>D</literalValue>
        <name>GradeCaptureBEFORE_D</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Campaign Member MQL Status</fullName>
        <actions>
            <name>Campaign_Member_MQL_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Contact.Marketing_Qualified__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Marketing_Qualified__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Updates campaign member to identify MQL status of associated lead/contact.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Count Multiple Touches</fullName>
        <actions>
            <name>CM_Last_Response_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CM_Number_of_Touches</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CM_Revert_Status_to_Responded</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CampaignMember.Status</field>
            <operation>equals</operation>
            <value>Touched</value>
        </criteriaItems>
        <description>Used to track multiple touches, including last touch date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>GradeCapture_A</fullName>
        <actions>
            <name>GradeCaptureBEFORE_A</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Lead.Lead_Grade__c</field>
            <operation>equals</operation>
            <value>A</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Lead_Grade__c</field>
            <operation>equals</operation>
            <value>A</value>
        </criteriaItems>
        <criteriaItems>
            <field>CampaignMember.HasResponded</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Captures &quot;Grade = A &quot; of lead/contact member</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>GradeCaptureAFTER_A</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>GradeCapture_B</fullName>
        <actions>
            <name>GradeCaptureBEFORE_B</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Lead.Lead_Grade__c</field>
            <operation>equals</operation>
            <value>B</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Lead_Grade__c</field>
            <operation>equals</operation>
            <value>B</value>
        </criteriaItems>
        <criteriaItems>
            <field>CampaignMember.HasResponded</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Captures &quot;Grade = B &quot; of lead/contact member</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>GradeCaptureAFTER_B</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>GradeCapture_C</fullName>
        <actions>
            <name>GradeCaptureBEFORE_C</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Lead.Lead_Grade__c</field>
            <operation>equals</operation>
            <value>C</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Lead_Grade__c</field>
            <operation>equals</operation>
            <value>C</value>
        </criteriaItems>
        <criteriaItems>
            <field>CampaignMember.HasResponded</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Captures &quot;Grade = C &quot; of lead/contact member</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>GradeCaptureAFTER_C</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>GradeCapture_D</fullName>
        <actions>
            <name>GradeCaptureBEFORE_D</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Lead.Lead_Grade__c</field>
            <operation>equals</operation>
            <value>D</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Lead_Grade__c</field>
            <operation>equals</operation>
            <value>D</value>
        </criteriaItems>
        <criteriaItems>
            <field>CampaignMember.HasResponded</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Captures &quot;Grade = D &quot; of lead/contact member</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>GradeCaptureAFTER_D</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
