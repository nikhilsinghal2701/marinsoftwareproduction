<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Insight_Updated</fullName>
        <description>Insight Updated</description>
        <protected>false</protected>
        <recipients>
            <field>AM_AD_del__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/Insight_Updated</template>
    </alerts>
    <rules>
        <fullName>Insight updated</fullName>
        <actions>
            <name>Insight_Updated</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>OR(

AND(
ISNEW(),
OR(
NOT(ISNULL(Date_Delivered_to_Client__c )) ,
NOT(ISNULL( Date_Implemented_by_Client__c )) ,
NOT(ISNULL( Date_Lift_Measured__c )) 
)),

OR(
ISCHANGED( Date_Delivered_to_Client__c ), 
ISCHANGED( Date_Implemented_by_Client__c ) ,
ISCHANGED( Date_Lift_Measured__c ) 
))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
