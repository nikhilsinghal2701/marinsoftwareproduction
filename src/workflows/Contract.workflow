<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Contract_Cancellation_Email_Alert</fullName>
        <description>Contract Cancellation Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>GlobalDirectorofInsideSales</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/Canceled_Contract</template>
    </alerts>
    <alerts>
        <fullName>Contract_End_Notification</fullName>
        <description>Contract End Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>Director_of_Direct_Accounts_Premium_AMER</recipient>
            <type>role</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Workflow_Templates/Contract_Expiration_Notice</template>
    </alerts>
    <alerts>
        <fullName>Contract_Owner_Change</fullName>
        <description>Contract Owner Change</description>
        <protected>false</protected>
        <recipients>
            <recipient>claszlo@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>clee1@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/Contract_Owner_Change</template>
    </alerts>
    <alerts>
        <fullName>Contract_Record_Change_New_or_updated_Customer_Status_or_Pricing_Term</fullName>
        <description>Contract Record Change (Enterprise)  - New or updated Customer Status or Pricing &amp; Term</description>
        <protected>false</protected>
        <recipients>
            <recipient>claszlo@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/Contract_Status_Pricing_Changed</template>
    </alerts>
    <alerts>
        <fullName>Marin_Pro_cancellations_alert</fullName>
        <ccEmails>salesops@marinsoftware.com</ccEmails>
        <description>Marin Pro cancellations alert</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/Marin_Pro_Cancellation_Notice</template>
    </alerts>
    <fieldUpdates>
        <fullName>Contract_End</fullName>
        <description>Copies Contract End Date into Contract End field for Contract Expiration notice</description>
        <field>Contract_End__c</field>
        <formula>EndDate</formula>
        <name>Contract End</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Un_check_Billing_Rate_s_Review</fullName>
        <description>This will un-check this box</description>
        <field>Billing_Rate_s_Review__c</field>
        <literalValue>0</literalValue>
        <name>Un-check Billing Rate(s) Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UncheckBillingRatesReview</fullName>
        <field>Billing_Rate_s_Review__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Billing Rate(s) Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Billing_Rate_s_Review</fullName>
        <field>Billing_Rate_s_Review__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Billing Rate(s) Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Contract_Renewal_WS</fullName>
        <apiVersion>30.0</apiVersion>
        <endpointUrl>https://login.salesforce.com/services/Soap/class/contractRenewalWS</endpointUrl>
        <fields>AccountId</fields>
        <fields>Account_Executive__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>mklinski@marinsoftware.com</integrationUser>
        <name>Contract Renewal WS</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Billing Rate Record%28s%29 to be Created%2FEdited</fullName>
        <actions>
            <name>Please_Review_Contract_and_Create_Billing_Rate_Record_s</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contract.Billing_Rate_s_Review__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>UncheckBillingRatesReview</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Contract Cancellation Alert</fullName>
        <actions>
            <name>Contract_Cancellation_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contract.Customer_Status__c</field>
            <operation>equals</operation>
            <value>Canceled Service</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract.EndDate</field>
            <operation>lessOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Contract_Renewal_WS</name>
                <type>OutboundMessage</type>
            </actions>
            <offsetFromField>Contract.EndDate</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Contract End Date</fullName>
        <actions>
            <name>Contract_End</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract.Customer_Status__c</field>
            <operation>notEqual</operation>
            <value>Canceled Service</value>
        </criteriaItems>
        <description>Copies Contract End Date into Contract End field</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contract Expiration Notice</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Contract.OwnerId</field>
            <operation>contains</operation>
            <value>Arokoyo,DeLotto,Hafiz,Law,Messerle,Scott,Yachouh</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract.Customer_Status__c</field>
            <operation>notEqual</operation>
            <value>Canceled Service</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract.Contract_End__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>Notifies the Director of Accounts that a contract is expiring in 10 days.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Contract_End_Notification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Contract.Contract_End__c</offsetFromField>
            <timeLength>-10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Contract Owner Change</fullName>
        <actions>
            <name>Contract_Owner_Change</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Notifies of a Contract Owner change</description>
        <formula>AND( 
ISCHANGED( OwnerId ),
 ISPICKVAL(Product_Version__c, &quot;Enterprise&quot;)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Marin Pro Cancellation Notice</fullName>
        <actions>
            <name>Marin_Pro_cancellations_alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract.Product_Version__c</field>
            <operation>equals</operation>
            <value>Professional</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract.Customer_Status__c</field>
            <operation>equals</operation>
            <value>Canceled Service</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New or Updated Contract</fullName>
        <actions>
            <name>Contract_Record_Change_New_or_updated_Customer_Status_or_Pricing_Term</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Notifies Finance of a new Contract, Pricing &amp; Term, or Customer Status changes.</description>
        <formula>AND((ISNEW()  || ISCHANGED(Customer_Status__c) ||  ISCHANGED(Pricing_Term__c )), ISPICKVAL (Product_Version__c, &quot;Enterprise&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Billing_Rate_Record_s_to_be_created</fullName>
        <assignedTo>eswenson@marinsoftware.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Please review updated Contract record and create new Billing Rate Type
 
Thank you!</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Billing Rate Record(s) to be created</subject>
    </tasks>
    <tasks>
        <fullName>Create</fullName>
        <assignedTo>eswenson@marinsoftware.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>New/Renewed Contract - Billing Rate Records need to be created</description>
        <dueDateOffset>-3</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Contract.Contract_Start__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Create</subject>
    </tasks>
    <tasks>
        <fullName>Please_Review_Contract_and_Create_Billing_Rate_Record_s</fullName>
        <assignedTo>eswenson@marinsoftware.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Please Review/Create Billing Rate Record(s)

Thank you!</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Please Review Contract and Create Billing Rate Record(s)</subject>
    </tasks>
</Workflow>
