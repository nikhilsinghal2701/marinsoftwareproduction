<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>FieldUpdate_Maintenance</fullName>
        <field>Service_Performed__c</field>
        <literalValue>Maintenance</literalValue>
        <name>FieldUpdate: Maintenance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_Detailed_Time_Presales</fullName>
        <field>Detailed_Time__c</field>
        <literalValue>Pre-Sales Activities</literalValue>
        <name>Field Update: Detailed Time Presales</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Marin_User_Department</fullName>
        <description>Pulls the Marin user department from the users details page.</description>
        <field>Marin_User_Department__c</field>
        <formula>User__r.Department</formula>
        <name>Marin User Department</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Marin_User_Region</fullName>
        <description>Pulls in the User region that is located on the user detail page.</description>
        <field>Marin_User_Region__c</field>
        <formula>text(User__r.User_Region__c)</formula>
        <name>Marin User Region</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Services_Performed_Internal</fullName>
        <field>Service_Performed__c</field>
        <literalValue>Internal</literalValue>
        <name>Services Performed: Internal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Field Update%3A Detailed Time Presales</fullName>
        <actions>
            <name>Field_Update_Detailed_Time_Presales</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow updates the detailed time to Pre-Sales Activities whenever an integration is being worked on during presales.</description>
        <formula>AND(
ISPICKVAL( Services_Sub_Type__c  , &quot;Integration&quot;),
ISPICKVAL( Integration__r.Project_Stage__c , &quot;Presales&quot;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Service Performed - Maintenance</fullName>
        <actions>
            <name>FieldUpdate_Maintenance</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule changes the Services Performed from External to Maintenance whenever the Integration Classification is Existing Customer - Maintenance (ECM)</description>
        <formula>ISPICKVAL( Integration__r.Project_Classification__c , &quot;Existing Customer - Maintenance (ECM)&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Services Performed - Internal</fullName>
        <actions>
            <name>Services_Performed_Internal</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Changes Service Performed to Internal</description>
        <formula>AND( 
OR(
ISPICKVAL( Integration__r.Project_Type__c , &quot;Internal&quot;),
ISPICKVAL(Integration__r.Project_Classification__c , &quot;Internal&quot;)
),

NOT( ISPICKVAL(Integration__r.Project_Classification__c , &quot;Existing Customer - Maintenance (ECM)&quot;)
))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A User Data</fullName>
        <actions>
            <name>Marin_User_Department</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Marin_User_Region</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule adds the Marin User Region and Marin User Department to the time sheet.</description>
        <formula>User__c &lt;&gt; null || ISCHANGED(User__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
