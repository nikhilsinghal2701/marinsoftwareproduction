<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Accout_Partner_Questionnaire</fullName>
        <description>When workflow fires this partner certification email alert will be sent.</description>
        <protected>false</protected>
        <recipients>
            <recipient>Primary Integration Architect</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary Marin Business Contact</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <field>Survey_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Professional_Services/Partner_Certification_Questionnaire_v2</template>
    </alerts>
    <alerts>
        <fullName>AlertAccOwnerRatingChange</fullName>
        <description>AlertAccOwnerRatingChange</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Ops/AccountRatingChange</template>
    </alerts>
    <alerts>
        <fullName>Alert_to_Bobby_of_Acct_with_Through_An_Agency_True</fullName>
        <description>Alert to Bobby of Acct with Through An Agency = True</description>
        <protected>false</protected>
        <recipients>
            <recipient>azakem@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>bhollis@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>eafable@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pearl.tin@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Marketing_Operations/EA_Bobby_ACCT_Agency_Alert</template>
    </alerts>
    <alerts>
        <fullName>EA_Internal_Bobby_HunterLead_Check_Alert</fullName>
        <description>EA_Internal_Bobby_HunterLead_Check_Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>bhollis@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Marketing_Operations/EA_Internal_Bobby_Account_Notification</template>
    </alerts>
    <alerts>
        <fullName>EA_Notification_Strat_Account</fullName>
        <description>EA_Notification_Strat_Account</description>
        <protected>false</protected>
        <recipients>
            <recipient>bhollis@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Marketing_Operations/EA_Bobby_Strat_Account_NA_APAC</template>
    </alerts>
    <alerts>
        <fullName>Email_Publisher_Profile_Documentation</fullName>
        <description>Email: Publisher Profile Documentation</description>
        <protected>false</protected>
        <recipients>
            <recipient>Primary Integration Architect</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Primary Marin Business Contact</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <field>Survey_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Professional_Services/Publisher_Profile_Questionnaire</template>
    </alerts>
    <alerts>
        <fullName>No_Customer_Alert</fullName>
        <description>No Customer Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>mgray@marinsoftware.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Workflow_Templates/No_Customer_Status_Update</template>
    </alerts>
    <fieldUpdates>
        <fullName>Account_Customer_Record_Type</fullName>
        <description>Account page layout specifically for Customer Accounts.</description>
        <field>RecordTypeId</field>
        <lookupValue>Customer_Account</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Account_Customer Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Fill_Data_com_Key</fullName>
        <field>Jigsaw</field>
        <formula>jigsaw_clean__Jigsaw_Id__c</formula>
        <name>Account- Fill Data.com Key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Client_Health_Status_Update_Date_field</fullName>
        <field>Client_Health_Status_Update_Date__c</field>
        <formula>TODAY()</formula>
        <name>Client Health Status Update Date field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Customer_Account</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Customer_Account</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Customer Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Do_Not_Disturb_Uncheck</fullName>
        <description>Unchecks this field after 30 days.</description>
        <field>Do_Not_Contact_Marketing__c</field>
        <literalValue>0</literalValue>
        <name>Do Not Disturb Uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Do_Not_Disturb_Update</fullName>
        <description>Updates the date field to TODAY&apos;s date.</description>
        <field>Do_Not_Disturb_Date__c</field>
        <formula>TODAY() + 30</formula>
        <name>Do Not Disturb Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_Current_Date</fullName>
        <field>Spend_Last_Updated__c</field>
        <formula>TODAY()</formula>
        <name>Field Update: Current Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LH_Last_Updated_Auto</fullName>
        <description>Updates the Lighthouse field for modifications automatically as opposed to relying on users to enter the date.</description>
        <field>LH_Last_Updated_Date__c</field>
        <formula>TODAY()</formula>
        <name>LH Last Updated Auto</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tier_Bronze</fullName>
        <description>Bronze &lt; $250K</description>
        <field>Tier__c</field>
        <literalValue>Bronze</literalValue>
        <name>Tier: Bronze</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tier_Gold</fullName>
        <description>Gold $500K - $1MM</description>
        <field>Tier__c</field>
        <literalValue>Gold</literalValue>
        <name>Tier: Gold</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tier_Platinum</fullName>
        <description>Platinum &gt;$1MM</description>
        <field>Tier__c</field>
        <literalValue>Platinum</literalValue>
        <name>Tier: Platinum</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tier_Silver</fullName>
        <description>Silver $250K - $500K</description>
        <field>Tier__c</field>
        <literalValue>Silver</literalValue>
        <name>Tier: Silver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateAERatingD</fullName>
        <field>Rating</field>
        <literalValue>D</literalValue>
        <name>UpdateAERatingD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateRecordTypeCustomerAccount</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Customer_Account</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>UpdateRecordTypeCustomerAccount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateRecordtypeProspectAccount</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Prospect_Account</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>UpdateRecordtypeProspectAccount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Account %E2%80%93 Fill Data%2Ecom Id</fullName>
        <actions>
            <name>Account_Fill_Data_com_Key</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Jigsaw</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.jigsaw_clean__Jigsaw_Id__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account_Prospect to Customer</fullName>
        <actions>
            <name>Account_Customer_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Status__c</field>
            <operation>equals</operation>
            <value>Active Customer</value>
        </criteriaItems>
        <description>When an Account moves from Prospect to Active, the record type should change only once.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Client Health Date Update</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.Client_Health_Status_Update_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Client_Health_Status__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Client Health Status Update Date field update</fullName>
        <actions>
            <name>Client_Health_Status_Update_Date_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR
(
NOT( ISPICKVAL (Client_Health_Status__c, &quot;&quot;)),
ISCHANGED( Client_Health_Status__c)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EA_Bobby_Strat_Account_NA_APAC_Notification</fullName>
        <actions>
            <name>EA_Notification_Strat_Account</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Per Bobby&apos;s request</description>
        <formula>AND(
OR(ISPICKVAL(Region__c,&quot;NA&quot;),ISPICKVAL(Region__c,&quot;APAC&quot;)),
PRIORVALUE(Strategic_Account__c ) &lt;&gt; Strategic_Account__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EA_Bobby_Through_Agency_Account</fullName>
        <actions>
            <name>Alert_to_Bobby_of_Acct_with_Through_An_Agency_True</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Through_an_Agency__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Notify Bobby of Accounts that get actively marked as Through An Agency</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EA_Internal_Bobby_HunterLead_Check</fullName>
        <actions>
            <name>EA_Internal_Bobby_HunterLead_Check_Alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(
PRIORVALUE(OwnerId)&lt;&gt;$User.Id,
$User.Id &lt;&gt;&quot;00550000001jH9O&quot;,
$User.Id &lt;&gt;&quot;00550000001lkyZ&quot;,
$User.Id &lt;&gt;&quot;00550000001lcPC&quot;,
$User.Id &lt;&gt;&quot;00550000001ihJI&quot;,
$User.Id &lt;&gt;&quot;00550000001k0ju&quot;,
$User.Id &lt;&gt;&quot;005500000010Thb&quot;,
$User.Id &lt;&gt;&quot;00550000001lcOx&quot;,
$User.Id &lt;&gt;&quot;00550000001jqPG&quot;,
$User.Id &lt;&gt;&quot;00550000001llAG&quot;,
$User.Id &lt;&gt;&quot;00550000001lfsv&quot;,
$User.Id &lt;&gt;&quot;00550000001j9MZ&quot;,
$User.Id &lt;&gt;&quot;00550000001lcOs&quot;,
$User.Id &lt;&gt;&quot;00550000001kBXG&quot;,
$User.Id &lt;&gt;&quot;00550000001k3es&quot;,
$User.Id &lt;&gt;&quot;00550000001jcrE&quot;,
$User.Id &lt;&gt;&quot;00550000001jq3j&quot;,
$User.Id &lt;&gt;&quot;00550000001lHbv&quot;,
$User.Id &lt;&gt;&quot;00550000001IOKZ&quot;,
$User.Id &lt;&gt;&quot;00550000001DBbH&quot;,
$User.Id &lt;&gt;&quot;00550000001kPNL&quot;,
$User.Id &lt;&gt;&quot;00550000001lVdM&quot;,
$User.Id &lt;&gt;&quot;00550000001kDMe&quot;,
$User.Id&lt;&gt;&quot;00550000001lDvh&quot;,
OR(ISPICKVAL(Account_Status__c,&quot;Hunter Lead&quot;)
,ISPICKVAL(PRIORVALUE(Account_Status__c),&quot;Hunter Lead&quot;))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EA_LH_Last_Updated_Auto</fullName>
        <actions>
            <name>LH_Last_Updated_Auto</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(
ISCHANGED( LH_Bing_Spender__c )
,ISCHANGED( LH_Facebook_Spender__c )
,ISCHANGED( LH_Google_Spender__c )
,ISCHANGED( LH_Lighthouse_URL__c )
,ISCHANGED( LH_Tracker_Technology__c )
,ISCHANGED( LH_PLA_Ads__c )
,ISCHANGED( LH_Yahoo_Spender__c )
,
AND(
 ISNEW(),
  OR(
  IF(LH_Bing_Spender__c=FALSE,FALSE,TRUE)
  ,IF(LH_Facebook_Spender__c=FALSE,FALSE,TRUE)
  ,IF(LH_Google_Spender__c=FALSE,FALSE,TRUE)
  ,IF(LH_Yahoo_Spender__c=FALSE,FALSE,TRUE)
  ,NOT(ISBLANK( LH_Lighthouse_URL__c ))
  ,NOT(ISBLANK( LH_PLA_Ads__c ))
  ,NOT(ISBLANK( LH_Tracker_Technology__c )))
)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Partner Certification Document</fullName>
        <actions>
            <name>Accout_Partner_Questionnaire</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends an email with a link to the partner certification document to the listed email.</description>
        <formula>AND(
NOT(ISBLANK(Date_Questionnaire_Submitted__c )),
$RecordType.Name = &apos;Partner Account&apos;,


NOT(INCLUDES(Partner_Type__c , &quot;Channel Connect Publisher&quot;))
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Publisher Profile Document</fullName>
        <actions>
            <name>Email_Publisher_Profile_Documentation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow rule sends an email whenever Channel Connect Partner is selected for the Publisher Profile Documentation</description>
        <formula>AND(
NOT(ISBLANK(Date_Questionnaire_Submitted__c )),
$RecordType.Name = &apos;Partner Account&apos;,


INCLUDES(Partner_Type__c , &quot;Channel Connect Publisher&quot;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Spend Last Updated</fullName>
        <actions>
            <name>Field_Update_Current_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Display_Monthly_Spend__c)||
ISCHANGED(Search_Monthly_Spend__c)||
ISCHANGED(Social_Monthly_Spend__c)||
ISCHANGED(Other_Monthly_Spend__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>No Customer Update</fullName>
        <actions>
            <name>No_Customer_Alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Account_Status__c</field>
            <operation>equals</operation>
            <value>No Customer</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RatingIsChanged</fullName>
        <actions>
            <name>AlertAccOwnerRatingChange</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>OR(
ISCHANGED(Rating),
ISCHANGED(Lead_Gen_Rating_Detail__c)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Tier%3A Bronze</fullName>
        <actions>
            <name>Tier_Bronze</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Auto-fills Tier based off of Total Monthly PPC.
Bronze &lt; $250K</description>
        <formula>AND( 
Total_Monthly_Spend_old__c &lt; 250000,
PRIORVALUE(Tier__c) = &apos;&apos;
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Tier%3A Gold</fullName>
        <actions>
            <name>Tier_Gold</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Auto-fills Tier based off of Total Monthly PPC.
Gold $500K - $1MM</description>
        <formula>AND(
Total_Monthly_Spend_old__c &lt; 1000000,
Total_Monthly_Spend_old__c &gt;= 500000,
PRIORVALUE(Tier__c) = &apos;&apos;
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Tier%3A Platinum</fullName>
        <actions>
            <name>Tier_Platinum</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Auto-fills Tier based off of Total Monthly PPC.
Platinum &gt;$1MM</description>
        <formula>AND(
Total_Monthly_Spend_old__c &gt;= 1000000,
PRIORVALUE( Tier__c) = &apos;&apos;
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Tier%3A Silver</fullName>
        <actions>
            <name>Tier_Silver</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Auto-fills Tier based off of Total Monthly PPC.
Silver $250K - $500K</description>
        <formula>AND(
Total_Monthly_Spend_old__c &gt;= 250000,
Total_Monthly_Spend_old__c &lt; 500000,
PRIORVALUE(Tier__c) = &apos;&apos;
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>UpdateAERatingD</fullName>
        <actions>
            <name>UpdateAERatingD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Status__c</field>
            <operation>equals</operation>
            <value>Not Target for Marin,No Longer in Business</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UpdateRecordTypeCustomerAccount</fullName>
        <actions>
            <name>UpdateRecordTypeCustomerAccount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Status__c</field>
            <operation>equals</operation>
            <value>Inactive Customer,Customer (via Agency),Active Customer</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>UpdateRecordTypeProspectAccount</fullName>
        <actions>
            <name>UpdateRecordtypeProspectAccount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Account_Status__c</field>
            <operation>equals</operation>
            <value>Not Target for Marin,Prospect,No Longer in Business,Client of Agency (Non-Marin)</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
