@isTest
private class TEST_OpportunityTrigger {
	
	@isTest static void test_trueups_category() {
		// Account Creation
        Account a = new Account();
        a.Name = 'Test';
        a.Type = 'Advertiser';
        a.Account_Status__c = 'Prospect';
        a.BillingCountry = 'United States';
        a.Region__c = 'NA';
        a.OwnerID = UserInfo.getUserId();
		insert a;
		
		// Opportunity Creation
       	Opportunity o = New Opportunity();
        o.Name = 'Test Opportunity';
        o.AccountId = a.Id;
        o.StageName = 'Lead Passed';
        o.Amount = 0;
        o.CloseDate = date.Today();
        o.Type = 'New Business';
        o.Type_Detail__c = 'New Business: TrueUp';
        o.OwnerId = UserInfo.getUserId();
        o.Product_Consultant__c = UserInfo.getUserId();
        o.ForecastCategoryName = 'Closed';
        insert o;

		// Retrive inserted opportunity
        o = [SELECT ForecastCategoryName FROM Opportunity WHERE Id =:o.Id];
        System.assertEquals('Omitted', o.ForecastCategoryName);

	}
	
}