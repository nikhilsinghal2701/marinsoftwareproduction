/***
	AccountBatch is Batch class for Scheduler AccountBatchScheduler this scheduler runs once in every day.
	
    This class calsulates no.of open cases , no.Of open cases aged >72hr
    Open Opportunity Amount, 
    closed lost Opportunity Amount in this quarter,closed won Opportunity Amount,
    no.Of active Integrations, no.Of inactive Integrations
     on accounts and its parent accounts and updates the values on the corresponding Account.
     
     AccountBatchUtils class is used to some calculations.
    
***/
global with sharing class AccountBatch implements Database.Batchable<sObject>, Database.Stateful {
    
     global Map<Id,Id> accChildParentMap;
     global Map<Id,Account> accWrapperMap;
     global List<Id> accountIdsList; // is for test purpose
     global AccountBatch(){
     	accWrapperMap = new Map<Id,Account>();
        accChildParentMap = new Map<Id,Id>();
     }
     global String query = 'SELECT ParentId,Parent.ParentId,Id,TotalAmtOpen_Opportunities__c,NoOfOpen_Cases__c '+
                                             'FROM Account';
     global Database.QueryLocator start(Database.BatchableContext BC){
     	 /**String query = 'SELECT ParentId,Parent.ParentId,Id,TotalAmtOpen_Opportunities__c,NoOfOpen_Cases__c '+
                                             'FROM Account';**/
        System.debug('query:'+query);
        return Database.getQueryLocator(query);
     }
      global void execute(Database.BatchableContext bcontext, List<sObject> scope){
        
        List<Account> accountList = (List<Account>)scope;
        List<Id> accIdList = new List<Id>();
        System.debug('accountList:'+accountList);
        for(Account acc: accountList){
        	accIdList.add(acc.Id);
        	if(acc.ParentId != null){
        		System.debug('parent Id:'+acc.ParentId);
	            accChildParentMap.put(acc.Id,acc.ParentId);
	            if(acc.Parent.ParentId != null)
	            	accChildParentMap.put(acc.ParentId,acc.Parent.ParentId);
        	} 
        }
        
       
        
												
		
      
      AccountBatchUtils accUtil = new AccountBatchUtils();
      
      
      AggregateResult [] closeWonAmtResult = accUtil.getClosewonAmtMap(accIdList);
      //map for open cases
      Map<String,Decimal> openCaseMap = accUtil.getOpenCases(accIdList);
      //map for aged cases
      Map<String,Decimal> openAgeMap = accUtil.getAgeCases(accIdList);
      //map for avg case
      Map<String,Decimal> caseAvgMap = accUtil.getCasesAvg(accIdList);
      //map for open opp
      Map<String,Decimal> openOppAmtMap = accUtil.getOpenOppAmtMap(accIdList);
      //map for closed won opp
      Map<String,Decimal> closewonAmtMap = accUtil.aggResultMapConverter(closeWonAmtResult,'AccountId','Closed Won');
      //map for closed lost opp
      Map<String,Decimal> closelostAmtMap = accUtil.aggResultMapConverter(closeWonAmtResult,'AccountId','Closed Lost');
      //active Integrations
      Map<String,Decimal> activeProjectMap = accUtil.getActiveProjectMap(accIdList);
      //Inactive Integrations
      Map<String,Decimal> inactiveProjectMap = accUtil.getInactiveProjectMap(accIdList);
      
      Map<String,Account> accUpdateMap = new Map<String,Account>();
      System.debug('accChildParentMap:'+accChildParentMap);
      if(!accountList.isEmpty()){
	      for(Account acc: accountList){
	      		String key = acc.Id;
	      		System.debug('key:'+key);
	      		Decimal openCasesCnt = 0.0;
	      		Decimal openAgedCnt = 0.0;
	      		Decimal caseAvgResolution = 0.0;
	      		Decimal openOppAmt = 0.0;
	      		Decimal closedWonAmt = 0.0;
	      		Decimal closedLostAmt = 0.0;
	      		Decimal activeProjectCnt = 0.0;
	      		Decimal inactiveProjectCnt = 0.0;
	      		
	      		Account acct = new Account();
	      		AccountBatchUtils acctBatchUtil = new AccountBatchUtils();
	      		//open cases
	      		if(!openCaseMap.isEmpty() && openCaseMap.containsKey(key) )
	      		{
	      			acct = acctBatchUtil.globalValue(key,'NoOfOpen_Cases__c',openCaseMap.get(key), accWrapperMap);
	      			openCasescnt = acct.NoOfOpen_Cases__c;
	      			
	      			accUpdateMap.put(key,acct);
	           		accWrapperMap.put(key,acct);
	      		}	
	      		
	      		//aged casesMap
	      		if(!openAgeMap.isEmpty() && openAgeMap.containsKey(key) )
	      		{
	      			acct = acctBatchUtil.globalValue(key,'NoOfOpen_Cases72hr__c',openAgeMap.get(key), accWrapperMap);
	      			openAgedCnt = acct.NoOfOpen_Cases72hr__c;
	      			
	      			accUpdateMap.put(key,acct);
	           		accWrapperMap.put(key,acct);
	      		}	
	      		//case avg
	      		
	      		if(!caseAvgMap.isEmpty() && caseAvgMap.containsKey(key) )
	      		{
	      			acct = acctBatchUtil.globalValue(key,'Average_Case_ResolutionTime__c',caseAvgMap.get(key), accWrapperMap);
	      			caseAvgResolution = acct.Average_Case_ResolutionTime__c;
	      			
	      			accUpdateMap.put(key,acct);
	           		accWrapperMap.put(key,acct);
	      		}	
	      		
	      		//open Opp
	      		if(!openOppAmtMap.isEmpty() && openOppAmtMap.containsKey(key) )
	      		{
	      			
	      			acct = acctBatchUtil.globalValue(key,'TotalAmtOpen_Opportunities__c',openOppAmtMap.get(key), accWrapperMap);
	      			openOppAmt = acct.TotalAmtOpen_Opportunities__c;
	      			
	      			accUpdateMap.put(key,acct);
	           		accWrapperMap.put(key,acct);
	      		}	
	      		
	      		//closed won Opp
	      		if(!closewonAmtMap.isEmpty() && closewonAmtMap.containsKey(key) )
	      		{
	      			
	      			acct = acctBatchUtil.globalValue(key,'Opportunity_Won_Amount__c',closewonAmtMap.get(key), accWrapperMap);
	      			closedWonAmt = acct.Opportunity_Won_Amount__c;
	      			
	      			accUpdateMap.put(key,acct);
	           		accWrapperMap.put(key,acct);
	      		}	
	      		
	      		//closed lost amt
	      		if(!closelostAmtMap.isEmpty() && closelostAmtMap.containsKey(key) )
	      		{
	      			acct = acctBatchUtil.globalValue(key,'Opportunity_Lost_Amount__c',closelostAmtMap.get(key), accWrapperMap);
	      			closedLostAmt = acct.Opportunity_Lost_Amount__c;
	      			
	      			accUpdateMap.put(key,acct);
	           		accWrapperMap.put(key,acct);
	      		}	
	      		
	      		
	      		//Open projects
	      		if(!activeProjectMap.isEmpty() && activeProjectMap.containsKey(key) )
	      		{
	      			/**acct = acc;
	      			acct.NoOfOpen_Integration__c = activeProjectMap.get(key);
	      			activeProjectCnt = activeProjectMap.get(key);
	      			if(!accWrapperMap.isEmpty() && accWrapperMap.containsKey(key))
	      			{
	      				acct = accWrapperMap.get(key);
	      				acct.NoOfOpen_Integration__c += activeProjectMap.get(key);
	      			}**/
	      			acct = acctBatchUtil.globalValue(key,'NoOfOpen_Integration__c',activeProjectMap.get(key), accWrapperMap);
	      			activeProjectCnt = acct.NoOfOpen_Integration__c;
	      			//activeProjectCnt = activeProjectMap.get(key);
	      			accUpdateMap.put(key,acct);
	           		accWrapperMap.put(key,acct);
	      		}	
	      		// Inactive Projects
	      		if(!inactiveProjectMap.isEmpty() && inactiveProjectMap.containsKey(key) )
	      		{
	      			acct = acctBatchUtil.globalValue(key,'NoOfClose_Integration__c',inactiveProjectMap.get(key), accWrapperMap);
	      			inactiveProjectCnt = acct.NoOfClose_Integration__c;
	      			
	      			accUpdateMap.put(key,acct);
	           		accWrapperMap.put(key,acct);
	      		}	
	      		
	      		
	      		
	      		while(accChildParentMap.containsKey(key))
	           {
	           		AccountBatchUtils accBatchUtil = new AccountBatchUtils();
	           		
	           		Account accObj = new Account();
	           		String pkey = accChildParentMap.get(key);
	           				           			
	           			//open Cases
	           			accObj = accBatchUtil.globalValue(pkey,'NoOfOpen_Cases__c', openCasescnt,accWrapperMap);
	           			//age Cases
	           			accObj = accBatchUtil.globalValue(pkey,'NoOfOpen_Cases72hr__c', openAgedCnt,accWrapperMap);
	           			//case avg 
	           			accObj = accBatchUtil.globalValue(pkey,'Average_Case_ResolutionTime__c', caseAvgResolution,accWrapperMap);
	           			//openOpp
	           			accObj = accBatchUtil.globalValue(pkey,'TotalAmtOpen_Opportunities__c', openOppAmt,accWrapperMap);
	           			//closed lost
	           			accObj = accBatchUtil.globalValue(pkey,'Opportunity_Lost_Amount__c', closedLostAmt,accWrapperMap);
	           			//closedwon
	           			accObj = accBatchUtil.globalValue(pkey,'Opportunity_Won_Amount__c', closedWonAmt,accWrapperMap);
	           			//active projects	
	           			accObj = accBatchUtil.globalValue(pkey,'NoOfOpen_Integration__c', activeProjectCnt,accWrapperMap);
	           			//inactive projects
	           			accObj = accBatchUtil.globalValue(pkey,'NoOfClose_Integration__c', inactiveProjectCnt,accWrapperMap);
	           				           				
	           			accUpdateMap.put(pkey,accObj);
	           			accWrapperMap.put(pkey,accObj);	
	           			
	           		System.debug('accUpdateMap:'+accUpdateMap);
	           		System.debug('accWrapperMap:'+accWrapperMap);
	           		
	           		if(accChildParentMap.containsKey(pkey))
	           		{	
	           			key = pkey;
	           		}	
	           		else
	           			break;
	           }//while loop end
	      }//for loop end
      }//If end
      System.debug('accUpdateMap:'+accUpdateMap);
      if(!accUpdateMap.isEmpty())
      {
      	try{
      		update accUpdateMap.values();
      	}
      	catch(Exception e){
      		String accDataStr = String.valueOf(accUpdateMap.values()).trim();
      		ErrorLog__c eLog = new ErrorLog__c(Error_Cause__c = e.getMessage(),
      								Error_Causing_Data__c = accDataStr,
      								Error_Source__c = 'AcccountBatch class ExceptionLine:'+String.valueOf(e.getLineNumber()),
      								Batch_Id__c = bcontext.getJobId());
      		insert eLog;
      	}
      }
      
        //print some limits
        System.debug(LoggingLevel.INFO, 'Heap Size: ' + Limits.getHeapSize()/1000 + 'KB');
        System.debug(LoggingLevel.INFO, 'Script Statement: ' + Limits.getScriptStatements());
        System.debug(LoggingLevel.INFO, '# of Queries: ' + Limits.getQueries());
        System.debug(LoggingLevel.INFO, '# of Query Rows: ' + Limits.getQueryRows());
        System.debug(LoggingLevel.INFO, '# of Aggregate Queries: ' + Limits.getAggregateQueries());
        System.debug(LoggingLevel.INFO, '# of DML Statements: ' + Limits.getDmlStatements());
        System.debug(LoggingLevel.INFO, '# of DML Rows: ' + Limits.getDmlRows());      
      
     }
      global void finish(Database.BatchableContext bcontext){} 
}