/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AccountMetricsTest {

    static testMethod void myUnitTest() {
        //create Account
        //Account Record type
        Map<String,Schema.RecordTypeInfo> accRecodtypeMap = Account.SObjectType.getDescribe().getRecordTypeInfosByName();
         Schema.RecordTypeInfo accRecordType = accRecodtypeMap.get('Customer Account');
         ID recordtypeId = accRecordType.getRecordTypeId();
         
         //create user
         //create user
        Profile p = [Select Id, Name from Profile where Name = 'System Administrator'];
        
        String namePrefix = 'TestUser' + math.rint(math.random() * 100000);
        
        AggregateResult[] userCount = [Select count(id) userCount From user where username like :namePrefix];
        
        Object users = userCount[0].get('userCount');
        
        User testUser = new User();
        testUser.Email = 'test@test.com';
        testUser.Username = namePrefix+users+'@testuser.test';

        testUser.LastName = 'testManager';
        testUser.Alias = 'testM';
        testUser.ProfileId = p.Id;
        testUser.LanguageLocaleKey = 'en_US';
        testUser.LocaleSidKey = 'en_US';
        testUser.TimeZoneSidKey = 'America/Chicago';
        testUser.EmailEncodingKey = 'UTF-8';
        
        User userManager = new User();
        userManager.Email = 'test@test.com';
        userManager.Username = namePrefix+users+'Manager@testuser.test';

        userManager.LastName = 'test';
        userManager.Alias = 'test';
        userManager.ProfileId = p.Id;
        userManager.LanguageLocaleKey = 'en_US';
        userManager.LocaleSidKey = 'en_US';
        userManager.TimeZoneSidKey = 'America/Chicago';
        userManager.EmailEncodingKey = 'UTF-8';
        insert userManager;
        
         
         
         
        //User user1 = SwarmTestUtility.createTestUser();
        //User user2 = SwarmTestUtility.createTestUser();
        testUser.Manager = userManager;
        
         System.runAs ( userManager ) {
	        Account acc1 = new Account(Name='Test Account1',ownerId = userManager.Id);
	        insert acc1;
	        
	        //create MAC
	        Marin_App_Client__c mac = new Marin_App_Client__c(Name='testClient',Account__c=acc1.Id,Client_ID__c=123753,
	        								Client_Status__c='Active');
	        insert mac;
	        
	        //create Monthly metric
	        Monthly_Metric__c mMetric = new Monthly_Metric__c(Ext_Id__c='951456',Marin_App_Client__c=mac.ID,Facebook_Spend__c=15,
	        								Google_Spend__c = 16,Criteo_Spend__c=87,Other_Publisher_Spend__c=62,Total_Spend_in_Bidding_Folders__c=752,
	        								Total_Spend__c = 784,Universal_Channels_Spend__c=10,Yahoo_Bing_Spend__c=80,Yahoo_Japan_Spend__c=96);
	        insert mMetric;
	        
	        
	       Advance_Metric__c advMetric = new Advance_Metric__c(Account__c = acc1.Id,
	        Criteo_Spend_M__c = 121, Criteo_Spend_W__c=12, Criteo_Spend_Y__c=222, 
	        Days_with_Late_Cost_Data_W__c=7, Days_with_Late_Cost_Data_M__c=14, Days_with_Late_Cost_Data_Y__c=21, 
	        Days_with_Late_Revenue_Data_W__c=7, Days_with_Late_Revenue_Data_M__c=14, Days_with_Late_Revenue_Data_Y__c=21, 
	        Revenue_to_Marin__c=4, Facebook_Spend_W__c=2, Facebook_Spend_M__c=4, Facebook_Spend_Y__c=6, Google_Spend_W__c=8, 
	        Google_Spend_M__c=10, Google_Spend_Y__c=12, 
	        In_App_Reporting_Hits_M__c=3, In_App_Reporting_Hits_W__c=6, In_App_Reporting_Hits_Y__c=9, 
	        MACs_with_Late_Cost_Data_W__c=4, MACs_with_Late_Cost_Data_Y__c=8, MACs_with_Late_Revenue_Data_W__c=12, 
	        MACs_with_Late_Revenue_Data_M__c=16, MACs_with_Late_Revenue_Data_Y__c=20, Management_Hits_M__c=24, 
	        Management_Hits_W__c=28, Management_Hits_Y__c=36, Number_of_Automated_Changes_M__c=40, 
	        Number_of_Automated_Changes_W__c=11, Number_of_Automated_Changes_Y__c=22, Number_of_Changes_Synced_from_PublisherM__c=33, 
	        Number_of_Changes_Synced_from_PublisherW__c=44, Number_of_Changes_Synced_from_PublisherY__c=55, 
	        Number_of_Manual_Changes_M__c=66, Number_of_Manual_Changes_W__c=77, Number_of_Manual_Changes_Y__c=88, 
	        of_PCAs_linked_as_of_previous_day__c=4, Of_Unique_User_Logins_as_of_previous_7__c=6, 
	        Of_Unique_User_Logins_as_of_previous_Y__c=8, Optimization_Hits_M__c=9, Optimization_Hits_W__c=5, 
	        Optimization_Hits_Y__c=5, Other_Hits_M__c=3, Other_Hits_W__c=3, Other_Hits_Y__c=3, 
	        Other_Publisher_Spend_M__c=4, Other_Publisher_Spend_W__c=4, Other_Publisher_Spend_Y__c=4,
	        Report_Creation_Hits_M__c=8, Report_Creation_Hits_W__c=5, Report_Creation_Hits_Y__c=33, Revenue_to_marin_M__c=5, 
	        Revenue_to_marin_Y__c=7, Spend_Per_Publisher_Presented_in_Graph_M__c=13, Spend_Per_Publisher_Presented_in_Graph_Y__c=45, 
	        Total_Spend_in_Bidding_Folder_W__c=71, Total_Spend_in_Bidding_Folder_M__c=74, Total_Spend_in_Bidding_Folder_Y__c=25, 
	        Total_Spend_W__c=45, Total_Spend_M__c=120, Total_Spend_Y__c=35, Universal_Channels_Spend__c=45, Universal_Channels_Spend_M__c=15, 
	        Universal_Channels_Spend_W__c=102, Universal_Channels_Spend_Y__c=45, Yahoo_Bing_Spend_M__c=96, 
	        Yahoo_Bing_Spend_W__c=71, Yahoo_Bing_Spend_Y__c=8, Yahoo_Japan_Spend_M__c=32, Yahoo_Japan_Spend_W__c=35, 
	        Yahoo_Japan_Spend_Y__c=42, of_PCAs_net_change_diff_btw_Day7_Day1_M__c=42, of_PCAs_net_change_diff_btw_Day7_Day1_Y__c=27, 
	        of_PCAs_net_change_diff_btw_Day7_Day1__c=41);
	        
	        insert advMetric;
	        Test.startTest();
	        ApexPages.StandardController std = new ApexPages.StandardController(acc1);
	        AccountMetrics obj = new AccountMetrics(std);
	     	obj.getSpendDataMonthly();
	     	obj.getmonthlyHitsCategory();
	     	obj.getMonthlyChangeCategory();
	     	obj.getTotalSpend_90Days();
	     	obj.getTotalSpend_395Days();
	     	obj.getSpendData();
	     	obj.getHitsCategory();
	     	obj.getChangeCategory();
	     	obj.getEstimatedRevenue_90Days();
	     	AccountMetrics.LineData lineObj = new AccountMetrics.LineData(String.valueOf(Date.today()),52.32);
	     	Test.stopTest();
         }
        //create Monthly Metric // for 13 months data per month total spend
        //create Metric for  for last 2 months +current month till date per day total day
    }
}