/**
 * This Apex Class is being used to test the CasePostFeedItem trigger built
 * Created by: Gary Sopko
 * Created Date: 07/24/2013
 * Please consult with Sales Operations/System Administration before updating.
 */
@isTest
private class TestCasePostFeedItem {

    static testMethod void Test1() {
        Account a = new Account();
        	a.Name = 'TestAccount';
        	a.Type = 'Advertiser';
        	a.Region__c = 'NA';
        	a.Account_Status__c = 'Active Customer';
        	a.BillingCountry = 'United States';
        	a.OwnerId = UserInfo.getUserId();
    	insert a;
    	Priority__c p1 = new Priority__c();
    		p1.Name__c = '1';
    		p1.Description__c = 'Test Description';
		insert p1;
		Priority__c p2 = new Priority__c();
			p2.Name__c = '2';
			p2.Description__c = 'Test Description';
		insert p2;
    	Case c = new Case();
    		c.Subject = 'TestCase';
    		c.Description = 'WhoCares';
    		c.Status = 'New';
    		c.AccountId = a.Id;
    		c.Reason = 'Test';
    		c.Case_Reason_Detail__c = 'Test';
    		c.Priority__c = [Select Id from Priority__c WHERE Name__c = '1' LIMIT 1].Id;
    	insert c;
    	for(Case updatec : [Select Id, Priority__c from Case WHERE Id = :c.Id]){
    		updatec.Priority__c = [Select Id from Priority__c WHERE Name__c = '2' LIMIT 1].Id;
    		update updatec;
    		updatec.Priority__c = [Select Id from Priority__c WHERE Name__c = '1' LIMIT 1].Id;
    		update updatec;
    	}
    }
}