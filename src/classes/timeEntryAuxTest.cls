@isTest
public class timeEntryAuxTest {
    public static testmethod void testAccountTrigger() {
        account a = new account(name = 'a');
        insert a;
        Marin_App_Client__c m = new Marin_App_Client__c(account__c = a.id, client_id__c = 1, customer_id__c = 1);
        insert m;
        case c = new case(account = a, subject = 't');
        insert c;
        workit2__timing__c t = new workit2__timing__c(workit2__case__c = c.id);
        insert t;
        workit2__time_entry__c e = new workit2__time_entry__c(workit2__timing__c = t.id);
        insert e;
        t = new workit2__timing__c(account__c = a.id);
        insert t;
        e = new workit2__time_entry__c(workit2__timing__c = t.id);
        insert e;

    }
}