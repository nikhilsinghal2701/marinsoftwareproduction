/**
 * This Apex Class is being used to test the ScheduledContractAutoCreate trigger built
 * Created by: Gary Sopko
 * Created Date: 05/10/2013
 * Please consult with Sales Operations/System Administration before updating.
 */
@isTest
private class TestScheduledContractAutoCreate {
    static testMethod void TestSchedule() {
    test.startTest();
	ScheduledContractAutoCreate sched = new ScheduledContractAutoCreate();
	String schedule = '0 0 23 * * ?';
	system.schedule('Nightly Creation', schedule, sched);
	test.stopTest();
    }
}