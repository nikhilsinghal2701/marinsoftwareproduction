/**
	AccountBatchScheduler schedules the calculation of Account Metrics 
	on Objects Opportuity, Cases, Integrations
**/
global with sharing class AdvanceMetricBatchScheduler implements Schedulable{
	
	
	  
	  global AdvanceMetricBatch testBatch = new AdvanceMetricBatch();
	  global void execute(SchedulableContext SC) {
	     AdvanceMetricBatch advanceBt = new AdvanceMetricBatch() ;
	     if(Test.isRunningTest())
	       advanceBt = testBatch;
	     Database.executeBatch(advanceBt,100);
	  }
	

}