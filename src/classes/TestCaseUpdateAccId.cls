/**
 * This Apex Class is being used to test the CaseUpdateAccId trigger built
 * Created by: Gary Sopko
 * Created Date: 08/03/2013
 * Please consult with Sales Operations/System Administration before updating.
 */
@isTest
private class TestCaseUpdateAccId {

    static testMethod void Test1() {
        Account a = new Account();
        	a.Name = 'Test Account';
            a.Type = 'Advertiser';
            a.Account_Status__c = 'Active Customer';
            a.Region__c = 'NA';
            a.BillingCountry = 'United States';
        insert a;
        Marin_App_Client__c mac = new Marin_App_Client__c();
        	mac.Account__c = a.Id;
        	mac.Client_Id__c = 10;
        	mac.Customer_Id__c = 1;
        	mac.Name = 'TestMAC';
    	insert mac;
    	Case c = new Case();
            c.Status = 'New';
            c.Client_Id__c = 10;
            c.Requestor__c = UserInfo.getUserId();
            c.Creation_Reason__c = 'Account';
            c.Subject = 'Test Subject';
            c.Description = 'Test Description';
            c.Reason = 'Account Setup';
        insert c;
        for(Case cupdate : [Select Id, Client_Id__c from Case WHERE Id = :c.Id]){
   	    	cupdate.Client_Id__c = 15;
       		update cupdate;
        }
    }
}