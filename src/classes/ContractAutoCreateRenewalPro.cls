public with sharing class ContractAutoCreateRenewalPro {
	public void CreateOppfromContract(){
		for(Contract con : [Select Id, Owner_Expiration_Notice_Date__c, AccountId, Account.Name, 
					EndDate, Product_Version__c, OwnerId, Contract_Type__c, Monthly_Min_Fee__c from Contract 
					WHERE  Owner_Expiration_Notice_Date__c = TODAY AND Customer_Status__c = 'Customer' 
					AND Product_Version__c = 'Professional']){
			Opportunity opp = new Opportunity();
			opp.Name = con.Account.Name + ' - Renewal - ' + con.EndDate;
			opp.Contract_Record__c = con.Id;
			opp.OwnerId = con.OwnerId;
			String rectyp = [Select Id from RecordType WHERE Name = 'Renewal' 
                             AND sObjectType = 'Opportunity' LIMIT 1].Id;
			opp.RecordTypeId = rectyp;
			opp.AccountId = con.AccountId;
			opp.CloseDate = con.EndDate;
			opp.Type = 'Renewal';
			opp.Type_Detail__c = 'Renewal Increase';
			opp.Amount = 0;
			opp.StageName = 'Solid Prospect';
			opp.Product_Version__c = con.Product_Version__c;
			opp.Current_Contract_End_Date__c = con.EndDate;
			opp.Contract_Length__c =  con.Contract_Type__c;
			opp.Proposed_Min_Fee__c = con.Monthly_Min_Fee__c;
			opp.NextStep = 'AM to Complete Renewal';
			opp.Situation__c = 'Account Manager, please fill in a situation';
			insert opp;
		}
	}
}