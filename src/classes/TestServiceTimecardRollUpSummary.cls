/**
 * This Apex Class is being used to test the ServiceTimecardRollUpSummary trigger built
 * Created by: Gary Sopko
 * Created Date: 06/13/2013
 * Please consult with Sales Operations/System Administration before updating.
 */
@isTest
private class TestServiceTimecardRollUpSummary {

    static testMethod void Test1() {
        // Step 1:  Create Account
        Account a = new Account();
            a.Name = 'Test Account';
            a.Type = 'Advertiser';
            a.Account_Status__c = 'Active Customer';
            a.Region__c = 'NA';
            a.BillingCountry = 'United States';
        insert a;
        // Step 2:  Create Case
        Case c = new Case();
            c.AccountId = a.Id;
            c.Status = 'New';
            c.Requestor__c = UserInfo.getUserId();
            c.Creation_Reason__c = 'Account';
            c.Subject = 'Test Subject';
            c.Description = 'Test Description';
            c.Reason = 'Account Setup';
        insert c;
        Case c2 = new Case();
            c2.AccountId = a.Id;
            c2.Status = 'New';
            c2.Requestor__c = UserInfo.getUserId();
            c2.Creation_Reason__c = 'Account';
            c2.Subject = 'Test Subject';
            c2.Description = 'Test Description';
            c2.Reason = 'Account Setup';
        insert c2;
        // Step 3:  Create Integration
        PS_Project_Record__c i = new PS_Project_Record__c();
            i.Account__c = a.Id;
            i.Project_Requester__c = UserInfo.getUserId();
            i.Project_Name__c = 'Test Project';
            i.Project_Type__c = 'Other';
            i.Onboarding_Location__c = 'North America - West';
            i.Project_Classification__c = 'Internal';
            i.Customer_Type__c = 'Internal';
            i.Comments__c = 'Test Comments';
        insert i;
        PS_Project_Record__c i2 = new PS_Project_Record__c();
            i2.Account__c = a.Id;
            i2.Project_Requester__c = UserInfo.getUserId();
            i2.Project_Name__c = 'Test';
            i2.Project_Type__c = 'Test';
            i2.Onboarding_Location__c = 'Test';
            i2.Project_Classification__c = 'Test';
            i2.Customer_Type__c = 'Test';
            i2.Comments__c = 'Testing Comments';
        insert i2;
        // Step 4:  Create Service Timecards
        SFDC_Service_Timecard__c tc1 = new SFDC_Service_Timecard__c();
            tc1.User__c = UserInfo.getUserId();
            tc1.Date_of_Service__c = date.Today();
            tc1.Hours_Worked__c = 15;
            tc1.Service_Performed__c = 'Internal';
            tc1.Services_Sub_Type__c = 'Integration';
            tc1.Account__c = a.Id;
            tc1.Case__c = c.Id;
        insert tc1;
        SFDC_Service_Timecard__c tc2 = new SFDC_Service_Timecard__c();
            tc2.User__c = UserInfo.getUserId();
            tc2.Date_of_Service__c = date.Today();
            tc2.Hours_Worked__c = 15;
            tc2.Service_Performed__c = 'Internal';
            tc2.Services_Sub_Type__c = 'Integration';
            tc2.Account__c = a.Id;
            tc2.Integration__c = i.Id;
        insert tc2;
        // Step 5:  Update Service Timecards
        for(SFDC_Service_Timecard__c tc1update : [Select Id, Case__c from SFDC_Service_Timecard__c WHERE Id = :tc1.Id]){
            tc1update.Case__c = c2.Id;
            update tc1update;
        }
        for(SFDC_Service_Timecard__c tc2update : [Select Id, Integration__c from SFDC_Service_Timecard__c WHERE Id = :tc2.Id]){
            tc2update.Integration__c = i2.Id;
            update tc2update;
        }
    }
}