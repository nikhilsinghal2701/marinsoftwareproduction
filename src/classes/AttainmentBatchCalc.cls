global class AttainmentBatchCalc implements Database.Batchable<sObject> {
    
    String query;
    Date startDate;
    public String month {set; get;}
    public Integer monthIndex {set; get;}
    public Boolean tooManyBatchJobs {set; get;}

    static Integer batchSize = 46;

    global AttainmentBatchCalc() {

    }
    
    global AttainmentBatchCalc(Integer monthIndex) {
        this.monthIndex = monthIndex;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        startDate = returnStartDate(monthIndex);
        return Database.getQueryLocator([select Id from Attainment__c WHERE Start_Date__c = :startDate ORDER BY Start_Date__c]);
    }

    global void execute(Database.BatchableContext BC, List<Attainment__c> scope) {
        String achain = '';

        for (Attainment__c a : scope) {
            achain += a.Id + ' ';
        }
        
        if (achain != null) {
            //AttainmentCalculation.AttainmentCalculation(achain);
        }
    }
    
    global void finish(Database.BatchableContext BC) {

        AsyncApexJob a = [SELECT id, ApexClassId,
                           JobItemsProcessed, TotalJobItems,
                           NumberOfErrors, CreatedBy.Email
                           FROM AsyncApexJob
                           WHERE id = :BC.getJobId()];
             
            String emailMessage = 'Your attainment recalcuation for month number ' + monthIndex 
                 + 'has finished. The job was spread over '
                 + a.totalJobItems
                 + ' batches. ' + a.jobitemsprocessed
                 + ' processed without any issues, and '
                 + a.numberOfErrors +
                 ' batches threw unhandled exceptions.';
             
            Messaging.SingleEmailMessage mail =
                  new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {a.createdBy.email};
            mail.setToAddresses(toAddresses);
            mail.setReplyTo('noreply@salesforce.com');
            mail.setSenderDisplayName('Batch Job Summary');
            mail.setSubject('Batch job completed');
            mail.setPlainTextBody(emailMessage);
            mail.setHtmlBody(emailMessage);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[]
                               { mail });
        
    }

    public static Date returnStartDate(Integer month) {
        if (month == null) month = System.Today().month();
        return Date.newInstance(System.Today().year(), month, 1);
    }

    //builds a picklist of months for selection.
    public List<selectOption> getMonths() {
        List<selectOption> options = new List<selectOption>();
        options.add(new selectOption('1', 'January'));
        options.add(new selectOption('2', 'February'));
        options.add(new selectOption('3', 'March'));
        options.add(new selectOption('4', 'April'));
        options.add(new selectOption('5', 'May'));
        options.add(new selectOption('6', 'June'));
        options.add(new selectOption('7', 'July'));
        options.add(new selectOption('8', 'August'));
        options.add(new selectOption('9', 'September'));
        options.add(new selectOption('10', 'October'));
        options.add(new selectOption('11', 'November'));
        options.add(new selectOption('12', 'December'));
        return options;
    }

    public void runBatch() {
        monthIndex = Integer.valueOf(month);

        if ([SELECT count() FROM AsyncApexJob WHERE JobType = 'BatchApex' AND (Status = 'Processing' OR Status = 'Preparing')] < 5){
            AttainmentBatchCalc batch = new AttainmentBatchCalc(monthIndex);
            Database.executeBatch(batch, batchSize);
        } else {
            tooManyBatchJobs = true;
        }


    }
    
}