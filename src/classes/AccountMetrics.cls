/**
AccountMetrics is for displaying graphs for a given account.
This class is used in Pages: AccountYearly, AccountMonthly, AccountGraph.
Test class for AccountMetrics is AccountMetricsTest.cls
**/
public with sharing class AccountMetrics {

    /*Static variable*/
    public static final String GOOGLE = 'GO';         
    public static final String YAHOO_BING = 'YB';         
    public static final String FACEBOOK = 'FB';         
    public static final String YAHOO_JAPAN = 'YJ';         
    public static final String CRITEO = 'CR';         
    public static final String OTHER_PUBLISHER = 'O';
    public static final String UNIVERSAL_CHANNEL = 'UC';         
    
    public static final String OPTIMIZATION = 'O';     
    public static final String REPORT_CREATION = 'C';     
    public static final String IN_APP_REPORTING = 'R';     
    public static final String MANAGEMENT = 'M'; 
        
    public static final String AUTOMATED_CHANGES = 'A';         
    public static final String MANUAL_CHANGES = 'M';         
    public static final String SYNCED_FROM_PUBLISHER = 'S';         

        
    public Decimal latecostData_w {get;set;}
    public Advance_Metric__c advMetric {get;set;}

    public Set<Id> accIds {get;set;}
    
    /****Monthly Data**************/       
    
    public List<Data> getSpendDataMonthly(){
        List<Data> spendMonthly = new List<Data>();
        
        spendMonthly.add(new Data(GOOGLE,advMetric.Google_Spend_M__c ));
        spendMonthly.add(new Data(YAHOO_BING,advMetric.Yahoo_Bing_Spend_M__c));
        spendMonthly.add(new Data(FACEBOOK,advMetric.Facebook_Spend_M__c));    
        spendMonthly.add(new Data(YAHOO_JAPAN,advMetric.Yahoo_Japan_Spend_M__c));
        spendMonthly.add(new Data(CRITEO,advMetric.Criteo_Spend_M__c));
        spendMonthly.add(new Data(OTHER_PUBLISHER,advMetric.Other_Publisher_Spend_M__c));
        spendMonthly.add(new Data(UNIVERSAL_CHANNEL,advMetric.Universal_Channels_Spend_M__c));
        return spendMonthly;
     }     

     public List<Data> getMonthlyHitsCategory(){
        List<Data> mHitsCategory= new List<Data>();

        mHitsCategory.add(new Data(OPTIMIZATION,advMetric.Optimization_Hits_M__c));    
        mHitsCategory.add(new Data(REPORT_CREATION,advMetric.Report_Creation_Hits_M__c));
        mHitsCategory.add(new Data(IN_APP_REPORTING,advMetric.In_App_Reporting_Hits_M__c));
        mHitsCategory.add(new Data(MANAGEMENT,advMetric.Other_Hits_M__c));
       
        return mHitsCategory;
     }  
     
      // Change per category for last 90 days
     public List<Data> getMonthlyChangeCategory(){
        List<Data> mChangeCategory= new List<Data>();
        mChangeCategory.add(new Data(AUTOMATED_CHANGES,advMetric.Number_of_Automated_Changes_M__c));
        mChangeCategory.add(new Data(MANUAL_CHANGES,advMetric.Number_of_Manual_Changes_M__c));
        mChangeCategory.add(new Data(SYNCED_FROM_PUBLISHER,advMetric.Number_of_Changes_Synced_from_PublisherM__c));
         
        return mChangeCategory;
     } 
         
     public List<LineData> getTotalSpend_90Days(){
          List<AggregateResult> totalSpend = new List<AggregateResult>();
          Date endDate = System.today().adddays(-1);
          Date startDate = startDate(endDate , 2); 
          totalSpend = [Select sum(Total_Spend__c) sumvar, Date__c 
                              from Monthly_Metric__c
                              where Marin_App_Client__r.Account__c In :accIds
                              and Date__c >= :startDate  AND Date__c <= :endDate
                              group by Date__c Order by Date__c];
          List<LineData> data = new List<LineData>();
          for(AggregateResult arObj : totalSpend){
              Date dt = (Date)arObj.get('Date__c');
                data.add(new LineData(format(dt , 'MMM'), (Decimal)arObj.get('sumvar')));
            }
          System.debug('data----'+data);
          return data;
     } 
         
     public List<LineData> getEstimatedRevenue_90Days() {
        List<AggregateResult> results = new List<AggregateResult>();
        Date endDate = System.today().adddays(-1);
        Date startDate = startDate(endDate , 2); 
        results = [SELECT sum(Revenue_to_marin__c) revenue, Date__c 
                              FROM Monthly_Metric__c
                              WHERE Marin_App_Client__r.Account__c In :accIds
                              and Date__c >= :startDate  AND Date__c <= :endDate
                              GROUP BY Date__c
                              ORDER BY Date__c];
        List<LineData> data = new List<LineData>();
        for(AggregateResult result : results){
            Date dt = (Date) result.get('Date__c');
            data.add(new LineData(format(dt , 'MMM'), (Decimal) result.get('revenue')));
        }
          System.debug('data----'+data);
          return data;
     }
    
    /****13Month Data**************/ 
         
         
     public List<LineData> getTotalSpend_395Days(){
        List<AggregateResult> totalSpend = new List<AggregateResult>();
        System.debug('accIds----'+accIds);
        //Date dt = System.today()-395;
        Date dt = System.today().addMonths(-13).toStartOfMonth();
        System.debug('-----dt'+dt);
        totalSpend = [Select sum(Total_Spend__c) sumvar, Date__c 
                            from Monthly_Metric__c 
                            where Marin_App_Client__r.Account__c In :accIds
                            and Date__c >= : dt
                            group by Date__c Order by Date__c];
        List<LineData> data = new List<LineData>();
        for(AggregateResult arObj : totalSpend){
              Date d = (Date)arObj.get('Date__c');
              data.add(new LineData(format(d , 'MMM-yy'), (Decimal)arObj.get('sumvar')));
         }
        System.debug('data----'+data);
        return data;
     }


    
    /****weekly Data**************/         
        
    public List<Data> getSpendData(){
       List<Data> spendData = new List<Data>();
     
        System.debug('advMetric  :'+advMetric  );
        spendData.add(new Data(GOOGLE,advMetric.Google_Spend_W__c));
        spendData.add(new Data(YAHOO_BING,advMetric.Yahoo_Bing_Spend_W__c));
        spendData.add(new Data(FACEBOOK,advMetric.Facebook_Spend_W__c));
        spendData.add(new Data(YAHOO_JAPAN,advMetric.Yahoo_Japan_Spend_W__c));  
        spendData.add(new Data(CRITEO,advMetric.Criteo_Spend_W__c));
        spendData.add(new Data(OTHER_PUBLISHER,advMetric.Other_Publisher_Spend_W__c));
        spendData.add(new Data(UNIVERSAL_CHANNEL,advMetric.Universal_Channels_Spend_W__c));
        //
        
        //latecostData_w = spendRevenue.Days_with_Late_Cost_Data_W__c;
        
        return spendData;
    }
    
     public List<Data> getHitsCategory(){
        List<Data> hCategory= new List<Data>();
            
        hCategory.add(new Data(OPTIMIZATION, advMetric.Optimization_Hits_W__c));    
        hCategory.add(new Data(REPORT_CREATION, advMetric.Report_Creation_Hits_W__c));
        hCategory.add(new Data(IN_APP_REPORTING, advMetric.In_App_Reporting_Hits_W__c));
        hCategory.add(new Data(MANAGEMENT, advMetric.Other_Hits_W__c));
                        
        return hCategory;
     }  
     // Change per category for last 7 days
     public List<Data> changeCategory;

     public List<Data> getChangeCategory(){
        List<Data> cCategory= new List<Data>();
        
        cCategory.add(new Data(AUTOMATED_CHANGES,advMetric.Number_of_Automated_Changes_W__c));
        cCategory.add(new Data(MANUAL_CHANGES,advMetric.Number_of_Changes_Synced_from_PublisherW__c));
        cCategory.add(new Data(SYNCED_FROM_PUBLISHER,advMetric.Number_of_Manual_Changes_W__c));
        
        
        return cCategory;
     }
     
    public final Account record;  
    public String accID ;
    
    public Boolean secure;
    
    public AccountMetrics(){}
    public AccountMetrics (ApexPages.StandardController controller){
          String accountId = controller.getId();
          record = (Account) controller.getRecord();
          String queryFieldsStr = 'Criteo_Spend_W__c, Criteo_Spend_M__c, Facebook_Spend_W__c, Google_Spend_W__c ,Days_with_Late_Cost_Data_W__c,Yahoo_Bing_Spend_W__c,Facebook_Spend_M__c,'
                          +'MACs_with_Late_Cost_Data_W__c,MACs_with_Late_Revenue_Data_W__c,Days_with_Late_Revenue_Data_W__c,' 
                          +'Other_Publisher_Spend_W__c,Universal_Channels_Spend_W__c,Revenue_to_Marin__c,'
                          +'Total_Spend_W__c,of_PCAs_linked_as_of_previous_day__c,Of_Unique_User_Logins_as_of_previous_7__c,'
                          +'of_PCAs_net_change_diff_btw_Day7_Day1__c,of_PCAs_Expired_as_of_previous_day_7__c, '
                          +'Optimization_Hits_W__c,In_App_Reporting_Hits_W__c,Report_Creation_Hits_W__c,'
                          +'Other_Hits_W__c,Number_of_Automated_Changes_W__c,Number_of_Changes_Synced_from_PublisherW__c,'
                          +'Number_of_Manual_Changes_W__c,Total_Spend_in_Bidding_Folder_W__c,Google_Spend_M__c,Other_Publisher_Spend_M__c,'
                          +'Yahoo_Japan_Spend_M__c,Yahoo_Japan_Spend_W__c, Yahoo_Bing_Spend_M__c,Days_with_Late_Cost_Data_M__c,MACs_with_Late_Cost_Data_M__c,'
                          +' Days_with_Late_Revenue_Data_M__c,MACs_with_Late_Revenue_Data_M__c,Total_Spend_M__c,Universal_Channels_Spend_M__c, '
                          +' of_PCAs_net_change_diff_btw_Day7_Day1_M__c,Report_Creation_Hits_M__c,In_App_Reporting_Hits_M__c,Other_Hits_M__c, '
                          +' Optimization_Hits_M__c,Number_of_Automated_Changes_M__c,Number_of_Changes_Synced_from_PublisherM__c,Number_of_Manual_Changes_M__c, '
                          +' Total_Spend_in_Bidding_Folder_M__c,Revenue_to_Marin_M__c ';
          
        String queryStr = 'SELECT '+queryFieldsStr+'  FROM Advance_Metric__c WHERE  Account__c=: accountId';
          Advance_Metric__c  spendRevenue = Database.query(queryStr);  
                 
        advMetric  = spendRevenue;
        accIds = new Set<Id>();
        System.debug('spendRevenue:'+spendRevenue);
        accIds = allChildAcc(accountId);
        accIds.add(accountId);
        
        setSecure();
        
     }
         
     public Set<Id> allChildAcc(Id accId){
         Set<Id> childAcc = new Set<Id> ();
         accIds.add(accId);
         if(accId != null){
             for(Account acc : [SELECT Id FROM Account WHERE ParentId = :accId OR
                                                            Parent.ParentId = :accId OR
                                                            Parent.Parent.ParentId = :accId OR
                                                            Parent.Parent.ParentId = :accId]){
                  childAcc.add(acc.Id);                                          
                
             }
             if(childAcc  != null && childAcc.size()>0){
                 for(Account a : [SELECT Id FROM Account WHERE ParentId in :childAcc OR
                                                            Parent.ParentId in :childAcc OR
                                                            Parent.Parent.ParentId in :childAcc OR
                                                            Parent.Parent.ParentId in :childAcc]){
                 
                    childAcc.add(a.Id);
                 }
             }                                              
        }
        return childAcc;
     }
         
         

         
    private Map<Id, Id> getRoleHierarchy() {
        Map<Id, Id> userRoles = new Map<Id, Id>();
        for (UserRole role : [SELECT ParentRoleId FROM UserRole]) {
            userRoles.put(role.Id, role.ParentRoleId);
        }
        return userRoles;
    }
         
    private Date startDate(Date endDate, Integer months){
        Date stDate = endDate.addMonths(-months).toStartOfMonth();
        return stDate ;
    }
         
    private String format(Date d, String pattern) {
         if (d == null) return ''; 
         DateTime dt = DateTime.newInstance(d.year(), d.month(), d.day());
         return dt.format(pattern);     //example - 'MM/dd/yyyy'
    }     
    
    public Boolean getSecure() {
        return secure;
    }
        
    public void setSecure() {
        Account record = [SELECT OwnerId, Owner.UserRoleId FROM Account WHERE Id = :record.Id];
        Id roleId = UserInfo.getUserRoleId();
        Id ownerRole = record.Owner.UserRoleId;    //record.Owner_Role_Id__c;
        
        if (record.OwnerId == UserInfo.getUserId()) {
            secure = true;
            return;
        }
        
        Map<Id, Id> userRoles = getRoleHierarchy();
        Id key = ownerRole;
        while(userRoles.containsKey(key)) {
            key = userRoles.get(key);
            if (key == roleId) {
                secure = true;
                return;
            } 
        }
    }        
         
     //wrapper class for data
     public class Data {
            public String publisher {get;set;}
            public Decimal spend {get;set;}
            public String category {get;set;}
            public Decimal hits{get;set;}
            public Decimal change{get;set;}
            public Date dt{get;set;}
            public Decimal totalSpend{get;set;}
            public Data(String publisher, Decimal spend)
            {
                    this.publisher = publisher;
                    if(spend != null)
                    {
                        this.spend = spend;
                    }
                    else{
                        this.spend = 0.0;
                    }
            }
            
            public Data(Date dt, Decimal totalSpend)
            {
                    this.dt = dt;
                    this.totalSpend = totalSpend;
            }
     }
         
    public class LineData{
        public String dt {get; private set;}
        public Decimal totalSpend{get; private set;}
    
    
        public LineData(String dt, Decimal totalSpend) {
            this.dt = dt;
            this.totalSpend= totalSpend;
        
        }
    }

}