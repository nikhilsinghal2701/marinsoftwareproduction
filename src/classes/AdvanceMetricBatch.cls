/***
    AdvanceMetricBatch is Batch class for Scheduler AdvanceMetricBatchScheduler this scheduler runs once in every day.
    
    This class calsulates Advance metric record for accounts and its parent accounts.
    AdvanceMetricsUtil class is used to some calculations.
    
***/
global with sharing class AdvanceMetricBatch implements Database.Batchable<sObject>, Database.Stateful {
    
     global Map<Id,Id> accChildParentMap;
     global Map<Id,Advance_Metric__c> advaWrapperMap;
     global List<Id> testAccId;
     
     global AdvanceMetricBatch(){
       advaWrapperMap = new Map<Id,Advance_Metric__c>();
        accChildParentMap = new Map<Id,Id>();
     }
     
     global Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT ParentId, Id FROM Account';
        if(Test.isRunningTest()){
            query += ' WHERE Id IN: testAccId ';
        }
        System.debug('query:'+query);
        return Database.getQueryLocator(query);
                                             
     }
     
     global void execute(Database.BatchableContext bcontext, List<sObject> scope){
        
        List<Account> accountList = (List<Account>)scope;
        List<Id> scopeIds = new List<Id>();

        Set<Id> parentChildIds = new Set<Id>();
        
        //these already have matrix created for them
        Set<Id> retrieveList = new Set<Id>();
        Set<Id> deleteList = new Set<Id>();
        
        parentChildIds.addAll(accChildParentMap.keySet());
        parentChildIds.addAll(accChildParentMap.values());
        
        Map<Id,Advance_Metric__c> advaWrapperMap = new Map<Id,Advance_Metric__c>();
		        
        for(Account acc: accountList) {
        	scopeIds.add(acc.Id);
        	
			if (! parentChildIds.contains(acc.Id)) {
				deleteList.add(acc.Id); //delete Advance_Metric__c for these Ids.
			}
			if (! parentChildIds.contains(acc.ParentId)) {
				deleteList.add(acc.ParentId); //delete Advance_Metric__c for these Ids.
			}
			
			//put in map
			accChildParentMap.put(acc.Id, acc.ParentId);
        }
        
        //delete list
        delete [SELECT Id FROM Advance_Metric__c WHERE Account__c IN :deleteList];
        deleteList.clear();
        
        //get list of accounts for which to get the advance metric
        retrieveList = getRetrieveList(accountList, accChildParentMap);

        System.debug('accChildParentMap:'+accChildParentMap);
        AdvanceMetricsUtil  adUtil = new AdvanceMetricsUtil ();
        
        for(Advance_Metric__c a :[SELECT a.of_PCAs_net_change_diff_btw_Day7_Day1__c, a.MACs_with_Late_Cost_Data_M__c,
                  a.of_PCAs_linked_as_of_previous_day__c, a.of_PCAs_Expired_as_of_previous_day_7__c,
                  a.Yahoo_Japan_Spend_Y__c, a.Yahoo_Japan_Spend_W__c, a.Yahoo_Japan_Spend_M__c, 
                  a.Yahoo_Bing_Spend_Y__c, a.Yahoo_Bing_Spend_W__c, a.Yahoo_Bing_Spend_M__c, 
                  a.Universal_Channels_Spend__c, a.Universal_Channels_Spend_Y__c, a.Universal_Channels_Spend_W__c, 
                  a.Universal_Channels_Spend_M__c, a.Total_Spend_in_Bidding_Folder_Y__c, a.Total_Spend_in_Bidding_Folder_M__c, 
                  a.Total_Spend_in_Bidding_Folder_W__c, a.Total_Spend_Y__c, a.Total_Spend_M__c, a.Total_Spend_W__c, 
                  a.SystemModstamp, a.Spend_Per_Publisher_Presented_in_Graph_Y__c, 
                  a.Spend_Per_Publisher_Presented_in_Graph_M__c, a.Revenue_to_marin_Y__c, a.Revenue_to_marin_M__c, 
                  a.Revenue_to_Marin__c, a.Report_Creation_Hits_Y__c, a.Report_Creation_Hits_W__c, 
                  a.Report_Creation_Hits_M__c, a.OwnerId, a.Other_Publisher_Spend_Y__c, 
                  a.Other_Publisher_Spend_W__c, a.Other_Publisher_Spend_M__c, a.Other_Hits_Y__c, a.Other_Hits_W__c, 
                  a.Other_Hits_M__c, a.Optimization_Hits_Y__c, a.Optimization_Hits_W__c, a.Optimization_Hits_M__c, 
                  a.Of_Unique_User_Logins_as_of_previous_Y__c, a.Of_Unique_User_Logins_as_of_previous_7__c, 
                  a.Number_of_Manual_Changes_Y__c, a.Number_of_Manual_Changes_W__c, a.Number_of_Manual_Changes_M__c, 
                  a.Number_of_Changes_Synced_from_PublisherY__c, a.Number_of_Changes_Synced_from_PublisherW__c, 
                  a.Number_of_Changes_Synced_from_PublisherM__c, a.Number_of_Automated_Changes_Y__c, 
                  a.Number_of_Automated_Changes_W__c, a.Number_of_Automated_Changes_M__c, a.Management_Hits_Y__c, 
                  a.Management_Hits_W__c, a.Management_Hits_M__c, a.MACs_with_Late_Revenue_Data_Y__c, 
                  a.MACs_with_Late_Revenue_Data_M__c, a.MACs_with_Late_Revenue_Data_W__c, a.MACs_with_Late_Cost_Data_Y__c, 
                  a.MACs_with_Late_Cost_Data_W__c, a.IsDeleted, a.In_App_Reporting_Hits_Y__c, a.In_App_Reporting_Hits_W__c,
                  a.In_App_Reporting_Hits_M__c, a.Google_Spend_M__c, a.Google_Spend_W__c, a.Google_Spend_Y__c, a.Facebook_Spend_W__c, 
                  a.Facebook_Spend_Y__c, a.Facebook_Spend_M__c, a.Days_with_Late_Revenue_Data_Y__c, a.Days_with_Late_Revenue_Data_M__c,
                  a.Days_with_Late_Revenue_Data_W__c, a.Days_with_Late_Cost_Data_Y__c, a.Days_with_Late_Cost_Data_M__c,
                  a.Days_with_Late_Cost_Data_W__c, a.Criteo_Spend_Y__c, a.Criteo_Spend_W__c, a.Criteo_Spend_M__c, a.Account__c, a.of_PCAs_net_change_diff_btw_Day7_Day1_Y__c, a.of_PCAs_net_change_diff_btw_Day7_Day1_M__c
            	FROM Advance_Metric__c a 
            	WHERE Account__c in :retrieveList])
		{
        
        	advaWrapperMap.put(a.Account__c, a); 
      	}
      
      System.debug('advaWrapperMap::After'+advaWrapperMap);
      
      adUtil.daysCostDataQueryWeekly(scopeIds, advaWrapperMap);
      adUtil.daysCostDataQueryMonthly(scopeIds, advaWrapperMap);
      adUtil.daysCostDataQueryYearly(scopeIds, advaWrapperMap);
      adUtil.daysLateRevenueQueryWeekly(scopeIds, advaWrapperMap);
      adUtil.daysLateRevenueQueryMonthly(scopeIds, advaWrapperMap);
      adUtil.daysLateRevenueQueryYearly(scopeIds, advaWrapperMap);
      adUtil.totalSpendQueryWeekly(scopeIds, advaWrapperMap);
      adUtil.totalSpendQueryMonthly(scopeIds, advaWrapperMap);
      adUtil.totalSpendQueryYearly(scopeIds, advaWrapperMap);
      adUtil.pcaLinkedPerDay(scopeIds, advaWrapperMap);
      adUtil.estimatedRevenue(scopeIds, advaWrapperMap);
      adUtil.pcaChangeDiff_7Day_1Day(scopeIds, advaWrapperMap);
      adUtil.pcaChangeDiff_7Day_1Day_Y(scopeIds, advaWrapperMap);
      adUtil.pcaChangeDiff_7Day_1Day_M(scopeIds, advaWrapperMap);

      System.debug('advaWrapperMap::After'+advaWrapperMap);
      System.debug('accChildParentMap:'+accChildParentMap);
      
      
    	for(Account acc: accountList){
        
        	String key = acc.Id;
	        Advance_Metric__c advanceMetric = advaWrapperMap.get(key) ;
	        System.debug('advanceMetric ::Map'+advanceMetric );
	        if (advanceMetric == null) {
	            advanceMetric = getAdvanceMetric(key);
	            advaWrapperMap.put(key, advanceMetric);
	        }
        
        	while(accChildParentMap.containsKey(key)){
	            System.debug('key---'+key);
	            Id parentAccountId = accChildParentMap.get(key); //key
	            if (parentAccountId == null) break;
	            Advance_Metric__c parentAdvanceMetric = advaWrapperMap.get(parentAccountId);
	            System.debug('parentAdvanceMetric ---'+parentAdvanceMetric );
	            if (parentAdvanceMetric == null) {
	                parentAdvanceMetric = getAdvanceMetric(parentAccountId);
	                advaWrapperMap.put(parentAccountId, parentAdvanceMetric);
	            }
            
	            //days with late cost data
	            parentAdvanceMetric.Days_with_Late_Cost_Data_W__c = AdvanceMetricsUtil.add(parentAdvanceMetric.Days_with_Late_Cost_Data_W__c, advanceMetric.Days_with_Late_Cost_Data_W__c);
	            parentAdvanceMetric.Days_with_Late_Cost_Data_M__c = AdvanceMetricsUtil.add(parentAdvanceMetric.Days_with_Late_Cost_Data_M__c, advanceMetric.Days_with_Late_Cost_Data_M__c);
	            parentAdvanceMetric.Days_with_Late_Cost_Data_Y__c = AdvanceMetricsUtil.add(parentAdvanceMetric.Days_with_Late_Cost_Data_Y__c, advanceMetric.Days_with_Late_Cost_Data_Y__c);
	               
	            parentAdvanceMetric.MACs_with_Late_Cost_Data_W__c = AdvanceMetricsUtil.add(parentAdvanceMetric.MACs_with_Late_Cost_Data_W__c, advanceMetric.MACs_with_Late_Cost_Data_W__c);
	            parentAdvanceMetric.MACs_with_Late_Cost_Data_M__c = AdvanceMetricsUtil.add(parentAdvanceMetric.MACs_with_Late_Cost_Data_M__c, advanceMetric.MACs_with_Late_Cost_Data_W__c) ;
	            parentAdvanceMetric.MACs_with_Late_Cost_Data_Y__c = AdvanceMetricsUtil.add(parentAdvanceMetric.MACs_with_Late_Cost_Data_Y__c, advanceMetric.MACs_with_Late_Cost_Data_Y__c);
	            
	            parentAdvanceMetric.MACs_with_Late_Revenue_Data_W__c = AdvanceMetricsUtil.add(parentAdvanceMetric.MACs_with_Late_Revenue_Data_W__c, advanceMetric.MACs_with_Late_Revenue_Data_W__c);
	            parentAdvanceMetric.MACs_with_Late_Revenue_Data_M__c = AdvanceMetricsUtil.add(parentAdvanceMetric.MACs_with_Late_Revenue_Data_M__c, advanceMetric.MACs_with_Late_Revenue_Data_M__c) ;
	            parentAdvanceMetric.MACs_with_Late_Revenue_Data_Y__c = AdvanceMetricsUtil.add(parentAdvanceMetric.MACs_with_Late_Revenue_Data_Y__c, advanceMetric.MACs_with_Late_Revenue_Data_Y__c);
	            
	            parentAdvanceMetric.Days_with_Late_Revenue_Data_W__c = AdvanceMetricsUtil.add(parentAdvanceMetric.Days_with_Late_Revenue_Data_W__c, advanceMetric.Days_with_Late_Revenue_Data_W__c);
	            parentAdvanceMetric.Days_with_Late_Revenue_Data_M__c = AdvanceMetricsUtil.add(parentAdvanceMetric.Days_with_Late_Revenue_Data_M__c, advanceMetric.Days_with_Late_Revenue_Data_M__c);
	            parentAdvanceMetric.Days_with_Late_Revenue_Data_Y__c = AdvanceMetricsUtil.add(parentAdvanceMetric.Days_with_Late_Revenue_Data_Y__c, advanceMetric.Days_with_Late_Revenue_Data_Y__c);
	            
	            parentAdvanceMetric.Total_Spend_W__c = (parentAdvanceMetric.Total_Spend_W__c  != null ? parentAdvanceMetric.Total_Spend_W__c : 0) + (advanceMetric.Total_Spend_W__c != null ? advanceMetric.Total_Spend_W__c :0) ;
	            parentAdvanceMetric.Total_Spend_M__c = (parentAdvanceMetric.Total_Spend_M__c  != null ? parentAdvanceMetric.Total_Spend_M__c : 0 )+ (advanceMetric.Total_Spend_M__c != null ? advanceMetric.Total_Spend_M__c :0 );
	            parentAdvanceMetric.Total_Spend_Y__c = (parentAdvanceMetric.Total_Spend_Y__c  != null ? parentAdvanceMetric.Total_Spend_Y__c : 0 )+ (advanceMetric.Total_Spend_Y__c != null ? advanceMetric.Total_Spend_Y__c :0 );
	            
	            parentAdvanceMetric.of_PCAs_linked_as_of_previous_day__c     = (parentAdvanceMetric.of_PCAs_linked_as_of_previous_day__c != null ? parentAdvanceMetric.of_PCAs_linked_as_of_previous_day__c : 0 )+ (advanceMetric.of_PCAs_linked_as_of_previous_day__c != null ? advanceMetric.of_PCAs_linked_as_of_previous_day__c :0 );
	            parentAdvanceMetric.of_PCAs_Expired_as_of_previous_day_7__c = (parentAdvanceMetric.of_PCAs_Expired_as_of_previous_day_7__c != null ? parentAdvanceMetric.of_PCAs_Expired_as_of_previous_day_7__c : 0) + (advanceMetric.of_PCAs_Expired_as_of_previous_day_7__c != null ? advanceMetric.of_PCAs_Expired_as_of_previous_day_7__c :0);
	            parentAdvanceMetric.of_PCAs_net_change_diff_btw_Day7_Day1__c = (parentAdvanceMetric.of_PCAs_net_change_diff_btw_Day7_Day1__c != null ? parentAdvanceMetric.of_PCAs_net_change_diff_btw_Day7_Day1__c : 0) + (advanceMetric.of_PCAs_net_change_diff_btw_Day7_Day1__c != null ? advanceMetric.of_PCAs_net_change_diff_btw_Day7_Day1__c : 0);
	            
	            parentAdvanceMetric.Of_Unique_User_Logins_as_of_previous_7__c  = (parentAdvanceMetric.Of_Unique_User_Logins_as_of_previous_7__c != null ? parentAdvanceMetric.Of_Unique_User_Logins_as_of_previous_7__c : 0) + (advanceMetric.Of_Unique_User_Logins_as_of_previous_7__c != null ? advanceMetric.Of_Unique_User_Logins_as_of_previous_7__c :0);
	            
	            parentAdvanceMetric.Total_Spend_in_Bidding_Folder_W__c = (parentAdvanceMetric.Total_Spend_in_Bidding_Folder_W__c != null ? parentAdvanceMetric.Total_Spend_in_Bidding_Folder_W__c : 0 )+ (advanceMetric.Total_Spend_in_Bidding_Folder_W__c != null ? advanceMetric.Total_Spend_in_Bidding_Folder_W__c :0); 
	            parentAdvanceMetric.Total_Spend_in_Bidding_Folder_M__c = (parentAdvanceMetric.Total_Spend_in_Bidding_Folder_M__c != null ? parentAdvanceMetric.Total_Spend_in_Bidding_Folder_M__c : 0 )+ (advanceMetric.Total_Spend_in_Bidding_Folder_M__c != null ? advanceMetric.Total_Spend_in_Bidding_Folder_M__c :0 );
	            parentAdvanceMetric.Total_Spend_in_Bidding_Folder_Y__c =  (parentAdvanceMetric.Total_Spend_in_Bidding_Folder_Y__c != null ? parentAdvanceMetric.Total_Spend_in_Bidding_Folder_Y__c : 0) + (advanceMetric.Total_Spend_in_Bidding_Folder_Y__c != null ? advanceMetric.Total_Spend_in_Bidding_Folder_Y__c :0);
	            
	            parentAdvanceMetric.Revenue_to_marin__c =  (parentAdvanceMetric.Revenue_to_marin__c != null ? parentAdvanceMetric.Revenue_to_marin__c : 0 )+ (advanceMetric.Revenue_to_marin__c != null ? advanceMetric.Revenue_to_marin__c :0);
	            parentAdvanceMetric.Revenue_to_marin_M__c = (parentAdvanceMetric.Revenue_to_marin_M__c != null ? parentAdvanceMetric.Revenue_to_marin_M__c :0) + (advanceMetric.Revenue_to_marin_M__c != null ? advanceMetric.Revenue_to_marin_M__c : 0);
	            parentAdvanceMetric.Revenue_to_marin_Y__c = (parentAdvanceMetric.Revenue_to_marin_Y__c != null ? parentAdvanceMetric.Revenue_to_marin_Y__c :0) + (advanceMetric.Revenue_to_marin_Y__c != null ? advanceMetric.Revenue_to_marin_Y__c : 0);
	            
	            parentAdvanceMetric.Other_Publisher_Spend_M__c = (parentAdvanceMetric.Other_Publisher_Spend_M__c != null ? parentAdvanceMetric.Other_Publisher_Spend_M__c : 0) + (advanceMetric.Other_Publisher_Spend_M__c != null ? advanceMetric.Other_Publisher_Spend_M__c : 0);
	            parentAdvanceMetric.Other_Publisher_Spend_W__c = (parentAdvanceMetric.Other_Publisher_Spend_W__c != null ? parentAdvanceMetric.Other_Publisher_Spend_W__c : 0) + (advanceMetric.Other_Publisher_Spend_W__c != null ? advanceMetric.Other_Publisher_Spend_W__c : 0);
	            parentAdvanceMetric.Other_Publisher_Spend_Y__c = (parentAdvanceMetric.Other_Publisher_Spend_Y__c != null ? parentAdvanceMetric.Other_Publisher_Spend_Y__c : 0) + (advanceMetric.Other_Publisher_Spend_Y__c != null ? advanceMetric.Other_Publisher_Spend_Y__c : 0);
	            
	            parentAdvanceMetric.Criteo_Spend_W__c =(parentAdvanceMetric.Criteo_Spend_W__c != null ? parentAdvanceMetric.Criteo_Spend_W__c :0)+ (advanceMetric.Criteo_Spend_W__c != null ? advanceMetric.Criteo_Spend_W__c : 0);
	            parentAdvanceMetric.Criteo_Spend_M__c =(parentAdvanceMetric.Criteo_Spend_M__c  != null ? parentAdvanceMetric.Criteo_Spend_M__c  :0)+ (advanceMetric.Criteo_Spend_M__c  != null ? advanceMetric.Criteo_Spend_M__c  : 0);
	            parentAdvanceMetric.Criteo_Spend_Y__c =(parentAdvanceMetric.Criteo_Spend_Y__c != null ? parentAdvanceMetric.Criteo_Spend_Y__c :0)+ (advanceMetric.Criteo_Spend_Y__c != null ? advanceMetric.Criteo_Spend_Y__c : 0);
	            
	            parentAdvanceMetric.Facebook_Spend_W__c = (parentAdvanceMetric.Facebook_Spend_W__c != null ? parentAdvanceMetric.Facebook_Spend_W__c : 0)+ (advanceMetric.Facebook_Spend_W__c != null ? advanceMetric.Facebook_Spend_W__c : 0);
	            parentAdvanceMetric.Facebook_Spend_M__c = (parentAdvanceMetric.Facebook_Spend_M__c != null ? parentAdvanceMetric.Facebook_Spend_M__c : 0)+ (advanceMetric.Facebook_Spend_M__c != null ? advanceMetric.Facebook_Spend_M__c : 0);
	            parentAdvanceMetric.Facebook_Spend_Y__c = (parentAdvanceMetric.Facebook_Spend_Y__c != null ? parentAdvanceMetric.Facebook_Spend_Y__c : 0)+ (advanceMetric.Facebook_Spend_Y__c != null ? advanceMetric.Facebook_Spend_Y__c : 0);
	            
	            parentAdvanceMetric.Google_Spend_W__c = (parentAdvanceMetric.Google_Spend_W__c != null ? parentAdvanceMetric.Google_Spend_W__c :0)+ (advanceMetric.Google_Spend_W__c != null ? advanceMetric.Google_Spend_W__c : 0);
	            parentAdvanceMetric.Google_Spend_M__c    = (parentAdvanceMetric.Google_Spend_M__c    != null ? parentAdvanceMetric.Google_Spend_M__c     :0)+ (advanceMetric.Google_Spend_M__c   != null ? advanceMetric.Google_Spend_M__c   : 0);
	            parentAdvanceMetric.Google_Spend_Y__c = (parentAdvanceMetric.Google_Spend_Y__c != null ? parentAdvanceMetric.Google_Spend_Y__c :0)+ (advanceMetric.Google_Spend_Y__c != null ? advanceMetric.Google_Spend_Y__c : 0);
	            
	            parentAdvanceMetric.Yahoo_Japan_Spend_W__c = (parentAdvanceMetric.Yahoo_Japan_Spend_W__c  != null ? parentAdvanceMetric.Yahoo_Japan_Spend_W__c  : 0) + (advanceMetric.Yahoo_Japan_Spend_W__c  != null ? advanceMetric.Yahoo_Japan_Spend_W__c  : 0);
	            parentAdvanceMetric.Yahoo_Japan_Spend_M__c = (parentAdvanceMetric.Yahoo_Japan_Spend_M__c  != null ? parentAdvanceMetric.Yahoo_Japan_Spend_M__c  : 0) + (advanceMetric.Yahoo_Japan_Spend_M__c  != null ? advanceMetric.Yahoo_Japan_Spend_M__c  : 0);
	            parentAdvanceMetric.Yahoo_Japan_Spend_Y__c = (parentAdvanceMetric.Yahoo_Japan_Spend_Y__c  != null ? parentAdvanceMetric.Yahoo_Japan_Spend_Y__c  : 0) + (advanceMetric.Yahoo_Japan_Spend_Y__c  != null ? advanceMetric.Yahoo_Japan_Spend_Y__c  : 0);
	            
	            parentAdvanceMetric.Universal_Channels_Spend_W__c =(parentAdvanceMetric.Universal_Channels_Spend__c != null ? parentAdvanceMetric.Universal_Channels_Spend__c : 0) + (advanceMetric.Universal_Channels_Spend__c != null ? advanceMetric.Universal_Channels_Spend__c : 0);
	            parentAdvanceMetric.Universal_Channels_Spend_M__c =(parentAdvanceMetric.Universal_Channels_Spend_M__c != null ? parentAdvanceMetric.Universal_Channels_Spend_M__c : 0) + (advanceMetric.Universal_Channels_Spend_M__c != null ? advanceMetric.Universal_Channels_Spend_M__c : 0);
	            parentAdvanceMetric.Universal_Channels_Spend_Y__c =(parentAdvanceMetric.Universal_Channels_Spend_Y__c != null ? parentAdvanceMetric.Universal_Channels_Spend_Y__c : 0) + (advanceMetric.Universal_Channels_Spend_Y__c != null ? advanceMetric.Universal_Channels_Spend_Y__c : 0);
	            
	            parentAdvanceMetric.Yahoo_Bing_Spend_W__c = AdvanceMetricsUtil.add(parentAdvanceMetric.Yahoo_Bing_Spend_W__c, advanceMetric.Yahoo_Bing_Spend_W__c);
	            parentAdvanceMetric.Yahoo_Bing_Spend_M__c = (parentAdvanceMetric.Yahoo_Bing_Spend_M__c  != null ? parentAdvanceMetric.Yahoo_Bing_Spend_M__c  : 0) +(advanceMetric.Yahoo_Bing_Spend_M__c  != null ? advanceMetric.Yahoo_Bing_Spend_M__c  : 0);
	            parentAdvanceMetric.Yahoo_Bing_Spend_Y__c = (parentAdvanceMetric.Yahoo_Bing_Spend_Y__c != null ? parentAdvanceMetric.Yahoo_Bing_Spend_Y__c : 0) +(advanceMetric.Yahoo_Bing_Spend_Y__c != null ? advanceMetric.Yahoo_Bing_Spend_Y__c : 0);
	            
	            parentAdvanceMetric.Management_Hits_W__c = (parentAdvanceMetric.Management_Hits_W__c != null ? parentAdvanceMetric.Management_Hits_W__c : 0) + (advanceMetric.Management_Hits_W__c != null ? advanceMetric.Management_Hits_W__c : 0);
	            parentAdvanceMetric.Management_Hits_M__c = (parentAdvanceMetric.Management_Hits_M__c != null ? parentAdvanceMetric.Management_Hits_M__c : 0) + (advanceMetric.Management_Hits_M__c != null ? advanceMetric.Management_Hits_M__c : 0);
	            parentAdvanceMetric.Management_Hits_Y__c = (parentAdvanceMetric.Management_Hits_Y__c != null ? parentAdvanceMetric.Management_Hits_Y__c : 0) + (advanceMetric.Management_Hits_Y__c != null ? advanceMetric.Management_Hits_Y__c : 0);
	            
	            parentAdvanceMetric.In_App_Reporting_Hits_W__c = (parentAdvanceMetric.In_App_Reporting_Hits_W__c  != null ? parentAdvanceMetric.In_App_Reporting_Hits_W__c  :0 ) + (advanceMetric.In_App_Reporting_Hits_W__c  != null ? advanceMetric.In_App_Reporting_Hits_W__c  : 0);
	            parentAdvanceMetric.In_App_Reporting_Hits_M__c = (parentAdvanceMetric.In_App_Reporting_Hits_M__c != null ? parentAdvanceMetric.In_App_Reporting_Hits_M__c :0 ) + (advanceMetric.In_App_Reporting_Hits_M__c != null ? advanceMetric.In_App_Reporting_Hits_M__c : 0);
	            parentAdvanceMetric.In_App_Reporting_Hits_Y__c = (parentAdvanceMetric.In_App_Reporting_Hits_Y__c != null ? parentAdvanceMetric.In_App_Reporting_Hits_Y__c :0 ) + (advanceMetric.In_App_Reporting_Hits_Y__c != null ? advanceMetric.In_App_Reporting_Hits_Y__c : 0);
	            
	            parentAdvanceMetric.Optimization_Hits_W__c = (parentAdvanceMetric.Optimization_Hits_W__c != null ? parentAdvanceMetric.Optimization_Hits_W__c : 0) + (advanceMetric.Optimization_Hits_W__c  != null ? advanceMetric.Optimization_Hits_W__c :0);
	            parentAdvanceMetric.Optimization_Hits_M__c = (parentAdvanceMetric.Optimization_Hits_M__c != null ? parentAdvanceMetric.Optimization_Hits_M__c : 0) + (advanceMetric.Optimization_Hits_M__c  != null ? advanceMetric.Optimization_Hits_M__c :0);
	            parentAdvanceMetric.Optimization_Hits_Y__c = (parentAdvanceMetric.Optimization_Hits_Y__c != null ? parentAdvanceMetric.Optimization_Hits_Y__c : 0) + (advanceMetric.Optimization_Hits_Y__c  != null ? advanceMetric.Optimization_Hits_Y__c :0);
	            
	            parentAdvanceMetric.Other_Hits_W__c = (parentAdvanceMetric.Other_Hits_W__c != null ? parentAdvanceMetric.Other_Hits_W__c : 0)+ (advanceMetric.Other_Hits_W__c != null ? advanceMetric.Other_Hits_W__c :0);
	            parentAdvanceMetric.Other_Hits_M__c = (parentAdvanceMetric.Other_Hits_M__c != null ? parentAdvanceMetric.Other_Hits_M__c : 0)+ (advanceMetric.Other_Hits_M__c != null ? advanceMetric.Other_Hits_M__c :0);
	            parentAdvanceMetric.Other_Hits_Y__c = (parentAdvanceMetric.Other_Hits_Y__c != null ? parentAdvanceMetric.Other_Hits_Y__c : 0)+ (advanceMetric.Other_Hits_Y__c != null ? advanceMetric.Other_Hits_Y__c :0);
	            
	            parentAdvanceMetric.Report_Creation_Hits_W__c = (parentAdvanceMetric.Report_Creation_Hits_W__c != null ? parentAdvanceMetric.Report_Creation_Hits_W__c : 0) +(advanceMetric.Report_Creation_Hits_W__c != null ? advanceMetric.Report_Creation_Hits_W__c : 0);
	            parentAdvanceMetric.Report_Creation_Hits_M__c = (parentAdvanceMetric.Report_Creation_Hits_M__c != null ? parentAdvanceMetric.Report_Creation_Hits_M__c : 0) +(advanceMetric.Report_Creation_Hits_M__c != null ? advanceMetric.Report_Creation_Hits_M__c : 0);
	            parentAdvanceMetric.Report_Creation_Hits_Y__c = (parentAdvanceMetric.Report_Creation_Hits_Y__c != null ? parentAdvanceMetric.Report_Creation_Hits_Y__c : 0) +(advanceMetric.Report_Creation_Hits_Y__c != null ? advanceMetric.Report_Creation_Hits_Y__c : 0);
	            
	            parentAdvanceMetric.Number_of_Automated_Changes_W__c = (parentAdvanceMetric.Number_of_Automated_Changes_W__c != null ? parentAdvanceMetric.Number_of_Automated_Changes_W__c : 0) + (advanceMetric.Number_of_Automated_Changes_W__c != null ? advanceMetric.Number_of_Automated_Changes_W__c : 0);
	            parentAdvanceMetric.Number_of_Automated_Changes_M__c = (parentAdvanceMetric.Number_of_Automated_Changes_M__c != null ? parentAdvanceMetric.Number_of_Automated_Changes_M__c : 0) + (advanceMetric.Number_of_Automated_Changes_M__c != null ? advanceMetric.Number_of_Automated_Changes_M__c : 0);
	            parentAdvanceMetric.Number_of_Automated_Changes_Y__c = (parentAdvanceMetric.Number_of_Automated_Changes_Y__c != null ? parentAdvanceMetric.Number_of_Automated_Changes_Y__c : 0) + (advanceMetric.Number_of_Automated_Changes_Y__c != null ? advanceMetric.Number_of_Automated_Changes_Y__c : 0);
	            
	            parentAdvanceMetric.Number_of_Changes_Synced_from_PublisherW__c = (parentAdvanceMetric.Number_of_Changes_Synced_from_PublisherW__c != null ? parentAdvanceMetric.Number_of_Changes_Synced_from_PublisherW__c : 0) + (advanceMetric.Number_of_Changes_Synced_from_PublisherW__c != null ? advanceMetric.Number_of_Changes_Synced_from_PublisherW__c  :0);
	            parentAdvanceMetric.Number_of_Changes_Synced_from_PublisherM__c = (parentAdvanceMetric.Number_of_Changes_Synced_from_PublisherM__c != null ? parentAdvanceMetric.Number_of_Changes_Synced_from_PublisherM__c : 0) + (advanceMetric.Number_of_Changes_Synced_from_PublisherM__c != null ? advanceMetric.Number_of_Changes_Synced_from_PublisherM__c  :0);
	            parentAdvanceMetric.Number_of_Changes_Synced_from_PublisherY__c = (parentAdvanceMetric.Number_of_Changes_Synced_from_PublisherY__c != null ? parentAdvanceMetric.Number_of_Changes_Synced_from_PublisherY__c : 0) + (advanceMetric.Number_of_Changes_Synced_from_PublisherY__c != null ? advanceMetric.Number_of_Changes_Synced_from_PublisherY__c  :0);
	            
	            parentAdvanceMetric.Number_of_Manual_Changes_W__c = (parentAdvanceMetric.Number_of_Manual_Changes_W__c != null ? parentAdvanceMetric.Number_of_Manual_Changes_W__c : 0) + (advanceMetric.Number_of_Manual_Changes_W__c !=  null ? advanceMetric.Number_of_Manual_Changes_W__c :0 );
	            parentAdvanceMetric.Number_of_Manual_Changes_M__c = (parentAdvanceMetric.Number_of_Manual_Changes_M__c != null ? parentAdvanceMetric.Number_of_Manual_Changes_M__c : 0) + (advanceMetric.Number_of_Manual_Changes_M__c !=  null ? advanceMetric.Number_of_Manual_Changes_M__c :0 );
	            parentAdvanceMetric.Number_of_Manual_Changes_Y__c = (parentAdvanceMetric.Number_of_Manual_Changes_Y__c != null ? parentAdvanceMetric.Number_of_Manual_Changes_Y__c : 0) + (advanceMetric.Number_of_Manual_Changes_Y__c !=  null ? advanceMetric.Number_of_Manual_Changes_Y__c :0 );
	            
	            parentAdvanceMetric.of_PCAs_net_change_diff_btw_Day7_Day1_Y__c = (parentAdvanceMetric.of_PCAs_net_change_diff_btw_Day7_Day1_Y__c != null ? parentAdvanceMetric.of_PCAs_net_change_diff_btw_Day7_Day1_Y__c : 0) + (advanceMetric.of_PCAs_net_change_diff_btw_Day7_Day1_Y__c !=  null ? advanceMetric.of_PCAs_net_change_diff_btw_Day7_Day1_Y__c :0 );
	            parentAdvanceMetric.of_PCAs_net_change_diff_btw_Day7_Day1_M__c = (parentAdvanceMetric.of_PCAs_net_change_diff_btw_Day7_Day1_M__c != null ? parentAdvanceMetric.of_PCAs_net_change_diff_btw_Day7_Day1_M__c : 0) + (advanceMetric.of_PCAs_net_change_diff_btw_Day7_Day1_M__c !=  null ? advanceMetric.of_PCAs_net_change_diff_btw_Day7_Day1_M__c :0 );
            
            	System.debug('advaWrapperMap----before upsert'+advaWrapperMap);
            	key = parentAccountId;
                
        	}//While loop end
  
    	}//for loop end
        try{
            upsert advaWrapperMap.values(); 
        }
        catch(Exception e){
            String accDataStr = String.valueOf(advaWrapperMap.values()).trim();
            ErrorLog__c eLog = new ErrorLog__c(Error_Cause__c = e.getMessage(),
                                    Error_Causing_Data__c = accDataStr,
                                    Error_Source__c = 'Advance Metric  class ExceptionLine:'+String.valueOf(e.getLineNumber()),
                                    Batch_Id__c = bcontext.getJobId());
            insert eLog;
        }

        //print some limits
        System.debug(LoggingLevel.INFO, 'Heap Size: ' + Limits.getHeapSize()/1000 + 'KB');
        System.debug(LoggingLevel.INFO, 'Script Statement: ' + Limits.getScriptStatements());
        System.debug(LoggingLevel.INFO, '# of Queries: ' + Limits.getQueries());
        System.debug(LoggingLevel.INFO, '# of Query Rows: ' + Limits.getQueryRows());
        System.debug(LoggingLevel.INFO, '# of Aggregate Queries: ' + Limits.getAggregateQueries());
        System.debug(LoggingLevel.INFO, '# of DML Statements: ' + Limits.getDmlStatements());
        System.debug(LoggingLevel.INFO, '# of DML Rows: ' + Limits.getDmlRows());

     }
     
     global void finish(Database.BatchableContext bcontext){
     
     } 
     
     public static Advance_Metric__c  getAdvanceMetric(Id accountId) {
        Advance_Metric__c a = new Advance_Metric__c(Account__c = accountId);
        a.of_PCAs_net_change_diff_btw_Day7_Day1__c =  0 ;
        a.MACs_with_Late_Cost_Data_M__c = 0;
        a.of_PCAs_linked_as_of_previous_day__c =  0;
        a.Yahoo_Japan_Spend_W__c =0 ;
        a.of_PCAs_Expired_as_of_previous_day_7__c = 0;
        a.Yahoo_Japan_Spend_Y__c =0;
        a.Yahoo_Japan_Spend_W__c = 0;
        a.Yahoo_Japan_Spend_M__c = 0;
        a.Yahoo_Bing_Spend_Y__c = 0;
        a.Yahoo_Bing_Spend_W__c =0;
        a.Yahoo_Bing_Spend_M__c = 0;
        a.Universal_Channels_Spend_Y__c = 0;
        a.Universal_Channels_Spend_W__c = 0; 
        a.Universal_Channels_Spend_M__c =0;
        a.Total_Spend_in_Bidding_Folder_Y__c =0;
        a.Total_Spend_in_Bidding_Folder_M__c =0;
        a.Total_Spend_in_Bidding_Folder_W__c =0;
        a.Total_Spend_Y__c = 0;
        a.Total_Spend_M__c =0;
        a.Total_Spend_W__c =0; 
        a.Spend_Per_Publisher_Presented_in_Graph_Y__c =0; 
        a.Spend_Per_Publisher_Presented_in_Graph_M__c = 0;
        a.Revenue_to_marin_Y__c = 0;
        a.Revenue_to_marin_M__c =0;
        a.Revenue_to_Marin__c = 0; 
        a.Report_Creation_Hits_Y__c = 0;
        a.Report_Creation_Hits_W__c = 0;
        a.Report_Creation_Hits_M__c = 0;
        a.Other_Publisher_Spend_W__c = 0;
        a.Other_Publisher_Spend_Y__c = 0;
        a.Other_Publisher_Spend_W__c = 0;
        a.Other_Publisher_Spend_M__c = 0;
        a.Other_Hits_Y__c =0;
        a.Other_Hits_W__c =0; 
        a.Other_Hits_M__c = 0;
        a.of_PCAs_net_change_diff_btw_Day7_Day1_Y__c = 0; 
        a.Optimization_Hits_Y__c = 0; 
        a.Optimization_Hits_W__c = 0;
        a.Optimization_Hits_M__c = 0; 
        a.Of_Unique_User_Logins_as_of_previous_Y__c = 0;
        a.Of_Unique_User_Logins_as_of_previous_7__c = 0;
        a.Number_of_Manual_Changes_Y__c = 0;
        a.Number_of_Manual_Changes_W__c =0;
        a.Number_of_Manual_Changes_M__c = 0;
        a.Number_of_Changes_Synced_from_PublisherY__c = 0;
        a.Number_of_Changes_Synced_from_PublisherW__c = 0;
        a.Number_of_Changes_Synced_from_PublisherM__c = 0;
        a.Number_of_Automated_Changes_Y__c = 0;
        a.Number_of_Automated_Changes_W__c = 0;
        a.Number_of_Automated_Changes_M__c = 0;
        a.Management_Hits_Y__c = 0; 
        a.Management_Hits_W__c = 0;
        a.Management_Hits_M__c = 0;
        a.MACs_with_Late_Revenue_Data_Y__c = 0;
        a.MACs_with_Late_Revenue_Data_M__c =0;
        a.MACs_with_Late_Revenue_Data_W__c = 0;
        a.MACs_with_Late_Cost_Data_Y__c = 0;
        a.MACs_with_Late_Cost_Data_W__c =0;
        a.In_App_Reporting_Hits_Y__c = 0;
        a.In_App_Reporting_Hits_W__c = 0;
        a.In_App_Reporting_Hits_M__c = 0;
        a.Google_Spend_M__c = 0;
        a.Google_Spend_W__c =0; 
        a.Google_Spend_Y__c = 0;
        a.Facebook_Spend_W__c =0;
        a.Facebook_Spend_Y__c = 0;
        a.Facebook_Spend_M__c= 0;
        a.Days_with_Late_Revenue_Data_Y__c = 0;
        a.Days_with_Late_Revenue_Data_M__c = 0;
        a.Days_with_Late_Revenue_Data_W__c = 0;
        a.Days_with_Late_Cost_Data_Y__c = 0;
        a.Days_with_Late_Cost_Data_M__c =0;
        a.Days_with_Late_Cost_Data_W__c = 0;
        a.Criteo_Spend_Y__c = 0;
        a.Criteo_Spend_W__c = 0;
        a.Criteo_Spend_M__c = 0;     
        a.of_PCAs_net_change_diff_btw_Day7_Day1_M__c = 0;   
        return a;
    }  
    
    public Set<Id> getRetrieveList(List<Account> scope, Map<Id,Id> accChildParentMap) {
    	Set<Id> retrieveList = new Set<Id>();
    	for (Account account : scope) {
    		retrieveList.add(account.Id);
    		Id parentId = account.ParentId;
    		while (parentId != null) {
    			retrieveList.add(parentId);
    			parentId = accChildParentMap.get(parentId);
    		}
    		
    	}
    	return retrieveList;
    }
}