/**
 * Author:      Luke Martell
 * Date:        5/23/2011
 *
 * Comments:
 * This controller maintains a new Web-to-Case form that allows the user to submit
 * an attachment. The attachment will automatically be assigned to the Case record
 * when it is created. In addition, a CC list field allows for the users to submit
 * up to 5 different e-mails (comma-separated).  
 */
public class CustomWebToCaseController {

    /**
     * Variables to control the different types of validation errors. Each validation
     * will show a different message on the VFP.
     */
    public Boolean noProblemAreaSupplied { get; set; }
    public Boolean noDescriptionSupplied { get; set; }
    public Boolean noPrioritySupplied { get; set; }
    public Boolean fileSizeTooLarge { get; set; }
    public Boolean noIssueSupplied { get; set; }
    public Boolean noEmailSupplied { get; set; }

    public String ccErrorMsg { get; set; }
    public Integer bodyLength { get; set; }

    /**
     * Variables to contain the Case, Attachment, and CC list data.
     */
    public Attachment attachment { get; set; }
    public Case webCase { get; set; }
    public String ccList { get; set; }

    /**
     * Constructor will pre-populate some data on the Case via query parameters being 
     * passed to the VFP. Each individual field will need to be mapped to a particular
     * Case field.
     */
    public CustomWebToCaseController() {
        String contactName = ApexPages.currentPage().getParameters().get('n');
        String emailAddress = ApexPages.currentPage().getParameters().get('e');
        String clientId = ApexPages.currentPage().getParameters().get('cid');
        
        attachment = new Attachment();
        webCase = new Case(
            SuppliedName = contactName,
            SuppliedEmail = emailAddress,
            Client_ID__c = (clientId != null ? Integer.valueOf(clientId) : null),
            Origin = 'Web To Case'
        );
    }
    
    /**
     * When the form has been submitted then split the CC list if data was filled out 
     * and populate the different CC fields. If an attachment was selected then create
     * the record after the Case record was successfully created. Any errors will return
     * back to the form page and display using the standard error format.
     */
    public PageReference createCase() {
        PageReference savePage = null;
        
        System.Savepoint sp = Database.setSavepoint();
        try {
            if (validateFormInput()) {
                if (ccList != null && ccList.length() > 0) {
                    String[] ccParts = ccList.split(',');
                    
                    for (Integer i = 0; i < ccParts.size(); i++) {
                        if (i == 0) { webCase.CC_1__c = ccParts[i]; }
                        if (i == 1) { webCase.CC_2__c = ccParts[i]; }
                        if (i == 2) { webCase.CC_3__c = ccParts[i]; }
                        if (i == 3) { webCase.CC_4__c = ccParts[i]; }
                        if (i == 4) { webCase.CC_5__c = ccParts[i]; }
                    }
                }
                insert webCase;
                
                if (attachment.Name != null) {
                    attachment.ParentId = webCase.Id;
                    insert attachment;
                }
                savePage = Page.WebToCaseThankYou;
                savePage.setRedirect(true);
            }
        } catch (System.Exception e) {
            if (e.getMessage().contains('INVALID_EMAIL_ADDRESS')) {
                String[] vals = e.getMessage().split(':');
                ccErrorMsg = 'Invalid e-mail in CC list: ' + vals[3];
            }
            Database.rollback(sp);
        } finally {
            attachment.body = null;
            attachment = new Attachment();
        }
        return savePage;
    }
    
    /**
     * The cancel button just returns back to the same page. Not sure if the window 
     * closure is handled by Javascript or what but the cancel button does nothing.
     */
    public PageReference cancel() {
        return null;
    }
    
    /** 
     * Validate all the form input. All the required fields are handled this way because
     * of the Marin branding on the VFP. 
     */
    private Boolean validateFormInput() {
        Boolean formIsValid = true;
        
        noDescriptionSupplied = false;
        noProblemAreaSupplied = false;
        noPrioritySupplied = false;
        fileSizeTooLarge = false;
        noIssueSupplied = false;
        noEmailSupplied = false;
        ccErrorMsg = null;
        
        if (webCase.SuppliedEmail == null || webCase.SuppliedEmail.length() == 0) { 
            noEmailSupplied = true; 
            formIsValid = false; 
        } else if (webCase.Case_Reason__c == null || webCase.Case_Reason__c.length() == 0) {
            noProblemAreaSupplied = true;
            formIsValid = false;
        } else if (webCase.Case_Reason_Detail__c == null || webCase.Case_Reason_Detail__c.length() == 0) {
            noIssueSupplied = true;
            formIsValid = false;
        } else if (webCase.Pritority__c == null || webCase.Pritority__c.length() == 0) {
            noPrioritySupplied = true;
            formIsValid = false;
        } else if (webCase.Description == null || webCase.Description.length() == 0) {
            noDescriptionSupplied = true;
            formIsValid = false;
        } else if (bodyLength > (1024*5000)) {
            fileSizeTooLarge = true;
            formIsValid = false;
        }
        return formIsValid;
    }
}