/**
 * This Apex Class is being used to test the prevent_dupe_attendee trigger built
 * Created by: Gary Sopko
 * Created Date: 04/22/2013
 * Please consult with Sales Operations/System Administration before updating.
 */
@isTest
private class testprevent_dupe_attendee {

    static testMethod void CreateContactInsertTwice() {
        //Insert Account
        Account a = new Account(Name = 'TestAccount');
            insert a;
    //Insert Marketing Operations Request
        Marketing_Operations_Request__c mor = new Marketing_Operations_Request__c(Name = 'test MOR', Status__c = TRUE, Request_Date__c = Date.Today());
        insert mor;
    //Insert Contact
        Contact c = new Contact(FirstName = 'test', LastName = 'test', AccountId = a.Id);
        insert c;
    //Insert Invited Contact
        Invited_Contact__c ic = new Invited_Contact__c(Contact__c = c.Id, Marketing_Operations_Request__c = mor.id);
        insert ic;  
    }
}