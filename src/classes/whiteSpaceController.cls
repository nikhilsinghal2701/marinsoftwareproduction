// Jonathan Hersh - jhersh@salesforce.com - 8/24/2009
/**
Modified by Mark Klinski for use on Account Plan page layout, using Products
and showing child accounts up to five levels deep.
**/

public with sharing class whiteSpaceController {
	public Account parentAcct				{ get; set; }
	public Product2[] products	{ get; set; }
	public tableRow[] trs					{ get; set; }
	
	public string error						{ get; set; }
	
	public whiteSpaceController(ApexPages.StandardController controller) {
		string acctID;
		SFDC_Acct_Plan__c acctPlan;

		if (ApexPages.currentpage().getparameters().get('aid') != null) {
			acctID = ApexPages.currentpage().getparameters().get('aid');
		} else {
			controller.addFields(new list<string>{'Account__c'});
           	acctPlan = (SFDC_Acct_Plan__c) controller.getRecord();
			acctID = acctPlan.Account__c;
		}

		if (acctID == null || acctID == '') {
			error = 'No Account ID was specified.';
			return;
		}
		
		parentAcct = [select id, name, parentid
			from Account
			where id = :acctID];
			
		if (parentAcct == null) {
			error = 'A valid account could not be found.';
			return;
		}
		
		// All products to filter
		products = [SELECT Id, Name, Family FROM Product2 WHERE IsActive = true ORDER BY Name, Family];
			
		if (products.size() == 0) {
			error = 'There are no active products.';
			return;
		}
			
		Id[] prodIDs = new Id[] {};
		
		for (Product2 product : products) {
			prodIDs.add(product.Id);
		}
			
		// All accounts in this family
		Account[] accts = [SELECT id, Account_Coordinates_Name__c 
			FROM Account
			WHERE ParentId = :parentAcct.id OR
			Parent.ParentId = :parentAcct.id OR
			Parent.Parent.ParentId = :parentAcct.id OR
			Parent.Parent.Parent.ParentId = :parentAcct.id OR
			Parent.Parent.Parent.Parent.ParentId = :parentAcct.id OR
			Parent.Parent.Parent.Parent.Parent.ParentId = :parentAcct.id OR
			id = :parentAcct.id ORDER BY Account_Coordinates_Name__c];
			
		ID[] acctIDs = new ID[] {};
		
		for (Account a : accts) {
			acctIDs.add( a.id );
		}
			
		// All assets on those accounts for the products we're interested in
		OpportunityLineItem[] assets = [select Id, OpportunityId, Opportunity.AccountId, PricebookEntry.Product2Id, PricebookEntry.Product2.Name
			from OpportunityLineItem 
			WHERE OpportunityId IN (SELECT Id FROM Opportunity WHERE AccountId = :acctIDs)];
			
		// Build the table data
		Map<ID, Set<ID>> tData = new Map<ID, Set<ID>> (); 
		Boolean[] rowData;
		trs = new tableRow[] {};
		
		for (Account a : accts) {
			tData.put(a.id, new Set<ID> {});
			rowData = new Boolean[] {};
			
			for (OpportunityLineItem oli : assets) {
				if (oli.Opportunity.AccountId == a.id) {
					Set<ID> tmp = tData.get(a.id);
					
					if (!tmp.contains(oli.PricebookEntry.Product2Id)) {
						tmp.add(oli.PricebookEntry.Product2Id);
					}
					
					tData.put(a.id, tmp);
				}
			}
				
			for (Product2 product : products) {
				rowData.add(tData.get(a.id).contains(product.Id));
			}
				
			trs.add(new tableRow(a, rowData));
		}
	}
	
	public class tableRow {
		public Account a		{ get; set; }
		public Boolean[] prods	{ get; set; }
		
		public tableRow( Account tmp, Boolean[] tmp2 ) {
			a = tmp;
			prods = tmp2;
		}
	}
	
}