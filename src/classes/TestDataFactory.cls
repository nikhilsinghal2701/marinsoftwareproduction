public with sharing class TestDataFactory {

	public static List<Account> createAccount(Integer numToInsert, Boolean doInsert, 
		Map<String, Object> attributes ) 
	{
		List<Account> records = new List<Account>();
		for(Integer i = 0; i < numToInsert; i++) {
			Account a = new Account(Name = 'Test Account' + i);
			records.add(a);
		}

		return createRecords(records, doInsert, attributes);
	}
	
	public static User createUser(String profileName, Boolean saveToDB) {
		string randomName = string.valueof(Datetime.now()).replace('-','').replace(':','').replace(' ','');
		Profile p = [SELECT Id FROM Profile WHERE Name = :profileName]; 
        User user = new User();
        user.Alias = 'MrUser';
        user.Email='MrUser@test.com';
        user.LastName = 'MrUser Last Name';
        user.ProfileId = p.Id;
        user.userName = randomName+system.now().millisecond()+'MrUser@test.com';
        user.EmailEncodingKey = 'UTF-8';
        user.LanguageLocaleKey = 'en_US';
        user.LocaleSidKey = 'en_US';
        user.TimeZoneSidKey = 'America/Los_Angeles';
        if (saveToDB){
        	insert user;
        }
        system.debug('user***'+user);
        return user;		
	}
	
	public static List<Opportunity> createOpportunity(Integer numToInsert, Boolean doInsert, 
		Map<String, Object> attributes ) 
	{
		List<Opportunity> records = new List<Opportunity>();
		for(Integer i = 0; i < numToInsert; i++) {
			Opportunity o = new Opportunity(Name = 'Test Opportunity' + i);
			records.add(o);
		}

		return createRecords(records, doInsert, attributes);
	}
	
	public static List<Attainment__c> createAttainment(Integer numToInsert, Boolean doInsert, 
		Map<String, Object> attributes ) 
	{
		List<Attainment__c> records = new List<Attainment__c>();
		for(Integer i = 0; i < numToInsert; i++) {
			Attainment__c o = new Attainment__c(Name = 'Test Attainment' + i);
			records.add(o);
		}

		return createRecords(records, doInsert, attributes);
	}
	
	public static List<Marin_App_Client__c> createMarinAppClient(Integer numToInsert, 
		Boolean doInsert, Map<String, Object> attributes ) 
	{
        /*List<AggregateResult> results = [SELECT max(Client_Id__c) maxClientId 
        	FROM Marin_App_Client__c];
        Decimal maxClientId = (Decimal) results.get(0).get('maxClientId');*/
        List<Marin_App_Client__c> clients = [SELECT Client_Id__c 
        	FROM Marin_App_Client__c
        	ORDER BY Client_Id__c DESC
        	LIMIT 1];
        Decimal maxClientId = clients.isEmpty() ? 1 : clients.get(0).Client_Id__c + 1;

		List<Marin_App_Client__c> records = new List<Marin_App_Client__c>();
		for(Integer i = 0; i < numToInsert; i++) {
	        maxClientId = (maxClientId == null) ? 0 : maxClientId + 1;
			Marin_App_Client__c a = new Marin_App_Client__c(Client_ID__c = maxClientId,
				Customer_ID__c = 781, Client_Status__c = 'New');
			records.add(a);
		}
		
		return createRecords(records, doInsert, attributes);
	}

	public static List<Metric__c> createMetric(Integer numToInsert, Boolean doInsert, 
		Map<String, Object> attributes ) 
	{
		List<Metric__c> records = new List<Metric__c>();

		for(Integer i = 0; i < numToInsert; i++) {
        
	        Metric__c metric  =  new Metric__c(Yahoo_Japan_Spend__c =2 , Yahoo_Japan_Spend_MTD__c = 3, Yahoo_Bing_Spend__c =  4, 
	        Yahoo_Bing_Spend_MTD__c =  1, Universal_Channels_Spend__c = 3 , Universal_Channels_Spend_MTD__c = 7, 
	        Unique_User_Logins__c= 5, Total_Spend_in_Bidding_Folders__c = 1,  Total_Spend_MTD__c= 3, 
	        Revenue_to_Marin__c = 1, Revenue_Data_on_Time__c = false, Report_Creation_Hits__c = 3, Other_Publisher_Spend__c=6, 
	        Other_Publisher_Spend_MTD__c = 2, Other_Hits__c = 2, Optimization_Hits__c =7 , Number_of_Manual_Changes__c =9, 
	        Number_of_Linked_PCAs__c = 1, Number_of_Expired_PCAs__c = 1, Number_of_Changes_Synced_from_Publisher__c = 1, Number_of_Automated_Changes__c =2, 
	        Management_Hits__c = 4, In_App_Reporting_Hits__c = 3, Google_Spend__c =7, Google_Spend_MTD__c =9, Facebook_Spend__c =9,
	        Cost_Data_on_Time__c = false);

			records.add(metric);
		}

		return createRecords(records, doInsert, attributes);
	}

	public static List<Monthly_Metric__c> createMonthlyMetric(Integer numToInsert, Boolean doInsert, 
		Map<String, Object> attributes ) 
	{
		List<Monthly_Metric__c> records = new List<Monthly_Metric__c>();

		for(Integer i = 0; i < numToInsert; i++) {
        
       		Monthly_Metric__c metric  =  new Monthly_Metric__c(Yahoo_Japan_Spend__c =2 ,  Yahoo_Bing_Spend__c =  4, 
				Universal_Channels_Spend__c = 3 , 
			 	Total_Spend_in_Bidding_Folders__c = 1,  
				Revenue_to_Marin__c = 1, Revenue_Data_on_Time__c = false, Report_Creation_Hits__c = 3, Other_Publisher_Spend__c=6, 
			 	Other_Hits__c = 2, Optimization_Hits__c =7 , Number_of_Manual_Changes__c =9, 
			 	Number_of_Changes_Synced_from_Publisher__c = 1, Number_of_Automated_Changes__c =2, 
				Management_Hits__c = 4, In_App_Reporting_Hits__c = 3, Google_Spend__c =7,  Facebook_Spend__c =9,
				Cost_Data_on_Time__c = false);
			
 
			records.add(metric);
		}

		createRecords(records, doInsert, attributes);
		for (Monthly_Metric__c metric : records) {
			metric.Ext_Id__c = metric.Marin_App_Client__c + '-' 
				+ metric.Date__c.month() + '-' + metric.Date__c.year();
			
		}
		insert records;
		return records;
	}
	
	public static List<SObject> createRecords(List<SObject> records, 
		Boolean doInsert, Map<String, Object> attributes)
	{
		Integer i = 0;
		for (Sobject record : records) {
			for (String key : attributes.keySet()) {
				Object value = attributes.get(key);
				if (value instanceof List<Object>) {
					record.put(key, ((List<Object>) value).get(i));
				} else {
					record.put(key, value);
				}
				
			}
			i++;
		}

		if (doInsert) {
			insert records;
		}
		return records;	
	}
}