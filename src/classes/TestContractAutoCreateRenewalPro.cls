/**
 * This Apex Class is being used to test the ContractAutoCreateRenealPro trigger built
 * Created by: Gary Sopko
 * Created Date: 05/10/2013
 * Please consult with Sales Operations/System Administration before updating.
 */
@isTest
private class TestContractAutoCreateRenewalPro {

    static testMethod void CreateContract() {
        // Step 1: Create Contract WHERE Product Version = Professional AND Contract Expiration Notice = TODAY
        Account acc = new Account();
	        acc.Name = 'TestAccount';
	        acc.Type = 'Advertiser';
	        acc.Account_Status__c = 'Active Customer';
	        acc.Region__c = 'NA';
	        acc.BillingCountry = 'United States';
	        acc.Product_Version__c = 'Professional';
	        acc.OwnerId = UserInfo.getUserId();
        insert acc;
        Contract c = new Contract();
	        c.AccountId = acc.Id;
	        c.Name = acc.Name;
	        c.OwnerId = acc.OwnerId;
	        c.Primary_Billing_Contact__c = 'Gary Sopko Testing';
	        c.Business_Type__c = 'Direct';
	        c.Product_Version__c = 'Professional';
	        c.StartDate = date.Today().addDays(-305);
	        c.EndDate = date.Today().addDays(60);
	        c.OwnerExpirationNotice = '60';
	        c.Contract_Type__c = '1 Year';
	        c.Customer_Status__c = 'Customer';
        insert c;
        ContractAutoCreateRenewalPro cacrp = new ContractAutoCreateRenewalPro();
        cacrp.CreateOppfromContract();
    }
}