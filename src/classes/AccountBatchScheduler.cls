/**
AccountBatchScheduler schedules the calculation of Account Metrics 
on Objects Opportuity, Cases, Integrations
 
**/
global with sharing class AccountBatchScheduler implements Schedulable{
	
	global AccountBatch testAccBatch = new AccountBatch();
	global void execute(SchedulableContext SC) {
     AccountBatch acctBt = new AccountBatch() ;
     if(Test.isRunningTest())
     	acctBt = testAccBatch;
     Database.executeBatch(acctBt,100);
  }
}