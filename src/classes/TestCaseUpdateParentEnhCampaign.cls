/**
 * This Apex Class is being used to test the CaseUpdateParentEnhCampaign trigger built
 * Created by: Gary Sopko
 * Created Date: 05/09/2013
 * Please consult with Sales Operations/System Administration before updating.
 */
@isTest
private class TestCaseUpdateParentEnhCampaign {

    static testMethod void InsertCase() {
        //1.  Insert Enhanced Campaign Migration Case
        Case c = new Case();
        c.Description = 'TestDescription';
        c.Subject = 'TestSubject';
        String rectyp = [Select Id from RecordType WHERE sObjectType = 'Case' AND Name = 'Enhanced Campaign Migration' LIMIT 1].Id;
        c.RecordTypeId = rectyp;
        c.Status = 'New';
        c.Client_Id__c = 100;
        c.White_Glove_Client__c = TRUE;
        c.Enhanced_Campaigns_Enabled__c = TRUE;
        c.Swim_Lane__c = 'A';
        c.Integration_Type__c = '123';
        c.Total_Enhanced_Campaigns__c = 123;
        c.Total_Campaigns__c = 123;
        insert c;
        //2.  Insert Enhanced Campaign Request Case
        Case ecr = new Case();
        ecr.Description = 'TestDescription';
        ecr.Subject = 'TestSubject';
        String rectyp2 = [Select Id from RecordType WHERE sObjectType = 'Case' AND Name = 'Support Services' LIMIT 1].Id;
        ecr.RecordTypeId = rectyp2;
        ecr.Case_Reason__c = 'Enhanced Campaigns';
        ecr.Status = 'New';
        ecr.Client_Id__c = 100;
        insert ecr;
        //Assert some stuff... meh... Let's not assert some stuff.
    }
    static testMethod void InsertException(){
    	//1.  Insert Enhanced Campaign Request Case
        Case ecr = new Case();
        ecr.Description = 'TestDescription';
        ecr.Subject = 'TestSubject';
        String rectyp2 = [Select Id from RecordType WHERE sObjectType = 'Case' AND Name = 'Support Services' LIMIT 1].Id;
        ecr.RecordTypeId = rectyp2;
        ecr.Case_Reason__c = 'Enhanced Campaigns';
        ecr.Status = 'New';
        ecr.Client_Id__c = 100;
        insert ecr;
    }
}