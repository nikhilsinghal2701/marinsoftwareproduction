/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class testupdateCustomerLookuponCase {

    static testMethod void myUnitTest() {
        //Step 1:  Create New Account
        Account a = new Account();
        	a.Name = 'TestAccount';
        	a.Type = 'Advertiser';
        	a.Region__c = 'NA';
        	a.Account_Status__c = 'Active Customer';
        	a.BillingCountry = 'United States';
        	a.OwnerId = UserInfo.getUserId();
    	insert a;
        //Step 2:  Create Customer Records (2)
        Customer__c customer = new Customer__c();
        	customer.Name = 'TestCusotmer';
        	customer.OwnerId = UserInfo.getUserId();
        	customer.Primary_OMM__c = UserInfo.getUserId();
        	customer.Primary_CSR__c = UserInfo.getUserId();
        	customer.Client_ID_s__c = '1';
        	customer.Account__c = a.Id;
        	customer.Marin_CIDs__c = '1';
		insert customer;
		Customer__c customer2 = new Customer__c();
			customer2.Name = 'TestCusotmer';
        	customer.OwnerId = UserInfo.getUserId();
        	customer2.Primary_OMM__c = UserInfo.getUserId();
        	customer2.Primary_CSR__c = UserInfo.getUserId();
        	customer2.Client_ID_s__c = '2';
        	customer.Account__c = a.Id;
        	customer2.Marin_CIDs__c = '2';
		insert customer2;
        //Step 3:  Create test Case
        Case c = new Case();
        	c.Client_Id__c = 1;
        	c.Subject = 'Test Subject';
        	c.Description = 'Who cares';
        	c.Requestor__c = UserInfo.getUserId();
            c.Creation_Reason__c = 'Account';
            c.Reason = 'Account Setup';
        insert c;
        Case[] cupdate = [Select Id, Client_Id__c, Primary_OMM_del__c from Case WHERE Id = :c.Id];
        cupdate[0].Client_ID__c = 2;
        update cupdate;
        cupdate[0].Client_ID__c = 5;
        update cupdate;
        cupdate[0].Primary_OMM_del__c = null;
        update cupdate;
        
    }
}