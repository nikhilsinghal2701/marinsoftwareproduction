/**
 * This Apex Class is being used to test the AdvanceMetricUpdateAccount trigger built
 * Created by: Gary Sopko
 * Created Date: 7/9/2013
 * Please consult with Sales Operations/System Administration before updating.
 */
@isTest
private class TestAdvanceMetricUpdateAccount {

    static testMethod void Test1() {
        Account a = new Account();
        	a.Name = 'TestAccount';
        	a.Type = 'Advertiser';
			a.Account_Status__c = 'Active Customer';
			a.BillingCountry = 'United States';
			a.Region__c = 'NA';
        insert a;
        Account a2 = new Account();
        	a2.Name = 'TestAccount2';
        	a2.Type = 'Advertiser';
			a2.Account_Status__c = 'Active Customer';
			a2.BillingCountry = 'United States';
			a2.Region__c = 'NA';
    	insert a2;
        Advance_Metric__c am = new Advance_Metric__c();
        	am.Account__c = a.Id;
    	insert am;
    	for(Advance_Metric__c advmet : [Select Id, Account__c from Advance_Metric__c WHERE Id = :am.Id]){
    		advmet.Account__c = a2.Id;
    		update advmet;
    	} 
    }
}