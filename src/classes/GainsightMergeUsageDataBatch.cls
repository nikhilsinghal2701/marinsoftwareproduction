// Marin Code
// Combine Duplicate usage data records. Meant to be run with a scope size of 1.

Global class GainsightMergeUsageDataBatch implements Database.Batchable<sObject> {

	// Variables needed to start our batch
	global final String query;
	global final Date startDate;
	global final Date endDate;

	global GainsightMergeUsageDataBatch(Date targetDate) {

		startDate = targetDate.toStartOfWeek();
		endDate = startDate.addDays(7);

		query = 'SELECT Id, JBCXM__Account__c FROM JBCXM__CustomerInfo__c';
	}

	global void execute(Database.BatchableContext BC, List<JBCXM__CustomerInfo__c> scope) {

		List<Id> listAcctId = new List<Id>();
		JBCXM__UsageData__c UDtoKeep;
		List<JBCXM__UsageData__c> listUDToDelete = new List<JBCXM__UsageData__c>();


		for (JBCXM__CustomerInfo__c CI : scope) {

			listAcctId.add(CI.JBCXM__Account__c);
		}

		// Query all Usage Data records for the related Account for the given date.
		for (JBCXM__UsageData__c UD : [SELECT Id, JBCXM__Account__c, JBCXM__Date__c, Name,
											Campaigns_Activity_Count__c, Campaigns_Total_Count__c, Client_On_Time_Data_Bidding__c, Client_On_Time_Data_Cost__c,
											Client_On_Time_Data_Page_Load__c, Client_On_Time_Data_Reporting__c, Client_On_Time_Data_Revenue__c, Creatives_Activity_Count__c,
											Creatives_Total_Count__c, Dimensions_All_Activity_Count__c, Dimensions_Roll_Up_Activity_Count__c,
											Dimensions_Tagging_Only_Activity_Count__c, Dimensions_Roll_Up_Count__c, Dimensions_Tagging_Only_Count__c,
											Dimensions_Total_Count__c, Folder_All_Activity_Count__c, Folder_On_Traffic_Activity_Count__c, Folders_On_Traffic_Count__c,
											Folders_On_Traffic_Spend_USD__c, Folders_Total_Spend_USD__c, Folder_Total_Count__c, Groups_Activity_Count__c, Groups_Total_Count__c,
											Keywords_Activity_Count__c, Keywords_Total_Count__c, Publisher_Account_Activity_Count__c,
											Publisher_Account_Active_Count__c, Publisher_Account_Deleted_Coun__c, Publisher_Account_Inactive__c, Publisher_Account_New_Count__c,
											Publisher_Account_Total_Count__c, Reports_All_Activity_Count__c, Reports_One_Time_All_Formats__c, Reports_Recurring_All_Formats__c,
											Reports_Total_Count__c, Revenue_Error__c, Rprt_1_tme_All_Frmt_Actvty_Cnt__c, Rprt_Rcrrng_All_Frmt_Actvty_Cnt__c, Spend__c,
											Support_Cases__c, test__c, Test_July18__c, Test_Measure__c, User_Activity_Click_Count__c, User_Activity_Login_Count__c
									   FROM JBCXM__UsageData__c
									   WHERE JBCXM__Account__c IN :listAcctId
										   AND Name = 'INSTANCELEVEL'
										   AND JBCXM__Date__c >= :startDate
										   AND JBCXM__Date__c < :endDate
									   LIMIT 10000]) {

			// Get the UD record to keep.
			if (UDtoKeep == null) {

				UDtoKeep = UD;
			}

			else {

				// Merge values onto the UDtoKeep, delete UD. In case of null values, treat as 0.
				if (UDtoKeep.Campaigns_Activity_Count__c				!= null) { UDtoKeep.Campaigns_Activity_Count__c					+= (UD.Campaigns_Activity_Count__c					!= null ? UD.Campaigns_Activity_Count__c				: 0); } else { UDtoKeep.Campaigns_Activity_Count__c					= (UD.Campaigns_Activity_Count__c				!= null ? UD.Campaigns_Activity_Count__c				: 0); }
				if (UDtoKeep.Campaigns_Total_Count__c					!= null) { UDtoKeep.Campaigns_Total_Count__c					+= (UD.Campaigns_Total_Count__c						!= null ? UD.Campaigns_Total_Count__c					: 0); } else { UDtoKeep.Campaigns_Total_Count__c					= (UD.Campaigns_Total_Count__c					!= null ? UD.Campaigns_Total_Count__c					: 0); }
				if (UDtoKeep.Client_On_Time_Data_Bidding__c				!= null) { UDtoKeep.Client_On_Time_Data_Bidding__c				+= (UD.Client_On_Time_Data_Bidding__c				!= null ? UD.Client_On_Time_Data_Bidding__c				: 0); } else { UDtoKeep.Client_On_Time_Data_Bidding__c				= (UD.Client_On_Time_Data_Bidding__c			!= null ? UD.Client_On_Time_Data_Bidding__c				: 0); }
				if (UDtoKeep.Client_On_Time_Data_Cost__c				!= null) { UDtoKeep.Client_On_Time_Data_Cost__c					+= (UD.Client_On_Time_Data_Cost__c					!= null ? UD.Client_On_Time_Data_Cost__c				: 0); } else { UDtoKeep.Client_On_Time_Data_Cost__c					= (UD.Client_On_Time_Data_Cost__c				!= null ? UD.Client_On_Time_Data_Cost__c				: 0); }
				if (UDtoKeep.Client_On_Time_Data_Page_Load__c			!= null) { UDtoKeep.Client_On_Time_Data_Page_Load__c			+= (UD.Client_On_Time_Data_Page_Load__c				!= null ? UD.Client_On_Time_Data_Page_Load__c			: 0); } else { UDtoKeep.Client_On_Time_Data_Page_Load__c			= (UD.Client_On_Time_Data_Page_Load__c			!= null ? UD.Client_On_Time_Data_Page_Load__c			: 0); }
				if (UDtoKeep.Client_On_Time_Data_Reporting__c			!= null) { UDtoKeep.Client_On_Time_Data_Reporting__c			+= (UD.Client_On_Time_Data_Reporting__c				!= null ? UD.Client_On_Time_Data_Reporting__c			: 0); } else { UDtoKeep.Client_On_Time_Data_Reporting__c			= (UD.Client_On_Time_Data_Reporting__c			!= null ? UD.Client_On_Time_Data_Reporting__c			: 0); }
				if (UDtoKeep.Client_On_Time_Data_Revenue__c				!= null) { UDtoKeep.Client_On_Time_Data_Revenue__c				+= (UD.Client_On_Time_Data_Revenue__c				!= null ? UD.Client_On_Time_Data_Revenue__c				: 0); } else { UDtoKeep.Client_On_Time_Data_Revenue__c				= (UD.Client_On_Time_Data_Revenue__c			!= null ? UD.Client_On_Time_Data_Revenue__c				: 0); }
				if (UDtoKeep.Creatives_Activity_Count__c				!= null) { UDtoKeep.Creatives_Activity_Count__c					+= (UD.Creatives_Activity_Count__c					!= null ? UD.Creatives_Activity_Count__c				: 0); } else { UDtoKeep.Creatives_Activity_Count__c					= (UD.Creatives_Activity_Count__c				!= null ? UD.Creatives_Activity_Count__c				: 0); }
				if (UDtoKeep.Creatives_Total_Count__c					!= null) { UDtoKeep.Creatives_Total_Count__c					+= (UD.Creatives_Total_Count__c						!= null ? UD.Creatives_Total_Count__c					: 0); } else { UDtoKeep.Creatives_Total_Count__c					= (UD.Creatives_Total_Count__c					!= null ? UD.Creatives_Total_Count__c					: 0); }
				if (UDtoKeep.Dimensions_All_Activity_Count__c			!= null) { UDtoKeep.Dimensions_All_Activity_Count__c			+= (UD.Dimensions_All_Activity_Count__c				!= null ? UD.Dimensions_All_Activity_Count__c			: 0); } else { UDtoKeep.Dimensions_All_Activity_Count__c			= (UD.Dimensions_All_Activity_Count__c			!= null ? UD.Dimensions_All_Activity_Count__c			: 0); }
				if (UDtoKeep.Dimensions_Roll_Up_Activity_Count__c		!= null) { UDtoKeep.Dimensions_Roll_Up_Activity_Count__c		+= (UD.Dimensions_Roll_Up_Activity_Count__c			!= null ? UD.Dimensions_Roll_Up_Activity_Count__c		: 0); } else { UDtoKeep.Dimensions_Roll_Up_Activity_Count__c		= (UD.Dimensions_Roll_Up_Activity_Count__c		!= null ? UD.Dimensions_Roll_Up_Activity_Count__c		: 0); }
				if (UDtoKeep.Dimensions_Tagging_Only_Activity_Count__c	!= null) { UDtoKeep.Dimensions_Tagging_Only_Activity_Count__c	+= (UD.Dimensions_Tagging_Only_Activity_Count__c	!= null ? UD.Dimensions_Tagging_Only_Activity_Count__c	: 0); } else { UDtoKeep.Dimensions_Tagging_Only_Activity_Count__c	= (UD.Dimensions_Tagging_Only_Activity_Count__c	!= null ? UD.Dimensions_Tagging_Only_Activity_Count__c	: 0); }
				if (UDtoKeep.Dimensions_Roll_Up_Count__c				!= null) { UDtoKeep.Dimensions_Roll_Up_Count__c					+= (UD.Dimensions_Roll_Up_Count__c					!= null ? UD.Dimensions_Roll_Up_Count__c				: 0); } else { UDtoKeep.Dimensions_Roll_Up_Count__c					= (UD.Dimensions_Roll_Up_Count__c				!= null ? UD.Dimensions_Roll_Up_Count__c				: 0); }
				if (UDtoKeep.Dimensions_Tagging_Only_Count__c			!= null) { UDtoKeep.Dimensions_Tagging_Only_Count__c			+= (UD.Dimensions_Tagging_Only_Count__c				!= null ? UD.Dimensions_Tagging_Only_Count__c			: 0); } else { UDtoKeep.Dimensions_Tagging_Only_Count__c			= (UD.Dimensions_Tagging_Only_Count__c			!= null ? UD.Dimensions_Tagging_Only_Count__c			: 0); }
				if (UDtoKeep.Dimensions_Total_Count__c					!= null) { UDtoKeep.Dimensions_Total_Count__c					+= (UD.Dimensions_Total_Count__c					!= null ? UD.Dimensions_Total_Count__c					: 0); } else { UDtoKeep.Dimensions_Total_Count__c					= (UD.Dimensions_Total_Count__c					!= null ? UD.Dimensions_Total_Count__c					: 0); }
				if (UDtoKeep.Folder_All_Activity_Count__c				!= null) { UDtoKeep.Folder_All_Activity_Count__c				+= (UD.Folder_All_Activity_Count__c					!= null ? UD.Folder_All_Activity_Count__c				: 0); } else { UDtoKeep.Folder_All_Activity_Count__c				= (UD.Folder_All_Activity_Count__c				!= null ? UD.Folder_All_Activity_Count__c				: 0); }
				if (UDtoKeep.Folder_On_Traffic_Activity_Count__c		!= null) { UDtoKeep.Folder_On_Traffic_Activity_Count__c			+= (UD.Folder_On_Traffic_Activity_Count__c			!= null ? UD.Folder_On_Traffic_Activity_Count__c		: 0); } else { UDtoKeep.Folder_On_Traffic_Activity_Count__c			= (UD.Folder_On_Traffic_Activity_Count__c		!= null ? UD.Folder_On_Traffic_Activity_Count__c		: 0); }
				if (UDtoKeep.Folders_On_Traffic_Count__c				!= null) { UDtoKeep.Folders_On_Traffic_Count__c					+= (UD.Folders_On_Traffic_Count__c					!= null ? UD.Folders_On_Traffic_Count__c				: 0); } else { UDtoKeep.Folders_On_Traffic_Count__c					= (UD.Folders_On_Traffic_Count__c				!= null ? UD.Folders_On_Traffic_Count__c				: 0); }
				if (UDtoKeep.Folders_On_Traffic_Spend_USD__c			!= null) { UDtoKeep.Folders_On_Traffic_Spend_USD__c				+= (UD.Folders_On_Traffic_Spend_USD__c				!= null ? UD.Folders_On_Traffic_Spend_USD__c			: 0); } else { UDtoKeep.Folders_On_Traffic_Spend_USD__c				= (UD.Folders_On_Traffic_Spend_USD__c			!= null ? UD.Folders_On_Traffic_Spend_USD__c			: 0); }
				if (UDtoKeep.Folders_Total_Spend_USD__c					!= null) { UDtoKeep.Folders_Total_Spend_USD__c					+= (UD.Folders_Total_Spend_USD__c					!= null ? UD.Folders_Total_Spend_USD__c					: 0); } else { UDtoKeep.Folders_Total_Spend_USD__c					= (UD.Folders_Total_Spend_USD__c				!= null ? UD.Folders_Total_Spend_USD__c					: 0); }
				if (UDtoKeep.Folder_Total_Count__c						!= null) { UDtoKeep.Folder_Total_Count__c						+= (UD.Folder_Total_Count__c						!= null ? UD.Folder_Total_Count__c						: 0); } else { UDtoKeep.Folder_Total_Count__c						= (UD.Folder_Total_Count__c						!= null ? UD.Folder_Total_Count__c						: 0); }
				if (UDtoKeep.Groups_Activity_Count__c					!= null) { UDtoKeep.Groups_Activity_Count__c					+= (UD.Groups_Activity_Count__c						!= null ? UD.Groups_Activity_Count__c					: 0); } else { UDtoKeep.Groups_Activity_Count__c					= (UD.Groups_Activity_Count__c					!= null ? UD.Groups_Activity_Count__c					: 0); }
				if (UDtoKeep.Groups_Total_Count__c						!= null) { UDtoKeep.Groups_Total_Count__c						+= (UD.Groups_Total_Count__c						!= null ? UD.Groups_Total_Count__c						: 0); } else { UDtoKeep.Groups_Total_Count__c						= (UD.Groups_Total_Count__c						!= null ? UD.Groups_Total_Count__c						: 0); }
				if (UDtoKeep.Keywords_Activity_Count__c					!= null) { UDtoKeep.Keywords_Activity_Count__c					+= (UD.Keywords_Activity_Count__c					!= null ? UD.Keywords_Activity_Count__c					: 0); } else { UDtoKeep.Keywords_Activity_Count__c					= (UD.Keywords_Activity_Count__c				!= null ? UD.Keywords_Activity_Count__c					: 0); }
				if (UDtoKeep.Keywords_Total_Count__c					!= null) { UDtoKeep.Keywords_Total_Count__c						+= (UD.Keywords_Total_Count__c						!= null ? UD.Keywords_Total_Count__c					: 0); } else { UDtoKeep.Keywords_Total_Count__c						= (UD.Keywords_Total_Count__c					!= null ? UD.Keywords_Total_Count__c					: 0); }
				if (UDtoKeep.Publisher_Account_Activity_Count__c		!= null) { UDtoKeep.Publisher_Account_Activity_Count__c			+= (UD.Publisher_Account_Activity_Count__c			!= null ? UD.Publisher_Account_Activity_Count__c		: 0); } else { UDtoKeep.Publisher_Account_Activity_Count__c			= (UD.Publisher_Account_Activity_Count__c		!= null ? UD.Publisher_Account_Activity_Count__c		: 0); }
				if (UDtoKeep.Publisher_Account_Active_Count__c			!= null) { UDtoKeep.Publisher_Account_Active_Count__c			+= (UD.Publisher_Account_Active_Count__c			!= null ? UD.Publisher_Account_Active_Count__c			: 0); } else { UDtoKeep.Publisher_Account_Active_Count__c			= (UD.Publisher_Account_Active_Count__c			!= null ? UD.Publisher_Account_Active_Count__c			: 0); }
				if (UDtoKeep.Publisher_Account_Deleted_Coun__c			!= null) { UDtoKeep.Publisher_Account_Deleted_Coun__c			+= (UD.Publisher_Account_Deleted_Coun__c			!= null ? UD.Publisher_Account_Deleted_Coun__c			: 0); } else { UDtoKeep.Publisher_Account_Deleted_Coun__c			= (UD.Publisher_Account_Deleted_Coun__c			!= null ? UD.Publisher_Account_Deleted_Coun__c			: 0); }
				if (UDtoKeep.Publisher_Account_Inactive__c				!= null) { UDtoKeep.Publisher_Account_Inactive__c				+= (UD.Publisher_Account_Inactive__c				!= null ? UD.Publisher_Account_Inactive__c				: 0); } else { UDtoKeep.Publisher_Account_Inactive__c				= (UD.Publisher_Account_Inactive__c				!= null ? UD.Publisher_Account_Inactive__c				: 0); }
				if (UDtoKeep.Publisher_Account_New_Count__c				!= null) { UDtoKeep.Publisher_Account_New_Count__c				+= (UD.Publisher_Account_New_Count__c				!= null ? UD.Publisher_Account_New_Count__c				: 0); } else { UDtoKeep.Publisher_Account_New_Count__c				= (UD.Publisher_Account_New_Count__c			!= null ? UD.Publisher_Account_New_Count__c				: 0); }
				if (UDtoKeep.Publisher_Account_Total_Count__c			!= null) { UDtoKeep.Publisher_Account_Total_Count__c			+= (UD.Publisher_Account_Total_Count__c				!= null ? UD.Publisher_Account_Total_Count__c			: 0); } else { UDtoKeep.Publisher_Account_Total_Count__c			= (UD.Publisher_Account_Total_Count__c			!= null ? UD.Publisher_Account_Total_Count__c			: 0); }
				if (UDtoKeep.Reports_All_Activity_Count__c				!= null) { UDtoKeep.Reports_All_Activity_Count__c				+= (UD.Reports_All_Activity_Count__c				!= null ? UD.Reports_All_Activity_Count__c				: 0); } else { UDtoKeep.Reports_All_Activity_Count__c				= (UD.Reports_All_Activity_Count__c				!= null ? UD.Reports_All_Activity_Count__c				: 0); }
				if (UDtoKeep.Reports_One_Time_All_Formats__c			!= null) { UDtoKeep.Reports_One_Time_All_Formats__c				+= (UD.Reports_One_Time_All_Formats__c				!= null ? UD.Reports_One_Time_All_Formats__c			: 0); } else { UDtoKeep.Reports_One_Time_All_Formats__c				= (UD.Reports_One_Time_All_Formats__c			!= null ? UD.Reports_One_Time_All_Formats__c			: 0); }
				if (UDtoKeep.Reports_Recurring_All_Formats__c			!= null) { UDtoKeep.Reports_Recurring_All_Formats__c			+= (UD.Reports_Recurring_All_Formats__c				!= null ? UD.Reports_Recurring_All_Formats__c			: 0); } else { UDtoKeep.Reports_Recurring_All_Formats__c			= (UD.Reports_Recurring_All_Formats__c			!= null ? UD.Reports_Recurring_All_Formats__c			: 0); }
				if (UDtoKeep.Reports_Total_Count__c						!= null) { UDtoKeep.Reports_Total_Count__c						+= (UD.Reports_Total_Count__c						!= null ? UD.Reports_Total_Count__c						: 0); } else { UDtoKeep.Reports_Total_Count__c						= (UD.Reports_Total_Count__c					!= null ? UD.Reports_Total_Count__c						: 0); }
				if (UDtoKeep.Revenue_Error__c							!= null) { UDtoKeep.Revenue_Error__c							+= (UD.Revenue_Error__c								!= null ? UD.Revenue_Error__c							: 0); } else { UDtoKeep.Revenue_Error__c							= (UD.Revenue_Error__c							!= null ? UD.Revenue_Error__c							: 0); }
				if (UDtoKeep.Rprt_1_tme_All_Frmt_Actvty_Cnt__c			!= null) { UDtoKeep.Rprt_1_tme_All_Frmt_Actvty_Cnt__c			+= (UD.Rprt_1_tme_All_Frmt_Actvty_Cnt__c			!= null ? UD.Rprt_1_tme_All_Frmt_Actvty_Cnt__c			: 0); } else { UDtoKeep.Rprt_1_tme_All_Frmt_Actvty_Cnt__c			= (UD.Rprt_1_tme_All_Frmt_Actvty_Cnt__c			!= null ? UD.Rprt_1_tme_All_Frmt_Actvty_Cnt__c			: 0); }
				if (UDtoKeep.Rprt_Rcrrng_All_Frmt_Actvty_Cnt__c			!= null) { UDtoKeep.Rprt_Rcrrng_All_Frmt_Actvty_Cnt__c			+= (UD.Rprt_Rcrrng_All_Frmt_Actvty_Cnt__c			!= null ? UD.Rprt_Rcrrng_All_Frmt_Actvty_Cnt__c			: 0); } else { UDtoKeep.Rprt_Rcrrng_All_Frmt_Actvty_Cnt__c			= (UD.Rprt_Rcrrng_All_Frmt_Actvty_Cnt__c		!= null ? UD.Rprt_Rcrrng_All_Frmt_Actvty_Cnt__c			: 0); }
				if (UDtoKeep.Spend__c									!= null) { UDtoKeep.Spend__c									+= (UD.Spend__c										!= null ? UD.Spend__c									: 0); } else { UDtoKeep.Spend__c									= (UD.Spend__c									!= null ? UD.Spend__c									: 0); }
				if (UDtoKeep.Support_Cases__c							!= null) { UDtoKeep.Support_Cases__c							+= (UD.Support_Cases__c								!= null ? UD.Support_Cases__c							: 0); } else { UDtoKeep.Support_Cases__c							= (UD.Support_Cases__c							!= null ? UD.Support_Cases__c							: 0); }
				if (UDtoKeep.test__c									!= null) { UDtoKeep.test__c										+= (UD.test__c										!= null ? UD.test__c									: 0); } else { UDtoKeep.test__c										= (UD.test__c									!= null ? UD.test__c									: 0); }
				if (UDtoKeep.Test_July18__c								!= null) { UDtoKeep.Test_July18__c								+= (UD.Test_July18__c								!= null ? UD.Test_July18__c								: 0); } else { UDtoKeep.Test_July18__c								= (UD.Test_July18__c							!= null ? UD.Test_July18__c								: 0); }
				if (UDtoKeep.Test_Measure__c							!= null) { UDtoKeep.Test_Measure__c								+= (UD.Test_Measure__c								!= null ? UD.Test_Measure__c							: 0); } else { UDtoKeep.Test_Measure__c								= (UD.Test_Measure__c							!= null ? UD.Test_Measure__c							: 0); }
				if (UDtoKeep.User_Activity_Click_Count__c				!= null) { UDtoKeep.User_Activity_Click_Count__c				+= (UD.User_Activity_Click_Count__c					!= null ? UD.User_Activity_Click_Count__c				: 0); } else { UDtoKeep.User_Activity_Click_Count__c				= (UD.User_Activity_Click_Count__c				!= null ? UD.User_Activity_Click_Count__c				: 0); }
				if (UDtoKeep.User_Activity_Login_Count__c				!= null) { UDtoKeep.User_Activity_Login_Count__c				+= (UD.User_Activity_Login_Count__c					!= null ? UD.User_Activity_Login_Count__c				: 0); } else { UDtoKeep.User_Activity_Login_Count__c				= (UD.User_Activity_Login_Count__c				!= null ? UD.User_Activity_Login_Count__c				: 0); }

				listUDToDelete.add(UD);
			}
		}

		if (!listUDToDelete.isEmpty()) { delete listUDToDelete; }
		if (UDtoKeep != null) { update UDtoKeep; }
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {

		return Database.getQueryLocator(Query);
	}

	global void finish(Database.BatchableContext BC) {}
}