@isTest
private class testPostFeedItemAccount {
    static testMethod void testCallout1() {
        Account a = new Account();
            a.Name = 'TestAccount';
            a.Type = 'Advertiser';
            a.Account_Status__c = 'Active Customer';
            a.BillingCountry = 'United States';
            a.Region__c = 'NA';
        insert a;
        String rid = a.Id;
        String hash = 'TestHash';
        String uid = UserInfo.getUserId();
        String uid2 = UserInfo.getUserId();
        String uid3 = UserInfo.getUserId();
        String uid4 = UserInfo.getUserId();
        String uid5 = UserInfo.getUserId();
        String uid6 = UserInfo.getUserId();
        String sessionid = UserInfo.getSessionId();
        String posttext = 'test text';
        test.startTest();
            PostFeedItemAccount.PostFeedItem(rid, hash, uid, sessionid, posttext);
        test.stopTest();
    }
    static testMethod void testCallout2() {
        Account a = new Account();
            a.Name = 'TestAccount';
            a.Type = 'Advertiser';
            a.Account_Status__c = 'Active Customer';
            a.BillingCountry = 'United States';
            a.Region__c = 'NA';
        insert a;
        String rid = a.Id;
        String hash = 'TestHash';
        String uid = UserInfo.getUserId();
        String uid2 = UserInfo.getUserId();
        String uid3 = UserInfo.getUserId();
        String uid4 = UserInfo.getUserId();
        String uid5 = UserInfo.getUserId();
        String uid6 = UserInfo.getUserId();
        String sessionid = UserInfo.getSessionId();
        String posttext = 'test text';
        test.startTest();
            PostFeedItemAccount.PostFeedItemFiveMention(rid, hash, uid, uid2, uid3, uid4, uid5, sessionid, posttext);
        test.stopTest();
    }
    static testMethod void testCallout3() {
        Account a = new Account();
            a.Name = 'TestAccount';
            a.Type = 'Advertiser';
            a.Account_Status__c = 'Active Customer';
            a.BillingCountry = 'United States';
            a.Region__c = 'NA';
        insert a;
        String rid = a.Id;
        String hash = 'TestHash';
        String uid = UserInfo.getUserId();
        String uid2 = UserInfo.getUserId();
        String uid3 = UserInfo.getUserId();
        String uid4 = UserInfo.getUserId();
        String uid5 = UserInfo.getUserId();
        String uid6 = UserInfo.getUserId();
        String sessionid = UserInfo.getSessionId();
        String posttext = 'test text';
        test.startTest();
            PostFeedItemAccount.PostFeedItemFourMention(rid, hash, uid, uid2, uid3, uid4, sessionid, posttext);
        test.stopTest();
    }
    static testMethod void testCallout4() {
        Account a = new Account();
            a.Name = 'TestAccount';
            a.Type = 'Advertiser';
            a.Account_Status__c = 'Active Customer';
            a.BillingCountry = 'United States';
            a.Region__c = 'NA';
        insert a;
        String rid = a.Id;
        String hash = 'TestHash';
        String uid = UserInfo.getUserId();
        String uid2 = UserInfo.getUserId();
        String uid3 = UserInfo.getUserId();
        String uid4 = UserInfo.getUserId();
        String uid5 = UserInfo.getUserId();
        String uid6 = UserInfo.getUserId();
        String sessionid = UserInfo.getSessionId();
        String posttext = 'test text';
        test.startTest();
            PostFeedItemAccount.PostFeedItemSixMention(rid, hash, uid, uid2, uid3, uid4, uid5, uid6, sessionid, posttext);
        test.stopTest();
    }
}