/**
 * This Apex Class is being used to test the ownerCopy trigger built
 * Created by: Gary Sopko
 * Created Date: 04/19/2013
 * Please consult with Sales Operations/System Administration before updating.
 */
@isTest
private class TestownerCopy {

    static testMethod void TestInsertCaseFirstIf() {
        //Query for Owner
        String o = [Select Id from User where Profile.Name = 'System Administrator' AND IsActive = TRUE LIMIT 1].Id;
        //CreateTestCase
        Case c = new Case(OwnerId = o, Subject = 'Test Subject', Description = 'TestDescription');
        insert c;
        //Ensure that the OwnerId and OwnerCopy fields match
        Case cvalid = [Select Id, OwnerId, Owner_Copy__c, Owner_Region__c, Owner_Department__c from Case where Id = :c.Id];
        system.AssertEquals(cvalid.OwnerId, cvalid.Owner_Copy__c);
        system.AssertNotEquals(cvalid.Owner_Region__c, null);
        system.AssertNotEquals(cvalid.Owner_Department__c, null);
    }
    static testMethod void TestInsertCaseElseIf(){
        //Query for Queue to assign case to
        String q = [Select Id, QueueId, SobjectType from QueueSobject where sobjectType = 'Case' LIMIT 1].QueueId;
        //CreateTestCase
        Case c = new Case(OwnerId = q, Subject = 'TestSubject', Description = 'TestDescription');
        insert c;
        //Ensure that the OwnerCopy, OwnerDpartment, and OwnerRegion are Null and that OwnerId = QueueId
        Case cvalid = [Select Id, Owner_Copy__c, Owner_Region__c, Owner_Department__c, OwnerId from Case where Id = :c.Id];
        system.AssertEquals(cvalid.Owner_Copy__c, null);
        system.AssertEquals(cvalid.Owner_Region__c, null);
        system.AssertEquals(cvalid.Owner_Department__c, null);
        system.AssertEquals(cvalid.OwnerId, q);
    }
}