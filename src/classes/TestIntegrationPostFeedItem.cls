/**
 * This Apex Class is being used to test the IntegrationPostFeedItem trigger built
 * Created by: Gary Sopko
 * Created Date: 07/24/2013
 * Please consult with Sales Operations/System Administration before updating.
 */
@isTest
private class TestIntegrationPostFeedItem {

    static testMethod void TestAMER() {
        Account a = new Account();
        	a.Name = 'TestAccount';
        	a.Type = 'Advertiser';
        	a.Region__c = 'NA';
        	a.Account_Status__c = 'Active Customer';
        	a.BillingCountry = 'United States';
        	a.OwnerId = UserInfo.getUserId();
    	insert a;
    	PS_Project_Record__c i = new PS_Project_Record__c();
    		i.Account__c = a.Id;
    		i.Project_Manager__c = UserInfo.getUserId();
    		i.Primary_Solution_Architect__c = UserInfo.getUserId();
    		i.Implementation_Health__c = 'Green';
    		i.onboarding_Location__c = 'North America - West';
		insert i;
		for(PS_Project_Record__c iupdate : [Select Id, Implementation_Health__c from PS_Project_Record__c WHERE Id = :i.Id]){
			iupdate.Implementation_Health__c = 'Red';
			update iupdate;
		}
    }
    static testMethod void Test6Mention() {
        Account a = new Account();
        	a.Name = 'TestAccount';
        	a.Type = 'Advertiser';
        	a.Region__c = 'NA';
        	a.Account_Status__c = 'Active Customer';
        	a.BillingCountry = 'United States';
        	a.OwnerId = UserInfo.getUserId();
    	insert a;
    	Opportunity o = new Opportunity();
    		o.Name = 'Test';
    		o.OwnerId = UserInfo.getuserId();
    		o.Product_Consultant__c = UserInfo.getUserId();
    		o.Type = 'New Business';
    		o.StageName = 'Lead Passed';
    		o.Amount = 500;
    		o.CloseDate = date.Today().addDays(1);
    		o.AccountId = a.Id;
		insert o;
    	PS_Project_Record__c i = new PS_Project_Record__c();
    		i.Account__c = a.Id;
    		i.Primary_Opportunity__c = o.Id;
    		i.Project_Manager__c = UserInfo.getUserId();
    		i.Primary_Solution_Architect__c = UserInfo.getUserId();
    		i.Implementation_Health__c = 'Green';
    		i.onboarding_Location__c = 'North America - West';
		insert i;
		for(PS_Project_Record__c iupdate : [Select Id, Implementation_Health__c from PS_Project_Record__c WHERE Id = :i.Id]){
			iupdate.Implementation_Health__c = 'Red';
			update iupdate;
		}
    }
	static testMethod void Test5Mention() {
        Account a = new Account();
        	a.Name = 'TestAccount';
        	a.Type = 'Advertiser';
        	a.Region__c = 'NA';
        	a.Account_Status__c = 'Active Customer';
        	a.BillingCountry = 'United States';
        	a.OwnerId = UserInfo.getUserId();
    	insert a;
    	Opportunity o = new Opportunity();
    		o.Name = 'Test';
    		o.OwnerId = UserInfo.getuserId();
    		o.Type = 'New Business';
    		o.StageName = 'Lead Passed';
    		o.Amount = 500;
    		o.CloseDate = date.Today().addDays(1);
    		o.AccountId = a.Id;
		insert o;
    	PS_Project_Record__c i = new PS_Project_Record__c();
    		i.Account__c = a.Id;
    		i.Primary_Opportunity__c = o.Id;
    		i.Project_Manager__c = UserInfo.getUserId();
    		i.Primary_Solution_Architect__c = UserInfo.getUserId();
    		i.Implementation_Health__c = 'Green';
    		i.onboarding_Location__c = 'North America - West';
		insert i;
		for(PS_Project_Record__c iupdate : [Select Id, Implementation_Health__c from PS_Project_Record__c WHERE Id = :i.Id]){
			iupdate.Implementation_Health__c = 'Red';
			update iupdate;
		}
    }
    static testMethod void TestEMEA() {
        Account a = new Account();
        	a.Name = 'TestAccount';
        	a.Type = 'Advertiser';
        	a.Region__c = 'NA';
        	a.Account_Status__c = 'Active Customer';
        	a.BillingCountry = 'United States';
        	a.OwnerId = UserInfo.getUserId();
    	insert a;
    	PS_Project_Record__c i = new PS_Project_Record__c();
    		i.Account__c = a.Id;
    		i.Project_Manager__c = UserInfo.getUserId();
    		i.Primary_Solution_Architect__c = UserInfo.getUserId();
    		i.Implementation_Health__c = 'Green';
    		i.onboarding_Location__c = 'EMEA';
		insert i;
		for(PS_Project_Record__c iupdate : [Select Id, Implementation_Health__c from PS_Project_Record__c WHERE Id = :i.Id]){
			iupdate.Implementation_Health__c = 'Red';
			update iupdate;
		}
    }
    static testMethod void TestAPAC() {
        Account a = new Account();
        	a.Name = 'TestAccount';
        	a.Type = 'Advertiser';
        	a.Region__c = 'NA';
        	a.Account_Status__c = 'Active Customer';
        	a.BillingCountry = 'United States';
        	a.OwnerId = UserInfo.getUserId();
    	insert a;
    	PS_Project_Record__c i = new PS_Project_Record__c();
    		i.Account__c = a.Id;
    		i.Project_Manager__c = UserInfo.getUserId();
    		i.Primary_Solution_Architect__c = UserInfo.getUserId();
    		i.Implementation_Health__c = 'Green';
    		i.onboarding_Location__c = 'APAC - Japan';
		insert i;
		for(PS_Project_Record__c iupdate : [Select Id, Implementation_Health__c from PS_Project_Record__c WHERE Id = :i.Id]){
			iupdate.Implementation_Health__c = 'Red';
			update iupdate;
		}
    }
}