/**
 * ####  DO NOT CHANGE  ####
 * All requests for changes to this file MUST be sent to elearning@marinsoftware.com
 *
 * Original Author:      Luke Martell
 * Date:        5/23/2011
 *
 * Last Updater:        Chris Mielke
 * Last Update:         11/2/2012
 *
 * Comments:
 * This controller maintains a new Web-to-Case form that allows the user to submit
 * an attachment. The attachment will automatically be assigned to the Case record
 * when it is created. In addition, a CC list field allows for the users to submit
 * up to 5 different e-mails (comma-separated).
 */
public with sharing class Web2CaseController {

    /**
     * Variables to control the different types of validation errors. Each validation
     * will show a different message on the VFP.
     */
    public Boolean noProblemAreaSupplied { get; set; }
    public Boolean noDescriptionSupplied { get; set; }
    public Boolean noSubjectSupplied { get; set; }
    public Boolean fileSizeTooLarge { get; set; }
    public Boolean noIssueSupplied { get; set; }
    public Boolean noEmailSupplied { get; set; }
    public Boolean noImpactSupplied { get; set; }
    public Boolean phoneFieldTooLarge { get; set; }
    public Boolean isPro { get; set; }
    public Boolean isEnterprise { get; set; }

    public String ccErrorMsg { get; set; }
    public Integer bodyLength { get; set; }

    /**
     * Variables to contain the Case, Attachment, and CC list data.
     */
    public Attachment attachment { get; set; }
    public Case webCase { get; set; }
    public Priority__c priority { get; }
    public String userType { get; set; }
    public String productVersion { get; set; }
    public String helpLink { get; set; }
    public String trackingList { get; set; }
    public Suggested_Article_Tracking__c trackingObj { get; set; }
    public String ccList { get; set; }
    public String impact = '';
    public Id userPriority { get; set; }
    public String languageDescription = '';

    /**
     * Constructor will pre-populate some data on the Case via query parameters being
     * passed to the VFP. Each individual field will need to be mapped to a particular
     * Case field.
     */
    public Web2CaseController() {
        // Fix for IE so it doesn't go into IE7 Compatibility Mode
        Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=edge');
    
        String contactName = ApexPages.currentPage().getParameters().get('n');
        String emailAddress = ApexPages.currentPage().getParameters().get('e');
        String clientId = ApexPages.currentPage().getParameters().get('cid');
        userType = ApexPages.currentPage().getParameters().get('type');
        productVersion = ApexPages.currentPage().getParameters().get('product_version');
        
        if (userType == 'pro') {
            isPro = true;
        } else {
            isEnterprise = true;
        }

        attachment = new Attachment();
        webCase = new Case(
            SuppliedName = contactName,
            SuppliedEmail = emailAddress,
            Client_ID__c = (clientId != null ? Integer.valueOf(clientId) : null),
            Origin = 'Web to Case',
            Group__c = 'Support Services',
            language__c = 'English'
        );
        
        // Set form parameters
        if (ApexPages.currentPage().getParameters().get('platformArea') != null) {
            webCase.Case_Reason__c = ApexPages.currentPage().getParameters().get('platformArea');
        }
        if (ApexPages.currentPage().getParameters().get('bestTopic') != null) {
            webCase.Case_Reason_Detail__c = ApexPages.currentPage().getParameters().get('bestTopic');
        }
        if (ApexPages.currentPage().getParameters().get('subject') != null) {
            webCase.Subject = ApexPages.currentPage().getParameters().get('subject');
        }
        if (ApexPages.currentPage().getParameters().get('description') != null) {
            webCase.description = ApexPages.currentPage().getParameters().get('description');
        }
        if (ApexPages.currentPage().getParameters().get('affectedIds') != null) {
            webCase.Affected_Ids__c = ApexPages.currentPage().getParameters().get('affectedIds');
        }
        if (ApexPages.currentPage().getParameters().get('activityLogIds') != null) {
            webCase.Activity_Log_Ids__c = ApexPages.currentPage().getParameters().get('activityLogIds');
        }     
        
        userType = ApexPages.currentPage().getParameters().get('type');
        
        if (userType == 'pro' || productVersion == 'Professional') {
            webCase.Product_Version__c = 'Professional';
            helpLink = 'http://prosupport.marinsoftware.com/home';
        } else {
            webCase.Product_Version__c = 'Enterprise';
            helpLink = 'https://marketinginsights.zendesk.com/home';
        }
    }

    /**
     * Gets the list of priorities
     */
     public List<selectOption> getPriorities() {
        List<selectOption> options = new List<selectOption>(); //new list for holding all of the picklist options
        options.add(new selectOption('', 'Select...')); //add the first option of '- None -' in case the user doesn't want to select a value or in case no values are returned from query below
        for (Priority__c Priority : [SELECT Id, Name__c FROM Priority__c ORDER BY Name]) { //query for priority records
            options.add(new selectOption(priority.Id, priority.Name__c)); //for all records found - add them to the picklist options
        }
        return options; //return the picklist options
    }

    /**
     * Gets the relevant information about Priority and returns it to the screen.
     */
    public PageReference updateImpact() {
        if (userPriority != null) {
            Priority__c priorityQuery = [SELECT Description__c FROM Priority__c WHERE Id = :userPriority ];
            impact = priorityQuery.Description__c;
        } else {
            impact = '';
        }
        return null;
    }

    public String getImpact() {
        return impact;
    }
    
    /**
     * Updates the language description depending on the language option that was selected
     */
    public PageReference updateLanguageDescription() {
        if (webCase.language__c == 'Deutsch - German') {
            languageDescription = 'Obwohl diese Seite nicht auf Deutsch angezeigt wird, füllen Sie bitte das Formular in der von Ihnen gewählten Sprache aus. Ein deutschsprachiger Sachbearbeiter vom Kundenservice wird Ihnen weiterhelfen.';
        } else if (webCase.language__c == 'Français - French') {
            languageDescription = 'Bien que cette page ne soit pas en français, veuillez compléter le formulaire dans la langue de votre choix. Un agent du service client vous répondra dans cette langue.';
        } else if (webCase.language__c == '日本語 - Japanese') {
            languageDescription = 'このページは日本語で表示されていませんが、フォームへの記入はお客様が指定した言語で行ってください。 日本語を話すサービス担当者が対応いたします。';
        } else if (webCase.language__c == '中文 - Chinese') {
            languageDescription = '尽管此页面并非中文，但请您以自选的语言填写表格。我们会有中文代理服务人员对您回应。';
        } else {
            languageDescription = '';
        }

        
        return null;
    }
    
    public String getLanguageDescription() {
        return languageDescription;
    }

    /**
     * When the form has been submitted then split the CC list if data was filled out
     * and populate the different CC fields. If an attachment was selected then create
     * the record after the Case record was successfully created. Any errors will return
     * back to the form page and display using the standard error format.
     */
    public PageReference createCase() {
        PageReference savePage = null;

        System.Savepoint sp = Database.setSavepoint();

        try {
            if (validateFormInput()) {
                if (ccList != null && ccList.length() > 0) {
                    String[] ccParts = ccList.split(',');

                    for (Integer i = 0; i < ccParts.size(); i++) {
                        if (i == 0) { webCase.CC_1__c = ccParts[i]; }
                        if (i == 1) { webCase.CC_2__c = ccParts[i]; }
                        if (i == 2) { webCase.CC_3__c = ccParts[i]; }
                        if (i == 3) { webCase.CC_4__c = ccParts[i]; }
                        if (i == 4) { webCase.CC_5__c = ccParts[i]; }
                    }
                } else {
                    // Make sure CC info is empty
                    webCase.CC_1__c = null;
                    webCase.CC_2__c = null;
                    webCase.CC_3__c = null;
                    webCase.CC_4__c = null;
                    webCase.CC_5__c = null;
                }
                webCase.Priority__c = userPriority;
                
                // Change Record Type for "Feature Request" cases
                if (webCase.Case_Reason__c == 'Feature Request') {
                    RecordType rt = [select Id from RecordType where Name = 'Feature Request' and SobjectType = 'Case' limit 1];
                    webCase.RecordTypeId = rt.Id;
                }
                
                insert webCase;

                if (trackingList != null && trackingList.length() > 0) {
                    // Break the tracking string apart into individual JSON strings
                    String[] trackingParts = trackingList.split('#@');
                    for (Integer i = 0; i < trackingParts.size(); i++) {
                        // Create the tracking object
                         trackingObj = new Suggested_Article_Tracking__c(
                                Case__c = webCase.Id
                         );

                        // Parse the JSON with Salesforce's EXTREMELE crappy JSON parser
                        // http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_methods_system_jsonparser.htm
                        JSONParser parser = JSON.createParser(trackingParts[i]);

                        // Loop through each item of the JSON
                        while (parser.nextToken() != null) {
                            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'clicked')) {
                                // Get the value.
                                parser.nextToken();
                               trackingObj.Clicked__c = parser.getBooleanValue();
                            } else if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'group')) {
                                // Get the value.
                                parser.nextToken();
                               trackingObj.Group__c = parser.getDecimalValue();
                            } else if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'reason')) {
                                // Get the value.
                                parser.nextToken();
                               trackingObj.Platform_Area__c = parser.getText();
                            } else if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'detail')) {
                                // Get the value.
                                parser.nextToken();
                               trackingObj.Topic__c = parser.getText();
                            } else if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'subject')) {
                                // Get the value.
                                parser.nextToken();
                               trackingObj.Subject__c = parser.getText();
                            } else if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'url')) {
                                // Get the value.
                                parser.nextToken();
                               trackingObj.URL__c = parser.getText();
                            } else if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'title')) {
                                // Get the value.
                                parser.nextToken();
                               trackingObj.Article_Title__c = parser.getText();
                            }
                        }
                        // Insert the object into Salesforce
                        insert trackingObj;
                    }
                }

                // Insert the attachment record
                if (attachment.Name != null) {
                    attachment.ParentId = webCase.Id;
                    insert attachment;
                }
               
                // Forward user to Thank You page
                savePage = Page.Web2CaseThankYou;
                savePage.getParameters().put('product_version', webCase.Product_Version__c);
                savePage.getParameters().put('case_id', webCase.Id);
                savePage.getParameters().put('case_reason', webCase.Case_Reason__c);
                savePage.getParameters().put('case_reason_detail', webCase.Case_Reason_Detail__c);
                savePage.getParameters().put('subject', webCase.Subject);
                savePage.getParameters().put('lang', webCase.language__c);
                savePage.setRedirect(true);
            }
        } catch (System.Exception e) {
            if (e.getMessage().contains('INVALID_EMAIL_ADDRESS')) {
                String[] vals = e.getMessage().split(':');
                ccErrorMsg = 'Invalid e-mail in CC list: ' + vals[3];
            }
            Database.rollback(sp);
        } finally {
            impact = '';
            attachment.body = null;
            attachment = new Attachment();
        }
        return savePage;
    }

    /**
     * The cancel button just returns back to the same page. Not sure if the window
     * closure is handled by Javascript or what but the cancel button does nothing.
     */
    public PageReference cancel() {
        return null;
    }

    /**
     * Validate all the form input. All the required fields are handled this way because
     * of the Marin branding on the VFP.
     */
    private Boolean validateFormInput() {
        Boolean formIsValid = true;

        noDescriptionSupplied = false;
        noProblemAreaSupplied = false;
        noSubjectSupplied = false;
        fileSizeTooLarge = false;
        noIssueSupplied = false;
        noEmailSupplied = false;
        noImpactSupplied = false;
        ccErrorMsg = null;

        if (webCase.SuppliedEmail == null || webCase.SuppliedEmail.length() == 0) {
            noEmailSupplied = true;
            formIsValid = false;
        }
        
        if (webCase.Case_Reason__c == null || webCase.Case_Reason__c.length() == 0) {
            noProblemAreaSupplied = true;
            formIsValid = false;
        } 
        
        if (webCase.Case_Reason_Detail__c == null || webCase.Case_Reason_Detail__c.length() == 0) {
            noIssueSupplied = true;
            formIsValid = false;
        } 
        
        if (webCase.Description == null || webCase.Description.length() == 0) {
            noDescriptionSupplied = true;
            formIsValid = false;
        } 
        
        if (webCase.Subject == null || webCase.Subject.length() == 0) {
            noSubjectSupplied = true;
            formIsValid = false;
        } 
        
        if (bodyLength > (1024*5000)) {
            fileSizeTooLarge = true;
            formIsValid = false;
        } 
        
        if (userPriority == null) {
            noImpactSupplied = true;
            formIsValid = false;
        }
        
        if (webCase.Client_Phone__c.length() > 40) {
            phoneFieldTooLarge = true;
            formIsValid = false;
        }
        return formIsValid;
    }
}