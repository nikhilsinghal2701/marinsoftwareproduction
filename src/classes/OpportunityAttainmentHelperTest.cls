/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class OpportunityAttainmentHelperTest {

    static testMethod void updateTest() {
    	String strRecordTypeId = [Select Id From RecordType Where SobjectType = 'Opportunity' and DeveloperName = 'Existing_Business'].Id;
    	User Top_Manager=TestDataFactory.createUser('Standard User', true);
        User Manager=TestDataFactory.createUser('Standard User', true);
        User OwnerUser=TestDataFactory.createUser('Standard User', true);
        List<Account> parentAccounts = TestDataFactory.createAccount(1, true, new Map<String, Object>{'Type' => 'Advertiser',
                                                                                                    'BillingCountry' => 'United States'});
        System.debug(parentAccounts);
        List<Opportunity> parentOpportunity = TestDataFactory.createOpportunity(300, true, new Map<String, Object>{'StageName' => 'Qualification Confirmed',
        																							'CloseDate' => date.Today(),
        																							'Amount' => 3000,
        																							'OwnerId' =>OwnerUser.id,
        																							'RecordTypeId' =>strRecordTypeId,
        																							'Customer_ID__c' => '12345',
        																							'Client_ID__c' => system.now().millisecond()+'12345', 
                                                                                                    'AccountId' => parentAccounts[0].id});
                                                                                                    
        List<Attainment__c> Top_Manager_attainment = TestDataFactory.createAttainment(1, true, new Map<String, Object>{'OwnerId' => Top_Manager.id,
        																	'Start_Date__c' => date.Today().toStartofMonth()});
        
        List<Attainment__c> Manager_attainment = TestDataFactory.createAttainment(1, true, new Map<String, Object>{'OwnerId' => Manager.id,
        																	'Start_Date__c' => date.Today().toStartofMonth(),
        																	'Manager_Attainment__c' => Top_Manager_attainment[0].Id
        																	});
        																	
       TestDataFactory.createAttainment(1, true, new Map<String, Object>{'OwnerId' => OwnerUser.id,
        																	'Start_Date__c' => date.Today().toStartofMonth(),
        																	'Manager_Attainment__c' => Manager_attainment[0].Id
       
       																	});
       
        List<Attainment__c> Top_Manager_attainment_Backdate = TestDataFactory.createAttainment(1, true, new Map<String, Object>{'OwnerId' => Top_Manager.id,
        																	'Start_Date__c' => date.Today().addMonths(-1)});
        
        List<Attainment__c> Manager_attainment_Backdate = TestDataFactory.createAttainment(1, true, new Map<String, Object>{'OwnerId' => Manager.id,
        																	'Start_Date__c' => date.Today().addMonths(-1),
        																	'Manager_Attainment__c' => Top_Manager_attainment[0].Id
        																	});
        																	
       TestDataFactory.createAttainment(1, true, new Map<String, Object>{'OwnerId' => OwnerUser.id,
        																	'Start_Date__c' => date.Today().addMonths(-1),
        																	'Manager_Attainment__c' => Manager_attainment[0].Id
       
       																	});
       
       															
       for(Opportunity opp :parentOpportunity){
       	opp.StageName='Closed Won';
       }																	
       Test.startTest();
       update parentOpportunity;
       Test.stopTest() ;
       
       List<Attainment_Opportunity__c> tAO = new List<Attainment_Opportunity__c>([SELECT ID 
												FROM Attainment_Opportunity__c ]);
		List<Attainment_Opportunity__c> ManagertAO = new List<Attainment_Opportunity__c>([SELECT ID 
												FROM Attainment_Opportunity__c where Attainment__c=:Manager_attainment[0].id]);
		Attainment__c ManagertAOAmount = [SELECT Quota_Closed__c FROM Attainment__c where id=:Manager_attainment[0].id];
		
		System.assertEquals(	900	
       							, (tAO.size())
								, 'Expecting 2100 Junction records'
								);
		
		System.assertEquals(	300	
       							, (ManagertAO.size())
								, 'Expecting 300 Manager Junction records'
								);
								
		System.assertEquals(	900000.00	
       							, (ManagertAOAmount.Quota_Closed__c)
								, 'Expecting 900000 Manager quota'
								);		                                                                                          
    }
    
        static testMethod void InsertTest() {
    	String strRecordTypeId = [Select Id From RecordType Where SobjectType = 'Opportunity' and DeveloperName = 'Existing_Business'].Id;
    	User Top_Top_Top_Top_Top_Manager=TestDataFactory.createUser('Standard User', true);
    	User Top_Top_Top_Top_Manager=TestDataFactory.createUser('Standard User', true);
    	User Top_Top_Top_Manager=TestDataFactory.createUser('Standard User', true);
    	User Top_Top_Manager=TestDataFactory.createUser('Standard User', true);
    	User Top_Manager=TestDataFactory.createUser('Standard User', true);
        User Manager=TestDataFactory.createUser('Standard User', true);
        User OwnerUser=TestDataFactory.createUser('Standard User', true);
        
        List<Account> parentAccounts = TestDataFactory.createAccount(1, true, new Map<String, Object>{'Type' => 'Advertiser',
                                                                                                    'BillingCountry' => 'United States'});
        System.debug(parentAccounts);
        List<Opportunity> parentOpportunity = TestDataFactory.createOpportunity(300, false, new Map<String, Object>{'StageName' => 'Qualification Confirmed',
        																							'CloseDate' => date.Today(),
        																							'Amount' => 3000,
        																							'OwnerId' =>OwnerUser.id,
        																							'RecordTypeId' =>strRecordTypeId,
        																							'Customer_ID__c' => '12345',
        																							'Client_ID__c' => system.now().millisecond()+'12345', 
                                                                                                    'AccountId' => parentAccounts[0].id});
                                                                                                    
         List<Attainment__c> Top_Top_Top_Top_Top_Manager_attainment = TestDataFactory.createAttainment(1, true, new Map<String, Object>{'OwnerId' => Top_Manager.id,
        																	'Start_Date__c' => date.Today().toStartofMonth()});
         List<Attainment__c> Top_Top_Top_Top_Manager_attainment = TestDataFactory.createAttainment(1, true, new Map<String, Object>{'OwnerId' => Top_Manager.id,
        																	'Start_Date__c' => date.Today().toStartofMonth(),
        																	'Manager_Attainment__c' => Top_Top_Top_Top_Top_Manager_attainment[0].Id});
         List<Attainment__c> Top_Top_Top_Manager_attainment = TestDataFactory.createAttainment(1, true, new Map<String, Object>{'OwnerId' => Top_Manager.id,
        																	'Start_Date__c' => date.Today().toStartofMonth(),
        																	'Manager_Attainment__c' => Top_Top_Top_Top_Manager_attainment[0].Id});
         List<Attainment__c> Top_Top_Manager_attainment = TestDataFactory.createAttainment(1, true, new Map<String, Object>{'OwnerId' => Top_Manager.id,
        																	'Start_Date__c' => date.Today().toStartofMonth(),
        																	'Manager_Attainment__c' => Top_Top_Top_Manager_attainment[0].Id});																																																			
        List<Attainment__c> Top_Manager_attainment = TestDataFactory.createAttainment(1, true, new Map<String, Object>{'OwnerId' => Top_Manager.id,
        																	'Start_Date__c' => date.Today().toStartofMonth(),
        																	'Manager_Attainment__c' => Top_Top_Manager_attainment[0].Id});
        
        List<Attainment__c> Manager_attainment = TestDataFactory.createAttainment(1, true, new Map<String, Object>{'OwnerId' => Manager.id,
        																	'Start_Date__c' => date.Today().toStartofMonth(),
        																	'Manager_Attainment__c' => Top_Manager_attainment[0].Id
        																	});
        																	
       TestDataFactory.createAttainment(1, true, new Map<String, Object>{'OwnerId' => OwnerUser.id,
        																	'Start_Date__c' => date.Today().toStartofMonth(),
        																	'Manager_Attainment__c' => Manager_attainment[0].Id
       
       																	});
       
        List<Attainment__c> Top_Manager_attainment_Backdate = TestDataFactory.createAttainment(1, true, new Map<String, Object>{'OwnerId' => Top_Manager.id,
        																	'Start_Date__c' => date.Today().addMonths(-1)});
        
        List<Attainment__c> Manager_attainment_Backdate = TestDataFactory.createAttainment(1, true, new Map<String, Object>{'OwnerId' => Manager.id,
        																	'Start_Date__c' => date.Today().addMonths(-1),
        																	'Manager_Attainment__c' => Top_Manager_attainment[0].Id
        																	});
        																	
       TestDataFactory.createAttainment(1, true, new Map<String, Object>{'OwnerId' => OwnerUser.id,
        																	'Start_Date__c' => date.Today().addMonths(-1),
        																	'Manager_Attainment__c' => Manager_attainment[0].Id
       
       																	});
       
       															
       for(Opportunity opp :parentOpportunity){
       	opp.StageName='Closed Won';
       }																	
       Test.startTest();
       insert parentOpportunity;
       Test.stopTest() ;
       List<Attainment_Opportunity__c> tAO = new List<Attainment_Opportunity__c>([SELECT ID 
												FROM Attainment_Opportunity__c ]);
       
		List<Attainment_Opportunity__c> ManagertAO = new List<Attainment_Opportunity__c>([SELECT ID 
												FROM Attainment_Opportunity__c where Attainment__c=:Manager_attainment[0].id]);
		Attainment__c ManagertAOAmount = [SELECT Quota_Closed__c FROM Attainment__c where id=:Manager_attainment[0].id];
		
		System.assertEquals(	2100	
       							, (tAO.size())
								, 'Expecting 2100 Junction records'
								);
		
		System.assertEquals(	300	
       							, (ManagertAO.size())
								, 'Expecting 300 Manager Junction records'
								);
								
		System.assertEquals(	900000.00	
       							, (ManagertAOAmount.Quota_Closed__c)
								, 'Expecting 900000 quota for manager'
								);						
																																																	
                                                                                                  
    }
}