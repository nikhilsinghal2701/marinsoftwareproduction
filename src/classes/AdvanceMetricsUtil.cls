/**
    AdvanceMetricsUtil is utils class for AdvanceMetricsBatch class
    this class does the calculation for the batch class and creates required maps - Account Id to Advance Record
    for logic 
**/
public with sharing class AdvanceMetricsUtil {

    public static final Integer LAST_WEEK = 8;
    public static final Integer LAST_3_MONTHS = 2;
    public static final Integer LAST_13_MONTHS = 12;
       
    
    /* daysCostDataQueryWeekly - Calculates value of fields Days_with_Late_Cost_Data_W__c,  
     *  MACs_with_Late_Cost_Data_W__c weekly and updates Map with value
     */           
    public void daysCostDataQueryWeekly(List<Id> accIdList, Map<Id,Advance_Metric__c> advaWrapperMap){
        List<AggregateResult> results = 
            [SELECT count_distinct(Marin_App_Client__c) mdistinct, 
                count(Id) cnt,Marin_App_Client__r.Account__c 
            FROM Metric__c 
            WHERE Date__c = Last_N_Days:8 AND Cost_Data_on_Time__c = FALSE 
                AND Marin_App_Client__r.Account__c in :accIdList 
            GROUP BY Marin_App_Client__r.Account__c];
                                    
        for (AggregateResult result : results) {
            String accountId = (String)result.get('Account__c');
            Advance_Metric__c advanceMetric = advaWrapperMap.get(accountId);
            if (advanceMetric == null) {
                advanceMetric =  new Advance_Metric__c(Account__c = accountId);
                advaWrapperMap.put(accountId, advanceMetric);
            }
            advanceMetric.Days_with_Late_Cost_Data_W__c = AdvanceMetricsUtil.add(advanceMetric.Days_with_Late_Cost_Data_W__c, (Decimal)result.get('cnt'));
            advanceMetric.MACs_with_Late_Cost_Data_W__c = AdvanceMetricsUtil.add(advanceMetric.MACs_with_Late_Cost_Data_W__c, (Decimal)result.get('mdistinct'));
        }     
    }
    
   /* daysCostDataQueryMonthly - Calculates value of fields Days_with_Late_Cost_Data_M__c,  
    *  MACs_with_Late_Cost_Data_M__c monthly and updates Map with value
    */
    public void daysCostDataQueryMonthly(List<Id> accIdList, Map<Id,Advance_Metric__c> advaWrapperMap){
        Date startDate = getStartDate(System.today(), LAST_3_MONTHS);
         
        List<AggregateResult> results = [SELECT count_distinct(Marin_App_Client__c) mdistinct, 
                                                count(Id) cnt,Marin_App_Client__r.Account__c 
                                            FROM Monthly_Metric__c
                                            WHERE Date__c >= :startDate 
                                                AND Cost_Data_on_Time__c = FALSE 
                                                AND Marin_App_Client__r.Account__c in :accIdList 
                                            GROUP BY Marin_App_Client__r.Account__c];
                                            
        for (AggregateResult result : results) {  
            String accountId = (String)result.get('Account__c');
            Advance_Metric__c advanceMetric = advaWrapperMap.get(accountId);
            if (advanceMetric == null) {
                advanceMetric =  new Advance_Metric__c(Account__c = accountId);
                advaWrapperMap.put(accountId, advanceMetric);
            }
            advanceMetric.Days_with_Late_Cost_Data_M__c = AdvanceMetricsUtil.add(advanceMetric.Days_with_Late_Cost_Data_M__c, (Decimal)result.get('cnt'));
            advanceMetric.MACs_with_Late_Cost_Data_M__c = AdvanceMetricsUtil.add(advanceMetric.MACs_with_Late_Cost_Data_M__c, (Decimal)result.get('mdistinct'));
        }
          
        System.debug('advaWrapperMap::After'+advaWrapperMap); 
    }
    
    
   /* daysCostDataQueryYearly - Calculates value of fields Days_with_Late_Cost_Data_M__c,   
    *  MACs_with_Late_Cost_Data_M__c yearly and updates Map with value
    */
    public void daysCostDataQueryYearly(List<Id> accIdList, Map<Id,Advance_Metric__c> advaWrapperMap){
        Date startDate = getStartDate(System.today(), LAST_13_MONTHS); 
        
        List<AggregateResult> results = [SELECT count_distinct(Marin_App_Client__c) mdistinct, 
                                                count(Id) cnt,Marin_App_Client__r.Account__c FROM Monthly_Metric__c
                                            WHERE Date__c >= :startDate
                                                AND Cost_Data_on_Time__c = FALSE 
                                                AND Marin_App_Client__r.Account__c in :accIdList 
                                            GROUP BY Marin_App_Client__r.Account__c];
        
        for (AggregateResult result : results) {  
            String accountId = (String)result.get('Account__c');
            Advance_Metric__c advanceMetric = advaWrapperMap.get(accountId);
            if (advanceMetric == null) {
                advanceMetric =  new Advance_Metric__c(Account__c = accountId);
                advaWrapperMap.put(accountId, advanceMetric);
            }
            advanceMetric.Days_with_Late_Cost_Data_Y__c = AdvanceMetricsUtil.add(advanceMetric.Days_with_Late_Cost_Data_Y__c, (Decimal)result.get('cnt'));
            advanceMetric.MACs_with_Late_Cost_Data_Y__c = AdvanceMetricsUtil.add(advanceMetric.MACs_with_Late_Cost_Data_Y__c, (Decimal)result.get('mdistinct'));
        }  
        System.debug('advaWrapperMap::After'+advaWrapperMap);  
    }
    
    /* daysLateRevenueQueryWeekly - Calculates value of fields Days_with_Late_Revenue_Data_W__c,    
     *  MACs_with_Late_Revenue_Data_W__c weekly and updates Map with value
     */
    public void daysLateRevenueQueryWeekly(List<Id> accIdList, Map<Id,Advance_Metric__c> advaWrapperMap){
        List<AggregateResult> daysLateRevenueWeekly 
              = [SELECT count_distinct(Marin_App_Client__c) mdistinct, count(Id) cnt,Marin_App_Client__r.Account__c
                FROM Metric__c 
                Where Date__c = Last_N_Days:8 
                    AND Revenue_Data_on_Time__c = FALSE 
                    AND Marin_App_Client__r.Account__c in :accIdList 
                GROUP BY Marin_App_Client__r.Account__c];
        for (AggregateResult result : daysLateRevenueWeekly) {  
            String accountId = (String)result.get('Account__c');
            Advance_Metric__c advanceMetric = advaWrapperMap.get(accountId);
            if (advanceMetric == null) {
                advanceMetric =  new Advance_Metric__c(Account__c = accountId);
                advaWrapperMap.put(accountId, advanceMetric);
            }

            advanceMetric.Days_with_Late_Revenue_Data_W__c = AdvanceMetricsUtil.add(advanceMetric.Days_with_Late_Revenue_Data_W__c, (Decimal)result.get('cnt'));
            advanceMetric.MACs_with_Late_Revenue_Data_W__c = AdvanceMetricsUtil.add(advanceMetric.MACs_with_Late_Revenue_Data_W__c, (Decimal)result.get('mdistinct'));
        }  

    }
    
    /* daysLateRevenueQueryMonthly - Calculates value of fields Days_with_Late_Revenue_Data_M__c,   
     *  MACs_with_Late_Revenue_Data_M__c monthly and updates Map with value
     */
    public void daysLateRevenueQueryMonthly(List<Id> accIdList, Map<Id,Advance_Metric__c> advaWrapperMap){
        Date startDate = getStartDate(System.today(), LAST_3_MONTHS);
        
        List<AggregateResult> daysLateRevenueMonthly 
            = [SELECT count_distinct(Marin_App_Client__c) mdistinct, count(Id) cnt,Marin_App_Client__r.Account__c
                FROM Monthly_Metric__c 
                Where Date__c >= :startDate  
                AND Revenue_Data_on_Time__c = FALSE 
                AND Marin_App_Client__r.Account__c in :accIdList 
                GROUP BY Marin_App_Client__r.Account__c];
                
        for (AggregateResult result : daysLateRevenueMonthly) {  
            String accountId = (String)result.get('Account__c');
            Advance_Metric__c advanceMetric = advaWrapperMap.get(accountId);
            if (advanceMetric == null) {
                advanceMetric =  new Advance_Metric__c(Account__c = accountId);
                advaWrapperMap.put(accountId, advanceMetric);
            }

            advanceMetric.Days_with_Late_Revenue_Data_M__c = AdvanceMetricsUtil.add(advanceMetric.Days_with_Late_Revenue_Data_M__c, (Decimal)result.get('cnt'));
            advanceMetric.MACs_with_Late_Revenue_Data_M__c = AdvanceMetricsUtil.add(advanceMetric.MACs_with_Late_Revenue_Data_M__c, (Decimal)result.get('mdistinct'));
                
        }  
    }
    
   
   /* daysLateRevenueQueryYearly - Calculates value of fields Days_with_Late_Revenue_Data_M__c, 
    *  MACs_with_Late_Revenue_Data_M__c yearly and updates Map with value
    */
    
    public void daysLateRevenueQueryYearly(List<Id> accIdList, Map<Id,Advance_Metric__c> advaWrapperMap){
        Date startDate = getStartDate(System.today(), LAST_13_MONTHS);
        
        List<AggregateResult> daysLateRevenueYearly
                         = [SELECT count_distinct(Marin_App_Client__c) mdistinct, count(Id) cnt,Marin_App_Client__r.Account__c
                            FROM Monthly_Metric__c 
                            WHERE Date__c >= :startDate  
                            AND Revenue_Data_on_Time__c = FALSE 
                            AND Marin_App_Client__r.Account__c in :accIdList 
                            GROUP BY Marin_App_Client__r.Account__c];
                            
        for (AggregateResult result : daysLateRevenueYearly) {  
            String accountId = (String)result.get('Account__c');
            Advance_Metric__c advanceMetric = advaWrapperMap.get(accountId);
            if (advanceMetric == null) {
                advanceMetric =  new Advance_Metric__c(Account__c = accountId);
                advaWrapperMap.put(accountId, advanceMetric);
            }

            advanceMetric.Days_with_Late_Revenue_Data_Y__c = AdvanceMetricsUtil.add(advanceMetric.Days_with_Late_Revenue_Data_Y__c, (Decimal)result.get('cnt'));
            advanceMetric.MACs_with_Late_Revenue_Data_Y__c = AdvanceMetricsUtil.add(advanceMetric.MACs_with_Late_Revenue_Data_Y__c, (Decimal)result.get('mdistinct'));
                
        }  
    }
    
    /*estimatedRevenue - Calculates value of fields Revenue_to_marin__c   
     *  yesterday and updates Map with value
     */
    public void estimatedRevenue (List<Id> accIdList, Map<Id,Advance_Metric__c> advaWrapperMap){
        List<AggregateResult> estimatedRevenue = [Select Marin_App_Client__r.Account__c, 
                        SUM(Revenue_to_marin__c) sumvar from 
                        Metric__c WHERE Date__c = Yesterday AND 
                        Marin_App_Client__r.Account__c in  :accIdList 
                        GROUP BY Marin_App_Client__r.Account__c];
                        
        for (AggregateResult result : estimatedRevenue) {  
            String accountId = (String)result.get('Account__c');
            Advance_Metric__c advanceMetric = advaWrapperMap.get(accountId);
            if (advanceMetric == null) {
                advanceMetric =  new Advance_Metric__c(Account__c = accountId);
                advaWrapperMap.put(accountId, advanceMetric);
            }

            advanceMetric.Revenue_to_marin__c = AdvanceMetricsUtil.add(advanceMetric.Revenue_to_marin__c, (Decimal)result.get('sumvar'));
                
        }  
    }
    
  /*totalSpendQueryWeekly - Calculates value of fields spend fields, hit fields 
   *  Weekly and updates Map with value
   */
      
    public void totalSpendQueryWeekly(List<Id> accIdList, Map<Id,Advance_Metric__c> advaWrapperMap){
        List<AggregateResult> totalSpendQueryWeekly = 
                                        [SELECT Sum(Total_Spend__c) sumVar, 
                                        Sum( Total_Spend_in_Bidding_Folders__c) totalbidding, 
                                        sum(Other_Publisher_Spend__c) otherSpend, 
                                        sum(Criteo_Spend__c) criteoSpend, 
                                        sum(Facebook_Spend__c) fbSpend, 
                                        sum(Google_Spend__c) gSpend, sum(Universal_Channels_Spend__c) uSpend, 
                                        sum(Yahoo_Japan_Spend__c) yjSpend, sum(Yahoo_Bing_Spend__c) ybSpen,
                                        sum(Management_Hits__c) magtHits, sum(In_App_Reporting_Hits__c)reportHits, 
                                        sum(Optimization_Hits__c) optHits, sum(Other_Hits__c) otherHits, sum(Report_Creation_Hits__c) createHits,
                                        sum(Number_of_Automated_Changes__c) autoChanges, 
                                        sum(Number_of_Changes_Synced_from_Publisher__c) syncChanges,
                                        sum( Number_of_Manual_Changes__c) manualChanges,
                                        Marin_App_Client__r.Account__c 
                                        FROM Metric__c
                                        WHERE Date__c = LAST_N_DAYS:8 AND  Marin_App_Client__r.Account__c in :accIdList
                                        GROUP BY Marin_App_Client__r.Account__c];
        
        for (AggregateResult result : totalSpendQueryWeekly) {  
            String accountId = (String)result.get('Account__c');
            Advance_Metric__c advanceMetric = advaWrapperMap.get(accountId);
            if (advanceMetric == null) {
                advanceMetric =  new Advance_Metric__c(Account__c = accountId);
                advaWrapperMap.put(accountId, advanceMetric);
            }

            advanceMetric.Total_Spend_W__c = AdvanceMetricsUtil.add(advanceMetric.Total_Spend_W__c, (Decimal)result.get('sumVar'));
            advanceMetric.Total_Spend_in_Bidding_Folder_W__c = AdvanceMetricsUtil.add(advanceMetric.Total_Spend_in_Bidding_Folder_W__c, (Decimal)result.get('totalbidding'));
            advanceMetric.Other_Publisher_Spend_W__c = AdvanceMetricsUtil.add(advanceMetric.Other_Publisher_Spend_W__c, (Decimal)result.get('otherSpend'));
            advanceMetric.Criteo_Spend_W__c = AdvanceMetricsUtil.add(advanceMetric.Criteo_Spend_W__c, (Decimal)result.get('criteoSpend'));
            advanceMetric.Facebook_Spend_W__c = AdvanceMetricsUtil.add(advanceMetric.Facebook_Spend_W__c, (Decimal)result.get('fbSpend'));
            advanceMetric.Google_Spend_W__c = AdvanceMetricsUtil.add(advanceMetric.Google_Spend_W__c, (Decimal)result.get('gSpend'));
            advanceMetric.Yahoo_Japan_Spend_W__c = AdvanceMetricsUtil.add(advanceMetric.Yahoo_Japan_Spend_W__c, (Decimal)result.get('yjSpend'));
            advanceMetric.Universal_Channels_Spend__c = AdvanceMetricsUtil.add(advanceMetric.Universal_Channels_Spend__c, (Decimal)result.get('uSpend'));
            advanceMetric.Yahoo_Bing_Spend_W__c = AdvanceMetricsUtil.add(advanceMetric.Yahoo_Bing_Spend_W__c, (Decimal)result.get('ybSpen'));
            advanceMetric.Management_Hits_W__c = AdvanceMetricsUtil.add(advanceMetric.Management_Hits_W__c, (Decimal)result.get('magtHits'));
            advanceMetric.In_App_Reporting_Hits_W__c = AdvanceMetricsUtil.add( advanceMetric.In_App_Reporting_Hits_W__c, (Decimal)result.get('reportHits'));
            advanceMetric.Optimization_Hits_W__c = AdvanceMetricsUtil.add(advanceMetric.Optimization_Hits_W__c, (Decimal)result.get('optHits'));
            advanceMetric.Other_Hits_W__c = AdvanceMetricsUtil.add(advanceMetric.Other_Hits_W__c, (Decimal)result.get('otherHits'));
            advanceMetric.Report_Creation_Hits_W__c = AdvanceMetricsUtil.add(advanceMetric.Report_Creation_Hits_W__c, (Decimal)result.get('createHits'));
            advanceMetric.Number_of_Automated_Changes_W__c = AdvanceMetricsUtil.add(advanceMetric.Number_of_Automated_Changes_W__c, (Decimal)result.get('autoChanges'));
            advanceMetric.Number_of_Changes_Synced_from_PublisherW__c = AdvanceMetricsUtil.add(advanceMetric.Number_of_Changes_Synced_from_PublisherW__c, (Decimal)result.get('syncChanges'));
            advanceMetric.Number_of_Manual_Changes_W__c = AdvanceMetricsUtil.add(advanceMetric.Number_of_Manual_Changes_W__c, (Decimal)result.get('manualChanges'));
                
        }  
    }
    
   /*totalSpendQueryMonthly - Calculates value of fields spend fields, hit fields   
    *  monthly and updates Map with value
    */
    
    public void totalSpendQueryMonthly(List<Id> accIdList, Map<Id,Advance_Metric__c> advaWrapperMap){
        
        Date startDate = getStartDate(System.today(), LAST_3_MONTHS);
        List<AggregateResult> totalSpendQueryMonthly = [SELECT Sum(Total_Spend__c)sumVar,  Sum( Total_Spend_in_Bidding_Folders__c) totalbidding , SUM(Revenue_to_marin__c) sumRevenue, Marin_App_Client__r.Account__c, sum(Other_Publisher_Spend__c) otherSpend, 
                                        sum(Management_Hits__c) magtHits, sum(In_App_Reporting_Hits__c)reportHits, 
                                        sum(Optimization_Hits__c) optHits, sum(Other_Hits__c) otherHits, sum(Report_Creation_Hits__c) createHits,
                                        sum(Criteo_Spend__c) criteoSpend, sum(Facebook_Spend__c) fbSpend, 
                                        sum(Google_Spend__c) gSpend, sum(Universal_Channels_Spend__c) uSpend, 
                                        sum(Yahoo_Japan_Spend__c) yjSpend, sum(Yahoo_Bing_Spend__c) ybSpen,
                                        sum(Number_of_Automated_Changes__c) autoChanges, 
                                        sum(Number_of_Changes_Synced_from_Publisher__c) syncChanges,
                                        sum( Number_of_Manual_Changes__c) manualChanges
                                        FROM Monthly_Metric__c
                                        WHERE Date__c >= :startDate AND  Marin_App_Client__r.Account__c in :accIdList
                                        GROUP BY Marin_App_Client__r.Account__c];
        
        for (AggregateResult result : totalSpendQueryMonthly) {  
            String accountId = (String)result.get('Account__c');
            Advance_Metric__c advanceMetric = advaWrapperMap.get(accountId);
            if (advanceMetric == null) {
                advanceMetric =  new Advance_Metric__c(Account__c = accountId);
                advaWrapperMap.put(accountId, advanceMetric);
            }

            advanceMetric.Total_Spend_M__c = (advanceMetric.Total_Spend_M__c != null ? advanceMetric.Total_Spend_M__c :0.0) + ((Decimal)result.get('sumVar') != null ? (Decimal)result.get('sumVar') : 0);
            advanceMetric.Revenue_to_marin_M__c = (advanceMetric.Revenue_to_marin_M__c != null ? advanceMetric.Revenue_to_marin_M__c :0.0) + ((Decimal)result.get('sumRevenue') != null ? (Decimal)result.get('sumRevenue') :0);
            advanceMetric.Total_Spend_in_Bidding_Folder_M__c = (advanceMetric.Total_Spend_in_Bidding_Folder_M__c != null ? advanceMetric.Total_Spend_in_Bidding_Folder_M__c :0.0) + ((Decimal)result.get('totalbidding') != null ? (Decimal)result.get('totalbidding') : 0);
            advanceMetric.Other_Publisher_Spend_M__c = (advanceMetric.Other_Publisher_Spend_M__c != null ? advanceMetric.Other_Publisher_Spend_M__c :0.0) + ((Decimal)result.get('otherSpend'));
            advanceMetric.Criteo_Spend_M__c = (advanceMetric.Criteo_Spend_M__c != null ? advanceMetric.Criteo_Spend_M__c :0.0 )+ ((Decimal)result.get('criteoSpend') != null ? (Decimal)result.get('criteoSpend') : 0);
            advanceMetric.Facebook_Spend_M__c = (advanceMetric.Facebook_Spend_M__c != null ? advanceMetric.Facebook_Spend_M__c :0.0) + ((Decimal)result.get('fbSpend') != null ? (Decimal)result.get('fbSpend') :0);
            advanceMetric.Google_Spend_M__c = (advanceMetric.Google_Spend_M__c != null ? advanceMetric.Google_Spend_M__c :0.0 )+ ((Decimal)result.get('gSpend') != null ? (Decimal)result.get('gSpend') : 0);
            advanceMetric.Yahoo_Japan_Spend_M__c = (advanceMetric.Yahoo_Japan_Spend_M__c != null ? advanceMetric.Yahoo_Japan_Spend_M__c :0.0) + ((Decimal)result.get('yjSpend') != null ? (Decimal)result.get('yjSpend') : 0);
            advanceMetric.Universal_Channels_Spend_M__c = (advanceMetric.Universal_Channels_Spend_M__c != null ? advanceMetric.Universal_Channels_Spend_M__c :0.0) + ((Decimal)result.get('uSpend') != null ? (Decimal)result.get('uSpend') : 0);
            advanceMetric.Yahoo_Bing_Spend_M__c = (advanceMetric.Yahoo_Bing_Spend_M__c != null ? advanceMetric.Yahoo_Bing_Spend_M__c :0.0 )+ ((Decimal)result.get('ybSpen') != null ? (Decimal)result.get('ybSpen') : 0);
            advanceMetric.Management_Hits_M__c = (advanceMetric.Management_Hits_M__c != null ? advanceMetric.Management_Hits_M__c :0.0) + ((Decimal)result.get('magtHits') != null ? (Decimal)result.get('magtHits') : 0);
            advanceMetric.In_App_Reporting_Hits_M__c = (advanceMetric.In_App_Reporting_Hits_M__c != null ? advanceMetric.In_App_Reporting_Hits_M__c :0.0) + ((Decimal)result.get('reportHits') != null ? (Decimal)result.get('reportHits') : 0);
            advanceMetric.Optimization_Hits_M__c = (advanceMetric.Optimization_Hits_M__c != null ? advanceMetric.Optimization_Hits_M__c :0.0 )+ ((Decimal)result.get('optHits') != null ?(Decimal)result.get('optHits') : 0);
            advanceMetric.Other_Hits_M__c = (advanceMetric.Other_Hits_M__c != null ? advanceMetric.Other_Hits_M__c :0.0) + ((Decimal)result.get('otherHits') != null ? (Decimal)result.get('otherHits') : 0);
            advanceMetric.Report_Creation_Hits_M__c = (advanceMetric.Report_Creation_Hits_M__c != null ? advanceMetric.Report_Creation_Hits_M__c :0.0) + ((Decimal)result.get('createHits') != null ? (Decimal)result.get('createHits') :0);
            advanceMetric.Number_of_Automated_Changes_M__c = (advanceMetric.Number_of_Automated_Changes_M__c != null ? advanceMetric.Number_of_Automated_Changes_M__c :0.0) +( (Decimal)result.get('autoChanges') != null ? (Decimal)result.get('autoChanges') :0);
            advanceMetric.Number_of_Changes_Synced_from_PublisherM__c = (advanceMetric.Number_of_Changes_Synced_from_PublisherM__c != null ? advanceMetric.Number_of_Changes_Synced_from_PublisherM__c :0.0) + ((Decimal)result.get('syncChanges') != null ? (Decimal)result.get('syncChanges') : 0);
            advanceMetric.Number_of_Manual_Changes_M__c = (advanceMetric.Number_of_Manual_Changes_M__c  != null ? advanceMetric.Number_of_Manual_Changes_M__c  :0.0) +( (Decimal)result.get('manualChanges') != null ? (Decimal)result.get('manualChanges') : 0);
            
        }    
    }
    
    /*totalSpendQueryYearly - Calculates value of fields spend fields, hit fields   
    *  yearly and updates Map with value
    */
    public void totalSpendQueryYearly(List<Id> accIdList, Map<Id,Advance_Metric__c> advaWrapperMap){
        List<AggregateResult> totalSpendQueryYearly = new List<AggregateResult> () ;
        Date startDate = getStartDate(System.today(), LAST_13_MONTHS);
        
        totalSpendQueryYearly = [SELECT Sum(Total_Spend__c) sumVar, Sum( Total_Spend_in_Bidding_Folders__c)totalbidding, 
                                    SUM(Revenue_to_marin__c) sumRevenue, Marin_App_Client__r.Account__c, sum(Other_Publisher_Spend__c) 
                                    otherSpend, sum(Criteo_Spend__c) criteoSpend, sum(Facebook_Spend__c) fbSpend, 
                                    sum(Google_Spend__c) gSpend, sum(Universal_Channels_Spend__c) uSpend, 
                                    sum(Yahoo_Japan_Spend__c) yjSpend, sum(Yahoo_Bing_Spend__c) ybSpen,
                                    sum(Management_Hits__c) magtHits, sum(In_App_Reporting_Hits__c)reportHits, 
                                    sum(Optimization_Hits__c) optHits, sum(Other_Hits__c) otherHits, sum(Report_Creation_Hits__c) createHits,
                                    sum(Number_of_Automated_Changes__c) autoChanges, 
                                    sum(Number_of_Changes_Synced_from_Publisher__c) syncChanges,
                                    sum( Number_of_Manual_Changes__c) manualChanges  
                                    FROM Monthly_Metric__c WHERE Date__c >= :startDate   AND  Marin_App_Client__r.Account__c in :accIdList
                                    GROUP BY Marin_App_Client__r.Account__c];
        for (AggregateResult result : totalSpendQueryYearly) {  
            Advance_Metric__c advanceMetric;
            if(!advaWrapperMap.isEmpty()&& advaWrapperMap.containsKey((String)result.get('Account__c'))){
                advanceMetric = advaWrapperMap.get((String)result.get('Account__c'));
                advanceMetric.Total_Spend_Y__c = AdvanceMetricsUtil.add(advanceMetric.Total_Spend_Y__c, (Decimal)result.get('sumVar'));
                advanceMetric.Total_Spend_in_Bidding_Folder_Y__c = AdvanceMetricsUtil.add(advanceMetric.Total_Spend_in_Bidding_Folder_W__c, (Decimal)result.get('totalbidding'));
                advanceMetric.Revenue_to_marin_Y__c = AdvanceMetricsUtil.add(advanceMetric.Revenue_to_marin_Y__c, (Decimal)result.get('sumRevenue'));
                advanceMetric.Other_Publisher_Spend_Y__c = AdvanceMetricsUtil.add(advanceMetric.Other_Publisher_Spend_Y__c, (Decimal)result.get('otherSpend'));
                advanceMetric.Criteo_Spend_Y__c = AdvanceMetricsUtil.add(advanceMetric.Criteo_Spend_Y__c, (Decimal)result.get('criteoSpend'));
                advanceMetric.Facebook_Spend_Y__c = AdvanceMetricsUtil.add(advanceMetric.Facebook_Spend_Y__c, (Decimal)result.get('fbSpend'));
                advanceMetric.Google_Spend_Y__c = AdvanceMetricsUtil.add(advanceMetric.Google_Spend_Y__c, (Decimal)result.get('gSpend'));
                advanceMetric.Yahoo_Japan_Spend_Y__c = AdvanceMetricsUtil.add(advanceMetric.Yahoo_Japan_Spend_Y__c, (Decimal)result.get('yjSpend'));
                advanceMetric.Universal_Channels_Spend_Y__c = AdvanceMetricsUtil.add(advanceMetric.Universal_Channels_Spend_Y__c, (Decimal)result.get('uSpend'));
                advanceMetric.Yahoo_Bing_Spend_Y__c = AdvanceMetricsUtil.add(advanceMetric.Yahoo_Bing_Spend_Y__c, (Decimal)result.get('ybSpen'));
                advanceMetric.Management_Hits_Y__c = AdvanceMetricsUtil.add(advanceMetric.Management_Hits_Y__c, (Decimal)result.get('magtHits'));
                advanceMetric.In_App_Reporting_Hits_Y__c = AdvanceMetricsUtil.add(advanceMetric.In_App_Reporting_Hits_Y__c, (Decimal)result.get('reportHits'));
                advanceMetric.Optimization_Hits_Y__c = AdvanceMetricsUtil.add(advanceMetric.Optimization_Hits_Y__c, (Decimal)result.get('optHits'));
                advanceMetric.Other_Hits_Y__c = AdvanceMetricsUtil.add(advanceMetric.Other_Hits_Y__c, (Decimal)result.get('otherHits'));
                advanceMetric.Report_Creation_Hits_Y__c = AdvanceMetricsUtil.add(advanceMetric.Report_Creation_Hits_Y__c, (Decimal)result.get('createHits'));
                advanceMetric.Number_of_Automated_Changes_Y__c = AdvanceMetricsUtil.add(advanceMetric.Number_of_Automated_Changes_Y__c, (Decimal)result.get('autoChanges'));
                advanceMetric.Number_of_Changes_Synced_from_PublisherY__c = AdvanceMetricsUtil.add(advanceMetric.Number_of_Changes_Synced_from_PublisherY__c, (Decimal)result.get('syncChanges'));
                advanceMetric.Number_of_Manual_Changes_Y__c = AdvanceMetricsUtil.add(advanceMetric.Number_of_Manual_Changes_Y__c, (Decimal)result.get('manualChanges'));
                   
            
            }else{
                advanceMetric =  new Advance_Metric__c(
                    Account__c = (String)result.get('Account__c')
                    
                );
                advanceMetric.Total_Spend_Y__c = (Decimal)result.get('sumVar') != null ? (Decimal)result.get('sumVar') : 0;
                advanceMetric.Total_Spend_in_Bidding_Folder_Y__c = (Decimal)result.get('totalbidding') != null ? (Decimal)result.get('totalbidding') : 0;
                advanceMetric.Revenue_to_marin_Y__c    = (Decimal)result.get('sumRevenue') != null ? (Decimal)result.get('sumRevenue') :0;
                advanceMetric.Other_Publisher_Spend_Y__c = (Decimal)result.get('otherSpend') != null ? (Decimal)result.get('otherSpend') :0 ;
                advanceMetric.Criteo_Spend_Y__c = (Decimal)result.get('criteoSpend') != null ?(Decimal)result.get('criteoSpend') : 0;
                advanceMetric.Facebook_Spend_Y__c = (Decimal)result.get('fbSpend') != null ? (Decimal)result.get('fbSpend') :0 ;
                advanceMetric.Google_Spend_Y__c = (Decimal)result.get('gSpend') != null ? (Decimal)result.get('gSpend') :0;
                advanceMetric.Yahoo_Japan_Spend_Y__c = (Decimal)result.get('yjSpend') != null ? (Decimal)result.get('yjSpend') :0;
                advanceMetric.Universal_Channels_Spend_Y__c = (Decimal)result.get('uSpend')!= null ? (Decimal)result.get('uSpend') : 0;
                advanceMetric.Yahoo_Bing_Spend_Y__c = (Decimal)result.get('ybSpen') != null ? (Decimal)result.get('ybSpen') : 0;
                advanceMetric.Management_Hits_Y__c = (Decimal)result.get('magtHits') != null ? (Decimal)result.get('magtHits') : 0;
                advanceMetric.In_App_Reporting_Hits_Y__c = (Decimal)result.get('reportHits') != null ? (Decimal)result.get('reportHits') : 0;
                advanceMetric.Optimization_Hits_Y__c = (Decimal)result.get('optHits') != null ? (Decimal)result.get('optHits') : 0;
                advanceMetric.Other_Hits_Y__c = (Decimal)result.get('otherHits') != null ? (Decimal)result.get('otherHits') : 0;
                advanceMetric.Report_Creation_Hits_Y__c = (Decimal)result.get('createHits') != null ? (Decimal)result.get('createHits') : 0;
                advanceMetric.Number_of_Automated_Changes_Y__c = (Decimal)result.get('autoChanges') != null ? (Decimal)result.get('autoChanges') :0;
                advanceMetric.Number_of_Changes_Synced_from_PublisherY__c = (Decimal)result.get('syncChanges') != null ? (Decimal)result.get('syncChanges') : 0 ;
                advanceMetric.Number_of_Manual_Changes_Y__c = (Decimal)result.get('manualChanges') != null ? (Decimal)result.get('manualChanges') : 0;
                
            }
            advaWrapperMap.put((String)result.get('Account__c'),advanceMetric);
        }  
    }
    
   /*pcaLinkedPerDay - Calculates value of fields of_PCAs_linked_as_of_previous_day__c, of_PCAs_Expired_as_of_previous_day_7__c    
    *  Of_Unique_User_Logins_as_of_previous_W__c for yesterday and updates Map with value
    */    
    public void pcaLinkedPerDay(List<Id> accIdList, Map<Id,Advance_Metric__c> advaWrapperMap){
        List<AggregateResult> pcaLinkedQuery = new List<AggregateResult> () ;
        pcaLinkedQuery = [SELECT Marin_App_Client__r.Account__c, sum(Number_of_Expired_PCAs__c) expiredpca, 
                                sum(Number_of_Linked_PCAs__c) linkedpca, sum( Unique_User_Logins__c)uniqueUser
                                FROM Metric__c 
                                WHERE Date__c = YESTERDAY
                                AND  Marin_App_Client__r.Account__c in :accIdList 
                                GROUP BY Marin_App_Client__r.Account__c];
        for (AggregateResult result : pcaLinkedQuery) {  
            Advance_Metric__c advanceMetric;
            if(!advaWrapperMap.isEmpty()&& advaWrapperMap.containsKey((String)result.get('Account__c'))){
                advanceMetric = advaWrapperMap.get((String)result.get('Account__c'));
                advanceMetric.of_PCAs_linked_as_of_previous_day__c   = (advanceMetric.of_PCAs_linked_as_of_previous_day__c != null ? advanceMetric.of_PCAs_linked_as_of_previous_day__c :0.0 )+ ((Decimal)result.get('linkedpca') != null ? (Decimal)result.get('linkedpca') : 0);
                advanceMetric.of_PCAs_Expired_as_of_previous_day_7__c = (advanceMetric.of_PCAs_Expired_as_of_previous_day_7__c != null ? advanceMetric.of_PCAs_Expired_as_of_previous_day_7__c :0.0)  + ((Decimal)result.get('expiredpca') != null ? (Decimal)result.get('expiredpca') :0);
                advanceMetric.Of_Unique_User_Logins_as_of_previous_7__c  = (advanceMetric.Of_Unique_User_Logins_as_of_previous_7__c  != null ? advanceMetric.Of_Unique_User_Logins_as_of_previous_7__c  : 0.0) + ((Decimal)result.get('uniqueUser') != null ? (Decimal)result.get('uniqueUser') : 0);
                
            }else{
                advanceMetric =  new Advance_Metric__c(
                    Account__c = (String)result.get('Account__c')
                    
                );
                advanceMetric.of_PCAs_linked_as_of_previous_day__c   = (Decimal)result.get('linkedpca') != null ? (Decimal)result.get('linkedpca') : 0;
                advanceMetric.of_PCAs_Expired_as_of_previous_day_7__c = (Decimal)result.get('expiredpca') != null ? (Decimal)result.get('expiredpca') :0;
                advanceMetric.Of_Unique_User_Logins_as_of_previous_7__c  = (Decimal)result.get('uniqueUser') != null ? (Decimal)result.get('uniqueUser') : 0;
                
            }
            advaWrapperMap.put((String)result.get('Account__c'),advanceMetric);
        }    
    }
    
   /*pcaChangeDiff_7Day_1Day - Calculates value of fields Number_of_Linked_PCAs__c  
    *  for yesterday and updates Map with value
    */
    public void pcaChangeDiff_7Day_1Day(List<Id> accIdList, Map<Id,Advance_Metric__c> advaWrapperMap){
        List<AggregateResult> pcaLinkedQuery = new List<AggregateResult> () ;
        date dt = System.today().addDays(-8);
        pcaLinkedQuery = [SELECT  Sum(Number_of_Linked_PCAs__c) numLink, Marin_App_Client__r.Account__c
                            FROM  Metric__c 
                            WHERE  date__c = :dt 
                            AND  Marin_App_Client__r.Account__c in :accIdList
                            AND Number_of_Linked_PCAs__c != null
                            GROUP BY Marin_App_Client__r.Account__c];
        for (AggregateResult result : pcaLinkedQuery) {  
            Advance_Metric__c advanceMetric = getAdvanceMetric(result, advaWrapperMap);
            Decimal numLink = result.get('numLink') == null ? 0 : (Decimal)result.get('numLink');
            Decimal diff 
                = AdvanceMetricsUtil.add(numLink, -(advanceMetric.of_PCAs_linked_as_of_previous_day__c));
            advanceMetric.of_PCAs_net_change_diff_btw_Day7_Day1__c
                 = AdvanceMetricsUtil.add(advanceMetric.of_PCAs_net_change_diff_btw_Day7_Day1__c, diff);
        }
           
    }
   
     public void pcaChangeDiff_7Day_1Day_Y(List<Id> accIdList, Map<Id,Advance_Metric__c> advaWrapperMap){

        Date startDate = getStartDate(System.today(), LAST_13_MONTHS);
        List<AggregateResult> pcaLinkedQuery = [SELECT  Sum(Number_of_Linked_PCAs__c) numLink, Marin_App_Client__r.Account__c
                            FROM  Monthly_Metric__c
                            WHERE  date__c >= :startDate
                                AND  Marin_App_Client__r.Account__c in :accIdList
                            GROUP BY Marin_App_Client__r.Account__c];
        
        for (AggregateResult result : pcaLinkedQuery) {  
            String accountId = (String)result.get('Account__c');
            Advance_Metric__c advanceMetric = advaWrapperMap.get(accountId);
            if (advanceMetric == null) {
                advanceMetric =  new Advance_Metric__c(Account__c = accountId);
                advaWrapperMap.put(accountId, advanceMetric);
            }
            Decimal numLink = result.get('numLink') == null ? 0 : (Decimal)result.get('numLink');
            advanceMetric.of_PCAs_net_change_diff_btw_Day7_Day1_Y__c 
                = AdvanceMetricsUtil.add(advanceMetric.of_PCAs_net_change_diff_btw_Day7_Day1_Y__c, numLink);
        }
           
    }
    
    public void pcaChangeDiff_7Day_1Day_M(List<Id> accIdList, Map<Id,Advance_Metric__c> advaWrapperMap){
        Date endDate = System.today().adddays(-1);
        Date startDate = getStartDate(System.today(), LAST_3_MONTHS);

        
        List<AggregateResult> pcaLinkedQuery = [SELECT  Sum(Number_of_Linked_PCAs__c) numLink, Marin_App_Client__r.Account__c
                            FROM  Monthly_Metric__c
                            WHERE  Date__c >= :startDate  AND Date__c <= :endDate
                            AND  Marin_App_Client__r.Account__c in :accIdList
                            AND Number_of_Linked_PCAs__c != null
                            GROUP BY Marin_App_Client__r.Account__c];
                            
        for (AggregateResult result : pcaLinkedQuery) {  
            String accountId = (String)result.get('Account__c');
            Advance_Metric__c advanceMetric = advaWrapperMap.get(accountId);
            if (advanceMetric == null) {
                advanceMetric =  new Advance_Metric__c(Account__c = accountId);
                advaWrapperMap.put(accountId, advanceMetric);
            }

            Decimal numLink = result.get('numLink') == null ? 0 : (Decimal)result.get('numLink');
            Decimal pcaLinkedAsOfPrevDay = (advanceMetric.of_PCAs_linked_as_of_previous_day__c == null) ? 0 : -(advanceMetric.of_PCAs_linked_as_of_previous_day__c);
            System.debug('numLink = ' + numLink);
            System.debug('pcaLinkedAsOfPrevDay = ' + pcaLinkedAsOfPrevDay);
            Decimal diff =  AdvanceMetricsUtil.add(numLink, pcaLinkedAsOfPrevDay);
            advanceMetric.of_PCAs_net_change_diff_btw_Day7_Day1_M__c   = AdvanceMetricsUtil.add(-pcaLinkedAsOfPrevDay, diff);
               
        }
           
    }
    
    /******Utility Methods **********************************/
    
    public static Decimal add(Decimal value1, Decimal value2) {
        value1 = (value1 == null) ? 0 : value1;
        value2 = (value2 == null) ? 0 : value2;
        return value1 + value2;
    }
    
    public Date getStartDate(Date endDate, Integer months){
        return endDate.addMonths(-months).toStartOfMonth();
    } 
    
    public Advance_Metric__c getAdvanceMetric(AggregateResult result, Map<Id,Advance_Metric__c> advaWrapperMap) {
        String accountId = (String)result.get('Account__c');
        Advance_Metric__c advanceMetric = advaWrapperMap.get(accountId);
        if (advanceMetric == null) {
            advanceMetric =  new Advance_Metric__c(Account__c = accountId);
            advaWrapperMap.put(accountId, advanceMetric);
        }
        return advanceMetric;
    }
}