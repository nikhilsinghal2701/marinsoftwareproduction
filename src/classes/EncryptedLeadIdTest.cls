/* Copyright Marin Software 2011. All rights reserved.
 *
 * @author: apotapov
 *
 * This file is currently maintained under Marin source control
 * in mscm project under resources/salesforce/triggers/ecrypt_lead_id.apex.
 * Any modifications to this code in SalesForce should be reflected
 * in the source file in mscm as well for versioning purposes.
 */
 
/* Unit test class for Encrypted Ids on the Lead class.
 */
 @isTest 
public class EncryptedLeadIdTest {
    public static testMethod void testEncryptedLeadId() {
    
        Lead lead = new Lead();
        lead.FirstName = 'Johnny';
        lead.LastName = 'Leader';
        lead.Company = 'Test Company';
        lead.Status = 'Open';
        lead.Business_Model__c = 'Agency';
        insert lead;
        
        Lead createdLead = [select Encrypted_Lead_Id__c from Lead where Id=:lead.Id LIMIT 1000];
        
        System.assert(createdLead.Encrypted_Lead_Id__c!=null);
    }
}