@isTest
private class CustomWebToCaseControllerTests {

    static testMethod void validateCancelReturnsNull() {
        CustomWebToCaseController controller = new CustomWebToCaseController();
        
        PageReference cancelPage = controller.cancel();
        System.assertEquals(null, cancelPage);
    }

    static testMethod void validateNoEmailWasSupplied() {
        CustomWebToCaseController controller = new CustomWebToCaseController();
        
        PageReference savePage = controller.createCase();
        System.assertEquals(null, savePage);
        System.assertEquals(true, controller.noEmailSupplied);
    }
    
    static testMethod void validateNoProblemAreaWasSupplied() {
        ApexPages.currentPage().getParameters().put('e', 'test@test.com');
        
        CustomWebToCaseController controller = new CustomWebToCaseController();
        
        PageReference savePage = controller.createCase();
        System.assertEquals(null, savePage);
        System.assertEquals(true, controller.noProblemAreaSupplied);
    }
    
    static testMethod void validateNoIssueWasSupplied() {
        ApexPages.currentPage().getParameters().put('e', 'test@test.com');
        
        CustomWebToCaseController controller = new CustomWebToCaseController();
        controller.webCase.Case_Reason__c = 'Test Reason';
        
        PageReference savePage = controller.createCase();
        System.assertEquals(null, savePage);
        System.assertEquals(true, controller.noIssueSupplied);
    }
    
    static testMethod void validateNoPriorityWasSupplied() {
        ApexPages.currentPage().getParameters().put('e', 'test@test.com');
        
        CustomWebToCaseController controller = new CustomWebToCaseController();
        controller.webCase.Case_Reason__c = 'Test Reason';
        controller.webCase.Case_Reason_Detail__c = 'Test Reason Detail';
        
        PageReference savePage = controller.createCase();
        System.assertEquals(null, savePage);
    }
    
    static testMethod void validateNoDescriptionWasSupplied() {
        ApexPages.currentPage().getParameters().put('e', 'test@test.com');
        
        CustomWebToCaseController controller = new CustomWebToCaseController();
        controller.webCase.Case_Reason__c = 'Test Reason';
        controller.webCase.Case_Reason_Detail__c = 'Test Reason Detail';
        controller.webCase.Pritority__c = 'Test Priority';
        
        PageReference savePage = controller.createCase();
        System.assertEquals(null, savePage);
        System.assertEquals(true, controller.noDescriptionSupplied);
    }
    
    static testMethod void validateCaseCreatedNoAttachmentOrCC() {
        ApexPages.currentPage().getParameters().put('n', 'Testy Testerson');
        ApexPages.currentPage().getParameters().put('e', 'test@test.com');
        
        CustomWebToCaseController controller = new CustomWebToCaseController();
        controller.webCase.Case_Reason__c = 'Test Reason';
        controller.webCase.Case_Reason_Detail__c = 'Test Reason Detail';
        controller.webCase.Pritority__c = 'Test Priority';
        controller.webCase.Description = 'Test Description';
        
        PageReference savePage = controller.createCase();
        System.assertNotEquals(null, savePage);
        
        Case newCase = 
            [SELECT Id, Case_Reason__c, Case_Reason_Detail__c, Pritority__c, Description,
                    SuppliedEmail, SuppliedName, Origin, CC_1__c, CC_2__c, CC_3__c,
                    CC_4__c, CC_5__c 
             FROM Case
             WHERE Id = :controller.webCase.Id
             LIMIT 1000];
        System.assertEquals('Test Reason', newCase.Case_Reason__c);
        System.assertEquals('Test Reason Detail', newCase.Case_Reason_Detail__c);
        System.assertEquals('Test Priority', newCase.Pritority__c);
        System.assertEquals('Test Description', newCase.Description);
        System.assertEquals('test@test.com', newCase.SuppliedEmail);
        System.assertEquals('Testy Testerson', newCase.SuppliedName);
        System.assertEquals('Web to Case', newCase.Origin);
        System.assertEquals(null, newCase.CC_1__c);
        System.assertEquals(null, newCase.CC_2__c);
        System.assertEquals(null, newCase.CC_3__c);
        System.assertEquals(null, newCase.CC_4__c);
        System.assertEquals(null, newCase.CC_5__c);
    }
    
    static testMethod void validateCaseCreatedWithAttachmentAnd5CCs() {
        ApexPages.currentPage().getParameters().put('n', 'Testy Testerson');
        ApexPages.currentPage().getParameters().put('e', 'test@test.com');
        
        CustomWebToCaseController controller = new CustomWebToCaseController();
        controller.webCase.Case_Reason__c = 'Test Reason';
        controller.webCase.Case_Reason_Detail__c = 'Test Reason Detail';
        controller.webCase.Pritority__c = 'Test Priority';
        controller.webCase.Description = 'Test Description';
        controller.ccList = 'cc_user1@test.com,cc_user2@test.com,cc_user3@test.com,cc_user4@test.com,cc_user5@test.com';
        controller.attachment.Name = 'Test File.txt';
        controller.attachment.Body = Blob.valueOf('Testing text');
        
        PageReference savePage = controller.createCase();
        
        Case newCase = 
            [SELECT Id, CC_1__c, CC_2__c, CC_3__c, CC_4__c, CC_5__c
             FROM Case
             WHERE Id = :controller.webCase.Id
             LIMIT 1000];
        Attachment[] attachments = 
            [SELECT Id, Name, Body
             FROM Attachment
             WHERE ParentId = :controller.webCase.Id
             LIMIT 1000];
              
        System.assertEquals('cc_user1@test.com', newCase.CC_1__c);
        System.assertEquals('cc_user2@test.com', newCase.CC_2__c);
        System.assertEquals('cc_user3@test.com', newCase.CC_3__c);
        System.assertEquals('cc_user4@test.com', newCase.CC_4__c);
        System.assertEquals('cc_user5@test.com', newCase.CC_5__c);
        System.assertNotEquals(null, attachments);
        System.assertEquals(1, attachments.size());
        System.assertEquals('Test File.txt', attachments[0].Name);
        System.assertEquals('Testing text', attachments[0].Body.toString());
    }
}