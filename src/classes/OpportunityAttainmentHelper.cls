/**
* @author Nikhil Singhal @ Perficient
* Created Date: 1/8/2015
*  Functionality :
*   Opportunity goes to Close
*   a.	For Opportunity Owner and Managers:
*   	i.	Insert Attainment Opportunity Record for Opportunity Owner based on matching owner with the Owner of the Attainment record, and Opportunity Close Month and Year match the Attainment Start Date Month and Year
*   	ii.	If Attainment Record has a Manager Attainment value, insert Attainment Opportunity record using the same matching logic, but this time for Manager instead of Opportunity Owner.
*			1.Repeat this process until you reach an Attainment record with no value in the Attainment Manger field.
*   b.  For Display Specialist:
*		i.	Query to see if there are any Opportunity Team members with a  role of “Display Specialist”
*		ii.	If there are, insert an Attainment Opportunity record for the Opportunity you are on, and the Attainment where the owner of the Attainment is the Display Specialist.
*			1.	Set the “Is Overlay” value to “True”  
*			2.	Match the record on Month and Year as in 1.a.i.
*		iii.	If Attainment Record has a Manager Attainment value, insert Attainment Opportunity record using the same matching logic, but this time for Manager instead of Display Specialist.
*			1.	Repeat this process until you reach an Attainment record with no value in the Attainment Manger field.
*	c.	Do not insert an Attainment Opportunity junction for someone if they already have it for that Opportunity. 
*	d.	Update all Attainment records affected to update the Quota Closed to the sum of the Qualifying Amount on the Attainment Opportunity records.
*	e.  Allow for Omit from Attainment so that if the field is marked true the code will ignore the opportunity and not create a join between that particular opportunity and attainment record.
*   f.  Additional Filters on Opportunity Attainment Creation. Create Attainment Opportunity records only for:
*				Record Type: New Business
*				Record Type: Existing Business
*				Record Type: Attainment Adjustment
*				Record Type of Transfer Business and Opportunity Type Detail Picklist field with the "Transfer Increase" set as the value.
*/



public with sharing class OpportunityAttainmentHelper {
	
    public static Map<id,Opportunity> Opp_Map = new Map<id,Opportunity>();
    public static Map<id,boolean> OppRT_Map = new Map<id,boolean>();
    public static Final String NEW_BUSINESS = 'Record_Type_1';
    public static Final String EXISTING_BUSINESS = 'Existing_Business';
    public static Final String ATTAINMENT_ADJUSTMENT = 'Attainment_Adjustment';
    public static Final String TRANSFER_BUSINESS = 'Transfer_Business';
    public static Final String TRANSFER_INCREASE = 'Transfer Increase';
    
    public static void getOppDetailsAfterInsert(List<Opportunity> new_values){
    	   List<Opportunity> changed_Opportunitys = new List<Opportunity>();
          //System.debug('OppRT_Mapbefore***************: ' +OppRT_Map);
          recordTypeMap();
          //System.debug('OppRT_Mapafter***************: ' +OppRT_Map);
          for (Opportunity new_Opportunity : new_values)
          {
            if ((OppRT_Map.containsKey(new_Opportunity.recordtypeid)) 
            	&& (!new_Opportunity.Omitted_from_Attainment__c) 
            	&& new_Opportunity.IsWon){
            	if(OppRT_Map.get(new_Opportunity.recordtypeid)){
            		if(new_Opportunity.Type_Detail__c == TRANSFER_INCREASE){
              		changed_Opportunitys.add(new_Opportunity);
            		}
            	}else {
            		changed_Opportunitys.add(new_Opportunity);
            	}
            }
          }
          
          if(!changed_Opportunitys.isEmpty()){
          	   try {
          	   	findAttainments(changed_Opportunitys);
          	   }catch(Exception e) {
    			System.debug('The following exception has occurred: ' + e.getMessage());    
				}
          }
    	
    }
     
    /*
   	*	Method to check for Opportunity if closed
   	*	Check if we have right recordtype opportunity
   	*	Check if Not ommited for attainment
   	*   Static mao used to make sure Workflow Update not fire the trigger again
    *	If all good go to Find attainment methods
    */	
    public static void getOppDetailsAfterUpdate(Map<ID,Opportunity> old_map, List<Opportunity> new_values)
      {
          List<Opportunity> changed_Opportunitys = new List<Opportunity>();
          //System.debug('OppRT_Mapbefore***************: ' +OppRT_Map);
          recordTypeMap();
          //System.debug('OppRT_Mapafter***************: ' +OppRT_Map);
          for (Opportunity new_Opportunity : new_values)
          {
            Opportunity old_Opportunity = old_map.get(new_Opportunity.Id);
            if ((OppRT_Map.containsKey(new_Opportunity.recordtypeid)) 
            	&& (!new_Opportunity.Omitted_from_Attainment__c) 
            	&& old_Opportunity.StageName != new_Opportunity.StageName 
            	&& new_Opportunity.IsWon  
            	&& (!Opp_Map.containskey(new_Opportunity.id))){
            	if(OppRT_Map.get(new_Opportunity.recordtypeid)){
            		if(new_Opportunity.Type_Detail__c == TRANSFER_INCREASE){
              		changed_Opportunitys.add(new_Opportunity);
              		Opp_Map.put(new_Opportunity.id,new_Opportunity);
            		}
            	}else {
            		changed_Opportunitys.add(new_Opportunity);
              		Opp_Map.put(new_Opportunity.id,new_Opportunity);
            	}
            }
          }
          
          if(!changed_Opportunitys.isEmpty()){
          	   try {
          	   	findAttainments(changed_Opportunitys);
          	   }catch(Exception e) {
    			System.debug('The following exception has occurred: ' + e.getMessage());    
				}
          }
		
      }


	 /*
   	*	Method to find attainments based on Oppotunity owner and Display Specialist
   	*	Check for team memeber if any as 'Display Specialist'
   	*   Save all Display Specialist and owner in map with opportunity key
   	*	Collect all atainment in map with User as key
   	*   we will go up to 6 peers attainment. 1 attainment + 6 manager attainments
    *	If all good go to create junction method
    */
    public static void findAttainments(List<Opportunity> opp_list)
      {
        Map<id,set<id>> mapOpp_Owners_DispSpec = new Map<id,set<id>>(); 
        set<id> set_Owners_DispSpec = new set<id>();
        set<string> IsDispspec = new set<string>(); 
        Map<id,list<Attainment__c>> map_Owners_DispSpec_Attainment = new Map<id,list<Attainment__c>>(); 
        //System.debug('opp_list***************: ' +opp_list);    
        
        //Find Display Specialist opportunity member
        for(OpportunityTeamMember oppm :[SELECT UserId, OpportunityId,  TeamMemberRole FROM OpportunityTeamMember WHERE OpportunityId in :opp_list AND
                         TeamMemberRole ='Display Specialist']){
            if(mapOpp_Owners_DispSpec.containsKey(oppm.OpportunityId)){
                mapOpp_Owners_DispSpec.get(oppm.OpportunityId).add(oppm.UserId);
            }else{
                mapOpp_Owners_DispSpec.put(oppm.OpportunityId,new set<id>{oppm.UserId});
            }
            IsDispspec.add(string.valueof(oppm.OpportunityId).toUpperCase()+string.valueof(oppm.UserId).toUpperCase());
            set_Owners_DispSpec.add(oppm.UserId);
         }
         
         for(Opportunity opp : opp_list){
            if(mapOpp_Owners_DispSpec.containsKey(opp.Id)){
                mapOpp_Owners_DispSpec.get(opp.Id).add(opp.ownerid);
            }else{
                mapOpp_Owners_DispSpec.put(opp.Id,new set<id>{opp.ownerid});
            }
            set_Owners_DispSpec.add(opp.ownerid);
         }
         //System.debug('mapOpp_Owners_DispSpec***************: ' +mapOpp_Owners_DispSpec); 
         //System.debug('mapOpp_Owners_DispSpec.keyset()***************: ' +mapOpp_Owners_DispSpec.keyset());
         //System.debug('set_Owners_DispSpec***************: ' +set_Owners_DispSpec);     
         for(Attainment__c att:[select id,Ownerid,Start_Date__c,
                                    Manager_Attainment__r.Id,Manager_Attainment__r.Start_Date__c, 
                                    Manager_Attainment__r.Manager_Attainment__r.Id,Manager_Attainment__r.Manager_Attainment__r.Start_Date__c,  
                                    Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Id,Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Start_Date__c, 
                                    Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Id,
                                    Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Start_Date__c, 
                                    Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Id,
                                    Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Start_Date__c, 
                                    Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__c
                                from Attainment__c 
                                where ownerid in:set_Owners_DispSpec]){
            if(map_Owners_DispSpec_Attainment.containsKey(att.Ownerid)){
                map_Owners_DispSpec_Attainment.get(att.Ownerid).add(att);
            }else{
                map_Owners_DispSpec_Attainment.put(att.Ownerid,new List<Attainment__c>{att});
            }
         }
         //System.debug('map_Owners_DispSpec_Attainment***************: ' +map_Owners_DispSpec_Attainment);     
         //System.debug('map_Owners_DispSpec_Attainment.keyset()***************: ' +map_Owners_DispSpec_Attainment.keyset());
         if(!map_Owners_DispSpec_Attainment.IsEmpty() && !mapOpp_Owners_DispSpec.IsEmpty()){
            try {
            CreateJunction(map_Owners_DispSpec_Attainment,mapOpp_Owners_DispSpec,IsDispspec,opp_list);
           	}catch(Exception e) {
    		System.debug('The following exception has occurred: ' + e.getMessage());    
			}
         }   
      }
      
     /*
   	*	Method to use attainments based on Oppotunity owner and Display Specialist 
   	*	Make sure we have close date month and year same as Attainment Month and Year
   	*   we need ot have Unique junction record for opportunity and attainment.
   	*	Using set to avoid duplicates
    *	If all good go to create junction records and send all attainment ids for calculation
    */
      
      public static void CreateJunction(Map<id,list<Attainment__c>> map_Owners_DispSpec_Attainment
                                        ,Map<id,set<id>> mapOpp_Owners_DispSpec
                                        ,set<string> IsDispspec
                                        ,List<Opportunity> opp_list){
        set<id> attIds = new set<id>();
        set<string> attoppIds = new set<string>();
        list<Attainment_Opportunity__c> InsertAO = new list<Attainment_Opportunity__c>();                                   
        for(Opportunity opp : opp_list){//loop for opportiunity
            if(mapOpp_Owners_DispSpec.containskey(opp.id)){//Check for key in Owner and display spec
                if(!mapOpp_Owners_DispSpec.get(opp.id).isEmpty()){//see if not empty
                    for(id odid:mapOpp_Owners_DispSpec.get(opp.id)){//run loop to get owner and disp spec
                        if(map_Owners_DispSpec_Attainment.containskey(odid)){//Check if aattainment map if having opportuity key
                            if(!map_Owners_DispSpec_Attainment.get(odid).IsEmpty()){//see if not empty
                            	//System.debug('map_Owners_DispSpec_Attainment***************: ' +map_Owners_DispSpec_Attainment.get(odid));
                                for(Attainment__c att :map_Owners_DispSpec_Attainment.get(odid)){//run all attainments
                                    if(att.Start_Date__c.Month() == opp.CloseDate.Month()
                                        &&  att.Start_Date__c.Year() == opp.CloseDate.Year()){
                                        Attainment_Opportunity__c AO = new Attainment_Opportunity__c();
                                        AO.Attainment__c=att.id;
                                        AO.Opportunity__c=opp.id;
                                        if(IsDispspec.Contains(string.valueof(opp.id).toUpperCase()+string.valueof(att.ownerid).toUpperCase())){
                                        AO.Is_Overlay__c=true;  
                                        }
                                        attIds.add(att.id);
                                        if(!alreadySet(att.id,opp.id,attoppIds)){
                                        InsertAO.add(AO);
                                        attoppIds.add(string.valueof(att.id).toUpperCase()+string.valueof(opp.id).toUpperCase());
                                        }
                                        if(att.Manager_Attainment__r.Id != null){
                                        if(!alreadySet(att.Manager_Attainment__r.Id,opp.id,attoppIds)){	
                                        InsertAO.add(new Attainment_Opportunity__c(Attainment__c = att.Manager_Attainment__r.Id, Opportunity__c = opp.id));
                                        attoppIds.add(string.valueof(att.Manager_Attainment__r.id).toUpperCase()+string.valueof(opp.id).toUpperCase());
                                        }
                                        attIds.add(att.Manager_Attainment__r.Id);
                                        }
                                        if (att.Manager_Attainment__r.Manager_Attainment__r.Id != null) {
                                        if(!alreadySet(att.Manager_Attainment__r.Manager_Attainment__r.Id,opp.id,attoppIds)){	
                                        InsertAO.add(new Attainment_Opportunity__c(Attainment__c = att.Manager_Attainment__r.Manager_Attainment__r.Id
                                        , Opportunity__c = opp.id));
                                        attoppIds.add(string.valueof(att.Manager_Attainment__r.Manager_Attainment__r.id).toUpperCase()+string.valueof(opp.id).toUpperCase());
                                        }
                                        attIds.add(att.Manager_Attainment__r.Manager_Attainment__r.Id);
                                        }
                                        if (att.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Id != null) {
                                        if(!alreadySet(att.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Id,opp.id,attoppIds)){	
                                        InsertAO.add(new Attainment_Opportunity__c(Attainment__c = att.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Id
                                        , Opportunity__c = opp.id));    
                                        attoppIds.add(string.valueof(att.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.id).toUpperCase()+string.valueof(opp.id).toUpperCase());
                                        }
                                        attIds.add(att.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.id);
                                        }
                                        if (att.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Id != null) {
                                        if(!alreadySet(att.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Id,opp.id,attoppIds)){	
                                        InsertAO.add(new Attainment_Opportunity__c(Attainment__c = att.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Id
                                        , Opportunity__c = opp.id));    
                                        attoppIds.add(string.valueof(att.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.id).toUpperCase()+string.valueof(opp.id).toUpperCase());
                                        }
                                        attIds.add(att.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.id);
                                        }
                                        if (att.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Id != null) {
                                        if(!alreadySet(att.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Id,opp.id,attoppIds)){
                                        InsertAO.add(new Attainment_Opportunity__c(Attainment__c = att.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Id
                                        , Opportunity__c = opp.id));
                                        attoppIds.add(string.valueof(att.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.id).toUpperCase()+string.valueof(opp.id).toUpperCase());
                                        }    
                                        attIds.add(att.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.id);
                                        }
                                        if (att.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__c != null) {
                                        if(!alreadySet(att.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__c,opp.id,attoppIds)){
                                        InsertAO.add(new Attainment_Opportunity__c(Attainment__c = att.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__c
                                        , Opportunity__c = opp.id));
                                        attoppIds.add(string.valueof(att.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__c).toUpperCase()+string.valueof(opp.id).toUpperCase());
                                        }    
                                        attIds.add(att.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__r.Manager_Attainment__c);
                                        }
                                    }
                                }
                            }
                        }
                        
                    }
                
                }
            }
            
        }
        try {
        //System.debug('InsertAO***************: ' +InsertAO);
        	
        if(!InsertAO.IsEmpty())Insert InsertAO;
        if(!attIds.IsEmpty())calculateAttainmentQuota(attIds);
        }catch(Exception e) {
    	System.debug('The following exception has occurred: ' + e.getMessage());    
		}
      }
		
	/*
   	*	Calculate and update ClosedQuotaAmount. 
   	*	find all the junction records where we have attainment associated and add qualifying amount
   	*   update sum to ClosedQuotaAmount in attainment
   	*/
        public static void calculateAttainmentQuota(Set<Id> attIds) {
            Map<id,list<Decimal>> map_Att_Opp = new Map<id,list<Decimal>>();
            List<Attainment__c> atts = new List<Attainment__c>();
            Decimal addamt =0;
            for(Attainment_Opportunity__c ao:[Select Attainment__c,Opportunity__c,Opportunity__r.Amount,Qualifying_Amount__c from Attainment_Opportunity__c where Attainment__c In: attIds] ){
            if(map_Att_Opp.containsKey(ao.Attainment__c)){
                map_Att_Opp.get(ao.Attainment__c).add(ao.Qualifying_Amount__c);
            }else{
                map_Att_Opp.put(ao.Attainment__c,new list<Decimal>{ao.Qualifying_Amount__c});
            }
                
            }
            
            for(Id ao : attIds){
            	if(map_Att_Opp.containskey(ao)){
            		if(!map_Att_Opp.get(ao).IsEmpty()){
            			for(decimal amt :map_Att_Opp.get(ao)){
            				addamt+=amt;
            			}
            		atts.add(new Attainment__c(Id = ao, Quota_Closed__c = addamt));
            		addamt =0;
            		}
            	}
            }
            try {
            if(!atts.IsEmpty())update atts;
            }catch(Exception e) {
    		System.debug('The following exception has occurred: ' + e.getMessage());    
			}
        }
        
    /*
   	*	Method to check unique
   	*/
        public static boolean alreadySet(id Attid,id oppid, set<string> attoppIds) {
			if(attoppIds.contains(string.valueof(attid).toUpperCase()+string.valueof(oppid).toUpperCase())){
			return true;
			} else {
			return false;
			}

        }
        
    /*
   	*	Method to get recordtypes.
   	*/
        public static void recordTypeMap() {
        	set<string> recTypename = new Set<String>{NEW_BUSINESS,EXISTING_BUSINESS,ATTAINMENT_ADJUSTMENT,TRANSFER_BUSINESS};
        	for (RecordType rt :[SELECT Id, Name,DeveloperName  
			      					FROM RecordType 
			    					 WHERE SObjectType = 'Opportunity'
			       						AND DeveloperName IN : recTypename])
			{
			   if (rt.DeveloperName == TRANSFER_BUSINESS){
			   	OppRT_Map.put(rt.id,true);
			   } else{
			   	OppRT_Map.put(rt.id,false);
			   }
			   
			} 
			        	
        }

}