/**
 * This Apex Class is being used to test the AccountCreatePartnerIntegrationTasks trigger built
 * Created by: Gary Sopko
 * Created Date: 08/09/2013
 * Please consult with Sales Operations/System Administration before updating.
 */
@isTest
private class TestAccountCreatePartnerIntegrationTasks {

    static testMethod void CreateAccount() {
        Account a = new Account();
        	a.Name = 'Test Account';
        	a.Type = 'Partner';
        	a.BillingCountry = 'United States';
        	a.Region__c = 'NA';
    	insert a;
    	Partner_Integration_Task__c[] pitcheck = [Select Id from Partner_Integration_Task__c WHERE Account__c = :a.Id];
    	system.assertEquals(pitcheck.size(),9);
    }
}