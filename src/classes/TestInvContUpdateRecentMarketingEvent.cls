/**
 * This Apex Class is being used to test the InvContUpdateRecentMarketingEvent trigger built
 * Created by: Gary Sopko
 * Created Date: 04/30/2013
 * Please consult with Sales Operations/System Administration before updating.
 */
@isTest
private class TestInvContUpdateRecentMarketingEvent {

    static testMethod void TestInvContact1() {
        //Step 1:  Insert Account
        Account a = new Account();
        	a.Name = 'TestAccount';
        insert a;
        //Step 2:  Insert Contact
        Contact c = new Contact();
        	c.AccountId = a.Id;
        	c.LastName = 'Test';
        insert c;
        //Step 3: Insert Two Marketing Events
        Marketing_Operations_Request__c mor = new Marketing_Operations_Request__c();
        	mor.Name = 'TestName';
        	mor.Request_Date__c = date.Today();
        	mor.Status__c = TRUE;
        insert mor;
        Marketing_Operations_Request__c mor2 = new Marketing_Operations_Request__c();
        	mor2.Name = 'TestName';
        	mor2.Request_Date__c = date.Today();
        	mor2.Status__c = TRUE;
        insert mor2;
        //Step 4:  Insert Invited Contact for mor
        Invited_Contact__c ic = new Invited_Contact__c();
        	ic.Contact__c = c.Id;
        	ic.Marketing_Operations_Request__c = mor.Id;
        insert ic;
        String morid = ic.Marketing_Operations_Request__c;
        //Step 5:  Insert Invited Contact for mor2
        Invited_Contact__c ic2 = new Invited_Contact__c();
        	ic2.Contact__c = c.Id;
        	ic2.Marketing_Operations_Request__c = mor2.Id;
        insert ic2;

    }
}