public with sharing class MetricTriggerActions {

    public static void doAfterInsert(List<Metric__c> metrics) {
        
        Set<Id> macs = new Set<Id>();
        Map<String, Monthly_Metric__c> monthlyMetricsUpdate =  new Map<String, Monthly_Metric__c>();

        for (Metric__c metric : metrics) {
            macs.add(metric.Marin_App_Client__c);
        }
        
        List<Monthly_Metric__c> monthlyMetrics = 
            [SELECT Ext_Id__c, Facebook_Spend__c, Other_Publisher_Spend__c, Criteo_Spend__c,
                Google_Spend__c, Universal_Channels_Spend__c, Yahoo_Japan_Spend__c,
                Yahoo_Bing_Spend__c, Management_Hits__c, In_App_Reporting_Hits__c, Optimization_Hits__c,
                Other_Hits__c , Report_Creation_Hits__c, Number_of_Automated_Changes__c,
                Number_of_Changes_Synced_from_Publisher__c, Number_of_Manual_Changes__c,
                Revenue_to_marin__c, Total_Spend_in_Bidding_Folders__c, Days_with_Late_Cost_Data__c,
                Days_with_Late_Revenue_Data__c,Number_of_Linked_PCAs__c,Cost_Data_on_Time__c ,
                Revenue_Data_on_Time__c 
             FROM Monthly_Metric__c 
             WHERE Marin_App_Client__c in :macs];
        System.debug('monthlyMetrics ---'+monthlyMetrics ); 
        Map<String, Monthly_Metric__c> monthlyMetricMap = new Map<String, Monthly_Metric__c>();
        List<Monthly_Metric__c> insertList = new List<Monthly_Metric__c>();
        for (Monthly_Metric__c monthlyMetric : monthlyMetrics) {
            monthlyMetricMap.put(monthlyMetric.Ext_Id__c, monthlyMetric);
        }           
        System.debug('monthlyMetricMap---'+monthlyMetricMap);
        for (Metric__c metric : metrics) {
            String extId = metric.Marin_App_Client__c + '-' + metric.Date__c.month() + '-' + metric.Date__c.year();
            System.debug('extId ---'+extId );
            
            Monthly_Metric__c monthlyMetric = monthlyMetricMap.get(extId);
            if (monthlyMetric == null) {
                monthlyMetric = new Monthly_Metric__c(Ext_Id__c = extId, 
                    Marin_App_Client__c = metric.Marin_App_Client__c, Date__c = metric.Date__c.toStartOfMonth());
                monthlyMetricMap.put(extId, monthlyMetric);
            }
            //if(metric.Date__c != null)
            //monthlyMetric.Date__c = metric.Date__c;
            System.debug('monthlyMetric ---'+monthlyMetric );
            if(metric.Cost_Data_on_Time__c  == false){
                monthlyMetric.Days_with_Late_Cost_Data__c = MetricTriggerActions.add(monthlyMetric.Days_with_Late_Cost_Data__c, 1);
                monthlyMetric.Cost_Data_on_Time__c = true;
            }
            if(metric.Revenue_Data_on_Time__c  == false){
                monthlyMetric.Days_with_Late_Revenue_Data__c = MetricTriggerActions.add(monthlyMetric.Days_with_Late_Revenue_Data__c, 1);
                monthlyMetric.Revenue_Data_on_Time__c =  true;
            }
    
            monthlyMetric.Number_of_Linked_PCAs__c = MetricTriggerActions.add(monthlyMetric.Number_of_Linked_PCAs__c , metric.Number_of_Linked_PCAs__c);
            monthlyMetric.Total_Spend__c = MetricTriggerActions.add(monthlyMetric.Total_Spend__c, metric.Total_Spend__c);
            monthlyMetric.Facebook_Spend__c = MetricTriggerActions.add(monthlyMetric.Facebook_Spend__c, metric.Facebook_Spend__c);
            monthlyMetric.Other_Publisher_Spend__c = MetricTriggerActions.add(monthlyMetric.Other_Publisher_Spend__c, metric.Other_Publisher_Spend__c);
            monthlyMetric.Criteo_Spend__c = MetricTriggerActions.add(monthlyMetric.Criteo_Spend__c, metric.Criteo_Spend__c);
            monthlyMetric.Google_Spend__c = MetricTriggerActions.add(monthlyMetric.Google_Spend__c, metric.Google_Spend__c);
            monthlyMetric.Universal_Channels_Spend__c = MetricTriggerActions.add(monthlyMetric.Universal_Channels_Spend__c, metric.Universal_Channels_Spend__c);
            monthlyMetric.Yahoo_Japan_Spend__c = MetricTriggerActions.add(monthlyMetric.Yahoo_Japan_Spend__c, metric.Yahoo_Japan_Spend__c);
            monthlyMetric.Yahoo_Bing_Spend__c = MetricTriggerActions.add(monthlyMetric.Yahoo_Bing_Spend__c, metric.Yahoo_Bing_Spend__c);
            monthlyMetric.Management_Hits__c = MetricTriggerActions.add(monthlyMetric.Management_Hits__c, metric.Management_Hits__c);
            monthlyMetric.In_App_Reporting_Hits__c = MetricTriggerActions.add(monthlyMetric.In_App_Reporting_Hits__c, metric.In_App_Reporting_Hits__c);
            monthlyMetric.Optimization_Hits__c = MetricTriggerActions.add(monthlyMetric.Optimization_Hits__c, metric.Optimization_Hits__c);
            monthlyMetric.Other_Hits__c = MetricTriggerActions.add(monthlyMetric.Other_Hits__c, metric.Other_Hits__c);
            monthlyMetric.Number_of_Automated_Changes__c = MetricTriggerActions.add(monthlyMetric.Number_of_Automated_Changes__c, metric.Number_of_Automated_Changes__c);
            monthlyMetric.Number_of_Changes_Synced_from_Publisher__c = MetricTriggerActions.add(monthlyMetric.Number_of_Changes_Synced_from_Publisher__c, metric.Number_of_Changes_Synced_from_Publisher__c);
            monthlyMetric.Number_of_Manual_Changes__c = MetricTriggerActions.add(monthlyMetric.Number_of_Manual_Changes__c, metric.Number_of_Manual_Changes__c);
            monthlyMetric.Total_Spend_in_Bidding_Folders__c = MetricTriggerActions.add(monthlyMetric.Total_Spend_in_Bidding_Folders__c, metric.Total_Spend_in_Bidding_Folders__c);
            //This value is MTD
            monthlyMetric.Revenue_to_marin__c = metric.Revenue_to_marin__c;    //MetricTriggerActions.add(monthlyMetric.Revenue_to_marin__c, metric.Revenue_to_marin__c);
            
            monthlyMetricsUpdate.put(extId, monthlyMetric);
        }

        System.debug('monthlyMetrics ---'+monthlyMetrics );
        if(monthlyMetricsUpdate.size() > 0) {
            Database.upsert(monthlyMetricsUpdate.values(), Monthly_Metric__c.Fields.Ext_Id__c, true);
        }
    }
    
    public static Decimal add(Decimal value1, Decimal value2) {
        value1 = (value1 == null) ? 0 : value1;
        value2 = (value2 == null) ? 0 : value2;
        return value1 + value2;
    }
}