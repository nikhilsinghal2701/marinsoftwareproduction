@isTest
private class Web2CaseControllerTest {

    static testMethod void validateNoEmailWasSupplied() {
        Web2CaseController controller = new Web2CaseController();
        
        PageReference savePage = controller.createCase();
        System.assertEquals(null, savePage);
        System.assertEquals(true, controller.noEmailSupplied);
    }
    
    static testMethod void validateNoProblemAreaWasSupplied() {
        ApexPages.currentPage().getParameters().put('e', 'test@test.com');
        
        Web2CaseController controller = new Web2CaseController();
        
        PageReference savePage = controller.createCase();
        System.assertEquals(null, savePage);
        System.assertEquals(true, controller.noProblemAreaSupplied);
    }
    
    static testMethod void validateNoIssueWasSupplied() {
        ApexPages.currentPage().getParameters().put('e', 'test@test.com');
        
        Web2CaseController controller = new Web2CaseController();
        controller.webCase.Case_Reason__c = 'Test Reason';
        
        PageReference savePage = controller.createCase();
        System.assertEquals(null, savePage);
        System.assertEquals(true, controller.noIssueSupplied);
    }
    
    static testMethod void validateNoDescriptionWasSupplied() {
        ApexPages.currentPage().getParameters().put('e', 'test@test.com');
        
        Web2CaseController controller = new Web2CaseController();
        controller.webCase.Case_Reason__c = 'Test Reason';
        controller.webCase.Case_Reason_Detail__c = 'Test Reason Detail';
        controller.webCase.Pritority__c = 'Test Priority';
        
        PageReference savePage = controller.createCase();
        System.assertEquals(null, savePage);
        System.assertEquals(true, controller.noDescriptionSupplied);
    }
    
    static testMethod void validateCaseCreatedNoAttachmentOrCC() {
        ApexPages.currentPage().getParameters().put('n', 'Testy Testerson');
        ApexPages.currentPage().getParameters().put('e', 'test@test.com');
        
        Web2CaseController controller = new Web2CaseController();
        controller.webCase.Case_Reason__c = 'Test Reason';
        controller.webCase.Case_Reason_Detail__c = 'Test Reason Detail';
        controller.webCase.Pritority__c = 'Test Priority';
        controller.webCase.Description = 'Test Description';
        
        PageReference savePage = controller.createCase();
        //System.assertNotEquals(null, savePage);
        
        List<Case> newCase = 
            [SELECT Id, Case_Reason__c, Case_Reason_Detail__c, Pritority__c, Description,
                    SuppliedEmail, SuppliedName, Origin, CC_1__c, CC_2__c, CC_3__c,
                    CC_4__c, CC_5__c 
             FROM Case
             WHERE Id = :controller.webCase.Id
             LIMIT 1000];
             
        if (newCase.size() > 0) {
            System.assertEquals('Test Reason', newCase[0].Case_Reason__c);
            System.assertEquals('Test Reason Detail', newCase[0].Case_Reason_Detail__c);
            System.assertEquals('Test Priority', newCase[0].Pritority__c);
            System.assertEquals('Test Description', newCase[0].Description);
            System.assertEquals('test@test.com', newCase[0].SuppliedEmail);
            System.assertEquals('Testy Testerson', newCase[0].SuppliedName);
            System.assertEquals('Web to Case', newCase[0].Origin);
            System.assertEquals(null, newCase[0].CC_1__c);
            System.assertEquals(null, newCase[0].CC_2__c);
            System.assertEquals(null, newCase[0].CC_3__c);
            System.assertEquals(null, newCase[0].CC_4__c);
            System.assertEquals(null, newCase[0].CC_5__c);
        }
    }
    
    static testMethod void validateCaseCreatedWithAttachmentAnd5CCs() {
        ApexPages.currentPage().getParameters().put('n', 'Testy Testerson');
        ApexPages.currentPage().getParameters().put('e', 'test@test.com');
        
        Web2CaseController controller = new Web2CaseController();
        controller.webCase.Case_Reason__c = 'Test Reason';
        controller.webCase.Case_Reason_Detail__c = 'Test Reason Detail';
        controller.webCase.Pritority__c = 'Test Priority';
        controller.webCase.Description = 'Test Description';
        controller.ccList = 'cc_user1@test.com,cc_user2@test.com,cc_user3@test.com,cc_user4@test.com,cc_user5@test.com';
        controller.attachment.Name = 'Test File.txt';
        controller.attachment.Body = Blob.valueOf('Testing text');
        
        PageReference savePage = controller.createCase();
        
        List<Case> newCase = 
            [SELECT Id, CC_1__c, CC_2__c, CC_3__c, CC_4__c, CC_5__c
             FROM Case
             WHERE Id = :controller.webCase.Id];
        if (newCase.size() > 0) {
            Attachment[] attachments = 
                [SELECT Id, Name, Body
                 FROM Attachment
                 WHERE ParentId = :controller.webCase.Id
                 LIMIT 1000];
                  
            System.assertEquals('cc_user1@test.com', newCase[0].CC_1__c);
            System.assertEquals('cc_user2@test.com', newCase[0].CC_2__c);
            System.assertEquals('cc_user3@test.com', newCase[0].CC_3__c);
            System.assertEquals('cc_user4@test.com', newCase[0].CC_4__c);
            System.assertEquals('cc_user5@test.com', newCase[0].CC_5__c);
            System.assertNotEquals(null, attachments);
            System.assertEquals(1, attachments.size());
            System.assertEquals('Test File.txt', attachments[0].Name);
            System.assertEquals('Testing text', attachments[0].Body.toString());
        }
    }
    
    static testMethod void validateCaseCreatedWithPhone() {
        ApexPages.currentPage().getParameters().put('n', 'Testy Testerson');
        ApexPages.currentPage().getParameters().put('e', 'test@test.com');
        
        Web2CaseController controller = new Web2CaseController();
        controller.webCase.Case_Reason__c = 'Test Reason';
        controller.webCase.Case_Reason_Detail__c = 'Test Reason Detail';
        controller.webCase.Pritority__c = 'Test Priority';
        controller.webCase.Description = 'Test Description';
        controller.webCase.Client_Phone__c = '123-456-7890';

        
        PageReference savePage = controller.createCase();
        
        List<Case> newCase = 
            [SELECT Id, Case_Reason__c, Case_Reason_Detail__c, Pritority__c, Description,
                    SuppliedEmail, SuppliedName, Origin, CC_1__c, CC_2__c, CC_3__c,
                    CC_4__c, CC_5__c 
             FROM Case
             WHERE Id = :controller.webCase.Id];
        if (newCase.size() > 0) {
            System.assertEquals('Test Reason', newCase[0].Case_Reason__c);
            System.assertEquals('Test Reason Detail', newCase[0].Case_Reason_Detail__c);
            System.assertEquals('Test Priority', newCase[0].Pritority__c);
            System.assertEquals(controller.webCase.Description, newCase[0].Description);
            System.assertEquals('test@test.com', newCase[0].SuppliedEmail);
            System.assertEquals('Testy Testerson', newCase[0].SuppliedName);
            System.assertEquals('Web to Case', newCase[0].Origin);
            System.assertEquals(null, newCase[0].CC_1__c);
            System.assertEquals(null, newCase[0].CC_2__c);
            System.assertEquals(null, newCase[0].CC_3__c);
            System.assertEquals(null, newCase[0].CC_4__c);
            System.assertEquals(null, newCase[0].CC_5__c);
        }
    }
    
    static testMethod void validateCaseCreatedWithTracking() {
        ApexPages.currentPage().getParameters().put('n', 'Testy Testerson');
        ApexPages.currentPage().getParameters().put('e', 'test@test.com');
        
        Web2CaseController controller = new Web2CaseController();
        controller.webCase.Case_Reason__c = 'Test Reason';
        controller.webCase.Case_Reason_Detail__c = 'Test Reason Detail';
        controller.webCase.Pritority__c = 'Test Priority';
        controller.webCase.Description = 'Test Description';
        controller.webCase.Client_Phone__c = '123-456-7890';
        controller.trackingList = '{"reason":"Account Setup","detail":"","subject":"","title":"Getting Started: A Guide to Linking Publisher Accounts","clicked":false,"url":"https://marketinginsights.zendesk.com/entries/20027223?locale=1&utm_source=marinsoftware.force.com/web2case&utm_medium=referral","group":1,"existing":false}#@{"reason":"Account Setup","detail":"","subject":"","title":"Getting Started: Managing Passwords and Users","clicked":false,"url":"https://marketinginsights.zendesk.com/entries/20027023?locale=1&utm_source=marinsoftware.force.com/web2case&utm_medium=referral","group":1,"existing":false}';
        
        PageReference savePage = controller.createCase();
        
        List<Case> newCase = 
            [SELECT Id, Case_Reason__c, Case_Reason_Detail__c, Pritority__c, Description,
                    SuppliedEmail, SuppliedName, Origin, CC_1__c, CC_2__c, CC_3__c,
                    CC_4__c, CC_5__c 
             FROM Case
             WHERE Id = :controller.webCase.Id];
        if (newCase.size() > 0) {
            System.assertEquals('Test Reason', newCase[0].Case_Reason__c);
            System.assertEquals('Test Reason Detail', newCase[0].Case_Reason_Detail__c);
            System.assertEquals('Test Priority', newCase[0].Pritority__c);
            System.assertEquals(controller.webCase.Description, newCase[0].Description);
            System.assertEquals('test@test.com', newCase[0].SuppliedEmail);
            System.assertEquals('Testy Testerson', newCase[0].SuppliedName);
            System.assertEquals('Web to Case', newCase[0].Origin);
            System.assertEquals(null, newCase[0].CC_1__c);
            System.assertEquals(null, newCase[0].CC_2__c);
            System.assertEquals(null, newCase[0].CC_3__c);
            System.assertEquals(null, newCase[0].CC_4__c);
            System.assertEquals(null, newCase[0].CC_5__c);
        }
    }
    
    static testMethod void validateCaseCreatedWithBadEmail() {
        ApexPages.currentPage().getParameters().put('n', 'Testy Testerson');
        ApexPages.currentPage().getParameters().put('e', 'thisShouldBeAnEmail');
        
        Web2CaseController controller = new Web2CaseController();
        controller.webCase.Case_Reason__c = 'Test Reason';
        controller.webCase.Case_Reason_Detail__c = 'Test Reason Detail';
        controller.webCase.Pritority__c = 'Test Priority';
        controller.webCase.Description = 'Test Description';
        
        PageReference savePage = controller.createCase();
    }
    
}