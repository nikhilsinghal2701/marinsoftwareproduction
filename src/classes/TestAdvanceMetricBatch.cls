@isTest
private class TestAdvanceMetricBatch {

    static testMethod void createAdvanceMetricForAcc() {
		Account acc1 = TestDataFactory.createAccount(1, true, new Map<String, Object>{}).get(0);
        Account acc2 = TestDataFactory.createAccount(1, true, 
        	new Map<String, Object>{'Name' => 'Test Account2','ParentId' => acc1.Id,
        	'Industry' => 'Auto', 'Business_Model__c' => 'Agency',
        	'Type' =>'Agency'}).get(0);

        Marin_App_Client__c mac = TestDataFactory.createMarinAppClient(1, true, 
        	new Map<String, Object>{'Account__c' => acc2.Id}).get(0);

        Date dt = System.today();
        Date closeDt = dt.addDays(-4);
        Date yesterday = dt.addDays(-1);
        
        Metric__c metric1  =  TestDataFactory.createMetric(1, true, 
        	new Map<String, Object>{'Date__c' => closeDt, 'Marin_App_Client__c' => mac.Id}).get(0);
        
        //Date dt = System.today();
        Date monthDT = yesterday.addMonths(-2);
        //Date yesterday = dt.addDays(-1);
        Metric__c metric4  =  TestDataFactory.createMetric(1, true, 
        	new Map<String, Object>{'Date__c' => monthDt, 'Marin_App_Client__c' => mac.Id}).get(0);
        	
        Date month1DT = yesterday.addMonths(-1);
        
        Metric__c metric5 = TestDataFactory.createMetric(1, true, 
        	new Map<String, Object>{'Date__c' => month1DT, 'Marin_App_Client__c' => mac.Id}).get(0);

        Date yearDt = yesterday.addMonths(-11);
        
        Metric__c metric6  =   TestDataFactory.createMetric(1, true, 
        	new Map<String, Object>{'Date__c' => yearDt, 'Marin_App_Client__c' => mac.Id}).get(0);
        
        Marin_App_Client__c mac1 = TestDataFactory.createMarinAppClient(1, true, 
        	new Map<String, Object>{'Account__c' => acc2.Id}).get(0);
        
        Metric__c metric2  =  TestDataFactory.createMetric(1, true, 
        	new Map<String, Object>{'Date__c' => closeDt, 'Marin_App_Client__c' => mac1.Id}).get(0);
        
        Metric__c metric3  =  TestDataFactory.createMetric(1, true, 
        	new Map<String, Object>{'Date__c' => closeDt, 'Marin_App_Client__c' => mac1.Id}).get(0);
        
        List<Id> accountIdsList = new List<Id>();
        accountIdsList.add(acc2.Id);
        
        Test.startTest();
            SchedulableContext sc;
            AdvanceMetricBatch batch = new AdvanceMetricBatch();
            batch.testAccId = accountIdsList;
            AdvanceMetricBatchScheduler obj = new AdvanceMetricBatchScheduler();
            obj.testBatch = batch;
            obj.execute(sc);
            
        Test.stopTest();
        
        AdvanceMetricsUtil objUtils = new AdvanceMetricsUtil();
        Map<Id,Advance_Metric__c> advanceMap = new Map<Id,Advance_Metric__c>();
        objUtils.totalSpendQueryWeekly(accountIdsList,advanceMap);
        advanceMap = new Map<Id,Advance_Metric__c>();
        objUtils.daysLateRevenueQueryWeekly(accountIdsList,advanceMap);
        advanceMap = new Map<Id,Advance_Metric__c>();
        objUtils.daysCostDataQueryYearly(accountIdsList,advanceMap);
        advanceMap = new Map<Id,Advance_Metric__c>();
        objUtils.daysCostDataQueryMonthly(accountIdsList,advanceMap);
        advanceMap = new Map<Id,Advance_Metric__c>();
        objUtils.daysLateRevenueQueryMonthly(accountIdsList,advanceMap);
        advanceMap = new Map<Id,Advance_Metric__c>();
        objUtils.daysLateRevenueQueryYearly(accountIdsList,advanceMap);
        Map<Id,Advance_Metric__c> accountMap = new Map<Id,Advance_Metric__c>();
        Advance_Metric__c advance = new Advance_Metric__c();
        accountMap.put(acc2.Id,advance);
        objUtils.estimatedRevenue(accountIdsList,accountMap);
        objUtils.daysCostDataQueryWeekly(accountIdsList,accountMap);
        objUtils.daysLateRevenueQueryWeekly(accountIdsList,accountMap);
        
            
    }
    static testMethod void createAdvanceMetricsUtil() {
        Account acc1 = TestDataFactory.createAccount(1, true, new Map<String, Object>{}).get(0);
        Account acc2 = TestDataFactory.createAccount(1, true, 
        	new Map<String, Object>{'Name' => 'Test Account2','ParentId' => acc1.Id,
        	'Industry' => 'Auto', 'Business_Model__c' => 'Agency',
        	'Type' =>'Agency'}).get(0);
        
        Marin_App_Client__c mac = TestDataFactory.createMarinAppClient(1, true, 
        	new Map<String, Object>{'Account__c' => acc2.Id}).get(0);
        	
        Date dt = System.today();        
        Date yesterday = dt.addDays(-1);
        Date closeDt = yesterday.addMonths(-2);
        
        Monthly_Metric__c metric1  = TestDataFactory.createMonthlyMetric(1, false,
        	new Map<String, Object>{'Marin_App_Client__c' => mac.Id,
        		'Date__c' => closeDt}).get(0);
 
       Date closeYrDt = yesterday.addMonths(-12);
        Monthly_Metric__c metric2  =  TestDataFactory.createMonthlyMetric(1, false,
        	new Map<String, Object>{'Marin_App_Client__c' => mac.Id,
        		'Date__c' => closeYrDt}).get(0);
        		
        Metric__c metric3  =  TestDataFactory.createMetric(1, true, 
        	new Map<String, Object>{'Date__c' => yesterday, 
        		'Marin_App_Client__c' => mac.Id}).get(0);
        
        
        List<Id> accountIdsList = new List<Id>();
        accountIdsList.add(acc2.Id);
        Map<Id,Advance_Metric__c> advanceMap = new Map<Id,Advance_Metric__c>();
        AdvanceMetricsUtil util = new AdvanceMetricsUtil();
        util.daysCostDataQueryMonthly(accountIdsList, advanceMap);
        util.daysCostDataQueryYearly(accountIdsList, advanceMap);
        util.daysLateRevenueQueryMonthly(accountIdsList, advanceMap);
        util.daysLateRevenueQueryYearly(accountIdsList, advanceMap);
        util.estimatedRevenue(accountIdsList, advanceMap);
    }
    
    static testMethod void createAccountMetrics() {
        Account acc1 = TestDataFactory.createAccount(1, true, new Map<String, Object>{}).get(0);
        Account acc2 = TestDataFactory.createAccount(1, true, 
        	new Map<String, Object>{'Name' => 'Test Account2','ParentId' => acc1.Id,
        	'Industry' => 'Auto', 'Business_Model__c' => 'Agency',
        	'Type' =>'Agency'}).get(0);

        Marin_App_Client__c mac = TestDataFactory.createMarinAppClient(1, true, 
        	new Map<String, Object>{'Account__c' => acc2.Id}).get(0);

        Date dt = System.today();
        Date closeDt = dt.addDays(-4);
        Date yesterday = dt.addDays(-1);

       Monthly_Metric__c metric1  = TestDataFactory.createMonthlyMetric(1, false,
        	new Map<String, Object>{'Marin_App_Client__c' => mac.Id,
        		'Date__c' => closeDt}).get(0);
 
        Marin_App_Client__c mac1 = TestDataFactory.createMarinAppClient(1, true, 
        	new Map<String, Object>{'Account__c' => acc2.Id}).get(0);
        
        Metric__c metric2  =  TestDataFactory.createMetric(1, true, 
        	new Map<String, Object>{'Date__c' => closeDt, 'Marin_App_Client__c' => mac.Id}).get(0);
        
        Metric__c metric3  =  TestDataFactory.createMetric(1, true, 
        	new Map<String, Object>{'Date__c' => yesterday, 'Marin_App_Client__c' => mac.Id}).get(0);

        Advance_Metric__c advance = new Advance_Metric__c();
        
        advance = getAdvanceMetric(acc2.id);
        insert advance;
        ApexPages.StandardController controller = new ApexPages.StandardController(acc2);
        AccountMetrics objAct = new AccountMetrics(controller);
        objAct.getChangeCategory();
        objAct.getHitsCategory();
        objAct.getSpendData();
        objAct.getTotalSpend_395Days();
        objAct.getTotalSpend_90Days();
        objAct.getMonthlyChangeCategory();
        objAct.getmonthlyHitsCategory();
        objAct.getSpendDataMonthly();
        
    }
    
    public static Advance_Metric__c  getAdvanceMetric(Id accountId) {
        Advance_Metric__c a = new Advance_Metric__c(Account__c = accountId);
        a.of_PCAs_net_change_diff_btw_Day7_Day1__c =  10 ;
        a.MACs_with_Late_Cost_Data_M__c = 10;
        a.of_PCAs_linked_as_of_previous_day__c =  10;
        a.of_PCAs_Expired_as_of_previous_day_7__c = 10;
        a.Yahoo_Japan_Spend_Y__c =10;
        a.Yahoo_Japan_Spend_W__c = 10;
        a.Yahoo_Japan_Spend_M__c = 10;
        a.Yahoo_Bing_Spend_Y__c = 10;
        a.Yahoo_Bing_Spend_W__c =10;
        a.Yahoo_Bing_Spend_M__c = 10;
        a.Universal_Channels_Spend_Y__c = 10;
        a.Universal_Channels_Spend_W__c = 10; 
        a.Universal_Channels_Spend_M__c =10;
        a.Total_Spend_in_Bidding_Folder_Y__c =10;
        a.Total_Spend_in_Bidding_Folder_M__c =10;
        a.Total_Spend_in_Bidding_Folder_W__c =10;
        a.Total_Spend_Y__c = 10;
        a.Total_Spend_M__c =10;
        a.Total_Spend_W__c =10; 
        a.Spend_Per_Publisher_Presented_in_Graph_Y__c =10; 
        a.Spend_Per_Publisher_Presented_in_Graph_M__c = 10;
        a.Revenue_to_marin_Y__c = 10;
        a.Revenue_to_marin_M__c =10;
        a.Revenue_to_Marin__c = 10; 
        a.Report_Creation_Hits_Y__c = 10;
        a.Report_Creation_Hits_W__c = 10;
        a.Report_Creation_Hits_M__c = 10;
        a.Other_Publisher_Spend_W__c = 10;
        a.Other_Publisher_Spend_Y__c = 10;
        a.Other_Publisher_Spend_W__c = 10;
        a.Other_Publisher_Spend_M__c = 10;
        a.Other_Hits_Y__c =10;
        a.Other_Hits_W__c =10; 
        a.Other_Hits_M__c = 10;
        a.of_PCAs_net_change_diff_btw_Day7_Day1_Y__c = 10; 
        a.Optimization_Hits_Y__c = 10; 
        a.Optimization_Hits_W__c = 10;
        a.Optimization_Hits_M__c = 10; 
        a.Of_Unique_User_Logins_as_of_previous_Y__c = 10;
        a.Of_Unique_User_Logins_as_of_previous_7__c = 10;
        a.Number_of_Manual_Changes_Y__c = 10;
        a.Number_of_Manual_Changes_W__c =10;
        a.Number_of_Manual_Changes_M__c = 10;
        a.Number_of_Changes_Synced_from_PublisherY__c = 10;
        a.Number_of_Changes_Synced_from_PublisherW__c = 10;
        a.Number_of_Changes_Synced_from_PublisherM__c = 10;
        a.Number_of_Automated_Changes_Y__c = 10;
        a.Number_of_Automated_Changes_W__c = 10;
        a.Number_of_Automated_Changes_M__c = 10;
        a.Management_Hits_Y__c = 10; 
        a.Management_Hits_W__c = 10;
        a.Management_Hits_M__c = 10;
        a.MACs_with_Late_Revenue_Data_Y__c = 10;
        a.MACs_with_Late_Revenue_Data_M__c =10;
        a.MACs_with_Late_Revenue_Data_W__c = 10;
        a.MACs_with_Late_Cost_Data_Y__c = 10;
        a.MACs_with_Late_Cost_Data_W__c =10;
        a.In_App_Reporting_Hits_Y__c = 10;
        a.In_App_Reporting_Hits_W__c = 10;
        a.In_App_Reporting_Hits_M__c = 10;
        a.Google_Spend_M__c = 10;
        a.Google_Spend_W__c =10; 
        a.Google_Spend_Y__c = 10;
        a.Facebook_Spend_W__c =10;
        a.Facebook_Spend_Y__c = 10;
        a.Facebook_Spend_M__c= 10;
        a.Days_with_Late_Revenue_Data_Y__c = 10;
        a.Days_with_Late_Revenue_Data_M__c = 10;
        a.Days_with_Late_Revenue_Data_W__c = 10;
        a.Days_with_Late_Cost_Data_Y__c = 10;
        a.Days_with_Late_Cost_Data_M__c =10;
        a.Days_with_Late_Cost_Data_W__c = 10;
        a.Criteo_Spend_Y__c = 10;
        a.Criteo_Spend_W__c = 10;
        a.Criteo_Spend_M__c = 10;     
        a.of_PCAs_net_change_diff_btw_Day7_Day1_M__c = 10;   
        return a;
    }     
}