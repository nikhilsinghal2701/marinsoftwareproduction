public with sharing class accountPlan360 {

    public final string acctID {get; private set;}

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public accountPlan360(ApexPages.StandardController controller) {
        SFDC_Acct_Plan__c acctPlan;

        if (ApexPages.currentpage().getparameters().get('aid') != null) {
            acctID = ApexPages.currentpage().getparameters().get('aid');
        } else {
            controller.addFields(new list<string>{'Account__c'});
            acctPlan = (SFDC_Acct_Plan__c) controller.getRecord();
            acctID = acctPlan.Account__c;
        }
    }

}