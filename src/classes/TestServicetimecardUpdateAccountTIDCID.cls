/**
 * This Apex Class is being used to test the ServiceTimecardUpdateAccountTIDCID trigger built
 * Created by: Gary Sopko
 * Created Date: 06/07/2013
 * Please consult with Sales Operations/System Administration before updating.
 */
@isTest
private class TestServicetimecardUpdateAccountTIDCID {

    static testMethod void ProperTest() {
        //Create two Accounts - 1 with MAC records and one without
        Account a = new Account();
	        a.Name = 'Some Company';
	        a.Type = 'Advertiser';
	        a.Account_Status__c = 'Active Customer';
	        a.Region__c = 'NA';
	        a.BillingCountry = 'United States';
        insert a;
        //Create two Marin App Client
        Marin_App_Client__c mac1 = new Marin_App_Client__c();
        Marin_App_Client__c mac2 = new Marin_App_Client__c();
        	mac1.Account__c = a.Id;
        	mac1.Client_ID__c = 415;
        	mac1.Customer_ID__c = 100;
        	mac1.Client_Status__c = 'Active';
        	mac2.Account__c = a.Id;
        	mac2.Client_ID__c = 650;
        	mac2.Customer_ID__c = 100;
        	mac2.Client_Status__c = 'Active';
        insert mac1;
        insert mac2;
        //Create a Case
        Case c = new Case();
        	c.Reason = 'Enhanced Campaigns';
        	c.Status = 'New';
        	c.AccountId = a.Id;
        	c.Client_ID__c = 415;
        	c.Subject = 'Why do I have to enter this?';
        	c.Description = 'Commas in code... commas in code';
        insert c;
        //Create two Service Timecards
        SFDC_Service_Timecard__c tc1 = new SFDC_Service_Timecard__c();
        SFDC_Service_Timecard__c tc2 = new SFDC_Service_Timecard__c();
        	tc1.Date_of_Service__c = date.Today();
        	tc1.User__c = UserInfo.getUserId();
        	tc1.Case__c = c.Id;
        	tc1.Hours_Worked__c = 3;
        	tc1.Marin_Client_ID__c = 415;
        	tc1.Service_Performed__c = 'Internal';
        	tc1.Services_Sub_Type__c = 'Professional Services Activities';
        	tc2.Date_of_Service__c = date.Today();
        	tc2.User__c = UserInfo.getUserId();
        	tc2.Case__c = c.Id;
        	tc2.Account__c = a.Id;
        	tc2.Service_Performed__c = 'Internal';
        	tc2.Services_Sub_Type__c = 'Client Services Activities';
        	tc2.Case__c = c.Id;
        insert tc1;
        insert tc2;
        //Now some system.assert some shit!

    }
    static testMethod void exceptionTest(){
    	Account a = new Account();
    	Account a2 = new Account();
    		a.Name = 'Test Shit';
    		a.Type = 'Advertiser';
	        a.Account_Status__c = 'Active Customer';
	        a.Region__c = 'NA';
	        a.BillingCountry = 'United States';
	        a2.Name = 'Who Cares';
	        a2.Type = 'Advertiser';
	        a2.Account_Status__c = 'Prospect';
	        a2.Region__c = 'NA';
	        a2.BillingCountry = 'Canada';
	    insert a;
	    insert a2;
    	Case c = new Case();
        	c.Reason = 'Enhanced Campaigns';
        	c.Status = 'New';
        	c.Client_ID__c = 415;
        	c.Subject = 'Why do I have to enter this?';
        	c.Description = 'Commas in code... commas in code';
        insert c;
        //Throw two types of exception Service Timecards
        SFDC_Service_Timecard__c tc3 = new SFDC_Service_Timecard__c();
        SFDC_Service_Timecard__c tc4 = new SFDC_Service_Timecard__c();
        	tc3.Date_of_Service__c = date.Today();
        	tc3.User__c = UserInfo.getUserId();
        	tc3.Case__c = c.Id;
        	tc3.Hours_Worked__c = 3;
        	tc3.Account__c = a.Id;
        	tc3.Marin_Client_ID__c = 960;
        	tc3.Service_Performed__c = 'Internal';
        	tc3.Services_Sub_Type__c = 'Professional Services Activities';
        	tc4.Date_of_Service__c = date.Today();
        	tc4.User__c = UserInfo.getUserId();
        	tc4.Account__c = a2.Id;
        	tc4.Service_Performed__c = 'Internal';
        	tc4.Services_Sub_Type__c = 'Client Services Activities';
        	tc4.Case__c = c.Id;
        try{
	        insert tc3;
	        insert tc4;
        } catch(Exception e){
        	system.debug(e);
        }
    }
}