public class PostFeedItemAccount{
	@future (callout=true)
	public static void PostFeedItem(String rid, String hash, String uid, String sessionid, String posttext){
        String salesforceHost = System.Url.getSalesforceBaseURL().toExternalForm();
        String RecordId = rid;
        String UserId = uid;
        String ChatterPostText = posttext;
        String Hashtag = hash;
        
        String url =  salesforceHost + '/services/data/v28.0/chatter/feeds/record/' + RecordId + '/feed-items';
        
        HttpRequest req = new HttpRequest();
        
        req.setMethod('POST');
        req.setEndpoint(url);
        req.setHeader('Content-type', 'application/json');
        req.setHeader('Authorization', 'OAuth ' + sessionid);
        req.setBody('{ "body" : { "messageSegments" : [ { "type": "mention", "id" : "' + UserId + '" }, { "type": "text",  "text" : "' + ' ' + ChatterPostText +  '" }, { "type": "Hashtag", "tag" : "' + Hashtag + '"}]}}');
        Http http = new Http();
           
        HTTPResponse res = http.send(req);   
    }
    @future (callout=true)
	public static void PostFeedItemFourMention(String rid, String hash, String uid, String uid2, String uid3, String uid4, String sessionid, String posttext){
        String salesforceHost = System.Url.getSalesforceBaseURL().toExternalForm();
        String RecordId = rid;
        String UserId = uid;
        String UserId2 = uid2;
        String UserId3 = uid3;
        String UserId4 = uid4;
        String ChatterPostText = posttext;
        String Hashtag = hash;
        
        String url =  salesforceHost + '/services/data/v28.0/chatter/feeds/record/' + RecordId + '/feed-items';
        
        HttpRequest req = new HttpRequest();
        
        req.setMethod('POST');
        req.setEndpoint(url);
        req.setHeader('Content-type', 'application/json');
        req.setHeader('Authorization', 'OAuth ' + sessionid);
        req.setBody('{ "body" : { "messageSegments" : [ { "type": "mention", "id" : "' + UserId + '" }, { "type": "mention", "id" : "' + UserId2 + '" }, { "type": "mention", "id" : "' + UserId3 + '" },{ "type": "mention", "id" : "' + UserId4 + '" },{ "type": "text",  "text" : "' + ' ' + ChatterPostText +  '" }, { "type": "Hashtag", "tag" : "' + Hashtag + '"}]}}');
        Http http = new Http();
           
        HTTPResponse res = http.send(req);   
    } 
    @future (callout=true)
	public static void PostFeedItemFiveMention(String rid, String hash, String uid, String uid2, String uid3, String uid4, String uid5, String sessionid, String posttext){
        String salesforceHost = System.Url.getSalesforceBaseURL().toExternalForm();
        String RecordId = rid;
        String UserId = uid;
        String UserId2 = uid2;
        String UserId3 = uid3;
        String UserId4 = uid4;
        String UserId5 = uid5;
        String ChatterPostText = posttext;
        String Hashtag = hash;
        
        String url =  salesforceHost + '/services/data/v28.0/chatter/feeds/record/' + RecordId + '/feed-items';
        
        HttpRequest req = new HttpRequest();
        
        req.setMethod('POST');
        req.setEndpoint(url);
        req.setHeader('Content-type', 'application/json');
        req.setHeader('Authorization', 'OAuth ' + sessionid);
        req.setBody('{ "body" : { "messageSegments" : [ { "type": "mention", "id" : "' + UserId + '" }, { "type": "mention", "id" : "' + UserId2 + '" }, { "type": "mention", "id" : "' + UserId3 + '" },{ "type": "mention", "id" : "' + UserId4 + '" },{ "type": "mention", "id" : "' + UserId5 + '" },{ "type": "text",  "text" : "' + ' ' + ChatterPostText +  '" }, { "type": "Hashtag", "tag" : "' + Hashtag + '"}]}}');
        Http http = new Http();
           
        HTTPResponse res = http.send(req);   
    }
    @future (callout=true)
	public static void PostFeedItemSixMention(String rid, String hash, String uid, String uid2, String uid3, String uid4, String uid5, String uid6, String sessionid, String posttext){
        String salesforceHost = System.Url.getSalesforceBaseURL().toExternalForm();
        String RecordId = rid;
        String UserId = uid;
        String UserId2 = uid2;
        String UserId3 = uid3;
        String UserId4 = uid4;
        String UserId5 = uid5;
        String UserId6 = uid6;
        String ChatterPostText = posttext;
        String Hashtag = hash;
        
        String url =  salesforceHost + '/services/data/v28.0/chatter/feeds/record/' + RecordId + '/feed-items';
        
        HttpRequest req = new HttpRequest();
        
        req.setMethod('POST');
        req.setEndpoint(url);
        req.setHeader('Content-type', 'application/json');
        req.setHeader('Authorization', 'OAuth ' + sessionid);
        req.setBody('{ "body" : { "messageSegments" : [ { "type": "mention", "id" : "' + UserId + '" }, { "type": "mention", "id" : "' + UserId2 + '" }, { "type": "mention", "id" : "' + UserId3 + '" },{ "type": "mention", "id" : "' + UserId4 + '" },{ "type": "mention", "id" : "' + UserId5 + '" },{ "type": "mention", "id" : "' + UserId6 + '" },{ "type": "text",  "text" : "' + ' ' + ChatterPostText +  '" }, { "type": "Hashtag", "tag" : "' + Hashtag + '"}]}}');
        Http http = new Http();
           
        HTTPResponse res = http.send(req);   
    }   
}