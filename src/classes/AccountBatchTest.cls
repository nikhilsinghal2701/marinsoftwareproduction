/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AccountBatchTest {

    static testMethod void myUnitTest() {
        
        //Account Record type
         Map<String,Schema.RecordTypeInfo> accRecodtypeMap = Account.SObjectType.getDescribe().getRecordTypeInfosByName();
         Schema.RecordTypeInfo accRecordType = accRecodtypeMap.get('Customer Account');
         ID recordtypeId = accRecordType.getRecordTypeId();
         
        //Cases Record type
         Map<String,Schema.RecordTypeInfo> caseRecodtypeMap = Case.SObjectType.getDescribe().getRecordTypeInfosByName();
         Schema.RecordTypeInfo caseRecordType = caseRecodtypeMap.get('Support Services');
         ID caserectypeId = caseRecordType.getRecordTypeId();
        
        //Opportunity Record type
         Map<String,Schema.RecordTypeInfo> oppRecodtypeMap = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName();
         Schema.RecordTypeInfo oppRecordType = oppRecodtypeMap.get('New Business');
         ID oppRectypeId = oppRecordType.getRecordTypeId();
         
        
        //create Account RecordTypeId = recordtypeId,
        //create user
        Profile p = [Select Id, Name from Profile where Name = 'System Administrator'];
        
        String namePrefix = 'TestUser' + math.rint(math.random() * 100000);
        
        AggregateResult[] userCount = [Select count(id) userCount From user where username like :namePrefix];
        
        Object users = userCount[0].get('userCount');
        
        User testUser = new User();
        testUser.Email = 'test@test.com';
        testUser.Username = namePrefix+users+'@testuser.test';

        testUser.LastName = 'testManager';
        testUser.Alias = 'testM';
        testUser.ProfileId = p.Id;
        testUser.LanguageLocaleKey = 'en_US';
        testUser.LocaleSidKey = 'en_US';
        testUser.TimeZoneSidKey = 'America/Chicago';
        testUser.EmailEncodingKey = 'UTF-8';
        
        User userManager = new User();
        userManager.Email = 'test@test.com';
        userManager.Username = namePrefix+users+'Manager@testuser.test';

        userManager.LastName = 'test';
        userManager.Alias = 'test';
        userManager.ProfileId = p.Id;
        userManager.LanguageLocaleKey = 'en_US';
        userManager.LocaleSidKey = 'en_US';
        userManager.TimeZoneSidKey = 'America/Chicago';
        userManager.EmailEncodingKey = 'UTF-8';
        insert userManager;
        
        
       
        testUser.Manager = userManager;
        System.runAs ( testUser ) {
        Account acc1 = new Account(Name='Test Account1',ownerId = testUser.Id);
        insert acc1;
        
        DateTime dt = DateTime.now();
        DateTime closeDt = dt.addDays(-4);
        //cases for acc1
        Case c1 = new Case(AccountId = acc1.Id,Status='New',Creation_Reason__c='Account',Subject='test',
        				Group__c='Support Services',Reason='Process Inquiry',Description='case1');
        insert c1;
        
        Account acc2 = new Account(RecordTypeId = recordtypeId,Name='Test Account2',ParentId = acc1.Id,Industry='Auto',Business_Model__c='Agency',Type='Agency');
        insert acc2;
        Case c2 = new Case(AccountId = acc2.Id,Status='New',Creation_Reason__c='Account',Subject='test',
        				Group__c='Support Services',Reason='Process Inquiry',Description='case2');
        insert c2;
        Opportunity opp2 = new Opportunity(AccountId = acc2.Id,RecordTypeId = oppRectypeId,Name='test2 opp',Type='New Business',
                                            Product_Version__c = 'Enterprise',CloseDate=Date.Today(),StageName = 'Lead Passed');
        insert opp2;
        
        
        PS_Project_Record__c projObj2 = new PS_Project_Record__c(Account__c = acc2.Id,Project_Requester__c=UserInfo.getUserId(),
                                            Project_Name__c = 'pjAcc2',Project_Type__c = 'Internal',
                                            Project_Classification__c = 'Internal',Project_Stage__c = 'Active',
                                            onboarding_Location__c = 'EMEA',Customer_Type__c='Internal',
                                            Comments__c = 'test12344',Business_Impact_Urgency__c='testacc2');
        insert projObj2;
        
         PS_Project_Record__c projObj3 = new PS_Project_Record__c(Account__c = acc2.Id,Project_Requester__c=UserInfo.getUserId(),
                                            Project_Name__c = 'pj2Acc2',Project_Type__c = 'Internal',
                                            Project_Classification__c = 'Internal',Project_Stage__c = 'Completed',
                                            onboarding_Location__c = 'EMEA',Customer_Type__c='Internal',
                                            Comments__c = 'test12344',Business_Impact_Urgency__c='testacc2');
        insert projObj3;
        
        Account acc3 = new Account(RecordTypeId = recordtypeId,Name='Test Account3',ParentId = acc1.Id,Industry='Auto',Business_Model__c='Agency',Type='Agency');
        insert acc3;
        Case c3 = new Case(AccountId = acc3.Id,Status='Closed',Creation_Reason__c='Account',Subject='test',Language__c='English',
        					Zendesk_link__c = 'No',
        					Group__c='Support Services',Reason='Process Inquiry',Description='case3',Closure_reason__c='Move',Closure_sub_reason__c='CID');
        insert c3;
        
        Account acc4 = new Account(RecordTypeId = recordtypeId,Name='Test Account4',ParentId = acc2.Id,Industry='Auto',Business_Model__c='Agency',Type='Agency');
        insert acc4;
        Case c4 = new Case(AccountId = acc4.Id,Status='New',Creation_Reason__c='Account',Subject='test',
        					Group__c='Support Services',Reason='Process Inquiry',Description='case4');
        insert c4;
        
        
        List<Id> accountIdsList = new List<Id>();
        accountIdsList.add(acc1.Id);
        accountIdsList.add(acc2.Id);
        accountIdsList.add(acc3.Id);
        accountIdsList.add(acc4.Id);
        
        Test.startTest();
            SchedulableContext sc;
            AccountBatch accBatch = new AccountBatch();
            accBatch.accountIdsList = accountIdsList;
            accBatch.query = 'SELECT ParentId,Parent.ParentId,Id,TotalAmtOpen_Opportunities__c,NoOfOpen_Cases__c '+
                                             'FROM Account WHERE Id IN: accountIdsList';
            //Database.executeBatch(accBatch);
            //accBatch.testAccId = accountIdsList;
            AccountBatchScheduler obj = new AccountBatchScheduler();
            obj.testAccBatch = accBatch;
            obj.execute(sc);
           
            //call to global value methods
            Map<Id,Account> accountMap = new Map<Id,Account>();
            accountMap.put(acc1.Id,acc1);
            accountMap.put(acc2.Id,acc2);
            accountMap.put(acc3.Id,acc3);
            accountMap.put(acc4.Id,acc4);
            
            AccountBatchUtils objUtils = new AccountBatchUtils();
            Account acct = objUtils.globalValue(acc1.Id,'NoOfOpen_Cases__c',2,accountMap);
            System.debug('In test methods:'+acct);
        Test.stopTest();
         }
    }
}