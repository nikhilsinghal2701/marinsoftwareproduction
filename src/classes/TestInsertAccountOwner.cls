@IsTest
public class TestInsertAccountOwner{
    static TestMethod void CreateInvContact(){
    //Insert Account
        Account a = new Account(Name = 'TestAccount');
            insert a;
    //Insert Marketing Operations Request
        Marketing_Operations_Request__c mor = new Marketing_Operations_Request__c(Name = 'test MOR', Status__c = TRUE, Request_Date__c = Date.Today());
        insert mor;
    //Insert Contact
        Contact c = new Contact(FirstName = 'test', LastName = 'test', AccountId = a.Id);
        insert c;
    //Insert Invited Contact
        Invited_Contact__c ic = new Invited_Contact__c(Contact__c = c.Id, Marketing_Operations_Request__c = mor.id);
        insert ic;  
    //Test Account Owner and AE/AM Text field is populating
        Invited_Contact__c[] checkic = [Select Id, AE_AM__c, AE_AM_Text__c from Invited_Contact__c WHERE Id = :ic.Id];
        for(Invited_Contact__c invcontact :checkic){ 
        system.assertNotEquals(invcontact.AE_AM__c, null);
        system.assertNotEquals(invcontact.AE_AM_Text__c, null);
        }
    }
}