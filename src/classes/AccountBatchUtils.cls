/**
AccountBatchUtils is utils class for AccountBatch class
this class does the calculation for the batch class and creates required maps for logic 
**/
public with sharing class AccountBatchUtils {
    
    
    public Map<String,Decimal> getOpenCases(List<Id> accIdList)
    {
         // aggregate query for open cases
        AggregateResult [] openCaseResult  = [SELECT AccountId,count(Id) cnt FROM Case 
                                                    WHERE Status != 'Closed' 
                                                    AND AccountId IN :accIdList 
                                                    GROUP BY AccountId];
         Map<String,Decimal> openCaseMap = aggResultMapConverter(openCaseResult,'AccountId');
         return openCaseMap;
    }
    
    public Map<String,Decimal> getAgeCases(List<Id> accIdList)
    {
         // aggregate query for open cases 72 hr
        AggregateResult [] openAgeResult = [SELECT AccountId,count(Id)cnt FROM Case 
                                                    WHERE Status != 'Closed' 
                                                    AND AccountId IN :accIdList
                                                    AND Case_Age__c >72
                                                    GROUP BY AccountId];
         Map<String,Decimal> openAgeMap = aggResultMapConverter(openAgeResult,'AccountId');
         return openAgeMap;
    }
    
    public Map<String,Decimal> getCasesAvg(List<Id> accIdList)
    {
         AggregateResult []  closedAvgResult = [SELECT AccountId,Avg(Case_Age__c) cnt FROM Case 
                                                    WHERE Status = 'Closed' 
                                                    AND AccountId IN :accIdList
                                                    AND DAY_ONLY(ClosedDate) = THIS_QUARTER
                                                    GROUP BY AccountId];
         Map<String,Decimal> caseAvgMap = aggResultMapConverter(closedAvgResult,'AccountId');
         return caseAvgMap;
    }
    
    public Map<String,Decimal> getOpenOppAmtMap(List<Id> accIdList)
    {
        AggregateResult [] openOppAmtResult = [SELECT AccountId,sum(Amount) cnt FROM Opportunity
                                                WHERE IsClosed = false
                                                AND AccountId IN :accIdList
                                                GROUP BY AccountId]; 
         Map<String,Decimal> openOppAmtMap = aggResultMapConverter(openOppAmtResult,'AccountId');
         return openOppAmtMap;
    }
    
    public AggregateResult [] getClosewonAmtMap(List<Id> accIdList)
    {
         AggregateResult [] closeWonAmtResult = [SELECT AccountId,StageName,sum(Amount) cnt FROM Opportunity
                                                WHERE IsClosed = true
                                                AND CloseDate = THIS_QUARTER
                                                AND AccountId IN :accIdList
                                                GROUP BY AccountId,StageName];
        
         return closeWonAmtResult;
    }
    
    public Map<String,Decimal> getActiveProjectMap(List<Id> accIdList)
    {
        AggregateResult [] openIntegrationCnt = [SELECT Account__c, count(Id) cnt 
                                                FROM PS_Project_Record__c
                                                WHERE Account__c IN :accIdList
                                                AND Project_Stage__c = 'Active'
                                                GROUP BY Account__c];
         Map<String,Decimal> activeProjectMap = aggResultMapConverter(openIntegrationCnt,'Account__c');
         return activeProjectMap;
    }
    
    public Map<String,Decimal> getInactiveProjectMap(List<Id> accIdList)
    {
        AggregateResult [] closeIntegrationCnt = [SELECT Account__c, count(Id) cnt 
                                                FROM PS_Project_Record__c
                                                WHERE Account__c IN :accIdList
                                                AND Project_Stage__c = 'Completed'
                                                GROUP BY Account__c];
         Map<String,Decimal> inactiveProjectMap = aggResultMapConverter(closeIntegrationCnt,'Account__c');
         return inactiveProjectMap;
    }
    
    public Map<String,Decimal> aggResultMapConverter(AggregateResult [] aResult,String accStr)
    {
        Map<String,Decimal> aResultMap = new Map<String,Decimal>();
        if(!aResult.isEmpty()){
            for(Integer i=0;i<aResult.size();i++){
                if(!aResultMap.isEmpty() && aResultMap.containsKey((String)aResult[i].get(accStr)))
                {
                    Decimal num = aResultMap.get(accStr)!=null?aResultMap.get(accStr):0.0;
                    num +=  (Decimal)aResult[i].get('cnt');
                    aResultMap.put((String)aResult[i].get(accStr),num);
                }
                else{
                    Decimal num = (Decimal)aResult[i].get('cnt');
                    aResultMap.put((String)aResult[i].get(accStr),num);
                }
            }
        }
        return aResultMap;
    } 
    
    //for opp closed lost and won
    public Map<String,Decimal> aggResultMapConverter(AggregateResult [] aResult,String accStr,String stgName)
    {
        Map<String,Decimal> aResultMap = new Map<String,Decimal>();
        if(!aResult.isEmpty()){
            for(Integer i=0;i<aResult.size();i++){
                if((String.valueOf(aResult[i].get('StageName')).equalsIgnoreCase(stgName))){
                    if(!aResultMap.isEmpty() && aResultMap.containsKey((String)aResult[i].get(accStr)))
                    {
                        Decimal num = aResultMap.get(accStr)!=null?aResultMap.get(accStr):0.0;
                        num +=  (Decimal)aResult[i].get('cnt');
                        aResultMap.put((String)aResult[i].get(accStr),num);
                    }
                    else{
                        Decimal num = (Decimal)aResult[i].get('cnt');
                        aResultMap.put((String)aResult[i].get(accStr),num);
                    }
                }
            }
        }
        return aResultMap;
    } 
    

    public Account globalValue(String key,String fieldName,Decimal currValue, Map<Id,Account> accWrapperMap)
    {
         Account accountObject = new Account();
                
                if(!accWrapperMap.isEmpty() && accWrapperMap.containsKey(key))
                {
                    Account accObj = new Account(Id = key);
                    accObj = accWrapperMap.get(key);
                    System.debug('currValue:'+currValue);
                    sObject sObj = accObj;
                    Decimal counter = 0.0;
                    counter = (Decimal)sObj.get(fieldName) != null?(Decimal)sObj.get(fieldName):0.0;//this field name should be taken from accField
                    System.debug('Util counter:'+counter);
                    if(currValue != null)
                        counter += currValue;
                    System.debug('counter:'+counter);
                    sObj.put(fieldName,counter);
                    accountObject = (Account) sObj;
                }
                else{
                    Account accObj = new Account(Id = key);
                    sObject sObj = accObj;
                    sObj.put(fieldName,currValue);
                    accountObject = (Account)sObj;
                }
            return accountObject;
    }
    
        
        /**
            // SpendPer publisher
    public Map<String,Advance_Metric__c> getSpendPerPublisherMap(List<Id> accId, Integer nDays){
                            
        nDays = 8;  
        AggregateResult [] spendPublisher = new AggregateResult[]{};                        
        String query = 'SELECT  Marin_App_Client__r.Account__c accId ,sum(Other_Publisher_Spend__c) otherSpend, sum(Criteo_Spend__c) criteoSpend, '+ 
                        'sum(Facebook_Spend__c) fbSpend, sum(Google_Spend__c) gSpend,sum(Universal_Channels_Spend__c) uSpend, '+
                        'sum(Yahoo_Japan_Spend__c) yjSpend,sum(Yahoo_Bing_Spend__c) ybSpen FROM Metric__c '+ 
                        'WHERE Date__c = LAST_N_DAYS:'+nDays+' AND  Marin_App_Client__r.Account__c IN :accId '+
                        'group by Marin_App_Client__r.Account__c';
        spendPublisher = Database.query(query);
        
        Map<String,Advance_Metric__c> spendPerPublisher = new Map<String,Advance_Metric__c>();
        
        for(Integer i=0;i<spendPublisher.size();i++)
        {
            String accIdKey = spendPublisher[i].get(Marin_App_Client__r.Account__c);
            if(!spendPerPublisher.isEmpty() && spendPerPublisher.containsKey(accIdKey))
            {
                Advance_Metric__c mObj = spendPerPublisher.get(accIdKey);
                mObj.Google_Spend__c += spendPublisher[i].get(gSpend);
                spendPerPublisher.put(accIdKey,mObj);
            }
        }
        
    }
        **/
        
      
}