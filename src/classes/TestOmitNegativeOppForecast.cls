/**
 * This Apex Class is being used to test the OmitNegativeOppForecast trigger built
 * Created by: Gary Sopko
 * Created Date: 04/02/2013
 * Please consult with Sales Operations/System Administration before updating.
 */
@isTest
public class TestOmitNegativeOppForecast {

     static testMethod void createOpportunity() {

       //Create Opportunity Record to test that Forecast Category is Omitted
       Opportunity opp = new Opportunity(Name = 'TestOpportunity', Amount = -2000, StageName = 'Seeking Approval',
               RecordTypeId = '012500000005TxE', Type = 'Renewal',
               Type_Detail__c = 'Renewal Decrease', Product_Version__c = 'Enterprise',
               ForecastCategoryName = 'Best Case', CloseDate = Date.Today(), Situation__c = 'TestSituation');
		system.debug('Forecast Category before Insert'+ opp.ForecastCategoryName);
		insert opp;

		//Retrieve new Opportunity
		Opportunity retrievedOpp = [Select Id, ForecastCategoryName from Opportunity where Id = :opp.Id];
		system.debug('Forecast Category after Insert'+ retrievedOpp.ForecastCategoryName);
		
		//Test that ForecastCategory = Omitted
		system.assertEquals('Omitted', retrievedOpp.ForecastCategoryName);
    }
}