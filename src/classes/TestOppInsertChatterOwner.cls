/**

 */
@isTest
private class TestOppInsertChatterOwner {

    static testMethod void Test1() {
		Account a = new Account();
			a.Name = 'Test Account';
			a.Type = 'Advertiser';
			a.Account_Status__c = 'Active Customer';
			a.Region__c = 'NA';
			a.OwnerId = UserInfo.getUserId();
			a.BillingCountry = 'United States';
		insert a;
		Opportunity o = new Opportunity();
			o.Name = 'Test Opportunity';
			o.AccountId = a.Id;
			o.StageName = 'Lead Passed';
			o.OwnerId = UserInfo.getUserId();
			o.Amount = 0;
			o.CloseDate = date.Today();
			o.Type = 'New Business';
			o.Type_Detail__c = 'New Business: Full Term';
		insert o;		
    }
}