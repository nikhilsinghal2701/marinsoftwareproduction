trigger prevent_dupe_attendee on Invited_Contact__c (before insert, after update) {

    for(Invited_Contact__c ic: trigger.new){
        if(ic.Contact__c != null){
            Integer count = [Select count() from Invited_Contact__c WHERE Contact__c = :ic.Contact__c
                             AND Marketing_Operations_Request__c = :ic.Marketing_Operations_Request__c];
            if(count >= 1 && trigger.isInsert){
                ic.Contact__c.addError('This Invited Contact is already in this Marketing Event');
            }
            else if(count > 1 && trigger.isUpdate){
            	ic.Contact__c.addError('This Invited Contact is already in this Marketing Event');
            }
        }
        
            /**String contact_id = ic.Contact__c;
            List<Invited_Contact__c> me_list = new List<Invited_Contact__c>{};
            me_list = [Select Contact__c from Invited_Contact__c where Marketing_Operations_Request__c =
            :ic.Marketing_Operations_Request__c];
            Integer maxCount;
            maxCount = [Select Count() from Invited_Contact__c where Marketing_Operations_Request__c =
            :ic.Marketing_Operations_Request__c] - 1;
            Boolean trigger_alert = false;
            Integer i = 0;
            //System.debug('maxCount = ' + maxCount);
            do{
                if( me_list[i].Contact__c == ic.Contact__c){
                trigger_alert = true;
                //System.debug('this has fired EAFABLE');
                ic.Contact__c.addError('This contact is already part of the invite list.');
                i = maxCount;}
                i++;
            }
            while(i <= maxCount);
        }
**/
    }
}