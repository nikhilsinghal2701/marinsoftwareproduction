trigger AttainmentInputManagerAttainment on Attainment__c (before insert, before update, after insert, after update) {
  List<Attainment__c> attainments = new List<Attainment__c>();
  Set<Id> attIdsToUpdateChain;
  String adminUserId = '00550000002rTKhAAM';
  Messaging.SingleEmailMessage mail;
  String[] emailErrorToAddresses = new String[] {'crmsupport@marinsoftware.com'};
  Set<id> ownerIds = new Set<id>();
  Boolean updatea = false;
  User owner;
  String achain = '';
  String oldtempachain;
  String tempachain;


  if (Trigger.isBefore) {
    // query for all the User records for the unique userIds in the records
    // create a map for a lookup / hash table for the user info
    for (Attainment__c a : trigger.new) {
        ownerIds.add(a.OwnerId);
    }

    Map<id, User> owners = new Map<id, User>([Select FirstName, LastName from User Where Id in :ownerIds]);

    for(Attainment__c a : trigger.new){

      if (owners.containsKey(a.OwnerId)) {
        if ((a.Name == null || a.Name == 'autocreate') && a.OwnerId != null && a.Start_Date__c != null) {
          a.Name = owners.get(a.OwnerId).FirstName + ' ' + owners.get(a.OwnerId).LastName + ' - ' + a.Start_Date__c.month() + ' ' + a.Start_Date__c.year();
        }

        if ((a.Manager_Attainment__c == null && a.Owner_Role__c != 'CRO') || (Trigger.isUpdate && (a.Owner_Manager__c != trigger.oldMap.get(a.Id).Owner_Manager__c || a.Start_Date__c != trigger.oldMap.get(a.Id).Start_Date__c))) {
          try{
            attainments = [Select Id from Attainment__c WHERE OwnerId = :a.Owner_Manager__c AND Start_Date__c = :a.Start_Date__c];
            if (attainments.size() == 1) {
              a.Manager_Attainment__c = attainments[0].Id;
              // a.Manager_Attainment__c = [Select Id from Attainment__c WHERE OwnerId = :a.Owner_Manager__c AND Start_Date__c = :a.Start_Date__c].Id;
            } else if (attainments.size() > 1) {
              a.Manager_Attainment__c = null;
              // There are multiple attainments for this manager. This needs to be set manually. Notify CRM Support.
              Messaging.reserveSingleEmailCapacity(1);
              mail = new Messaging.SingleEmailMessage();

              mail.setToAddresses(emailErrorToAddresses);
              mail.setSubject('Too many manager attainments: ' + a.Name + ' (' + a.Id + ')');
              mail.setPlainTextBody('Attainment: ' + a.Name + ' (' + a.Id + ') has multiple potential manager attainments and ' +
                'needs to be have the manager attainment field set manually.');
              Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

            }
          } catch (Exception e) {
            system.debug('AN ERROR WAS THROWN: ' + e);
          }
        }
      }
    }
  } else {
    // This is After.
    attIdsToUpdateChain = new Set<Id>();
    
    for (Attainment__c a : trigger.new) {
      if (trigger.isinsert || 
          (trigger.isupdate && a.Manager_Attainment__c != trigger.oldMap.get(a.Id).Manager_Attainment__c)) {

        // Add this attainment record to have its Chain (re)calculated.
        attIdsToUpdateChain.add(a.Id);

        // Store old Attainment Chains for updated records because the Manager Attainment
        // change requries attainment recalculation.
        if (Trigger.isUpdate) {
          achain += trigger.oldMap.get(a.Id).Attainment_Chain_18__c + ' ';
        }

      }
    }

    if (attIdsToUpdateChain.size() > 0) {
      
      // Recalculate Attainment Chain for these records.
      //attainments = AttainmentCalculation.calculateAttainmentChain(attIdsToUpdateChain);

      try {
        update attainments;

        for (Attainment__c aRecalc : attainments) {
          achain += aRecalc.Attainment_Chain_18__c + ' ';
        }

      } catch (Exception e) {
        System.debug('AN ERROR WAS THROWN: ' + e);
      }

    }

    if (achain.trim().length() > 0) {
      try{
        System.debug('Recalculating...');
        //AttainmentCalculation.AttainmentCalculation(achain);
      } catch (Exception e) {
       system.debug('AN ERROR WAS THROWN: ' + e);
      }
    }
  }
}