/* Copyright Marin Software 2011. All rights reserved.
 *
 * @author: apotapov
 *
 * This file is currently maintained under Marin source control
 * in mscm project under resources/salesforce/triggers/ecrypt_lead_id.apex.
 * Any modifications to this code in SalesForce should be reflected
 * in the source file in mscm as well for versioning purposes.
 */
 
 
/* This trigger is designed to generate encrypted version of Lead Id
 * when a Lead object is created in SalesForce. The idea is to be able
 * to pass the lead id to Marin App as a url parameter without exposing
 * the actual id.
 *
 * The original source of the code can be found here:
 * http://success.salesforce.com/ideaView?c=09a30000000D9y3&id=087300000006twoAAA
 *
 */
trigger EncryptedLeadId on Lead (before insert) {
    for (Lead lead : Trigger.new) {
        try {
            DateTime now = System.now();
            String formattedNow = now.formatGmt('yyyy-MM-dd') + 
                                  'T' + 
                                  now.formatGmt('HH:mm:ss') +
                                  '.' + 
                                  now.formatGmt('SSS') +
                                  'Z';
            //some salt for encryption
            String canonical = lead.id + lead.FirstName + lead.LastName + formattedNow;
            Blob digest = Crypto.generateDigest('MD5', Blob.valueOf(canonical));
            String token = EncodingUtil.base64Encode(digest);
            token = token.replaceAll('=','');
            
            //make sure that the token is url encoded
            token = Encodingutil.urlEncode(token, 'UTF-8');
            token = token.replaceAll('%','_');
            token = token.replaceAll('/','_');
    
            //make sure we fit into the field.
            if (token.length() > 255) { 
                token = token.substring(0,254);
            }
            
            //assign the generated token as an encrypted lead id
            lead.Encrypted_Lead_Id__c = token;
            
        } catch (Exception e) {
            system.debug('Error generating encrypted id:' + e);
        }
    }
}