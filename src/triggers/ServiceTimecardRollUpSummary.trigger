trigger ServiceTimecardRollUpSummary on SFDC_Service_Timecard__c (after insert, after update) {
    //Variables and Lists and stuff...
    list<AggregateResult> hours = new list<AggregateResult>();
    //What happens to the old case shit
    String name = UserInfo.getName();
    if(name != 'Asa Strout-Hearick'){
	    if(trigger.isUpdate){
	        for(SFDC_Service_Timecard__c oldtc : trigger.old){
	        	for(SFDC_Service_Timecard__c tc : trigger.new){
	        		system.debug('OLDTC CASE:::: ' + oldtc.Case__c);
	        		system.debug('TC CASE::::' + tc.Case__c);
		        	if(tc.Case__c != null){
			            for(Case c : [Select Id, Total_Hours_Worked__c from Case WHERE Id = :oldtc.Case__c]){
			                hours = [Select sum(Hours_Worked__c)hoursworked from SFDC_Service_Timecard__c WHERE Case__c = :c.Id];
			                if(hours.size() > 0){
			                    String hoursst = ''+hours[0].get('hoursworked');
			                    if(hoursst == 'null'){
			                        hoursst = '0';
			                    }
			                    Decimal finalhours = Decimal.valueof(hoursst);
			                    c.Total_Hours_Worked__c = finalhours;
			                    update c;
			                }
		            	}
		            	for(Case c : [Select Id, Total_Hours_Worked__c from Case WHERE Id = :tc.Id]){
		            		hours = [Select sum(Hours_Worked__c)hoursworked from SFDC_Service_Timecard__c WHERE Case__c = :c.Id];
			                if(hours.size() > 0){
			                    String hoursst = ''+hours[0].get('hoursworked');
			                    if(hoursst == 'null'){
			                        hoursst = '0';
			                    }
			                    Decimal finalhours = Decimal.valueof(hoursst);
			                    c.Total_Hours_Worked__c = finalhours;
			                    update c;
			                }
		            	}
		        	} else if(oldtc.Integration__c != null){
		        		for(PS_Project_Record__c i : [Select Id, Total_Hours_Worked__c from PS_Project_Record__c WHERE Id = :oldtc.Integration__c]){
		        			hours = [Select sum(Hours_Worked__c)hoursworked from SFDC_Service_Timecard__c WHERE Integration__c = :i.Id];
		        			if(hours.size() > 0){
		        				String hoursst = ''+hours[0].get('hoursworked');
		        				if(hoursst == 'null'){
		        					hoursst = '0';
		        				}
		        				Decimal finalhours = Decimal.valueof(hoursst);
		        				i.Total_Hours_Worked__c = finalhours;
		        				update i;
		        			}
		        		}
		        		for(PS_Project_Record__c i : [Select Id, Total_Hours_Worked__c from PS_Project_Record__c WHERE Id = :tc.Integration__c]){
		        			hours = [Select sum(Hours_Worked__c)hoursworked from SFDC_Service_Timecard__c WHERE Integration__c = :i.Id];
		        			if(hours.size() > 0){
		        				String hoursst = ''+hours[0].get('hoursworked');
		        				if(hoursst == 'null'){
		        					hoursst = '0';
		        				}
		        				Decimal finalhours = Decimal.valueof(hoursst);
		        				i.Total_Hours_Worked__c = finalhours;
		        				update i;
		        			}
		        		}
		        	}
	        	}
	    	}
	    }
	    //What happens to the new case
	    if(trigger.isInsert){
	        for(SFDC_Service_Timecard__c tc : trigger.new){
	            if(tc.Case__c != null){
	                for(Case c : [Select Id, Total_Hours_Worked__c from Case WHERE Id = :tc.Case__c]){
	                    hours = [Select sum(Hours_Worked__c)hoursworked from SFDC_Service_Timecard__c WHERE Case__c = :c.Id];
	                    if(hours.size() > 0){
	                        String hoursst = ''+hours[0].get('hoursworked');
	                        if(hoursst == 'null'){
	                            hoursst = '0';
	                        }
	                        Decimal finalhours = Decimal.valueof(hoursst);
	                        c.Total_Hours_Worked__c = finalhours;
	                        update c;
	                    }
	                }
	            } else if(tc.Integration__c != null){
	                for(PS_Project_Record__c i : [Select Id, Total_Hours_Worked__c from PS_Project_Record__c WHERE Id = :tc.Integration__c]){
	                    hours = [Select sum(Hours_Worked__c)hoursworked from SFDC_Service_Timecard__c WHERE Integration__c = :i.Id];
	                    if(hours.size() > 0){
	                        String hoursst = ''+hours[0].get('hoursworked');
	                        if(hoursst == 'null'){
	                            hoursst = '0';
	                        }
	                        Decimal finalhours = Decimal.valueof(hoursst);
	                        i.Total_Hours_Worked__c = finalhours;
	                        update i;
	                    }
		            }
	            }
	    	}
	    }
    }
}