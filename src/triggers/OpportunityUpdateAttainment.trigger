/***
    Created By: Gary Sopko 
    Created Date: 04/03/2013
    Description: Apex trigger on Opportunities to update the Opportunity Attainment field with the Attainment Record for the Opportunity
    Owner that month
        
    Notes:
    	Before/After Everything --> If Type = Professional Services or if Type Detail = AM: Credit, then mark it as being Omitted and don't even bother. 
    	Before Insert
    		Step 1:  Determine if Omitted from Attainment  -- If Omitted from Attainment, then don't do any calculating
    		Step 2:  If this isn't being omitted, then achange and updatea will both evaluate to true so that an Attainment record is associated and the updates to the Attainment are made
    		Step 3:  As achange == true, then we assign new Attainment
    		Step 4:  End code statement
    	After Insert
    		Step 1:  Determine if Omitted from Attainment -- If Omitted from Attainment, then don't do any calculating
    		Step 2:  If this isn't being omitted, then achange and updatea will both evaluate to true so that the associated Attainment record is updated
    		Step 3:  Assign Attainment Chain to variable
    		Step 4:  Send Attainment Chain variable -- achain -- to AttainmentCalculation class
    	Before Update
    		Step 1:  Determine if Omitted from Attainment -- If Omitted from Attainment, then don't do any calculating
    		Step 2:  Determine achange and upatea 
    		Step 3:  If achange then find new Attainment and associate it to the Opportunity
    		Step 4:  End code statment
    	After Update
    		Step 1:  Determine if Omitted from Attainment -- If Omitted from Attainment, then don't do any calculating
    		Step 2:  Determine achange and updatea
    		Step 3:  If achange, then assign new and old Attainment Chain to tempachain and tempachain2 variables
    		Step 4:  If updatea and achange = false, then assign new Attainment Chain to tempchain, leave tempchain2 null
    		Step 5:  Aggregate information from tempachain and tempachain2 into one string
    		Step 6:  Pass off string to AttainmentCalculations class
    
**/

trigger OpportunityUpdateAttainment on Opportunity (before insert, after insert, before update, after update) {
  List<Attainment__c> attainments = new List<Attainment__c>();

	//Boolean to determine if the Attainment associated to the Opportunity has changed
    Boolean achange = false;
    //Boolean to determine if the Attainment associated to the Opportunity should be updated.
    Boolean updatea = false;
    //Boolean to determine if the Opportunity is Omitted from Attainment
    Boolean omitted = false;
    //String that is passed off to class to do calculations. This string should contain Attainment Id's for
    //Attainments to update delimited by a " "
    String achain = '';
    //Strings which are evenutally concatenated together to create achain
    String tempachain;
    String tempachain2;
    // To minimize DML, create sets to get all needed attainments in one query for SA attainments.
    Set<Id> saIds = new Set<Id>();
    Set<Date> saDates = new Set<Date>();
    Map<Id, Attainment__c> attsSA;
    List<Integer> oppsToUpdateSA = new List<Integer>();
    Integer oppsI = 0;
	Map<Attainment__c, Id> attsSAInverse = new Map<Attainment__c, Id>();
	Attainment__c attKey;
	Date qualDate;
	


    /**
    TO DO
    Move all the Attainment lookup DML outside of the for() loop:
    -- Fetch all the potential attainments for only the opportunities that matter by doing something like this:

    Map<ID,Attainment__c> attMap = new Map<ID,Attainment__c>([SELECT OwnerId, Start_Date__c FROM Attainment__c  WHERE OwnerId IN :ownerIds AND Start_Date__c = :startDates]);

    -- Flip the keys and values:

	Map<Attainment__c, Id> attMapInverse = new Map<Attainment__c, Id>();
	for (Id key : attMap.keySet()) {
    	attMapInverse.put(new Attainment__c(OwnerId = attMap.get(key).OwnerId, Start_Date__c = attMap.get(key).Start_Date__c), key);
	}

	-- And then when needed, look up the Attainment Id by doing:

	Attainment__c attkey = new Attainment__c(OwnerId = '00550000001krh9', Start_Date__c = Date.newInstance(2014,03,01));
    System.Debug(attMapInverse.get(attkey));
    
    */

	if (trigger.isBefore) {
		for (Opportunity o : trigger.new) {
			if ((o.Qualified_Date__c != null && ((o.Sales_Associate__c != null && o.Attainment_SA__c == null) || 
					(o.Secondary_Sales_Associate__c != null && o.Attainment_2nd_SA__c == null))) || 
						(trigger.isupdate && (trigger.oldMap.get(o.Id).Sales_Associate__c != o.Sales_Associate__c || 
							trigger.oldMap.get(o.Id).Secondary_Sales_Associate__c != o.Secondary_Sales_Associate__c ||
								trigger.oldMap.get(o.Id).Qualified_Date__c != o.Qualified_Date__c))) {
				// Does this opp have SA/2SA but no SA attainments?
				// Has the SA/2SA changed?
				// Has the Qual Date changed?

				// If any of the above, add this opp Id to a Set to later do a SOQL query and grab all the 
				// attainment records with for all those folks and all the appropriate months.
				saIds.add(o.Sales_Associate__c);
				if (o.Secondary_Sales_Associate__c != null) {
					saIds.add(o.Secondary_Sales_Associate__c);
				}

				if (o.Qualified_Date__c != null) {
					saDates.add(o.Qualified_Date__c.toStartofMonth());
				}

				// We only need to update these opps.
				oppsToUpdateSA.add(oppsI);
			}
			oppsI++;
		}
		// Get new attainments, setting to null where SA/2SA has been reset to null.
		if (!saIds.isEmpty() && !saDates.isEmpty()) {
			attsSA = new Map<ID,Attainment__c>([SELECT Id, OwnerId, Start_Date__c FROM Attainment__c WHERE OwnerId IN :saIds AND Start_Date__c IN :saDates]);

			// Flip the keys and values so we can search.
			for (Id key : attsSA.keySet()) {
		    	attsSAInverse.put(new Attainment__c(OwnerId = attsSA.get(key).OwnerId, Start_Date__c = attsSA.get(key).Start_Date__c), key);
			}
		}

		// Just update the opps that need to be updated re: SA.
		for (Integer oppI : oppsToUpdateSA) {

			if (trigger.new[oppI].Sales_Associate__c == null || trigger.new[oppI].Qualified_Date__c == null) {
				trigger.new[oppI].Attainment_SA__c = null;
				// Primary SA is required per VR. Assume null for 2nd.
				trigger.new[oppI].Attainment_2nd_SA__c = null;

			} else {
				qualDate = trigger.new[oppI].Qualified_Date__c.toStartofMonth();
				attKey = new Attainment__c(OwnerId = trigger.new[oppI].Sales_Associate__c, Start_Date__c = qualDate);
				trigger.new[oppI].Attainment_SA__c = attsSAInverse.get(attKey);

				// Set 2nd SA Attainment.
				if (trigger.new[oppI].Secondary_Sales_Associate__c != null) {
					attKey = new Attainment__c(OwnerId = trigger.new[oppI].Secondary_Sales_Associate__c, Start_Date__c = qualDate);
					trigger.new[oppI].Attainment_2nd_SA__c = attsSAInverse.get(attKey);
				}
			}
		}
	}

    for(Opportunity o : trigger.new){
    	if((o.Type_Detail__c == 'AM: Credit' || o.Type == 'Professional Services') && trigger.isbefore){
    		o.Omitted_from_Attainment__c = true;
    		o.Attainment__c = null;
    	}
    	/**
    	TO DO:
    	Should Omitted_from_Attainment__c change back to false when Type or Type_Detail__c are changed from the above to something else?
    	*/

    	if ((trigger.isupdate && trigger.oldMap.get(o.Id).Omitted_from_Attainment__c == true && o.Omitted_from_Attainment__c == true) || (trigger.isinsert && o.Omitted_from_Attainment__c == true)){
    		omitted = true;
    	}

    	system.debug('OMITTED BEFORE EVALUATION OF OMITTED ::: ' + omitted);
    	
    	if (omitted == false) {
	        Decimal amount = o.Amount;
	        Date closedate = o.CloseDate;
	        Id ownerid = o.OwnerId;
	        Boolean omittedFromAttainment = o.Omitted_from_Attainment__c;
	        Date sd = o.CloseDate.toStartofMonth();
	        String stage = o.StageName;
	        if (trigger.isinsert) {
	            updatea = true;
	            achange = true;
	            if(trigger.isbefore){
	            	try{
			          attainments = [Select Id from Attainment__c WHERE OwnerId = :ownerid AND Start_Date__c = :sd];
			          if (attainments.size() == 1) {
			           o.Attainment__c = attainments[0].Id;
			          } else {
			           o.Attainment__c = null;
			          }
	            	} catch(Exception e){
	            		system.debug('AN EXCEPTION WAS THROWN!!! EXCEPTION: ' + e);
	            	}
	            }
	            if (trigger.isafter) {
	                tempachain = o.Attainment_Chain__c;
	                system.debug('ACHAIN AFTER INSERT ::: ' + achain);
	                tempachain2 = null;
	            }
	        }

	        if (trigger.isupdate) {
	            if(amount != trigger.oldMap.get(o.Id).Amount || stage != trigger.oldMap.get(o.Id).StageName){
	                updatea = true;
	            }
	            if(closedate != trigger.oldMap.get(o.Id).CloseDate || ownerid != trigger.oldMap.get(o.Id).OwnerId || omittedFromAttainment != trigger.oldMap.get(o.Id).Omitted_from_Attainment__c || o.Attainment__c == null){
	                updatea = true;
	                achange = true;
	            }
	            if (trigger.isbefore && achange == true) {
	            	if (omittedFromAttainment == false) {
			          attainments = [Select Id from Attainment__c WHERE OwnerId = :ownerid AND Start_Date__c = :sd];
			          if (attainments.size() == 1) {
			           o.Attainment__c = attainments[0].Id;
			          } else {
			           o.Attainment__c = null;
			           /** 
			           TO DO
			           This should create new attainment records where they don't exist.
			           **/
			          }
	            	} else {
	            		o.Attainment__c = null;
	            	}
	            }   
	            if(trigger.isafter && achange == true && updatea == true){
	                tempachain = o.Attainment_Chain__c;
	                system.debug('ACHAIN AFTER UPDATE updatea = true and achage = true::: ' + tempachain);
	                tempachain2 = trigger.oldMap.get(o.Id).Attainment_Chain__c;
	                system.debug('ACHAIN2 AFTER UPDATE updatea = true and achange = true::: ' + tempachain2);
	            } 
	            if(trigger.isafter && updatea == true && achange == false){
	                tempachain = o.Attainment_Chain__c;
	                system.debug('ACHAIN AFTER UPDATE updatea = true and achange = false::: ' + tempachain);
	                tempachain2 = null;
	            }
	        }

	        if(trigger.isafter && updatea == true){
	          	if (tempachain != null) {
	          		achain += tempachain + ' ';
	          	}

	          	if (tempachain2 != null) {
	          		achain += tempachain2 + ' ';
	          	}
	        }
    	}
    }

	if (trigger.isafter) {
		for (Opportunity o : trigger.new) {
			if (trigger.isInsert && o.Qualified_Date__c != null) {
				updatea = true;

				if (o.Sales_Associate__c != null) {
					achain += o.Attainment_Chain_SA__c + ' ';

					if (o.Secondary_Sales_Associate__c != null) {
						achain += o.Attainment_Chain_2nd_SA__c + ' ';
					}
				}

			} else if (trigger.isUpdate) {
				if (trigger.oldMap.get(o.Id).Sales_Associate__c != o.Sales_Associate__c || 
						trigger.oldMap.get(o.Id).Initial_Qualification_Value__c != o.Initial_Qualification_Value__c || 
							trigger.oldMap.get(o.Id).Qualified_Date__c != o.Qualified_Date__c) {

					updatea = true;

					if (trigger.oldMap.get(o.Id).Sales_Associate__c != null) {
						achain += trigger.oldMap.get(o.Id).Attainment_Chain_SA__c;
					}

					if (o.Sales_Associate__c != null) {
						achain += o.Attainment_Chain_SA__c;
					}

				}

				if (trigger.oldMap.get(o.Id).Secondary_Sales_Associate__c != o.Secondary_Sales_Associate__c || 
						trigger.oldMap.get(o.Id).Initial_Qualification_Value__c != o.Initial_Qualification_Value__c || 
							trigger.oldMap.get(o.Id).Qualified_Date__c != o.Qualified_Date__c) {

					updatea = true;

					if (trigger.oldMap.get(o.Id).Secondary_Sales_Associate__c != null) {
						achain += trigger.oldMap.get(o.Id).Attainment_Chain_2nd_SA__c;
					}

					if (o.Secondary_Sales_Associate__c != null) {
						achain += o.Attainment_Chain_2nd_SA__c;
					}

				}
			}
		}
	}


	if (trigger.isafter && updatea == true && achain.trim().length() > 0){
	  	try{
	    	//AttainmentCalculation.AttainmentCalculation(achain);
	  	} catch(Exception e){
	  		system.debug('AN EXCEPTION WAS THROWN!!! EXCEPTION ::: ' + e);
	  	}
	}

    system.debug('ACHANGE AT END OF TRIGGER ::: ' + achange);
    system.debug('UPDATEA AT END OF TRIGGER ::: ' + updatea);
}