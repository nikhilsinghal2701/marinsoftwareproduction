trigger InvContUpdateRecentMarketingEvent on Invited_Contact__c (before insert, before update) {
    for(Invited_Contact__c ic: trigger.new){
        String con = ic.Contact__c;
        if(ic.Most_Recent_Marketing_Event__c == null){
            Integer count = [Select count() from Invited_Contact__c WHERE Contact__c = :con];
            if(count > 0){
            String recent = [Select Id, CreatedDate, Marketing_Operations_Request__c 
                             from Invited_Contact__c WHERE Contact__c = :con ORDER BY 
                             CreatedDate DESC LIMIT 1].Marketing_Operations_Request__c;
            ic.Most_Recent_Marketing_Event__c = recent;
            }
        }
    }
}