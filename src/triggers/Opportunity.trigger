trigger Opportunity on Opportunity (before insert, before update) {

	String typeDetailString = 'New Business: TrueUp';
	String forecastCategoryNameString = 'Omitted';
	Date closeDateDate = Date.newinstance(2014, 1, 1);

	for (Opportunity so : Trigger.new) {
		if (so.Type_Detail__c == typeDetailString && (so.CloseDate == null || so.CloseDate >= closeDateDate)) {
			so.ForecastCategoryName = forecastCategoryNameString;
		}
	}

}