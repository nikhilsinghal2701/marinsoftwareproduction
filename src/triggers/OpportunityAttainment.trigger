trigger OpportunityAttainment on Opportunity (after update,after insert) {
	
	if(Trigger.isInsert){
		OpportunityAttainmentHelper.getOppDetailsAfterInsert(Trigger.new);	
	}else if(Trigger.isAfter){
		OpportunityAttainmentHelper.getOppDetailsAfterUpdate(Trigger.oldMap,Trigger.new);
	}

}