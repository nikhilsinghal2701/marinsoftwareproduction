trigger AdvanceMetricUpdateAccount on Advance_Metric__c (after insert, after update) {
	for(Advance_Metric__c a : trigger.new){
		if(a.Account__c != null){
			if(trigger.isInsert){
				for(Account acc : [Select Id, Advance_Metric__c from Account WHERE Id = :a.Account__c AND Advance_Metric__c = null]){
					acc.Advance_Metric__c = a.Id;
					update acc;
				}
			} else if(trigger.isupdate){
				if(a.Account__c != trigger.oldMap.get(a.Id).Account__c){
					for(Account acc : [Select Id, Advance_Metric__c from Account WHERE Id = :a.Account__c AND Advance_Metric__c = null]){
						acc.Advance_Metric__c = a.Id;
						update acc;
					}
				}
			}
		}
	}
}