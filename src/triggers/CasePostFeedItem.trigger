trigger CasePostFeedItem on Case (after insert, after update) {
	for(Case c : trigger.new){
		Boolean send = false;
		if(trigger.isinsert && c.PriorityValue__c == 'Critical'){
			send = true;
		}
		if(trigger.isupdate){
			if(c.PriorityValue__c == 'Critical' && c.Priority__c != trigger.oldMap.get(c.Id).Priority__c){
				send = true;
			}
		}
		if(test.isRunningTest()){
			send = false;
		}
		String rid = c.AccountId;
		String casenumber = c.CaseNumber;
		String link = string.valueof(System.URL.getSalesforceBaseURL().toExternalForm() + '/' + c.Id);
		String hash = 'P1Case';
		String uid = c.AccountOwnerId__c;
		String posttext = 'A P1 support ticket has been logged for this account.  The Case Number is: ' + casenumber + '.  You can access the ticket by going to the following link: ' + link;
		String sessionid = UserInfo.getSessionId();
		if(send){
			PostFeedItemAccount.PostFeedItem(rid, hash, uid, sessionid, posttext);
		}
	}
}