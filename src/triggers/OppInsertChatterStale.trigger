trigger OppInsertChatterStale on Opportunity (after update) {
    for(Opportunity o : trigger.new){
        Boolean send = false;
        if((o.Stalled_Opp_Category__c  == 'Solid Prospect' || o.Stalled_Opp_Category__c  == 'Seeking Approval' || o.Stalled_Opp_Category__c  == 'Selected' || o.Stalled_Opp_Category__c  == 'Negotiation/Review') && o.Type == 'New Business' && o.Type_Detail__c != 'New Business: TrueUp' && o.Stalled_Opp_Category__c != trigger.oldMap.get(o.Id).Stalled_Opp_Category__c){
            send = true;
        }
        if(Test.isRunningTest()){
            send = false;
        }
        String rid = o.Id;
        String hash = 'StalledOpp';
        String uid = o.OwnerId;
        String sessionid = UserInfo.getSessionId();
        String status = o.Stalled_Opp_Category__c;
        String posttext = 'This opportunity has become stalled at a stage less than ' + status + '. This opportunity will automatically be closed in 30 days if not at ' + status + ' or above.';
        if(send){
            PostFeedItemAccount.PostFeedItem(rid, hash, uid, sessionid, posttext);
        }
        
    }

}