trigger AccountCreatePartnerIntegrationTasks on Account (after insert) {
	for(Account a : trigger.new){
		if(a.Type == 'Partner'){
			//Engagement Phase
			Partner_Integration_Task__c engagement = new Partner_Integration_Task__c();
				engagement.Account__c = a.Id;
				engagement.Stage__c = 'New';
				engagement.Certification_Stage__c = 'Engagement Phase';
			insert engagement;
			Partner_Integration_Task__c irp = new Partner_Integration_Task__c();
				irp.Account__c = a.Id;
				irp.Stage__c = 'New';
				irp.Certification_Stage__c = 'Internal Review Phase';
			insert irp;
			Partner_Integration_Task__c scoping = new Partner_Integration_Task__c();
				scoping.Account__c = a.Id;
				scoping.Stage__c = 'New';
				scoping.Certification_Stage__c = 'Scoping Phase';
			insert scoping;
			Partner_Integration_Task__c design = new Partner_Integration_Task__c();
				design.Account__c = a.Id;
				design.Stage__c = 'New';
				design.Certification_Stage__c = 'Design Phase';
			insert design;
			Partner_Integration_Task__c development = new Partner_Integration_Task__c();
				development.Account__c = a.Id;
				development.Stage__c = 'New';
				development.Certification_Stage__c = 'Development Phase';
			insert development;
			Partner_Integration_Task__c testing = new Partner_Integration_Task__c();
				testing.Account__c = a.Id;
				testing.Stage__c = 'New';
				testing.Certification_Stage__c = 'Testing and QA Phase';
			insert testing;
			Partner_Integration_Task__c documentation = new Partner_Integration_Task__c();
				documentation.Account__c = a.Id;
				documentation.Stage__c = 'New';
				documentation.Certification_Stage__c = 'Documentation Phase';
			insert documentation;
			Partner_Integration_Task__c release = new Partner_Integration_Task__c();
				release.Account__c = a.Id;
				release.Stage__c = 'New';
				release.Certification_Stage__c = 'Release Phase';
			insert release;
			Partner_Integration_Task__c ifr = new Partner_Integration_Task__c();
				ifr.Account__c = a.Id;
				ifr.Stage__c = 'Optional';
				ifr.Certification_Stage__c = 'Internal Feature Request';
			insert ifr;
		}
	}
}