trigger InsertAccountOwner on Invited_Contact__c (before insert, before update) {
    
    for(Invited_Contact__c ic : trigger.new){
		try{
		if(ic.Contact__c != null){
        String contact = ic.Contact__c;
        String cAccId = [Select Id, AccountId from Contact where Id = :contact LIMIT 1].AccountId;
    	String a = [Select Id, OwnerId from Account where Id = :cAccId LIMIT 1].OwnerId;
		ic.Account_Owner__c = a;
       	String u = [Select Id, Name from User where Id = :a LIMIT 1].Name;
        ic.AE_AM_Text__c = u;
		}
	}catch(QueryException q){
    	ic.addError('It does not appear that this Contact has an Account.  Please associate an Account to the Contact before you associate them to any Marketing Events');
    	}
    } 
}