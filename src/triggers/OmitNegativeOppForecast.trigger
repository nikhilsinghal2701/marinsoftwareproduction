/**
 * This Apex Trigger is being used for compliance purposes to denote that all 
 * Opportunities with a negative Amount must be Omitted from Forecasting
 * Created by: Gary Sopko
 * Created Date: 04/02/2013
 * Please consult with Sales Operations/System Administration before updating.
 */

trigger OmitNegativeOppForecast on Opportunity (before insert, before update) {

for(Opportunity o: trigger.new) {
	if((o.Amount < 0 || o.Type == 'Professional Services') && o.ForecastCategory != 'Omitted'){
		o.ForecastCategoryName = 'Omitted';
}
}
}