trigger OppInsertChatterOwner on Opportunity (after insert) {
	Boolean send = false;
	for(Opportunity o : trigger.new){
		if(o.Type_Detail__c != 'New Business: TrueUp'){
			send = true;
		}
		if(Test.isRunningTest()){
			send = false;
		}
		String rid = o.AccountId;
		String uid = o.AccountOwnerId__c;
		String sessionid = UserInfo.getSessionId();
		String hash = 'OppCreate';
		String posttext = 'An Opportunity has been created for this Account:  https://na3.salesforce.com/'+o.Id;
		if(send){
			PostFeedItemAccount.PostFeedItem(rid, hash, uid, sessionid, posttext);
		}
	}
}