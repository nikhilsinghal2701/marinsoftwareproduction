trigger OppInsertChatterStale11 on Opportunity (after update) {
    for(Opportunity o : trigger.new){
        Boolean send = false;
        if(o.Chatter_Post_Update__c  == true){
            send = true;
        }
        if(Test.isRunningTest()){
            send = false;
        }
        String rid = o.Id;
        String hash = 'StalledOpp';
        String uid = o.OwnerId;
        String sessionid = UserInfo.getSessionId();
        String status = o.Stalled_Opp_Category__c;
        String posttext = 'This opportunity has a probability of less than or equal to 90% and a close date in the last month or earlier. Note all opportunities must be moved to 95% by the 5th business day of the following month. By the 5th business day please move this opportunity to 95% or move out the close date.';
        if(send){
            PostFeedItemAccount.PostFeedItem(rid, hash, uid, sessionid, posttext);
        }
        
    }

}