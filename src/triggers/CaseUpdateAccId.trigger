trigger CaseUpdateAccId on Case (before insert, before update) {
	Boolean updateaccid = false;
	for(Case c : trigger.new){
		if(trigger.isinsert && c.AccountId == null && c.Client_Id__c != null){
			updateaccid = true;
		} if(trigger.isupdate){
			if((c.Client_Id__c != trigger.oldMap.get(c.Id).Client_Id__c) ||(c.AccountId == null && c.Client_Id__c != null)){
				updateaccid = true;
			}
		}
		if(updateaccid){
			try{
				for(Marin_App_Client__c mac : [Select Account__c, Client_Id__c from Marin_App_Client__c WHERE Client_Id__c = :c.Client_Id__c]){
					c.AccountId = mac.Account__c;
				}
			} catch (Exception e){
				system.debug('AN EXCEPTION WAS THROWN ::: ' + e);
			}
		}
	}
}