trigger ownerCopy on Case (before insert, before update) {

    // When the Case Owner field is changed the Owner_Copy__c field is changed to match
    
    //Loop through incoming records
    for(Case c : Trigger.new) {
    
        //Owner Changed
        if(c.OwnerId != c.Owner_copy__c) {
    	system.debug('OwnerId:::: '+ c.OwnerId);
    	system.debug('Owner Copy::: '+ c.Owner_Copy__c);
            //Check that owner is a user and not a queue
            if(    ((String)c.OwnerId).substring(0,3) == '005' ) {
            c.Owner_Copy__c = c.OwnerID;
            }
    else if ( ((String)c.OwnerId).substring(0,3) == '00G') {
        // in case of queue we need to clear out the copy field && Owner Region && Owner Department
        c.Owner_Copy__c = null;
        c.Owner_Region__C = null;
        c.Owner_Department__c = null;
            }
        } 
    }
}