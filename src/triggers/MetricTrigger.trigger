trigger MetricTrigger on Metric__c (after insert) {

	if (Trigger.isAfter) {
		if (Trigger.isInsert) {
			MetricTriggerActions.doAfterInsert(Trigger.new);
		}
	}
}