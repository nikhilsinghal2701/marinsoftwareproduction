trigger ServiceTimecardUpdateAccountTIDCID on SFDC_Service_Timecard__c (before insert, before update) {
    for(SFDC_Service_Timecard__c tc : trigger.new){
    	String name = UserInfo.getName();
	    if(name != 'Asa Strout-Hearick'){
	    	if(tc.Marin_Client_Id__c != null){
		        try{
		            for(Marin_App_Client__c mac : [Select Account__c, Customer_Id__c, Client_Id__c from Marin_App_Client__c WHERE Client_Status__c = 'Active' AND Client_Id__c = :tc.Marin_Client_Id__c AND Account__c != null LIMIT 1]){
		            	tc.Marin_Customer_Id__c = mac.Customer_Id__c;
	                	tc.Account__c = mac.Account__c;
		           	}
	    		} catch(Exception e){ 
	    			system.debug('AN EXCEPTION WAS THROWN!!! ::::::: '+e);
	    		}
	      	} else if(tc.Account__c != null){
		       	try{
		       		for(Marin_App_Client__c mac : [Select Account__c, Customer_Id__c from Marin_App_Client__c WHERE Client_Status__c = 'Active' AND Account__c = :tc.Account__c LIMIT 1]){
		               	tc.Marin_Customer_Id__c = mac.Customer_Id__c;
		       		}
		        } 
		        catch(Exception e){
	        		system.debug('AN EXCEPTION WAS THROWN!!! ::::::: ' + e);
		       	}
	    	}
		}
    }
}