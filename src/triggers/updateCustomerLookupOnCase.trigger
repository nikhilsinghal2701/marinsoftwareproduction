/**
 * Author:      Luke Martell
 * Date:        09-09-2010
 *
 * Modified By: Luke Martell
 * Date:        10-20-2010
 *
 * Comments:
 * This trigger does a lookup against the Customer__c object to see if the Client_ID__c
 * field on the Case object matches any of the Marin CID's on the Customer__c object.
 * Since the Marin CID's field is a comma-delimited list, the trigger had to craft a
 * dynamic SOQL statement to use all the variations of Client ID's. This presents a
 * slight performance issue if there are large numbers of Case records or a large bulk
 * insert of Case records.
 *
 * UPDATE:
 * The ownership of the new Case records is now driven by the Primary_CSR__c field on
 * the Customer__c record. If no Primary_CSR__c field is populated then by default the
 * ownership is pulled from a custom setting called 'Default Primary CSR'.
 *
 * Recommendation if performance does become an issue:
 *
 * Create a master-detail object to represent the Marin CID's. This way a SOQL query can
 * be crafted to utilize the IN-clause rather than having to create a large list of LIKE
 * statements. The new SOQL statement might look something like the SOQL statement below
 * if the related object was called Marin_CIDs__c.
 *
 * SELECT Id,
 *      (SELECT Id, Name FROM Marin_CIDs__r WHERE Name IN ('12345', '23456'))
 * FROM Customer__c
 *
 * The code would need to be reworked to use the new SOQL query but it wouldn't be too
 * hard to get everything working.
 *
 * UPDATE (12-13-2010):
 * Removed the assumption that Customer__c records could possibly have more than one CID.
 * As of right now, the new assumption is that all Customer__c records will only have one
 * CID value.
 *
 * UPDATE (8/3/2012) ASA STROUT-HEARICK
 * Changed code to have customer.primary_csr__C fill in c.primary_omm_del__c instead of c.OwnerId. Simple Replace OwnerId to primary_omm_del__c.
 */
trigger updateCustomerLookupOnCase on Case (before insert, after update) {
   
     Map<String, Case> customerIdMap = new Map<String, Case>();
     Map<Id, Case> updateOwnerMap = new Map<Id, Case>();
        
    for (Case c : Trigger.new) {
        if (Trigger.isInsert && c.Client_ID__c != null) {
            c.No_matching_Client_ID__c = true;
            customerIdMap.put(String.valueOf(c.Client_ID__c.intValue()), c);
        } else if (Trigger.isUpdate) {
            if (c.Client_ID__c != null && Trigger.oldMap.get(c.Id).Client_ID__c != c.Client_ID__c) {
                customerIdMap.put(String.valueOf(c.Client_ID__c.intValue()), c);
            }
            if (Trigger.oldMap.get(c.Id).Primary_OMM_del__c != c.Primary_OMM_del__c) {
                updateOwnerMap.put(c.Id, c);
            }
        }
    }

    if (!customerIdMap.isEmpty()) {
        Map<String, DefaultPrimaryCSR__c> csrMap = DefaultPrimaryCSR__c.getAll();
        Map<Id, Case> caseUpdateMap = new Map<Id, Case>();

        for (Customer__c customer :
            [SELECT Id, Marin_CIDs__c, Primary_CSR__c, Account__c
             FROM Customer__c
             WHERE Marin_CIDs__c IN :customerIdMap.keySet()]) {
               
            if (customerIdMap.containsKey(customer.Marin_CIDs__c)) {
                Case c = customerIdMap.get(customer.Marin_CIDs__c);

                if (Trigger.isInsert) {
                    if (customer.Primary_CSR__c != null) {
                        c.Primary_OMM_del__c = customer.Primary_CSR__c;
                    } else if (!csrMap.isEmpty()) {
                        c.Primary_OMM_del__c = csrMap.values()[0].User_Id__c;
                    }
                    c.No_matching_Client_ID__c = false;
                    c.Customer__c = customer.Id;                    

                } else if (!caseUpdateMap.containsKey(c.Id)) {
                    Case uc = new Case(
                        Id = c.Id,
                        Customer__c = customer.Id                                           
                    );

                    if (customer.Primary_CSR__c != null) {
                        uc.Primary_OMM_del__c = customer.Primary_CSR__c;
                    } else if (!csrMap.isEmpty()) {
                        uc.Primary_OMM_del__c = csrMap.values()[0].User_Id__c;
                    }
                    caseUpdateMap.put(c.Id, uc);
                }
            }
            
        }

        if (!caseUpdateMap.isEmpty()) {
            try {
                update caseUpdateMap.values();
            } catch (System.DmlException de) {
                for (Id cid : caseUpdateMap.keySet()) {
                    Trigger.newMap.get(cid).addError(de.getMessage());
                }
            }
        }
      
    }

    if (!updateOwnerMap.isEmpty()) {
        List<Case> casesToUpdate = new List<Case>();

        for (Id caseId : updateOwnerMap.keySet()) {
            Case c = Trigger.newMap.get(caseId);

            if (c.Owner_Override__c) {
                casesToUpdate.add(
                    new Case(
                        Id = caseId,
                        Owner_Override__c = false,
                        Primary_OMM_del__c = c.Primary_OMM_del__c
                       
                    )
                );
            }
        }

        if (!casesToUpdate.isEmpty()) {
            try {
                update casesToUpdate;
            } catch (System.DmlException de) {
                for (Id caseId : updateOwnerMap.keySet()) {
                    Trigger.newMap.get(caseId).addError(de.getMessage());
                }
            }
        }
    }
}