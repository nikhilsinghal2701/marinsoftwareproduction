trigger IntegrationPostFeedItem on PS_Project_Record__c (after update) {
	for(PS_Project_Record__c i : trigger.new){
		Boolean send = false;
		if((i.Implementation_Health__c == 'Yellow' || i.Implementation_Health__c == 'Red') && i.Implementation_Health__c != trigger.oldMap.get(i.Id).Implementation_Health__c){
			send = true;
		}
		if(Test.isRunningTest()){
			send = false;
		}
		String rid = i.Account__c;
		String hash = 'HealthChange';
		String uid = i.AccountOwnerid__c;
		String uid2;
		if(i.On_Boarding_Region__c == 'AMER'){
			uid2 = '00550000001D92s';
		} else if(i.On_Boarding_Region__c == 'EMEA'){
			uid2 = '00550000001iwwA';
		} else if(i.On_Boarding_Region__c == 'APAC'){
			uid2 = '00550000001knLT';
		}
		String uid3 = i.Project_Manager__c;
		String uid4 = i.Primary_Solution_Architect__c;
		String uid5 = i.OpportunityOwnerId__c;
		String uid6 = i.OpportunityProductConsultantId__c;
		String sessionid = UserInfo.getSessionId();
		String status = i.Implementation_Health__c;
		String link = string.valueof(System.URL.getSalesforceBaseURL().toExternalForm() + '/' + i.Id);
		String posttext = 'An integration has gone into a ' + status + ' health status.  You can access the integration record by going to the following link: ' + link;			
		if(send && uid5 == null){
			PostFeedItemAccount.PostFeedItemFourMention(rid, hash, uid, uid2, uid3, uid4, sessionid, posttext);
		}
		else if(send && uid5 != null && uid6 == null){
			PostFeedItemAccount.PostFeedItemFiveMention(rid, hash, uid, uid2, uid3, uid4, uid5, sessionid, posttext);
		}
		else if(send && uid6 != null){
			PostFeedItemAccount.PostFeedItemSixMention(rid, hash, uid, uid2, uid3, uid4, uid5, uid6, sessionid, posttext);
		}
	}
}