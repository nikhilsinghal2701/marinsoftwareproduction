trigger OppInsertChatterStale10 on Opportunity (after update) {
    for(Opportunity o : trigger.new){
        Boolean send = false;
        if((o.Stalled_Opp_Category10__c  == 'Solid Prospect' || o.Stalled_Opp_Category10__c  == 'Seeking Approval' || o.Stalled_Opp_Category10__c  == 'Selected' || o.Stalled_Opp_Category10__c  == 'Negotiation/Review') && o.Type == 'New Business' && o.Type_Detail__c != 'New Business: TrueUp' && o.Stalled_Opp_Category10__c != trigger.oldMap.get(o.Id).Stalled_Opp_Category10__c){
            send = true;
        }
        if(Test.isRunningTest()){
            send = false;
        }
        String rid = o.Id;
        String hash = 'StalledOpp';
        String uid = o.OwnerId;
        String sessionid = UserInfo.getSessionId();
        String status = o.Stalled_Opp_Category10__c;
        String posttext = 'This opportunity has become stalled at a stage less than ' + status + '. This opportunity will automatically be closed in 10 days if not at ' + status + ' or above.';
        if(send){
            PostFeedItemAccount.PostFeedItem(rid, hash, uid, sessionid, posttext);
        }
        
    } 

}