trigger setMarinAccountValues on WorkIt2__Time_Entry__c (before insert) {

    set<id> timingIds = new set<id>();
    
    for (workit2__time_entry__c e: trigger.new)
        timingIds.add(e.workit2__timing__c);
        
    map<id, workit2__timing__c> timingMap = new map<id, workit2__timing__c>([select id, workit2__case__c,
            workit2__case__r.accountId, workit2__case__r.Client_ID__c, Account__c, Integration__c, Integration__r.account__c 
            from workit2__timing__c where id in :timingIds]);

    set<id> accountIds = new set<id>();
    set<decimal> clientIds = new set<decimal>();
    
    for (workit2__time_entry__c e: trigger.new) {
        workit2__timing__c t = timingMap.get(e.workit2__timing__c);
        
        if (t.account__c != null)
            e.account__c = t.account__c;
        else if (t.Integration__c != null)
            e.account__c = t.Integration__r.account__c;
        else if (t.workit2__case__c != null) {
            e.account__c = t.workit2__case__r.accountId;
            e.Client_Id__c = t.workit2__case__r.Client_Id__c;
            clientIds.add(e.Client_Id__c);
        }
            
        accountIds.add(e.account__c);
    }
    
    map<string, integer> statusMap = new map<string, integer> {'Active' => 0, 'New' => 1, 'Sync Error' => 2,
                                                                'Inactive' => 3, 'Deleted' => 4};
    
    map<id, Marin_App_Client__c> marinClientMap = new map<id, Marin_App_Client__c>();
    map<id, Marin_App_Client__c> marinClientMap2 = new map<id, Marin_App_Client__c>();
    
    for (Marin_App_Client__c m : [select Account__c, Customer_Id__c, Client_Id__c, Client_Status__c 
            from Marin_App_Client__c WHERE account__c in :accountIds]) {
        
        if (!(marinClientMap.containsKey(m.Account__c)))       
            marinClientMap.put(m.Account__c, m);
        else if (statusMap.get(m.Client_Status__c) < statusMap.get(marinClientMap.get(m.Account__c).Client_Status__c)) 
            marinClientMap.put(m.Account__c, m);
     }

    for (Marin_App_Client__c m : [select Account__c, Customer_Id__c, Client_Id__c, Client_Status__c 
            from Marin_App_Client__c WHERE account__c in :accountIds and Client_Id__c in :clientIds]) {
        
        if (!(marinClientMap2.containsKey(m.Account__c)))       
            marinClientMap2.put(m.Account__c, m);
        else if (statusMap.get(m.Client_Status__c) < statusMap.get(marinClientMap2.get(m.Account__c).Client_Status__c)) 
            marinClientMap2.put(m.Account__c, m);
     }
         
    for (workit2__time_entry__c e: trigger.new) {
        if (e.Client_Id__c != null) {
            if (marinClientMap2.containsKey(e.account__c))
                e.Customer_Id__c = marinClientMap2.get(e.account__c).Customer_Id__c;
        }
        else if (marinClientMap.containsKey(e.account__c)) {
            e.Customer_Id__c = marinClientMap.get(e.account__c).Customer_Id__c;
            //e.Client_ID__c = marinClientMap.get(e.account__c).Client_Id__c;
        }
    }
}