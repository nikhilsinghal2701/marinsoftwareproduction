<!-- Copyright (c) 2011 JBara Software, Inc.
---- Purpose                    : This page is used to configure the revenue bands.
----                              
---- Other comments (optional)  : None.
-->
<apex:page Controller="JBCXM.SummaryExtension" sidebar="false" title="Revenue Band Configuration"  tabStyle="Administration__tab">
 <apex:includeScript value="{!JSENCODE(URLFOR($Resource.JBCXM__JQuery,'js/jquery-1.5.2.min.js'))}"/>
 <apex:includeScript value="{!JSENCODE(URLFOR($Resource.JBCXM__JQuery,'js/jquery-ui-1.8.13.custom.min.js'))}"/>
 <apex:stylesheet value="{!JSENCODE(URLFOR($Resource.JBCXM__JQuery, 'css/redmond/jquery-ui-1.8.11.custom.css'))}"/>
 <apex:includeScript value="{!JSENCODE(URLFOR($Resource.JBCXM__SurveyUtil,'SurveyUtil.js'))}"/>
 <apex:sectionHeader title="{!customLabelsMap['RevenueBand']} Configuration" />
 <a href="/apex/Administration?sfdc.tabName={!($CurrentPage.parameters.tabId)}" style="text-decoration:none;">&nbsp;«&nbsp;Back</a>
 <style type = "text/css">
 	.ui-icon{
 		cursor:pointer;
 	}
 	.errorRowClass{
 		background-color: #fbe3e4;
 	}
 	.dummyMessageTable .messageCell {
 		text-align : left;
 	}
 	.requiredExample {
 		border-left: 3px solid #CC0000;
	    font-size: 100%;
	    margin-right: 1px;
	    padding-bottom: 3px;
	    padding-top: 2px;
	    vertical-align: 1px;
	    width: 100%;
 	}
 	
 	input:focus, input.focus {
	    border: 1px solid #5695DB;
	    box-shadow: 0 1px 2px #DDDDDD inset, 0 0 5px #5695DB;
	    outline: medium none;
	}
 	
 </style>
	<script type = "text/javascript">
	var j$ = jQuery.noConflict();
	var configJson = new Array();
	var isRBConfigured = "false";
	var existingRBConfig, uiCustomLabelMap; 
	j$(document).ready(function() {
		configJson =  eval({!(RevenueBandConfiguration)});
		uiCustomLabelMap = eval({!(customLabelsMapJSON)});
		isRBConfigured = "{!(isRevenueBandConfigured)}";
		existingRBConfig = (isRBConfigured == "true") ? JSON.stringify(configJson) : "";
		 if(configJson != null  && configJson.length > 0) {
		 	populateRBConfigBase(uiCustomLabelMap);
		 	j$(".bandOnSelection").val(configJson[0].type); //always it returns a list.
		 	
		 	var rowMarkup = '';
		 	j$(configJson).each(function(){
		 		rowMarkup += addBandRows();
		 	});
		 	j$(".tbodyRevenueSection").html(rowMarkup);
		 	if(j$(".removeBandRow").length <= 1) {
				j$(".removeBandRow").first().css({"opacity":"0.4"});
			}
		 	var indx = 0;
		 	j$(configJson).each(function(k,v) {
		 		j$(".labelValue").slice(indx).val(v.name);
		 		j$(".minValue").slice(indx).val((v.min == 0) ? v.min : (v.min-1)+'+');
		 		j$(".maxValue").slice(indx).val(v.max);
		 		indx++;
		 	});
		 	j$(".maxValue").slice(indx-1).parent().find("span.requiredExample").hide();
		 }
	});
	
	function populateRBConfigBase(uiCustomLabelMap) {
		var htmlStr 	= '<span>'+uiCustomLabelMap.RevenueBand+' on : </span>';
    	htmlStr 	    += '<select class = "bandOnSelection"><option value = "MRR__c">'+uiCustomLabelMap.MRR+'</option>';
    	htmlStr 	    += '<option value = "ASV__c">'+uiCustomLabelMap.ASV+'</option></select>';
    	j$(".RevenueBandConfigOn").append(htmlStr);
	}
	
	function addBandRows(thisPointer) {
	   	var rowHTML = '';
	   	rowHTML += '<tr class = "trCompleteBandRow">';
        rowHTML += '<td><span class="requiredExample"></span><input  class = "labelValue"  type = "text" maxlength="40" /></td><td><span class="requiredExample"></span><input  class = "minValue" disabled = "disabled" type = "text" /></td><td><span class="requiredExample"></span><input  class = "maxValue"  type = "text" /></td>'
        rowHTML += '<td title = "Add"><span class = "ui-icon ui-icon-plus addBandRow"></span></td><td title = "Remove"><span class = "ui-icon ui-icon-minus removeBandRow" ></span></td></tr>';
        if(!thisPointer) {
        	return rowHTML;
        }
        else {
         	j$(thisPointer).parent().parent().after(rowHTML);
         	j$(thisPointer).parent().parent().next().find(".minValue").val(j$.trim(j$(thisPointer).parent().parent().find(".maxValue").val())+'+');
         	toggleRequiredBlockForMaxValue();
        }                        		
	}
	   
   	function remBandRows(thisPointer) {
   		j$(thisPointer).parent().parent().remove();
   		toggleRequiredBlockForMaxValue();
   	}
   	
   	/**
   	*This function is used to toggle the required block for band max value input text box.
   	**/
   	function toggleRequiredBlockForMaxValue() {
       	var rbListSize = j$(".tbodyRevenueSection .maxValue").length;
       	j$("span.requiredExample").show();
       	j$(".maxValue").slice(rbListSize-1).parent().find("span.requiredExample").hide();
   	}
   	
  	var bandStorageClass = function() {
  		this.type = null;
    	this.name = null;
    	this.min = null;
    	this.max = null;
    }
    
    /**
    *This function is used to validate the revenue band config then save.
    **/
    function getBandConditionToSave() {
    	var configArray 		= new Array();	
    	var sortedLabelArray 	= new Array();
    	var isLabelEmpty		= false;
    	var bandStorageObj;
    	j$(".trCompleteBandRow").each(function() {
    		bandStorageObj = new bandStorageClass();
    		bandStorageObj.type = j$(".bandOnSelection").val();
    		bandStorageObj.name = j$.trim(j$(this).find(".labelValue").val());
    		if(bandStorageObj.name == '') {
    			isLabelEmpty = true;	
    		}
    		else {
    			sortedLabelArray[bandStorageObj.name] = bandStorageObj.name;
    		}
    		bandStorageObj.min = (j$.trim(j$(this).find(".minValue").val()) == '' || isNaN(j$.trim(j$(this).find(".minValue").val()).split('+')[0])) ? null : parseFloat(j$.trim(j$(this).find(".minValue").val()).split('+')[0])+1;
    		bandStorageObj.max = (j$.trim(j$(this).find(".maxValue").val()) == '' || isNaN(j$.trim(j$(this).find(".maxValue").val()))) ? null : parseFloat(j$(this).find(".maxValue").val());
    		configArray.push(bandStorageObj);
    	});
    	var sortedLabelArrayLength = 0;
    	//To get the length of associative array.
    	for(var key in sortedLabelArray) {
    		sortedLabelArrayLength++;
    	}
    		    	
    	if(!validateUniqueLabels(isLabelEmpty, (configArray.length == sortedLabelArrayLength)) || !validateMaxGtrMin(configArray)) {
    		j$(".dummyAllAdminNewBtn").attr("disabled", false);
               j$(".dummyAllAdminNewBtn").removeClass("btnDisabled").addClass("btn");
    	}
    	else {	    	
    		j$("#jBaraUIViewValidationsContainer").hide();	    		
    		//To comparing previous value with new value(If is there any change in config then only we will save to db).
    		var finalRBStr = JSON.stringify(configArray);
    		if(finalRBStr === existingRBConfig) {
    			redirectFun();
    		}
    		else {
    			saveRevenueConfig(finalRBStr);
    		}
    	}
	 }
	 
	 function redirectFun() {
	 	window.location = "/apex/Administration"; 
	 }
	
	 function disableBtn(obj) {
                j$(obj).attr("disabled", true);
                j$(obj).removeClass("btn").addClass("btnDisabled");
            }
     
    /**
    *This function is used to Check the Uniqueness of Band names.
    **/       
    function validateUniqueLabels(bolFlag1, bolFlag2) {
		j$(".errorRowClass").removeClass("errorRowClass");
		var validFlag = true;
		if(bolFlag1 || !bolFlag2) {
			var errorList= new Array();
           	var errorDupQues = new JBaraErrorClass();
           	errorDupQues.msg = (bolFlag1) ? "Please enter band name." : "Please enter unique band names.";
           	errorDupQues.header = '';
           	errorList.push(errorDupQues);
           	showJBaraValidationErrors('jBaraUIViewValidationsContainer', errorList);
			validFlag = false;
		}
		return validFlag;
     }
     
    /**
    *This function is used to check band range.
    **/ 
	function validateMaxGtrMin(inputArray) {
     	j$(".errorRowClass").removeClass("errorRowClass");
     	var returnFlag = true;
     	var inputArrayLenth = inputArray.length;
     	for(var i = 0; i < inputArrayLenth; i++) {
     		if(inputArray[i].min == null || (inputArray[i].max == null && i+1 != inputArrayLenth)) {     		
     			returnFlag = false;
     		}
     		else if(((i+1 != inputArrayLenth) && (parseFloat(inputArray[i].min) >= parseFloat(inputArray[i].max))) || (i+1 == inputArrayLenth && inputArray[i].max != null && (parseFloat(inputArray[i].min) >= parseFloat(inputArray[i].max)))) {
     			returnFlag = false;
     		}
     		if(i > 0) {
     			if(parseFloat(inputArray[i].min) <= parseFloat(inputArray[i-1].max)) {
     				returnFlag = false;
     			}
     		}
     		if(!returnFlag) { break; } 
     	}     	
     	if(!returnFlag) {
     		var errorList= new Array();
        	var errorDupQues = new JBaraErrorClass();
        	errorDupQues.msg = "Please correct Minimum and Maximum values..";
        	errorDupQues.header = '';
        	errorList.push(errorDupQues);
        	showJBaraValidationErrors('jBaraUIViewValidationsContainer', errorList);
        	j$(this).addClass("errorRowClass");
     	}
     	return returnFlag;
     }
     
	j$(".addBandRow").live("click",function() {
		if(j$.trim(j$(this).parent().parent().find(".maxValue").val()) == '' || isNaN(j$.trim(j$(this).parent().parent().find(".maxValue").val()))) {
			j$(this).parent().parent().find(".maxValue").focus();
			alert('Please enter numeric value');
		}
		else {
			if(j$(".trCompleteBandRow").length < 10) {
				addBandRows(this);
				j$(".removeBandRow").first().css({"opacity":"1"});
				if(j$(".trCompleteBandRow").length == 10){
					j$(".addBandRow").css({"opacity":"0.4"});
				}else{
					j$(".addBandRow").css({"opacity":"1"});
				}
				
			}
			else{
				j$(".addBandRow").css({"opacity":"0.4"});
			}
		}
	});
	
	j$(".removeBandRow").live("click",function() {
		
		if(j$(".removeBandRow").length <= 1){
			j$(".removeBandRow").first().css({"opacity":"0.4"});
		}
		else{
		remBandRows(this);
		j$(".addBandRow").css({"opacity":"1"});
		if(j$(".removeBandRow").length <= 1){
			j$(".removeBandRow").first().css({"opacity":"0.4"});
		}
		}
		
	});
	
	j$(".maxValue").live("focusout",function() {
		j$(this).parent().parent().next().find(".minValue").val(j$.trim(j$(this).parent().parent().find(".maxValue").val())+'+');
	});
	
   
	
	</script>
	<apex:pageBlock title="Configure {!customLabelsMap['RevenueBand']}">
	
	<apex:pageBlockButtons location="top" >
          <div style="clear: both;">
                 <input type="button" value=" Save " class="btn dummyAllAdminNewBtn" onclick="disableBtn(this); getBandConditionToSave();"/>
                 <input type="button" value=" Cancel " class="btn dummyAllAdminNewBtn" onclick="disableBtn(this);redirectFun();"/>
         </div>
     </apex:pageBlockButtons>
    <div class = "mainForSection" style = "width:600px; margin: 0 auto;">
    	<div style = "text-align: center;" class="RevenueBandConfigOn">    
    		<span Id="jBaraUIViewValidationsContainer"></span>
		</div>
		<table style = "border-collapse:collapse; margin-top:10px" cellpadding="2">
			<thead><th>Label</th><th>Minimum</th><th>Maximum</th></thead>
			<tbody class = "tbodyRevenueSection">
				
			</tbody>
		</table>
	</div>	
	</apex:pageBlock>
		
		<apex:form >
		
        <apex:actionFunction name="saveRevenueConfig"  action="{!saveRevenueBandConfig}" oncomplete="redirectFun();" >
        <apex:param name="rbConfigStr" value=""/>
        </apex:actionFunction>
		</apex:form>
		
</apex:page>