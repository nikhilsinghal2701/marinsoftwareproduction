<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Improve phone blitz effectiveness with RapidFire Cold Calling for Appexchange. RapidFire Cold Calling eliminates the tediousness of logging cold call activity in Salesforce.com.</description>
    <label>RapidFire ColdCalling</label>
    <tab>standard-Lead</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Opportunity</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>RFController__c</tab>
    <tab>Customer__c</tab>
    <tab>PS_Project_Record__c</tab>
    <tab>Account_Development_Plan__c</tab>
    <tab>Lift__c</tab>
    <tab>Metric__c</tab>
</CustomApplication>
