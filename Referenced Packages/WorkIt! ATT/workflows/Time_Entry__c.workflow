<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>WorkIt_Session_Overtime_Level_Green</fullName>
        <field>Color__c</field>
        <literalValue>Green</literalValue>
        <name>WorkIt! Session Overtime Level: Green</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WorkIt_Session_Overtime_Level_Red</fullName>
        <field>Color__c</field>
        <literalValue>Red</literalValue>
        <name>WorkIt! Session Overtime Level: Red</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WorkIt_Session_Overtime_Level_Yellow</fullName>
        <field>Color__c</field>
        <literalValue>Yellow</literalValue>
        <name>WorkIt! Session Overtime Level: Yellow</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
