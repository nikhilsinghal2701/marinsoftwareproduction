<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>The comprehensive Automatic Time Tracking solution for Force.com.</description>
    <label>WorkIt!</label>
    <tab>standard-Lead</tab>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Case</tab>
    <tab>Timing__c</tab>
    <tab>Time_Entry__c</tab>
    <tab>Session_Type__c</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
</CustomApplication>
